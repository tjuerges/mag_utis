	;Dieses kleine Programm schaltet alle 1000stel Sekunden
	;die Hintergrundfarbe von schwarz auf wei� resp. um-
	;gekehrt.
	;L�uft so garantiert auf einem ST. Den Rest wei� ich
	;nicht, da ich keinen anderen ST-like habe.
	;Gestoppt wird das Programm einfach durch einen
	;Jdisint #13-Aufruf.

	include	tos_fnkt	;Viele, viele Macros

	;Hardwareregister
timer_a_vorteiler	=	7
timer_a_data	=	123	;Hier den entsprechenden Wert eintragen,
				;der dem Timer A sagt, wie lange er warten
				;soll. Nun ist die Frequenz ca. 999.02 Hz.
COLOR00	=	$ffff8240
IER_A	=	$fffffa07
IER_B	=	$fffffa09
IPR_A	=	$fffffa0b
IPR_B	=	$fffffa0d
IMR_A	=	$fffffa13
IMR_B	=	$fffffa15
ISR_A	=	$fffffa0f
ISR_B	=	$fffffa11
;*********************************************
;*********************************************
;*********************************************
los:
	bra	reserve
;*********************************************
int:
	;movem.l	d0-a6,-(sp)	;Ordentlich die Regs. retten.
					;Ist hier nicht notwendig, da
					;keine Regs ver�ndert werden.
	move	#$2700,sr	;Damit niemand st�rt.
	bchg	#0,(COLOR00).w	;Farbbit switchen.
	bclr	#5,(ISR_A).w	;Wichtig, da sonst kein neuer Int. des
				;Timer A auftreten bzw. bearbeitet wird.
	;movem.l	(sp)+,d0-a6	;Und wieder zur�ck mit dem Krempel.
	rte	;Tsch��.

	copyright
reserve:	;Hier wird alles initialisiert.
	init	stack(pc)
	Cconws	info(pc)
	Jdisint	#13	;Sicherheitshalber den Interrupt zuvor sperren.
	Xbtimer	int(pc),#timer_a_data,#timer_a_vorteiler,#0
	Jenabint	#13	;Und nun ab daf�r...
	Ptermres	#0,#$100+reserve-los	;Ordentlich beenden.

info:	dc.b	$d,$a,'MAG-1000, (c) Magnum -BlackBox-.',$d,$a,0