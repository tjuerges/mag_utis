	include	tos_fnkt

;eine demonstration, welche geschwindigkeitsunterschiede sich
;mit und ohne blitterbenutzung ergeben.
;hier horizontales bildschirm scrolling ohne blitter.

	Supexec	start(pc)
	Pterm0
start:
	Physbase
	move.l	d0,a5
	move.w	#639,d0
	move	#$2700,sr
page_loop:
	move.l	a5,a6
	REPT	40*400
		roxr.w	(a6)+
	ENDR
	dbf	d0,page_loop
	move	#$2300,sr
	rts
