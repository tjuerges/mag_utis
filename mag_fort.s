	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	;output	f:\mag_utis\mag_fort.prg	;So hei�t das PRG-File.

	init
start:
	clr	handle
	clr.l	adresse
	clr.l	laenge

	Fsfirst	#0,name(pc)
	tst.l	d0
	bmi	fehler

	Fgetdta
	move.l	d0,a0
	lea	d_length(a0),a1
	move.l	(a1),laenge

	Fopen	#0,name(pc)
	move	d0,d7
	move	d0,handle
	bmi	fehler

	move.l	laenge(pc),d0
	Malloc	d0
	move.l	d0,adresse
	beq	fehler
	move.l	d0,a0

	Fread	(a0),laenge(pc),d7
	move.l	laenge(pc),d1
	cmp.l	d0,d1
	bne	fehler

	bsr	handle_abgeben

	move.l	laenge(pc),d0
	clr	d1
	bsr	return_suchen
	move	d0,anzahl

__SAVEREGS	SET	0
	Random
__SAVEREGS	SET	1
	swap	d0
	and.l	#$ffff,d0
	clr.l	d1
	move	anzahl(pc),d1
	divu	d1,d0
	swap	d0
	and.l	#$ffff,d0
	move	d0,d7
	move.l	adresse(pc),a0
	moveq	#1,d1
	bsr	return_suchen
	move.l	a0,a3
	cmp	anzahl(pc),d7
	blo.s	.ok
	move.l	adresse(pc),a2
	add.l	laenge(pc),a2
	clr.b	(a2)
	bra.s	.letzter_reim

.ok:	moveq	#1,d1
	moveq	#1,d0
	bsr.s	return_suchen
	clr.b	(a0)

.letzter_reim:	
	lea	start_text(pc),a0
	bsr.s	print
	lea	(a3),a0
	bsr.s	print

	bsr.s	speicher_abgeben

	bsr.s	taste
ende:	clr	-(sp)
	trap	#1

print:
__SAVEREGS	SET	0
	Cconws	(a0)
__SAVEREGS	SET	1
	rts
taste:
__SAVEREGS	SET	0
	Cnecin
__SAVEREGS	SET	1
	rts

speicher_abgeben:
	move.l	adresse(pc),a0
	Mfree	(a0)
	clr.l	adresse
	rts
handle_abgeben:
	Fclose	handle(pc)
	clr	handle
	rts

fehler:
	tst.l	adresse
	beq.s	.weiter
	bsr.s	speicher_abgeben
.weiter:
	tst.l	handle
	beq.s	.weiter2
	bsr.s	handle_abgeben
.weiter2:
	bra.s	ende

return_suchen:
	tst	d1
	bne.s	mit_grenze

	clr	d1
.loop1:
	subq.l	#1,d0
	beq.s	.suchen_ende
	cmp.b	#$a,(a0)+
	bne.s	.loop1

.ret_gef:
	addq	#1,a0
	subq.l	#1,d0
	beq.s	.suchen_ende
	cmp.b	#$a,(a0)+
	bne.s	.loop1

.trenner_gefunden:
	addq	#1,d1
	subq.l	#1,d0
	bne.s	.loop1

.suchen_ende:
	addq	#1,d1
	move	d1,d0
	rts

mit_grenze:
	tst	d0
	beq.s	.gef
.loop2:
	cmp.b	#$a,(a0)+
	bne.s	.loop2
.ret_gef:
	addq	#1,a0
	cmp.b	#$a,(a0)+
	bne.s	.loop2

	subq	#1,d0
	bne.s	.loop2
.gef:
	rts

	data
name:	dc.b	'Mag_Fort.dat',0
start_text:	dc.b	$d,$a,$a,27,'v'
	dc.b	27,'pMag_Fort (Fortune cookie)'
	dc.b	', (c) Magnum -BlackBox-.',$d,$a
	dc.b	'Quellen: Omar Khayaam, Murphys Gesetz u.a.',27,'q',$d,$a,0
	copyright
	bss
adresse:	ds.l	1
laenge:	ds.l	1
anzahl:	ds.w	1
handle:	ds.w	1

