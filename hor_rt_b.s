	include	tos_fnkt

;eine demonstration, welche geschwindigkeitsunterschiede sich
;mit und ohne blitterbenutzung ergeben.
;hier horizontales bildschirmscrolling mit blitter

	Supexec	start(pc)
	Pterm0
start:
	Physbase
	move.l	d0,a6
	move.w	#638,d0
	move.w	#$2700,sr
	init_blit	#2,#2,a6,#$ffff,#$ffff,#$ffff,#2,#2,a6,#40,#400,#2,#3,#1,#192
rt_loop:
	blit	a6,a6,#40,#400,#3,#1
	dbf	d0,rt_loop
	move.w	#$2300,sr
	rts
