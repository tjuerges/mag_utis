DEBUG	=	0
	output	f:\mag_utis\mag_spol.acc

	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	include	include\aes

;ein spooler, der sich die begrenzten multitasking-f�higkeiten
;des st zu nutze macht. ist nicht gerade der br�ller, aber der
;mensch freut sich. achja: es werden auch die anormalen st-zeichen,
;z.b. � in normale gewandelt.

begin
	IFEQ	DEBUG
	cmp.l	#0,sp
	bne	kein_accessory
	lea	stack(pc),sp
	ELSEIF
	init	stack(pc)
	ENDC

	appl_init
	IFEQ	DEBUG
	menu_register	appl_id(pc),#menu_name,menu_id
	bmi	kein_accessory
	ENDC

main_loop1
	lea	message_buffer(pc),a4
	bsr	clean_up_message_buffer
	lea	message_buffer(pc),a4
main_loop
	IFEQ	DEBUG
	evnt_mesag	a4
	cmp	#40,(a4)
	bne.s	main_loop
	ENDC

mag_spool_aktiviert
	lea	init_str(pc),a0
	bsr	alert
	move	int_out(pc),printer_flag
	subq	#1,printer_flag

	fsel_input	#path,#file_name
	tst	int_out+2
	IFEQ	DEBUG
	beq	main_loop1
	ELSEIF
	beq	ende
	ENDC

	lea	path(pc),a0
.loop
	cmp.b	#'.',(a0)+
	bne.s	.loop
	subq	#2,a0
	lea	file_name(pc),a1
.loop2
	move.b	(a1)+,(a0)+
	bne.s	.loop2

	Fsfirst	#0,path(pc)
	tst.l	d0
	bmi	error

	Fgetdta
	move.l	d0,a6
	move.l	$1a(a6),laenge

	Malloc	$1a(a6)
	move.l	d0,adresse
	beq	kein_freier_speicher

	Fopen	#0,path(pc)
	move	d0,handle
	bmi	error

	move.l	adresse(pc),d0
	move.l	d0,a0
	Fread	(a0),laenge(pc),handle(pc)

	Fclose	handle(pc)

print_loop
	move.l	adresse(pc),a5
	move.l	laenge(pc),d6
.loop_aussen
	moveq	#39,d7
.loop_innen
	move.b	(a5)+,d3
	tst	printer_flag
	bne.s	.weiter

	cmp.b	#'\',d3
	bne.s	.kein_bs
	move	#134,d3
	bra.s	.weiter
.kein_bs
	cmp.b	#'�',d3
	blt.s	.weiter
	bne.s	.kein_ue
	moveq	#125,d3
	bra.s	.weiter
.kein_ue
	cmp.b	#'�',d3
	bne.s	.kein_ae
	moveq	#123,d3
	bra.s	.weiter
.kein_ae
	cmp.b	#'�',d3
	bne.s	.kein_oe
	moveq	#124,d3
	bra.s	.weiter
.kein_oe
	cmp.b	#'�',d3
	bne.s	.kein_AE
	moveq	#91,d3
	bra.s	.weiter
.kein_AE
	cmp.b	#'�',d3
	bne.s	.kein_OE
	moveq	#92,d3
	bra.s	.weiter
.kein_OE
	cmp.b	#'�',d3
	bne.s	.kein_UE
	moveq	#93,d3
	bra.s	.weiter
.kein_UE
	cmp.b	#'�',d3
	bne.s	.weiter
	moveq	#126,d3
.weiter
	bsr.s	prt
	moveq	#10,d0
	bsr.s	timer
	subq.l	#1,d6
	beq.s	print_fertig
	dbf	d7,.loop_innen

	IFEQ	DEBUG
	move.l	#500,d0
	bsr.s	timer
	ENDC
	bra.s	.loop_aussen
	moveq	#$d,d3
	bsr.s	prt
	moveq	#$a,d3
	bsr.s	prt
	bra.s	print_fertig
prt
	Bconout	d3,#0
	rts
print_fertig
	move.l	adresse(pc),a0
	Mfree	(a0)

	bsr	clean_up_message_buffer

ende
	IFEQ	DEBUG
	Cconout	#7
	bra	main_loop
	ELSEIF
	lea	fertig_str(pc),a0
	bsr.s	alert
	Pterm0
	ENDC

;****************************************
;*************Subroutinen****************
;****************************************
timer
	evnt_timer	d0
	rts
alert
	form_alert	#1,a0
	rts
clean_up_message_buffer
	moveq	#3,d0
	move.l	a4,a0
.loop
	clr.l	(a0)+
	dbf	d0,.loop
	clr.l	path
	clr.l	file_name
	rts
kein_freier_speicher
	lea	kein_speicher_str(pc),a0
	bsr.s	alert
	bsr.s	clean_up_message_buffer
	bra	main_loop
error
	form_error	d0
	Fclose	handle(pc)
	bsr.s	clean_up_message_buffer
	bra	main_loop
kein_accessory
	lea	kein_acc_str(pc),a0
	bsr	alert
	IFEQ	DEBUG
.loop
	moveq	#-1,d0
	bsr	timer
	bra.s	.loop
	ELSEIF
	Pterm0
	ENDC


	data
menu_name	dc.b	"  Magnum's Spooler  ",0
kein_acc_str	dc.b	'[3][ L�uft nur als Accessory. | Kein ACC-Slot frei? ][ Kapiert ]',0
gib_namen_str	dc.b	'[1][ Bitte File ausw�hlen! ][ O.K. ]',0
init_str	dc.b	'[1][ MAG-SPOL, | (c) Magnum -BlackBox-. | Bitte Druckereinstellung | ausw�hlen! ][ Deutsch | Englisch ]',0
kein_speicher_str	dc.b	'[3][ Nicht gen�gend | freier Speicher | vorhanden. | Breche ab. ][ All right ]',0
	copyright
	IFNE	DEBUG
fertig_str	dc.b	'[1][ Fertig! ][ Uff ]',0
	ENDC
	bss
message_buffer	ds.l	4
path	ds.l	64
file_name	ds.l	5
adresse	ds.l	1
laenge	ds.l	1
handle	ds.w	1
acc_id	ds.w	1
menu_id	ds.w	1
printer_flag	ds.w	1
	IFEQ	DEBUG
newstack	ds.l	1024
stack
	ENDC