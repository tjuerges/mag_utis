	include	tos_fnkt

;ein n�tzliches utility, welches einen screensaver,
;eine mausbeschleunigung und eine drucker- und eine
;tastatureinstellung beinhaltet. alles ordentlich mit
;dem xbra-verfahren.

taste1	=	1
taste2	=	2
taste3	=	0

start
	bra	prgstart
new_mouse_xbra	dc.l	'XBRA','MGBT'
new_mouse_old	dc.l	0
new_mousevec
	movem.l	d0/a0-a1,-(sp)
	cmp.b	#$f8,(a0)
	bcs.s	new_mousevec1	;relativer Mausvektor?
	cmp.b	#$fc,(a0)+
	bhi.s	new_mousevec1	;Ende, wenn nicht
	lea	hz200(pc),a1
	move.l	$04ba.w,d0
	sub.l	(a1),d0	;Differenz zum letzten Aufruf bilden
	move.l	$04ba.w,(a1)	;200Hz-Timerwert merken
	subq.l	#3,d0	;>5ms zur�ck?
	bcc.s	new_mousevec1	;dann Ende
	move.b	(a0),d0
	add.b	d0,(a0)+	;sonst die Koordinaten verdoppeln
	move.b	(a0),d0
	add.b	d0,(a0)
new_mousevec1
	movem.l	(sp)+,d0/a0-a1
old_mousevec
	move.l	new_mouse_old(pc),-(sp)
	rts	;Alten Mausvektor anspringen

jmp_hz_200_new	dc.l	0
new_hz200
	movem.l	d0/a0-a1,-(sp)
	move.l	vdi_mouse(pc),a0
	lea	new_mousevec(pc),a1
	cmp.l	(a0),a1
new_hz200a
	beq.s	jmp_200
	move.l	(a0),old_mousevec+2
	move.l	a1,(a0)	;dann den eigenen nochmal einsetzen
	or	#$2700,sr
	move.l	jmp_hz_200_new(pc),$114.w
jmp_200
	movem.l	(sp)+,d0/a0-a1
jmp_hz200
	move.l	jmp_hz_200_new(pc),-(sp)
	rts

new_kbd_xbra	dc.l	'XBRA','MGBT'
new_kbd_old	dc.l	0
new_keyboard
	movem.l	d0/a0,-(sp)
	move.l	kbshift(pc),a0
	move.b	(a0),d0
	btst	#taste1,d0
	beq.s	old_keyboard
	btst	#taste2,d0
	beq.s	old_keyboard
	btst	#taste3,d0
	bne.s	switch_screen
old_keyboard
	movem.l	(sp)+,d0/a0
jmp_keyboard
	move.l	new_kbd_old(pc),-(sp)
	rts	;fertig

switch_screen
	not.b	screen_flag	;"Bildschirm wieder an"-Flag

	lea	$ffff8207.w,a0
	move.b	$ffff8203.w,d0	;synchro
.loop
	cmp.b	(a0),d0
	bne.s	.loop
	bchg	#0,3(a0)	;Bild an/aus
	bra.s	old_keyboard

kbshift	dc.l	0	;zeiger auf kb-status
hz200	dc.l	0	;200Hz-Z�hler f�r die dyn.Mausbeschl.
vdi_mouse	dc.l	0
screen_flag	dc.b	0	;0 => Bildschirm ist an, <>0 => aus
	copyright
ende
	even
prgstart
	init
	Kbrate	#1,#$f
	Setprt	#%000100
	Supexec	super_init(pc)	;Initialierung in Supervisormode
	Cconws	text(pc)	;alles o.k.
	Ptermres	#0,#ende-start+$100

super_init
	move.l	$4f2.w,a0
	lea	$24(a0),a0
	move.l	(a0),kbshift

	bclr	#1,$ffff820a.w
	move	sr,-(sp)
	or	#$0700,sr	;IRQs aus

	Kbdvbase
	move.l	d0,a0
	lea	$10(a0),a0
	;move.l	(a0),old_mousevec+2	;originalen Mausvektor merken
	move.l	(a0),new_mouse_old	;merken f�r die XBRA-Kennung
	move.l	#new_mousevec,(a0)	;und neuen Mausvektor rein
	move.l	a0,vdi_mouse

	;move.l	$114.w,jmp_hz200+2
	move.l	$114.w,jmp_hz_200_new
	move.l	#new_hz200,$114.w

	;move.l	$118.w,jmp_keyboard+2
	move.l	$118.w,new_kbd_old	;s.o.
	move.l	#new_keyboard,$118.w	;Keyboardtreiber erweitern

	rte

	data
text	dc.b	$a,$d,'MAG-AUTO, (c) Magnum -BlackBox-.',$a,$d
	dc.b	'De-/aktivieren mit CONTROL-SHIFT-SHIFT!',$d,$a,0
