	include	tos_fnkt

;eine line-routine von magnum. entnommen aus:
;david f. rogers 'procedural elements for computer graphics'
;besonderheit: schnell. dies wird hier demonstriert.

	init	stack(pc)
start
	Cursconf	#0
	Supexec	begin(pc)
	Cursconf	#1
	Cconin
	Pterm0
begin
	Logbase
	move.l	d0,a6
	move.l	$466.w,d5

	moveq	#0,d1
	moveq	#0,d0
	move.l	#639,d2
	moveq	#0,d3
	move.l	#399,d4
.loop1
	bsr	line
	addq	#1,d3
	dbf	d4,.loop1


	move.l	#639,d0
	move.l	#399,d1
	moveq	#0,d2
	moveq	#0,d3
	move.l	#399,d4
.loop2
	bsr.s	line
	addq	#1,d3
	dbf	d4,.loop2


	move.l	#639,d0
	move.l	#399,d1
	move.l	#639,d2
	moveq	#0,d3
	move.l	#639,d4
.loop3
	bsr.s	line
	subq	#1,d2
	dbf	d4,.loop3


	moveq	#0,d0
	moveq	#0,d1
	move.l	#639,d2
	move.l	#399,d3
	move.l	#639,d4
.loop4
	bsr.s	line
	subq	#1,d2
	dbf	d4,.loop4

	move.l	$466.w,d6
	sub.l	d5,d6
	move.l	d6,d1
break
	divu	#70,d1
	swap	d1
	move	d1,d2
	swap	d1
	and.l	#$ffff,d1
	bsr	prt_dez
	Cconout	#'.'
	moveq	#1,d7
	bsr	division
	rts

	include	line_xor.s

division
;errechnet beliebig viele Nachkommastellen...
;(naja also nur bis etwa 10000stel)
;erwartet in d2.w den ersten rest und d7=1
;benutzt d2,d7
	mulu	#10,d7
	and.l	#$ffff,d2
	mulu	d7,d2
	tst	d2
	beq.s	ende
	divu	#70,d2
	swap	d2
	move	d2,d3
	swap	d2
	and.l	#$ffff,d2
	move.l	d2,d1
	bsr.s	prt_dez

	mulu	#10,d7
	and.l	#$ffff,d3
	tst	d3	
	beq.s	ende
	mulu	d7,d3
	divu	#70,d3
	swap	d3
	move	d3,d2
	swap	d3
	and.l	#$ffff,d3
	move.l	d3,d1
	bsr.s	prt_dez
ende
	rts

	include	prt_dez.s

	data
taste	dc.b	' Sekunden wurden etwa ben�tigt. Taste:',$d,$a,0
