	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	;konvertiert einen '/' in ein '\' w�hrend einer Fileoperation
	;des GEMDOS.

start:
	bra.s	los
	dc.l	'XBRA','SLSH'
old_jmp:	dc.l	0
new_trap:
	lea	(sp),a0
	btst	#$d,(sp)
	bne.s	weiter
	move.l	usp,a0
weiter:
	cmp	#$3d,(a0)
	beq.s	ist_ok
	cmp.	#$4e,(a0)
	beq.s	ist_ok
	cmp.	#$3c,(a0)
	bne.s	old_trap
ist_ok:
	lea	2(a0),a0
	move.l	(a0),a0
loop:
	tst.b	(a0)
	beq.s	old_trap
	cmp.b	#'/',(a0)
	beq.s	gefunden
return:
	addq.l	#1,a0
	bra.s	loop
gefunden:
	move.b	#'\',(a0)
	bra.s	return
old_trap:
	move.l	old_jmp(pc),-(sp)
	rts
ende:
los:
	init	stack(pc)
	Supexec	begin(pc)
	Ptermres	#0,#$100+ende-start
begin:
	move.l	$84.w,old_jmp
	move.l	#new_trap,$84.w
	rts
