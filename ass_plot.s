	include	tos_fnkt

;eine plot-routine von magnum.
;besonderheit: schnell. dies wird hier demonstriert.
;es werden 640*400 punkte gesetzt und die zeit gestoppt.

	init	stack
start
	Supexec	begin
	Cconws	taste
	Cconin
	Pterm0
begin
	Logbase
	move.l	d0,a6
	move.l	#399,d1
	move.l	$466.w,d5
.y_loop
	move.l	#639,d0
.x_loop
	bsr.s	plot
	dbf	d0,.x_loop
	dbf	d1,.y_loop
	move.l	$466.w,d6
	sub.l	d5,d6
	move.l	d6,d1
break
	divu	#70,d1
	swap	d1
	move	d1,d2
	swap	d1
	and.l	#$ffff,d1
	bsr.s	prt_dez
	Cconout	#'.'
	moveq	#1,d7
	moveq	#4,d6
.loop
	bsr.s	division
	dbf	d6,.loop
	rts
	include	plot.s

division
;errechnet beliebig viele Nachkommastellen...
;(naja also nur bis etwa 10000stel)
;erwartet in d2.w den ersten rest und d7=1
;benutzt d2,d2,d7
	mulu	#10,d7
	and.l	#$ffff,d2
	mulu	d7,d2
	divu	#70,d2
	swap	d2
	move	d2,d3
	swap	d2
	and.l	#$ffff,d2
	move.l	d2,d1
	bsr.s	prt_dez

	mulu	#10,d7
	and.l	#$ffff,d3
	mulu	d7,d3
	divu	#70,d3
	swap	d3
	move	d3,d2
	swap	d3
	and.l	#$ffff,d3
	move.l	d3,d1
	bsr.s	prt_dez
	rts

	include	prt_dez.s

	data
taste	dc.b	' Sekunden wurden etwa ben�tigt. Taste:',$d,$a,0
