	opt	a+,c+,o+,w+
	output	c:\ship.acc
appl_init	=	$0A000100
appl_write	=	$0C020101
menu_register	=	$23010101	;Auszug aus der GEM-Library
evnt_mesag	=	$17000101
form_alert	=	$34010101
graf_mouse	=	$4E010101

ACC_ID	=	$0400	;Messagenummern des XACC-Protokolls
ACC_OPEN	=	$0401	;der Protokollstufe 0
ACC_CLOSE	=	$0402
ACC_ACC	=	$0403
AC_OPEN	=	40	;Original-Messages
AC_CLOSE	=	41

version	=	$13	;Version 1.3
	text
start	move.l	A0,D0	;Zeiger auf die Basepage, wenn ACC
	bne.s	start1	;0<> kein ACC =>
	st	app	;-1=PRG (0 steht f�r ACC)
	move.l	4(SP),D0	;Basepage vom Stack holen
start1
	lea	own_stack(PC),SP	;Eigenen Stack einrichten (f�r ACC n�tig!)
	pea	ende-start+$0100.w	;Programml�nge
	move.l	D0,-(SP)	;Basepageadr des Starters
	move.l	#$4A0000,-(SP)
	trap	#1	;Mshrink()
	lea	12(SP),SP

	move.l	#appl_init,D0
	bsr	aes	;appl_init()
	move.w	D0,ap_id

	lea	addr_in(PC),A6
	lea	int_in(PC),A5	;zwei globale Variablen

	btst	#7,app(PC)	;PC-relative-Adressierung spart 1 Word
	bne.s	mainloop	;wenn PRG direkt starten

	move.w	ap_id(PC),(A5)
	lea	acc_name(PC),A0
	move.l	A0,(A6)
	move.l	#menu_register,D0
	bsr	aes	;menu_register()
	move.w	D0,menu_id	;ID merken

;************************************************************************
;* Die Hauptschleife des ACCs inkl. XACC-Protokoll der Stufe 0	*
;************************************************************************
event_loop	lea	evnt_buff(PC),A0
	move.l	A0,(A6)
	move.l	#evnt_mesag,D0
	bsr	aes	;evnt_mesag()

	moveq	#AC_CLOSE,D0
	cmp.w	(A0),D0
	bne.s	event_loop1	;AC_CLOSE? Nein! => weiter
	moveq	#0,D0
	bsr	send_id	;Message ACC_ID an das Hauptprogramm
	bra.s	event_loop

event_loop1	moveq	#AC_OPEN,D0
	cmp.w	(A0),D0
	bne.s	event_loop2	;AC_OPEN? Nein => weiter

	lea	mesag_buff(PC),A0
	move.w	#ACC_OPEN,(A0)+	;ACC wurde aufgerufen
	move.w	ap_id(PC),(A0)+
	clr.w	(A0)
	moveq	#0,D0	;Empf�nger das Hauptprogramm
	bsr	send_message

	bsr.s	mainloop	;das ACC-Hauptprogramm ausf�hren

	lea	mesag_buff(PC),A0
	move.w	#ACC_CLOSE,(A0)+	;ACC wurde beendet
	move.w	ap_id(PC),(A0)+
	clr.w	(A0)
	moveq	#0,D0	;Empf�nger das Hauptprogramm
	bsr	send_message

	bra.s	event_loop

event_loop2	cmpi.w	#ACC_ACC,(A0)	;Message von einem ACC?
	bne.s	event_loop	;Nein => wieder auf einen Event warten
	move.w	14(A0),D0	;ID des ACCs holen
	bsr.s	send_id	;und Message ACC_ID verschicken
	bra.s	event_loop

;************************************************************************
;* Das eigentliche Hauptprogramm	*
;************************************************************************
mainloop	move.w	#1,(A5)
	lea	acc_alert(PC),A0
	move.l	A0,(A6)
	move.l	#form_alert,D0
	bsr.s	aes	;form_alert()
	subq.w	#2,D0
	beq.s	mainloop1	;Abbruch angew�hlt

	moveq	#2,D0
	bsr.s	defmouse	;Busy Bee anschalten
	pea	park_all_hd(PC)
	move.w	#$26,-(SP)
	trap	#14	;Alle max.16 Platten parken
	addq.l	#6,SP
	moveq	#0,D0
	bsr.s	defmouse	;Mauszeiger wieder als Pfeil

mainloop1	btst	#7,app(PC)
	beq.s	mainloop2	;wenn ACC dann nur RTS
	clr.w	-(SP)
	trap	#1	;sonst Pterm0()
mainloop2	rts

;************************************************************************
;* Mauszeiger umdefineren (D0=neue Mausform)	*
;************************************************************************
defmouse	move.w	D0,int_in	;Mausform merken
	move.l	#graf_mouse,D0	;graf_mouse()

;************************************************************************
;* Mein eigener kleiner AES-Aufruf	*
;* D0=Die ersten 4 Eintr�ge im contrl-Array (Bytebreite!)	*
;************************************************************************
aes	movem.l	D1-A6,-(SP)
	lea	contrl(PC),A0
	clr.l	(A0)
	clr.l	4(A0)	;int_in-Array l�schen
	movep.l	D0,1(A0)	;und die neuen Daten eintragen
	lea	aes_para_blk(PC),A0
	move.l	A0,D1
	move.w	#$C8,D0
	trap	#2	;AES aufrufen
	move.w	int_out(PC),D0
	movem.l	(SP)+,D1-A6
	rts

;************************************************************************
;* ACC_ID-Message verschicken (D0=Empf�nger)	*
;************************************************************************
send_id	lea	mesag_buff(PC),A0
	move.w	#ACC_ID,(A0)+
	move.w	ap_id(PC),(A0)+	;appl_init()-ID
	clr.w	(A0)+	;nicht mehr als 16 Bytes
	move.w	#version<<8!0,(A0)+	;Versionsnummer & Protokollstufe
	lea	acc_name+2(PC),A1
	move.l	A1,(A0)+	;Zeiger auf den ACC-Namen (Hier=Men�name)
	move.w	menu_id(PC),(A0)	;menu_register()-ID

;************************************************************************
;* 8 Word-Message an D0 verschicken	*
;************************************************************************
send_message	move.w	D0,(A5)	;ID des Empf�ngers nach int_in
	move.w	#16,2(A5)	;16 Bytes versenden
	lea	mesag_buff(PC),A0
	move.l	A0,(A6)	;Zeiger auf den Message-Buffer
	move.l	#appl_write,D0
	bra.s	aes	;Message abschicken

;************************************************************************
;* Alle max.16 Platten parken	*
;************************************************************************
park_all_hd	movem.l	D0-D4/A0,-(SP)
	move.w	$043E.w,-(SP)	;flock retten
	st	$043E.w	;und sperren
	moveq	#0,D4	;Controllernummer 0-7
park_all_hd1	moveq	#0,D1	;Laufwerk 0
	bsr.s	park_all_hd3	;parken
	moveq	#1,D1	;Laufwerk 1
	bsr.s	park_all_hd3	;parken
	addq.w	#1,D4	;n�chster Controller
	cmp.w	#8,D4	;fertig?
	bne.s	park_all_hd1	;nein, n�chsten Controller nehmen
	move.w	(SP)+,$043E.w	;flock restaurieren
	movem.l	(SP)+,D0-D4/A0
	rts

park_all_hd3	lea	$FFFF8604.w,A0
	move.w	#$88,$02(A0)	;Zugriff auf HDC
	moveq	#0,D3
	move.w	D4,D3	;Drive nummer
	lsl.w	#5,D3
	swap	D3
	ori.l	#$1B008A,D3	;"Park unit"
	move.l	D3,(A0)	;1.Byte senden
	bsr.s	park_short_time
	bne.s	park_all_hd5
	lsl.w	#5,D1	;Unit nummer
	swap	D1
	move.w	#$8A,D1
	moveq	#3,D3	;4 Byte senden
park_all_hd4	move.l	D1,(A0)	;Byte 2-5 senden
	bsr.s	park_short_time
	bne.s	park_all_hd5	;Timeout =>
	ext.l	D1
	dbra	D3,park_all_hd4
	move.w	D1,$02(A0)	;und das 6.Byte senden
	moveq	#0,D0
	move.l	D0,(A0)
	move.l	#800,D0	;4 Sekunden Timeout
	bsr.s	park_wait_hd	;auf das Parken warten
	bne.s	park_all_hd5	;Timeout =>
	move.w	D1,$02(A0)
	nop
	moveq	#2,D0
	and.w	(A0),D0
park_all_hd5	move.w	#$80,$02(A0)	;FDC wieder selektieren
	nop
	move.w	(A0),D0
	rts

park_short_time	moveq	#30,D0	;0.15 Sekunden-Timeout
park_wait_hd	add.l	$04BA.w,D0	;Endewert des Timers
park_wait_hd1	btst	#5,$FFFFFA01.w	;HDC fertig?
	beq.s	park_wait_hd2	;Ja! => (Z-Flag=1)
	cmp.l	$04BA.w,D0	;Timeoutzeit erreicht?
	bhs.s	park_wait_hd1	;Nein! => weiter warten
	moveq	#-1,D0	;Timeout (Z-Flag=0)
park_wait_hd2	rts

;************************************************************************
;* Nun ein paar wichtige Daten	*
;************************************************************************
	data
aes_para_blk	dc.l	contrl
	dc.l	global
	dc.l	int_in
	dc.l	int_out
	dc.l	addr_in
	dc.l	addr_out

acc_name	dc.b	'  Park Harddisk',0
acc_alert	dc.b	'[2][Sollen die Harddisks|geparkt werden?][Ja|Nein]',0

;************************************************************************
;* Und ordentlich Zwischenspeicher	*
;************************************************************************
	bss
global	ds.w	15	;Die Standard-AES-Felder
contrl	ds.w	5
int_in	ds.w	16
int_out	ds.w	7
addr_in	ds.l	2
addr_out	ds.l	1
evnt_buff	ds.w	8	;Buffer f�r evnt_mesag()
mesag_buff	ds.w	8	;Message-Buffer f�r appl_write()
menu_id	ds.w	1	;ID des ACCs durch menu_register()
ap_id	ds.w	1	;ID des ACCs durch appl_init()
app	ds.w	1	;0=ACC, -1=PRG
	ds.l	128	;jedem Programm seinen eigenen Stack
own_stack
ende
