  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	fu�ball.prg	;So hei�t das PRG-File.

;Sollte mal ein Fu�ballspiel werden...

CON	=	2
IKBD	=	1
JOY0	=	$FE
JOY1	=	$FF
COLOR0	=	$ffff8240
DBASEH	=	$ffff8201
ISRB	=	$fffffa11
keyctl	=	$fffffc00
player_height	=	16
feld_breite	=	640
feld_hoehe	=	400
min_x	=	1
min_y	=	1
max_x	=	feld_breite-1-23
max_y	=	feld_hoehe-player_height

	text
start
	init	stack(pc)
begin
	init_gem
	moveq	#-1,d0
	bsr	malloc
	cmp.l	#feld_breite*feld_hoehe/4+256,d0
	bls	.ende
	move.l	#feld_breite*feld_hoehe/4+256,d0
	bsr	malloc
	move.l	d0,my_screens
	add.l	#256,d0
	and.l	#$ffffff00,d0
	lea	screen_tab(pc),a5
	move.l	d0,phys_screen(a5)
	add.l	#feld_breite*feld_hoehe/8,d0
	move.l	d0,log_screen(a5)

	graf_mkstate
	move.l	int_out+2(pc),d0
	move.l	d0,x0
	sub.l	#$200020,d0
	move.l	d0,x1
	move.l	x0(pc),old_x0
	move.l	x1(pc),old_x1

	v_hide_c
	wind_update	#BEG_UPDATE

	bsr	main

	wind_update	#END_UPDATE
	v_show_c	#1

	move.l	my_screens(pc),a0
	Mfree	(a0)
.ende
	exit_gem
	Pterm0
malloc
	Malloc	d0
	rts
	include	include\call_gem

;*******Hauptprogramm*************
main
	bsr	init
	Supexec	main_loop_init(pc)
	bsr	exit
	rts

;*******Hauptschleife
main_loop_init
	lea	screen_tab(pc),a5
main_loop
	tst.b	escape_flag
	bne.s	ende

	bsr	test_joy

	bsr.s	refresh

	bsr.s	draw

	tst.b	button0
	beq.s	.no_button0
	bsr	do_button0
.no_button0
	
	tst.b	button1
	beq.s	.no_button1
	bsr	do_button1
.no_button1
	move.l	(_frclock).w,d0
.loop	cmp.l	(_frclock).w,d0
	beq.s	.loop
	bra.s	main_loop
ende
	rts


;*******Unterprogramme************
refresh
	move.l	#refresh_buffer0,refresh_buffer(a5)
	move.l	refresh_address0(pc),refresh_address(a5)
	bsr	refresh_background
	move.l	#refresh_buffer1,refresh_buffer(a5)
	move.l	refresh_address1(pc),refresh_address(a5)
	bsr	refresh_background
	rts

draw
	move.l	#player0,player(a5)
	move	x0(pc),x(a5)
	move	y0(pc),y(a5)
	bsr	draw_player
	move.l	refresh_address(a5),refresh_address0
	
	move.l	#player1,player(a5)
	move	x1(pc),x(a5)
	move	y1(pc),y(a5)
	bsr	draw_player
	move.l	refresh_address(a5),refresh_address1
	rts

init
	bsr	setup_screen
	bsr	setup_daten
	bsr	setup_keyboard
	bsr	setup_interrupts
	rts

exit
	bsr	clr_interrupts
	bsr	clr_keyboard
	bsr	clr_screen
	rts

test_keyboard
	Bconstat	#CON
	tst	d0
	beq.s	.no_keys
	Bconin
	cmp.b	#27,d0
	seq	d0
.no_keys
	rts

test_joy
	move.b	joy0(pc),d0
	beq.s	.no_move0
	move	x0(pc),d1
	move	y0(pc),d2

	btst	#0,d0
	beq.s	.nicht_oben0
	subq	#1,d2
.nicht_oben0
	btst	#2,d0
	beq.s	.nicht_links0
	subq	#1,d1
.nicht_links0
	btst	#1,d0
	beq.s	.nicht_rechts0
	addq	#1,d2
.nicht_rechts0
	btst	#3,d0
	beq.s	.nicht_unten0
	addq	#1,d1
.nicht_unten0
	move	x0(pc),old_x0
	move	y0(pc),old_y0

	tst	d1
	bhi.s	.x0_not_0
	moveq	#min_x,d1
	bra.s	.x0_ok
.x0_not_0
	cmp	#max_x,d1
	bls.s	.x0_ok
	move	#max_x,d1
.x0_ok
	move	d1,x0

	tst	d2
	bhi.s	.y0_not_0
	moveq	#min_y,d2
	bra.s	.y0_ok
.y0_not_0
	cmp	#max_y,d2
	bls.s	.y0_ok
	move	#max_y,d2
.y0_ok
	move	d2,y0
.no_move0

	btst	#7,d0
	beq.s	.no_button0
	st	button0
.no_button0

	move.b	joy1(pc),d0
	beq.s	.no_move1
	move	x1(pc),d1
	move	y1(pc),d2

	btst	#0,d0
	beq.s	.nicht_oben1
	subq	#1,d2
.nicht_oben1
	btst	#2,d0
	beq.s	.nicht_links1
	subq	#1,d1
.nicht_links1
	btst	#1,d0
	beq.s	.nicht_rechts1
	addq	#1,d2
.nicht_rechts1
	btst	#3,d0
	beq.s	.nicht_unten1
	addq	#1,d1
.nicht_unten1
	move	x1(pc),old_x1
	move	y1(pc),old_y1

	tst	d1
	bhi.s	.x1_not_0
	moveq	#min_x,d1
	bra.s	.x1_ok
.x1_not_0
	cmp	#max_x,d1
	bls.s	.x1_ok
	move	#max_x,d1
.x1_ok
	move	d1,x1

	tst	d2
	bhi.s	.y1_not_0
	moveq	#min_y,d2
	bra.s	.y1_ok
.y1_not_0
	cmp	#max_y,d2
	bls.s	.y1_ok
	move	#max_y,d2
.y1_ok
	move	d2,y1
.no_move1

	btst	#7,d0
	beq.s	.no_button1
	st	button1
.no_button1

	rts

refresh_background	;Erwartet *(struct main) in a5
	move.l	refresh_address(a5),a6
	move.l	refresh_buffer(a5),a1
	move.l	a6,d0
	btst	#0,d0
	bne	.byte

	move.l	(a1)+,(a6)
	move.l	(a1)+,80(a6)
	move.l	(a1)+,160(a6)
	move.l	(a1)+,240(a6)
	move.l	(a1)+,320(a6)
	move.l	(a1)+,400(a6)
	move.l	(a1)+,480(a6)
	move.l	(a1)+,560(a6)
	move.l	(a1)+,640(a6)
	move.l	(a1)+,720(a6)
	move.l	(a1)+,800(a6)
	move.l	(a1)+,880(a6)
	move.l	(a1)+,960(a6)
	move.l	(a1)+,1040(a6)
	move.l	(a1)+,1120(a6)
	move.l	(a1),1200(a6)
	rts
.byte
	addq.l	#1,a1
	move.b	(a1)+,(a6)
	move	(a1)+,1(a6)
	move.b	(a1)+,3(a6)
	move.b	(a1)+,80(a6)
	move	(a1)+,81(a6)
	move.b	(a1)+,83(a6)
	move.b	(a1)+,160(a6)
	move	(a1)+,161(a6)
	move.b	(a1)+,163(a6)
	move.b	(a1)+,240(a6)
	move	(a1)+,241(a6)
	move.b	(a1)+,243(a6)
	move.b	(a1)+,320(a6)
	move	(a1)+,321(a6)
	move.b	(a1)+,323(a6)
	move.b	(a1)+,400(a6)
	move	(a1)+,401(a6)
	move.b	(a1)+,403(a6)
	move.b	(a1)+,480(a6)
	move	(a1)+,481(a6)
	move.b	(a1)+,483(a6)
	move.b	(a1)+,560(a6)
	move	(a1)+,561(a6)
	move.b	(a1)+,563(a6)
	move.b	(a1)+,640(a6)
	move	(a1)+,641(a6)
	move.b	(a1)+,643(a6)
	move.b	(a1)+,720(a6)
	move	(a1)+,721(a6)
	move.b	(a1)+,723(a6)
	move.b	(a1)+,800(a6)
	move	(a1)+,801(a6)
	move.b	(a1)+,803(a6)
	move.b	(a1)+,880(a6)
	move	(a1)+,881(a6)
	move.b	(a1)+,883(a6)
	move.b	(a1)+,960(a6)
	move	(a1)+,961(a6)
	move.b	(a1)+,963(a6)
	move.b	(a1)+,1040(a6)
	move	(a1)+,1041(a6)
	move.b	(a1)+,1043(a6)
	move.b	(a1)+,1120(a6)
	move	(a1)+,1121(a6)
	move.b	(a1)+,1123(a6)
	move.b	(a1)+,1200(a6)
	move	(a1)+,1201(a6)
	move.b	(a1),1203(a6)
	rts

draw_player	;Erwartet *(struct main) in a5
	move.l	log_screen(a5),a6	;Screenadresse
	move.l	player(a5),a1	;Grafikdaten f�r Spieler
	move.l	refresh_buffer(a5),a2

	moveq	#0,d0
	moveq	#0,d1
	move	x(a5),d0	;Aktuelle x/y-Position f�r jeden Spieler
	move	y(a5),d1
	moveq	#0,d2
	move	d0,d2
	lsr	#3,d2
	and	#7,d0
	eor	#7,d0
	lsl	#4,d1
	move	d1,d3
	add	d3,d3
	add	d3,d3
	add	d3,d1
	add	d1,d2
	lea	(a6,d2.l),a6
	move.l	a6,refresh_address(a5)
	btst	#0,d2	;Ungrade Bildschirmadresse!
	bne	.byte

	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,80(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,160(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,240(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,320(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,400(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,480(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,560(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,640(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,720(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,800(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,880(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,960(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,1040(a6)
	move.l	(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.l	d4,1120(a6)
	move.l	(a6),(a2)
	move.l	(a1),d4
	rol.l	d0,d4
	eor.l	d4,1200(a6)
	rts

.byte
	addq.l	#1,a2
	move.b	(a6),(a2)+
	move	1(a6),(a2)+
	move.b	3(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,3(a6)
	ror.l	#8,d4
	eor	d4,1(a6)
	swap	d4
	eor.b	d4,(a6)
	move.b	80(a6),(a2)+
	move	81(a6),(a2)+
	move.b	83(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,83(a6)
	ror.l	#8,d4
	eor	d4,81(a6)
	swap	d4
	eor.b	d4,80(a6)
	move.b	160(a6),(a2)+
	move	161(a6),(a2)+
	move.b	163(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,163(a6)
	ror.l	#8,d4
	eor	d4,161(a6)
	swap	d4
	eor.b	d4,160(a6)
	move.b	240(a6),(a2)+
	move	241(a6),(a2)+
	move.b	243(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,243(a6)
	ror.l	#8,d4
	eor	d4,241(a6)
	swap	d4
	eor.b	d4,240(a6)
	move.b	320(a6),(a2)+
	move	321(a6),(a2)+
	move.b	323(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,323(a6)
	ror.l	#8,d4
	eor	d4,321(a6)
	swap	d4
	eor.b	d4,320(a6)
	move.b	400(a6),(a2)+
	move	401(a6),(a2)+
	move.b	403(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,403(a6)
	ror.l	#8,d4
	eor	d4,401(a6)
	swap	d4
	eor.b	d4,400(a6)
	move.b	480(a6),(a2)+
	move	481(a6),(a2)+
	move.b	483(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,483(a6)
	ror.l	#8,d4
	eor	d4,481(a6)
	swap	d4
	eor.b	d4,480(a6)
	move.b	560(a6),(a2)+
	move	561(a6),(a2)+
	move.b	563(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,563(a6)
	ror.l	#8,d4
	eor	d4,561(a6)
	swap	d4
	eor.b	d4,560(a6)
	move.b	640(a6),(a2)+
	move	641(a6),(a2)+
	move.b	643(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,643(a6)
	ror.l	#8,d4
	eor	d4,641(a6)
	swap	d4
	eor.b	d4,640(a6)
	move.b	720(a6),(a2)+
	move	721(a6),(a2)+
	move.b	723(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,723(a6)
	ror.l	#8,d4
	eor	d4,721(a6)
	swap	d4
	eor.b	d4,720(a6)
	move.b	800(a6),(a2)+
	move	801(a6),(a2)+
	move.b	803(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,803(a6)
	ror.l	#8,d4
	eor	d4,801(a6)
	swap	d4
	eor.b	d4,800(a6)
	move.b	880(a6),(a2)+
	move	881(a6),(a2)+
	move.b	883(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,883(a6)
	ror.l	#8,d4
	eor	d4,881(a6)
	swap	d4
	eor.b	d4,880(a6)
	move.b	960(a6),(a2)+
	move	961(a6),(a2)+
	move.b	963(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,963(a6)
	ror.l	#8,d4
	eor	d4,961(a6)
	swap	d4
	eor.b	d4,960(a6)
	move.b	1040(a6),(a2)+
	move	1041(a6),(a2)+
	move.b	1043(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,1043(a6)
	ror.l	#8,d4
	eor	d4,1041(a6)
	swap	d4
	eor.b	d4,1040(a6)
	move.b	1120(a6),(a2)+
	move	1121(a6),(a2)+
	move.b	1123(a6),(a2)+
	move.l	(a1)+,d4
	rol.l	d0,d4
	eor.b	d4,1123(a6)
	ror.l	#8,d4
	eor	d4,1121(a6)
	swap	d4
	eor.b	d4,1120(a6)
	move.b	1200(a6),(a2)+
	move	1201(a6),(a2)+
	move.b	1203(a6),(a2)
	move.l	(a1),d4
	rol.l	d0,d4
	eor.b	d4,1203(a6)
	ror.l	#8,d4
	eor	d4,1201(a6)
	swap	d4
	eor.b	d4,1200(a6)
	rts

do_button0
	clr.b	button0
	not	(COLOR0).w
	not	(COLOR0).w
	rts

do_button1
	clr.b	button1
	not	(COLOR0).w
	not	(COLOR0).w
	rts

new_ikbd
	move.l	d0,-(sp)
	move.l	a0,-(sp)
	lea	(keyctl).w,a0
	move.b	(a0),d0
	btst	#7,d0
	beq.s	.ende	;Kein Zeichen angekommen
	btst	#0,d0	;Receive Buffer voll?
	beq.s	.ende	;Nein
	move.b	2(a0),d0	;Zeichen holen...
	tst.b	hole_joy0	;War das n�chste f�r JOY0 bestimmt?
	beq.s	.nicht_joy0	;N��
	move.b	d0,joy0
	clr.b	hole_joy0
	bra.s	.ende
.nicht_joy0
	tst.b	hole_joy1
	beq.s	.nicht_joy1
	move.b	d0,joy1
	clr.b	hole_joy1
	bra.s	.ende
.nicht_joy1
	cmp.b	#JOY0,d0
	seq	hole_joy0
	bra.s	.ende
	cmp.b	#JOY1,d0
	seq	hole_joy1
	bra.s	.ende
	cmp.b	#1,d0
	seq	escape_flag
.ende
	move.l	(sp)+,a0
	move.l	(sp)+,d0
	move.b	#%10111111,(ISRB).w
	rte

new_vbl
	move	#$2700,sr
	addq.l	#1,(_frclock).w
	movem.l	d0/a0,-(sp)
	lea	screen_tab(pc),a0
	move.l	log_screen(a0),d0
	move.l	phys_screen(a0),log_screen(a0)
	move.l	d0,phys_screen(a0)
	lsr.l	#8,d0
	lea	(DBASEH).w,a0
	movep	d0,(a0)
	movem.l	(sp)+,d0/a0
	rte
;*******Hilfsroutinen*************
setup_interrupts
	Setexc	new_vbl(pc),#28
	move.l	d0,old_vbl
	Setexc	new_ikbd(pc),#70
	move.l	d0,old_ikbd
	rts

clr_interrupts
	move.l	old_ikbd(pc),a0
	Setexc	(a0),#70
	move.l	old_vbl(pc),a0
	Setexc	(a0),#28
	rts

setup_keyboard
	Bconout	#$12,#4	;Disable mousereports
	Bconout	#$14,#4	;Joyreports on
	rts

clr_keyboard
	Bconout	#$1a,#4	;Joyreports off
	Bconout	#8,#4	;Mouse on
	rts

setup_screen
	lea	screen_tab(pc),a5
	Physbase
	move.l	d0,phys
	Logbase
	move.l	d0,log

	moveq	#0,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	move.l	d0,a1
	move.l	phys_screen(a5),a6	;Logischen Bildschirm l�schen.
	move.l	#feld_breite*feld_hoehe/128+1,d7
	add.l	#feld_breite*feld_hoehe/4+32,a6
.loop
	movem.l	d0-d6/a1,-(a6)
	subq.l	#1,d7
	bne.s	.loop

	Setscreen	#-1,phys_screen(a5),log_screen(a5)
	rts

clr_screen
	Setscreen	#-1,phys(pc),log(pc)
	rts

setup_daten
	lea	screen_tab(pc),a5
	move.l	phys_screen(a5),a6
	move.l	a6,refresh_address0
	move.l	a6,refresh_address1
	lea	refresh_buffer0(pc),a1
	lea	refresh_buffer1(pc),a2
	moveq	#player_height-1,d7
.loop
	move.l	(a6),(a1)+
	move.l	(a6)+,(a2)+
	dbf	d7,.loop
	clr.b	escape_flag
	rts

;*******Datenbereich***********
	data
ball
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000111100000000000
	dc.l	%000000001111110000000000
	dc.l	%000000001111110000000000
	dc.l	%000000000111100000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000

player0
	dc.l	%000000000000000000000000
	dc.l	%001110000000000000000000
	dc.l	%010110000000000000000000
	dc.l	%000110000000000000000000
	dc.l	%000110000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000111110000000000
	dc.l	%000011111000001111100000
	dc.l	%000111110000000111110000
	dc.l	%000011111000001111100000
	dc.l	%000000000111110000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000

player1
	dc.l	%000000000000000000000000
	dc.l	%001110000000000000000000
	dc.l	%000001000000000000000000
	dc.l	%000010000000000000000000
	dc.l	%000100000000000000000000
	dc.l	%001111000000000000000000
	dc.l	%000000000111110000000000
	dc.l	%000011111111111111100000
	dc.l	%000100011111111100010000
	dc.l	%000011111111111111100000
	dc.l	%000000000111110000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000
	dc.l	%000000000000000000000000

;*******BSS-Bereich************
	bss
	rsreset
log_screen	rs.l	1
phys_screen	rs.l	1
refresh_address	rs.l	1
refresh_buffer	rs.l	1
player	rs.l	1
x	rs.w	1
y	rs.w	1
screen_tab_size	rs.b	0

screen_tab	ds.b	screen_tab_size

my_screens	ds.l	1
old_ikbd	ds.l	1
old_vbl	ds.l	1
phys	ds.l	1
log	ds.l	1
refresh_address0	ds.l	1
refresh_address1	ds.l	1
refresh_buffer0	ds.l	player_height+1
refresh_buffer1	ds.l	player_height+1
x0	ds.w	1
y0	ds.w	1
old_x0	ds.w	1
old_y0	ds.w	1
x1	ds.w	1
y1	ds.w	1
old_x1	ds.w	1
old_y1	ds.w	1
	even
joy0	ds.b	1
joy1	ds.b	1
button0	ds.b	1
button1	ds.b	1
hole_joy0	ds.b	1
hole_joy1	ds.b	1
escape_flag	ds.b	1