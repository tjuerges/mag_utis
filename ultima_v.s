	include	tos_fnkt
;patcht das gemdos, soda� ein zugriff auf laufwerk a: auf das
;aktuelle laufwerk und verzeichnis umgebogen wird.
;ich habe es f�r ultima V ben�tigt (lief n�mlich nicht auf hd!!!).

start:
	bra.s	los
	dc.l	'XBRA','PDSK'
old_jmp:	dc.l	0
new_trap:
	move.l	usp,a0
	btst	#5,(sp)
	beq.s	.user
	lea	6(sp),a0
.user:
	cmp	#$3d,(a0)
	beq.s	.ist_ok
	cmp.	#$4e,(a0)
	beq.s	.ist_ok
	cmp.	#$3c,(a0)
	bne.s	old_trap
.ist_ok
	lea	2(a0),a0
	move.l	a0,d0
	move.l	(a0),a0
	cmp.b	#'A',(a0)
	beq.s	.gefunden
	cmp.b	#'a',(a0)
	bne.s	old_trap
.gefunden
	cmp.b	#':',1(a0)
	bne.s	old_trap
	cmp.b	#'\',2(a0)
	bne.s	old_trap
	move.l	d0,a0
	addq.l	#3,(a0)
old_trap:
	move.l	old_jmp(pc),-(sp)
	rts
ende:
los:
	init
	Supexec	begin(pc)
	Cconws	hallo(pc)
	Cconin
	Ptermres	#0,#$100+ende-start
begin:
	move.l	$84.w,old_jmp
	move.l	#new_trap,$84.w
	rts
hallo:	dc.b	27,'EThis little Programm patches',$d,$a
	dc.b	'following GEMDOS-functions for',$d,$a
	dc.b	'running any program from your',$d,$a
	dc.b	'Harddisk instead of drive A:',$d,$a
	dc.b	'Fopen, Fcreate & Fsfirst',$d,$a,$a
	dc.b	"Don't be scared, it",$d,$a
	dc.b	'is not a virus!',$d,$a,$a
	dc.b	'If you are asked to insert',$d,$a
	dc.b	'any disk in drive A:, only',$d,$a
	dc.b	'press mouse-button or any key.',$d,$a
	dc.b	9,'Written by Magnum.',$d,$a,$a
	dc.b	'Press any key!',0
