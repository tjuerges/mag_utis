start_address	=	$60000	;hier hin wird klax_start geladen
start_track	=	80	;starttrack. ab hier liegt klax_start
	include	tos_fnkt

;bootsektor-programm f�r klax (modifiziert).

	bra.s	start
filler	dc.b	'MAGNUM'
serial	dc.b	$22,$22,$22
bps	dc.b	0,2
spc	dc.b	2
res	dc.b	1,0
nfats	dc.b	0
ndirs	dc.b	0,0
nsects	dc.b	$34,3
media	dc.b	$f8
spf	dc.b	0,0
spt	dc.b	10,0
nsides	dc.b	1,0
nhid	dc.b	$34,3
execflag	dc.b	0,0
ldmode	dc.b	0,0
ssect	dc.b	$ff,$ff
sctcnt	dc.b	$ff,$ff
ldaddr	dc.b	0,4,0,0
fatbuf	dc.b	0,0,8,0
fname	dc.b	'KLAXbooter!'
dummy	dc.b	0

start
	move.l	#start_address,a0
	moveq	#start_track,d1
	bsr.s	flop
goon
	lea	$1400(a0),a0
	addq	#1,d1
	bsr.s	flop
begin
	lea	-$1400(a0),a0
	jmp	(a0)
flop
	Floprd	#10,#0,d1,#1,#0,(a0)
	tst.w	d0
	bne.s	ende
	rts
ende
	illegal
	dc.b	' Greetings must go to: DOK, 007, Otti, JM, Schotti,'
	dc.b	' Olli, Steffi, Schirky and Draku!!'
	dc.b	' Copy this disk with 82 tracks, 10 sectors '
	dc.b	' and single sided.'
	dc.b	' AND THINK ABOUT IT: If you like KLAXing'
	dc.b	' buy this great game !!!!!!!!!!!!!!!   MAGNUM.'
