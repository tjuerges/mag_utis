	include	tos_fnkt

;Hardcopy-Routine f�r 24-Nadel-Drucker. Entlehnt von einem anderen Author.
;Verbessert von mir.
;01-01-1992

los
	bra	hc_install

;Kennzeichnung nach Xbra-Konvention
	dc.b	'XBRA'	;4 Bytes 'XBRA'
	dc.b	'HC24'	;4 Bytes Programm-Name
_xb_old	dc.l	0	;4 Bytes f�r alten Vector

regs	reg	d0-d5/a0-a2

hcstart
	tst	(prt_cnt).w	;Ist das Programm schon bei der Arbeit?
	bne.s	ist_schon_aktiv
	movem.l	regs,-(sp)	;Register retten, man wei� ja nie.
	bsr.s	dojob	;Do it...
	movem.l	(sp)+,regs
	move	#-1,(prt_cnt).w	;Abmelden, Hardcopy fertig.
ist_schon_aktiv
	rts	;Und Tsch��.

dojob
	sub.l	#46,(savptr).w	;Tip aus dem Profibuch.
	Kbshift	#-1	;Tastaturstatus feststellen.
	add.l	#46,(savptr).w
	move.b	d0,d1
	and.b	#3,d0	;Alt&Help gedr�ckt?
	bne.s	ist_schon_aktiv	;Nee: Kleiner Programmiertechnischer Kniff.

shifttst
	clr.b	d2
	btst	#1,d1	;Shift gedr�ckt?
	beq.s	noshift
	move.b	#1,d2	;ja gro�e,HC
noshift
	bsr	sendcr	;Zeilenvorschub
	bsr	setlf	;Linefeed f�r Grafik einstellen
	move.l	(_v_bas_ad).w,a0
	tst.b	d2
	bne.s	vert_hc	;d2=1 =>gro�e HC

;------------------------------------------------------------------------
;	Hor. Hardcopy
;------------------------------------------------------------------------
; Registerbelegung:
; d0-
; d1- Bitz�hler f�r Puffer
; d2/6- Byte/Zeilenz f Puffer
; d3- 4mal Z�hler f 1.Druckbyte (jedes Bit 2x)
; d4- zu druckendes Zeichen
; d5- Anz Zeich (f chrout)
; a0- Screenadr
; a1- Kopie von a0, die aber ver�ndert wird
; a2- Adr Zeichenstring (f�r chrout)
;------------------------------------------------------------------------
	moveq	#33,d6	;34 Zeilen (34*12=408)
_scr
	moveq	#79,d2	;eine Zeile drucken (80 B)
	bsr	grafon	;Grafikzeile senden
row
	moveq	#7,d1	;ein Byte bearbeiten
byte
	move.l	a0,a1
	moveq	#2,d5	;das ganze 3x
col
	moveq	#3,d3
four
	add	d4,d4
	add	d4,d4
	btst	d1,(a1)
	beq.s	offset	;Punkt gesetzt?
	addq	#2,d4	;ja,aber nur 1 Punkt f Drucker statt 2
	btst	d1,80(a1)	;teste nachfolg Punkt
	beq.s	offset
	addq	#1,d4	;und setze ggf den fehl Druckerpunkt
offset
	lea	80(a1),a1
	dbf	d3,four
	move	d4,-(sp)	;Druckbyte rett
	bsr	chrout	;und ausgeben
	dbf	d5,col	;das 3x
	move	4(sp),d4
	bsr.s	chrout	;und die 3. byte wg der horiz
	move	2(sp),d4	;Verdopplg nochm raus
	bsr.s	chrout
	move	(sp),d4
	bsr.s	chrout
	addq.l	#6,sp	;Stack reinigen
	dbf	d1,byte	;ganzes Byte abarbeiten
	addq.l	#1,a0	;n�chst Byte
	dbf	d2,row	;Zeile bearbeiten
	bsr	sendcr	;Zeilenvorschub senden
	lea	880(a0),a0	;offset f�r die n�chst 12 Pixelzeilen
	dbf	d6,_scr	;ganzer Bildsch
	bsr	sendcr
bye
	bsr	reslf
	rts

;-----------------------------------------------------------------------------
; Vertikale HC (gro�)
;-----------------------------------------------------------------------------
; Registerbelegung
; d1 - Bitz�hler f Puffer
; d2 - Bytez�hler f Puffer
; d3 - Zeilenz "
; d4 - zu druckendes Zeichen
; d5 - Anz Zeichen (f chrout)
; a0 - Screenadresse
; a1 - Kopie von a0, die aber ver�ndert wird
; a2 - Adresse Zeichenstring (f chrout)
;-----------------------------------------------------------------------------
vert_hc
	lea	31920(a0),a0	;letzte Bildschirmzeile
	moveq	#79,d3
v_screen
	move.l	a0,a1
	bsr.s	v_grafon
	move	#399,d2
v_row
	moveq	#7,d1
v_byte
	lsl.l	#3,d4
	btst	d1,(a1)
	beq.s	noadd
	addq.l	#7,d4
noadd
	dbf	d1,v_byte
	move	d4,-(sp)	;Byte 3
	lsr.l	#8,d4
	move	d4,-(sp)	;Byte 2
	lsr	#8,d4
	move	d4,-(sp)	;Byte 1
	moveq	#2,d5
dr_col
	move	(sp),d4	;3 mal 3 Bytes ausgeben
	bsr.s	chrout
	move	2(sp),d4
	bsr.s	chrout
	move	4(sp),d4
	bsr.s	chrout
	dbf	d5,dr_col
	addq.l	#6,sp
	lea	-80(a1),a1
	dbf	d2,v_row
	bsr.s	sendcr
	addq.l	#1,a0
	dbf	d3,v_screen
	bsr.s	sendcr
	bra.s	bye

;-----------------------------------------------------------------------------
; Unterprogramm zur Zeichenausgabe Drucker
;-----------------------------------------------------------------------------
chrout
	ext	d4
	sub.l	#46,(savptr).w
	Bconout	d4,#0
	add.l	#46,(savptr).w
	rts
grafon
	lea	grafdat(pc),a2	;Grafikmodus hor HC einstell
	bra.s	strout
v_grafon
	lea	v_grdat(pc),a2	;Grafik-Modus vert HC ein
	bra.s	strout
sendcr
	lea	feed(pc),a2	;Zeilenvorschub
	bra.s	strout
setlf
	lea	lfdat(pc),a2	;Zeil-Vorsch Grafik on
	bra.s	strout
reslf
	lea	oldlf(pc),a2	;normal Zeil Vorsch ein
strout
	move.b	(a2)+,d4	;gibt durch a2 adr String
	cmp.b	#255,d4
	beq.s	.ende
	bsr.s	chrout
	bra.s	strout
.ende
	rts

lfdat	dc.b	27,'3',24,255
oldlf	dc.b	27,'2',255
grafdat	dc.b	27,'$',30,0,27,'*',39,0,5,255	;$:Rand f�r kleine HC
						;*:Grafikmodus
v_grdat	dc.b	27,'$',30,0,27,'*',39,176,4,255	;$:Rand f�r gro�e HC
						;*:Grafikmodus
feed	dc.b	$d,$a,255
	copyright
ende
	even
hc_install
	init	stack(pc)
	Supexec	bieg(pc)
	Cconws	hallo(pc)

	Ptermres	#0,#$100+ende-los,-(sp)	;Prg resident machen

;Prg ist resident und wird an dieser Stelle verlassen
bieg:	move.l	(scr_dump).w,_xb_old	;alten HC-Vector nach Xbra-Konv rett
	move.l	#hcstart,(scr_dump).w	;HC-Vect auf neue Routine setzen
	rts	
hallo	hello_world	'MAG-24HC'
	dc.b	'Hardcopy f�r 24-Nadel-Drucker ist',$d,$a
	dc.b	'instaliert.',$d,$a
	dc.b	'ALT+HELP:	Normale Hardcopy',$d,$a
	dc.b	'SHIFT+ALT+HELP:	Gro�e Hardcopy',$d,$a,0
