	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	gem	;Vdi- & Aes-Macros einbinden.

	include	tron0815.h

	init
	open_gem
begin
	bsr	load_rsrc
	bsr	first
.init
	bsr	init_menu
	cmp.b	#call_menu,menu
	bne.s	.weiter1
	bsr	call_menu
.weiter1
	cmp.b	#short_cut,menu
	bne.s	begin
	bsr	short_cut
	bra.s	.init

start
	bsr	klick_aus
	moveq	#-1,d0
	bsr	maus
	clr.l	score1
	clr.l	score2
	move.b	#9,anzahl
.loop
	bsr	init
.loop_do
	bsr	warten
	bsr	speed_zeit
	bsr	joystick_abfrage
	bsr	calc_koord
	bsr	test_bonus
	bsr	kollision
	btst	#abbruch,flag
	bne.s	.abbruch
	move.l	screen(pc),a6
	move	x1(pc),d0
	move	y1(pc),d1
	bsr	punkt
	move	x2(pc),d0
	move	y2(pc),d1
	bsr	punkt
	bra.s	.loop_do
.abbruch
	btst	#spielende,flag
	beq.s	.ende
	bsr	stop_play
	subq.b	#1,anzahl
	bne.s	.loop
.ende
	moveq	#0,d0
	bsr	maus
	bsr	klick_an
	bsr	hiscore_update
	bsr	bestenliste
	bsr	key
	bsr	init_menu
	rts


first
	graf_mouse	#ARROW,#dummy
	bsr	clr_scrn
	Getrez
	cmp.b	#2,d0
	beq.s	.ok
	
	Pterm0
	form_alert	#1,#laeuft_nicht_str
	Physbase
	move.l	d0,screen

	lea	inaktiv(pc),a0
	move.l	a0,aktivkey1
	move.l	a0,aktivkey2
	bset	#spielende,flag
	bclr	#bildswitch,flag
	bset	#key1,flag
	bset	#key2,flag
	bclr	#screen_mem,flag

	Malloc	#32000
	move.l	d0,file_addresse
	bpl.s	.weiter
	bset	#screen_mem,flag
.weiter
	move.b	$484.w,klick
	clr.b	$ffff8240.w
	bsr	hiscore_load
	bsr	sample_load
	bsr	pic_load
	btst	#tonswitch,flag
	bne.s	.weiter_ton
	form_alert	#1,#ton_an_str
.weiter_ton
	rts


hintergrund_einladen
	btst	#bildswitch,flag
	bne.s	.nichts
	lea	name(pc),a0
	add.b	#'0',d0
	move.b	d0,(a0)+
	move.b	#'.',(a0)+
	lea	(a0),a1
	move.b	#'M',(a0)+
	move.b	#'C',(a0)+
	move.b	#'R',(a0)+
	clr.b	(a0)

	Dsetpath	ordnerpic(pc)

	Fsfirst	#0,tron(pc)
	cmp	#OK,d0
	bne.s	.load_doo
	btst	#screen_mem,flag
	beq.s	.load_doo
	Fopen	#0,tron(pc)
	move	d0,handle
	bmi	file_error
	move.l	file_addresse(pc),a0
	Fread	d0,(a0),#-1
	move.l	d0,file_length
	Fclose	handle(pc)
	bsr.s	bild_entpacken
	bra.s	.nichts

.load_doo
	move.b	#'D',(a1)+
	move.b	#'O',(a1)+
	move.b	#'O',(a1)+
	clr.b	(a1)

	Fsfirst	#0,tron(pc)
	cmp	#OK,d0
	bne.s	.nichts

	Fopen	#0,tron(pc)
	move	d0,handle
	bmi	file_error
	move.l	screen(pc),a0
	Fread	d0,(a0),#-1
	move.l	d0,file_length
	Fclose	handle(pc)
.nichts
	v_bar	#0,#0,#639,#399
  	v_bar	#1,#1,#638,#398
	rts


bild_entpacken
	move.l	file_addresse(pc),a1
	move.l	screen(pc),a2
	move.l	file_length(pc),d4
	bsr	start_iff_decrunch
	rts

sample_einladen
	lea	name(pc),a0
	Dsetpath	ordnersnd(pc)
  fname$=ordnersnd$+sample_name$+".SND"
  IF EXIST(fname$)
    OPEN "I",#1,fname$
    samplelaenge%=LOF(#1)
    CLOSE #1
    '
    samplestart%=MALLOC(samplelaenge%)
    IF samplestart%>0 AND SGN(samplestart%)<>-1
      BLOAD fname$,samplestart%
    ELSE
      ALERT 3," Nicht gen�gend Speicherplatz vorhanden. ",1," Abbruch ",dummy|
    ENDIF
  ENDIF
	rts

sample_load
  sample_einladen("END")
  sample%(max_samples|,0)=samplestart%
  sample%(max_samples|,1)=samplelaenge%
  '
  INC max_samples|
  sample_einladen("SPED")
  sample%(max_samples|,0)=samplestart%
  sample%(max_samples|,1)=samplelaenge%
  '
  INC max_samples|
  sample_einladen("EAT")
  sample%(max_samples|,0)=samplestart%
  sample%(max_samples|,1)=samplelaenge%
  '
  INC max_samples|
  sample_einladen("DARK")
  sample%(max_samples|,0)=samplestart%
  sample%(max_samples|,1)=samplelaenge%
  '
	bclr	#tonswitch,flag
	rts

pic_load
  LOCAL dummy$
  '
  OPEN "i",#1,ordnerpic$+"HELP.DAT"
  score_pic$=INPUT$(LOF(#1),#1)
  CLOSE #1
  '
  OPEN "i",#1,ordnerpic$+"PAND.DAT"
  panda$=INPUT$(LOF(#1),#1)
  CLOSE #1
  '
  OPEN "i",#1,ordnerpic$+"LEVL.DAT"
  INPUT #1,lfast$
  INPUT #1,dummy$
  INPUT #1,lslow$
  INPUT #1,dummy$
  INPUT #1,rfast$
  INPUT #1,dummy$
  INPUT #1,rslow$
  INPUT #1,dummy$
  CLOSE #1
	rts

init
  LOCAL i|,dummy|
	bsr	clr_scn
	bsr	hintergrund_einladen
	clr.l	zaehler
  REPEAT
    neux1&=RAND(3)-1
    neuy2&=RAND(3)-1
    neuy1&=RAND(3)-1
    neux2&=RAND(3)-1
  UNTIL ((neux2& OR neuy2&)<>0) AND ((neux1& OR neuy1&)<>0)
  '
  setup_spieler(1)
  setup_spieler(2)
  setup_spieler(3) !Bonusgegenstand #1
  setup_spieler(4) !Bonusgegenstand #2
	move.b	#3,speed1
	move.b	#3,speed2
	clr.b	t1
	clr.b	t2
	bset	#abbruch,flag
	bset	#spielende,flag
	bsr	klick_aus
  GRAPHMODE 3
  DEFTEXT 1,0,0,4,13
  FOR dummy|=1 TO 3
    FOR i|=1 TO 2
      TEXT x1&-16,y1&-4,spieler1$
      TEXT x2&-16,y2&-4,spieler2$
      DELAY 0.5
    NEXT i|
  NEXT dummy|
  DEFTEXT 1,0,0,13,13
  GRAPHMODE 1
	rts

setup_spieler(spieler_nummer|)
  LOCAL xdummy&,ydummy&,ok!,x&,y&
  '
  REPEAT
    x&=RAND(639)
    y&=RAND(399)
	bclr	#ok,flag
    '
    FOR xdummy&=x&-abstand| TO x&+abstand|
      IF POINT(xdummy&,y&-abstand|)=1 OR POINT(xdummy&,y&+abstand|)=1
	bset	#ok,flag
        xdummy&=x&+abstand|
      ENDIF
    NEXT xdummy&
    '
    IF ok!=TRUE
      FOR ydummy&=y&-abstand| TO y&+abstand|
        IF POINT(x&-abstand|,ydummy&)=1 OR POINT(x&+abstand|,ydummy&)=1
	bset	#ok,flag
          ydummy&=y&+abstand|
        ENDIF
      NEXT ydummy&
    ENDIF
  UNTIL ok!=TRUE
  '
  SELECT spieler_nummer|
  CASE 1
	move	x(pc),x1
	move	y(pc),y1
  CASE 2
	move	x(pc),x2
	move	y(pc),y2
  CASE 3
    REPEAT
      bonus_art1|=RAND(255)
    UNTIL ((bonus_art1|>=ASC("A")) AND (bonus_art1|<=ASC("Z"))) OR ((bonus_art1|>=ASC("a")) AND (bonus_art1|<=ASC("z")))
	move	x(pc),bonusx1
	move	y(pc),bonusy1
	bclr	#bonus1,flag
	move.b	#8,bonus_breite1
	move.b	#17,bonus_hoehe1
    TEXT bonusx1&,bonusy1&,CHR$(bonus_art1|)
  CASE 4
    REPEAT
      bonus_art2|=RAND(255)
    UNTIL ((bonus_art2|>=ASC("A")) AND (bonus_art2|<=ASC("Z"))) OR ((bonus_art2|>=ASC("a")) AND (bonus_art2|<=ASC("z")))
	move	x(pc),bonusx2
	move	y(pc),bonusy2
	bclr	#bonus2,flag
	move.b	#8,bonus_breite2
	move.b	#17,bonus_hoehe2
    TEXT bonusx2&,bonusy2&,CHR$(bonus_art2|)
  ENDSELECT
	rts

joystick_abfrage
  LOCAL dnx1&,dny1&,dnx2&,dny2&
  '
	move	neux1(pc),dnx1
	move	neux2(pc),dnx2
	move	neuy1(pc),dny1
	move	neuy2(pc),dny2
	clr	neux1
	clr	neuy1
	clr	neux2
	clr	neuy2
  key|=@get_scan
  '
  IF key1!=FALSE
    ' Spieler 1
    SELECT STICK(0)
    CASE 0
	move	dnx1(pc),neux1
	move	dny1(pc),neuy1
    CASE 1
      ' oben
	move	#-1,neuy1
    CASE 2
      ' unten
	move	#1,neuy1
    CASE 4
      ' links
      move	#-1,neux1
    CASE 8
      ' rechts
      move	#1,neux1
    CASE 9
      ' rechts oben
      move	#1,neux1
      move	#-1,neuy1
    CASE 5
      ' links oben
	move	#1,neux1
	move	#1,neuy1
    CASE 6
      ' links unten
	move	#-1,neux1
	move	#1,neuy1
    CASE 10
      ' rechts unten
	move	#1,neux1
	move	#1,neuy1
    ENDSELECT
    '
  ELSE
    '
    SELECT key|
    CASE 0
	move	dnx1(pc),neux1
	move	dny1(pc),neuy1
    CASE o1|
      ' oben
	move	#-1,neuy1
    CASE u1|
      ' unten
	move	#1,neuy1
    CASE l1|
      ' links
      move	#-1,neux1
    CASE r1|
      ' rechts
	move	#1,neux1
    CASE ro1|
      ' rechts oben
      move	#1,neux1
      move	#-1,neuy1
    CASE lo1|
      ' links oben
      move	#-1,neux1
      move	#-1,neuy1
    CASE lu1|
      ' links unten
      move	#-1,neux1
      move	#1,neuy1
    CASE ru1|
      ' rechts unten
      move	#1,neux1
      move	#1,neuy1
    DEFAULT
	move	dnx1(pc),neux1
	move	dny1(pc),neuy1
    ENDSELECT
  ENDIF
  '
  '
  ' Spieler 2
  IF key2!=FALSE
    SELECT STICK(1)
    CASE 0
      neux2&=dnx2&
      neuy2&=dny2&
    CASE 1
      ' oben
      neuy2&=-1
    CASE 2
      ' unten
      neuy2&=1
    CASE 4
      ' links
      neux2&=-1
    CASE 8
      ' rechts
      neux2&=1
    CASE 9
      ' rechts oben
      neux2&=1
      neuy2&=-1
    CASE 5
      ' links oben
      neux2&=-1
      neuy2&=-1
    CASE 6
      ' links unten
      neux2&=-1
      neuy2&=1
    CASE 10
      ' rechts unten
      neux2&=1
      neuy2&=1
    ENDSELECT
    '
  ELSE
    '
    SELECT key|
    CASE 0
      neux2&=dnx2&
      neuy2&=dny2&
    CASE o2|
      ' oben
      neuy2&=-1
    CASE u2|
      ' unten
      neuy2&=1
    CASE l2|
      ' links
      neux2&=-1
    CASE r2|
      ' rechts
      neux2&=1
    CASE ro2|
      ' rechts oben
      neux2&=1
      neuy2&=-1
    CASE lo2|
      ' links oben
      neux2&=-1
      neuy2&=-1
    CASE lu2|
      ' links unten
      neux2&=-1
      neuy2&=1
    CASE ru2|
      ' rechts unten
      neux2&=1
      neuy2&=1
    DEFAULT
      neux2&=dnx2&
      neuy2&=dny2&
    ENDSELECT
  ENDIF
  '
	bsr	feuerknopf
	rts

feuerknopf
  IF key1!=FALSE
    IF STRIG(0)=TRUE
      IF t1|=0
        IF speed1|>0
	move.b	#2,fact
	subq.b	#1,speed1
          DEC speed1|
          play_sample(2)
	move.b	zeit(pc),t1
        ENDIF
      ENDIF
    ENDIF
    '
  ELSE
    '
    IF key|=su1|
      IF t1|=0
        IF speed1|>0
	move.b	#2,fact1
	subq.b	#1,speed1
          play_sample(2)
	move.b	zeit(pc),t1
        ENDIF
      ENDIF
    ENDIF
  ENDIF
  '
  IF key2!=FALSE
    IF STRIG(1)=TRUE
      IF t2|=0
        IF speed2|>0
	move.b	#2,fact2
	subq.b	#1,speed2
          play_sample(2)
          move.b	zeit(pc),t2
        ENDIF
      ENDIF
    ENDIF
    '
  ELSE
    IF key|=su2|
      IF t2|=0
        IF speed2|>0
        move.b	#2,fact2
        subq.b	#1,speed2
          play_sample(2)
          move.b	zeit(pc),t2
        ENDIF
      ENDIF
    ENDIF
  ENDIF
  '
	tst.b	t1
	bne.s	.weiter_t1
	move.b	#1,fact1
.weiter_t1
	tst.b	t2
	bne.s	.weiter_t2
	move.b	#1,fact2
.weiter_t2
	rts

calc_koord
  '
  SELECT fact1|
  CASE 1
	add	neux1(pc),x1
	add	neuy1(pc),y1
  CASE 2
	add	neux1(pc),x1
	add	neux1(pc),x1
	add	neuy1(pc),y1
	add	neuy1(pc),y1
  ENDSELECT
  '
  SELECT fact2|
  CASE 1
	add	neux2(pc),x2
	add	neuy2(pc),y2
  CASE 2
	add	neux2(pc),x2
	add	neux2(pc),x2
	add	neuy2(pc),y2
	add	neuy2(pc),y2
  ENDSELECT
	rts

test_bonus
	clr.b	bonus_hit1
	clr.b	bonus_hit2
  '
  IF bonus1!=TRUE
    IF (x1&>=bonusx1&) AND (x1&<bonusx1&+bonus_breite1|) AND (y1&<=bonusy1&) AND (y1&>bonusy1&-bonus_hoehe1|)
	move.b	#1,bonus_hit1
	add.l	#500,score1
    ENDIF
    IF (x2&>=bonusx1&) AND (x2&<bonusx1&+bonus_breite1|) AND (y2&<=bonusy1&) AND (y2&>bonusy1&-bonus_hoehe1|)
	move.b	#1,bnonus_hit2
	add.l	#500,score2
    ENDIF
  ENDIF
  '
  IF bonus2!=TRUE
    IF (x1&>=bonusx2&) AND (x1&<bonusx2&+bonus_breite2|) AND (y1&<=bonusy2&) AND (y1&>bonusy2&-bonus_hoehe2|)
	move.b	#2,bonus_hit1
	add.l	#500,score1
    ENDIF
    IF (x2&>=bonusx2&) AND (x2&<bonusx2&+bonus_breite2|) AND (y2&<=bonusy2&) AND (y2&>bonusy2&-bonus_hoehe2|)
	add.l	#500,score2
	move.b	#2,bonus_hit2
    ENDIF
  ENDIF
	rts

kollision
  '
  SELECT bonus_hit1|
  CASE 1
    play_sample(3)
	bset	#bonus1,flag
    TEXT bonusx1&,bonusy1&," "
  CASE 2
    play_sample(3)
	bset	#bonus2,flag
    TEXT bonusx2&,bonusy2&," "
  DEFAULT
    IF POINT(x1&,y1&)=1
      play_sample(1)
	add.l	zaehler(pc),score2
      crash(spieler1$)
    ENDIF
  ENDSELECT
  '
  SELECT bonus_hit2|
  CASE 1
    play_sample(3)
	bset	#bonus1,flag
    TEXT bonusx1&,bonusy1&," "
  CASE 2
    play_sample(3)
	bset	#bonus2,flag
    TEXT bonusx2&,bonusy2&," "
  DEFAULT
    IF POINT(x2&,y2&)=1
      play_sample(1)
      add.l	zaehler(pc),score1
      crash(spieler2$)
    ENDIF
  ENDSELECT
	rts

crash(spieler$)
  LOCAL dummy|,llength&
  '
  IF y1&<40 OR y2&<40
    IF y1&>360 OR y2&>360
      ' Mitte
	move	#200-9,leistenhoehe
    ENDIF
    ' Unten
	move	#400-18,leistenhoehe
  ELSE
    ' OBEN
	clr	leistenhoehe
  ENDIF
	bsr	scoreleiste
  DEFTEXT 1,0,0,4,13
  ' Crashtext mittig  in der Leiste ausgegeben
  dummy|=LEN("Crash: "+spieler$)*3
  TEXT 340-dummy|,leistenhoehe&+11,"Crash: "+spieler$
  '
  TEXT 6,leistenhoehe&+11,spieler1$+": "+STR$(score1%)
  llength&=LEN(spieler1$+": "+STR$(score1%))+1
  LINE 4+llength&*6,leistenhoehe&,4+llength&*6,leistenhoehe&+17
  LINE 5+llength&*6,leistenhoehe&,5+llength&*6,leistenhoehe&+17
  '
  llength&=LEN(spieler2$+": "+STR$(score2%))+1
  LINE 637-llength&*6,leistenhoehe&,637-llength&*6,leistenhoehe&+17
  LINE 636-llength&*6,leistenhoehe&,636-llength&*6,leistenhoehe&+17
  TEXT 641-llength&*6,leistenhoehe&+11,spieler2$+": "+STR$(score2%)
  '
  DEFTEXT 1,0,0,13,13
  '
	subq	#1,level
	bclr	#abbruch,flag
  DELAY 2
  '
	bsr	key
  SELECT taste|
  CASE 16
	bclr	#spielende,flag
  DEFAULT
	bset	#spielende,flag
  ENDSELECT
	rts

ende
  LOCAL dummy|
  '
	move.l	file_addresse(pc),a0
	Mfree	(a0)
  FOR dummy|=0 TO max_samples|
    IF sample%(dummy|,0)>0
      ~MFREE(sample%(dummy|,0))
    ENDIF
  NEXT dummy|
  '
	bsr	klick_an
	moveq	#0,d0
	bsr	maus
	graf_mouse	#2,#0
	rsrc_free
	close_gem
	Pterm0
	illegal
;********************************
rsrc_load
	rsrc_load	#rsrc_name
	tst	int_out
	beq.s	.fehler
	rsrc_gaddr	#R_TREE,#MENU
	move.l	addr_out(pc),menu_adr
	rts
.fehler
	form_alert	#1,#rsrc_not_found_str
	bra.s	ende
;********************************
key
	clr.b	taste
	clr.b	maustaste
  REPEAT
    taste|=@get_scan
    maustaste|=MOUSEK
  UNTIL taste|<>0 OR maustaste|=1 OR maustaste|=2
	rts
;********************************
hiscore_load
  LOCAL i|,scoreend$
  '
  scoreend$=tron0815$+".SCR"
  IF EXIST(scoreend$)=FALSE
    OPEN "O",#1,scoreend$
    FOR i|=0 TO 9
      PRINT #1,"Nobody"
      PRINT #1,0
    NEXT i|
    CLOSE #1
  ENDIF
  OPEN "I",#1,scoreend$
  FOR i|=0 TO 9
    INPUT #1,score$(i|)
    INPUT #1,score%(i|)
  NEXT i|
  CLOSE #1
	rts
;********************************
hiscore_save
  LOCAL i|,scoreend$
  '
  scoreend$=tron0815$+".SCR"
  OPEN "O",#1,scoreend$
  FOR i|=0 TO 9
    PRINT #1,score$(i|)
    PRINT #1,score%(i|)
  NEXT i|
  CLOSE #1
	rts
;********************************
hiscore_update
  LOCAL score1$,score2$,scorex$,i|,dummy$
  '
  score1$=STR$(score1%)
  WHILE LEN(score1$)<6
    score1$="0"+score1$
  WEND
  '
  score2$=STR$(score2%)
  WHILE LEN(score2$)<6
    score2$="0"+score2$
  WEND
  '
  FOR i|=0 TO 9
    scorex$=STR$(score%(i|))
    WHILE LEN(scorex$)<6
      scorex$="0"+scorex$
    WEND
    dummy$(i|)=scorex$+score$(i|)
  NEXT i|
  dummy$(10)=score1$+spieler1$
  dummy$(11)=score2$+spieler2$
  '
  QSORT dummy$(-)
  '
	bsr	clr_scn
  FOR i|=0 TO 9
    score%(i|)=VAL(LEFT$(dummy$(i|),6))
    score$(i|)=MID$(dummy$(i|),7)
  NEXT i|
  '
	bsr	hiscore_save
	rts
;********************************
play_sample(sample_nummer|)
  LOCAL start%,laenge%
  '
	subq.b	#1,sample_nummer
  start%=sample%(sample_nummer|,0)
  laenge%=sample%(sample_nummer|,1)
  IF start%>0 AND laenge%>0 AND tonswitch!=TRUE
	bsr	stop_play
    CALL play%(start%,laenge%)
  ENDIF
	rts
;********************************
play_sample_lang(sample_nummer|)
  LOCAL start%,laenge%
  '
	subq.b	#1,sample_nummer
  start%=sample%(sample_nummer|,0)
  laenge%=sample%(sample_nummer|,1)
  IF start%>0 AND laenge%>0 AND tonswitch!=TRUE
	bsr	stop_play
    CALL play_immer%(start%,laenge%)
  ENDIF
	rts
;********************************
stop_play
	Jdisint	#13
	rts
;********************************
klick_aus
	Supexec	.super
	rts
  SPOKE &H484,(PEEK(&H484) AND 12)
.super
	move.b	$484.w,d0
	and.b	#12,d0
	move.b	d0,$484.w
	rts
;********************************
klick_an
	Supexec	.super
	rts
.super
	move.b	klick(pc),$484.w
	rts
;********************************
warten
	move.l	d0,-(sp)
	move	level(pc),d0
	mulu	#10,d0
	subq	#1,d0
.loop	dbf	d0,.loop
	move.l	(sp)+,d0
	rts
;********************************
speed_zeit
	addq.l	#1,zaehler
	tst.b	t1
	bls.s	.weiter_t1
	subq.b	#1,t1
.weiter_t1
	tst.b	t2
	bls.s	.weiter_t2
	subq.b	#1,t2
.weiter_t1
	rts
;********************************
maus(switch!)
  LOCAL m$,i|
  '
  SELECT switch!
  CASE -1
    REM x-koordinate|y-koordinate|modus(1=normal -1=XOR)|maskenfarbe|cursorfarbe
    REM bitmuster der maske
    REM bitmuster des cursors
    '
    m$=MKI$(0)+MKI$(0)+MKI$(1)+MKI$(0)+MKI$(1)
    FOR i|=1 TO 16
      m$=m$+MKI$(0)
    NEXT i|
    FOR i|=1 TO 16
      m$=m$+MKI$(0)
    NEXT i|
    DEFMOUSE m$
    CALL maus_aus%
  DEFAULT
    DEFMOUSE 0
    CALL maus_an%
    SHOWM
  ENDSELECT
	rts
;********************************
scoreleiste
  LOCAL i|
  '
  COLOR 0
  FOR i|=2 TO 16
    LINE 2,leistenhoehe&+i,637,leistenhoehe&+i
  NEXT i|
  PBOX 0,leistenhoehe&,639,leistenhoehe&+18
  GRAPHMODE 3
  PBOX 0,leistenhoehe&,639,leistenhoehe&+18
  GRAPHMODE 1
  COLOR 1
  BOX 0,leistenhoehe&,639,leistenhoehe&+18
  BOX 1,leistenhoehe&+1,638,leistenhoehe&+17
	rts
;********************************
init_menu
  LOCAL a$,a%,dummy|
  '
  MENU KILL
resto.tron.re:
  RESTORE resto.tron.re
  '
  ERASE med.tron$()
  DIM med.tron$(25)
  DATA TRON 08/15 , <i> �ber TRON 08/15... ,-- Keine ACCs. Sorry! --, ACC1, ACC2, ACC3
  DATA  ACC4, ACC5, ACC6,""
  DATA File , <b> Bestenliste ,-----------------, <s> Start, <q> Ende,""
  DATA Optionen , <1> Spieler 1 , <2> Spieler 2, <l> Level,------------------,  <d> Darstellung
  DATA   <g> Bilder,  <t> Sounds,""
  DATA "",***
  '
  '
  a%=0
  DO
    READ a$
    IF a$="  Spieler 1 "
      a$=SPACE$(2)+spieler1$
    ENDIF
    IF a$="  Spieler 2"
      a$=SPACE$(2)+spieler2$
    ENDIF
    EXIT IF a$="***"
    med.tron$(a%)=a$
    INC a%
  LOOP
  '
  '
  MENU med.tron$()
  '
  FOR dummy|=3 TO 9
    MENU dummy|,2
  NEXT dummy|
  '
  IF bildswitch!=TRUE
    MENU 22,1
  ELSE
    MENU 22,0
  ENDIF
  '
  IF tonswitch!=TRUE
    MENU 23,1
  ELSE
    MENU 23,0
  ENDIF
	rts
;********************************
short_cut
  LOCAL dummy%,dummy|
  '
  dummy%=MENU(14)
  dummy|=BYTE(dummy%)
  '
  SELECT dummy|
  CASE "q","Q"
    tron_ende
  CASE "s","S"
    start
  CASE "b","B"
    bestenliste
  CASE "l","L"
    level
  CASE "d","D"
    darstellung
  CASE "1"
    spieler1
  CASE "2"
    spieler2
  CASE "t","T"
    SOUND s
  CASE "g","G"
    bilder
  CASE "i","I"
	bsr	about_tron
  ENDSELECT
	rts
;********************************
call_menu
  LOCAL a%
  '
  MENU OFF
  a%=MENU(0)
  ON a%-0 GOSUB about_tron,warum_keine_acc,empty,empty,empty,empty
  ON a%-6 GOSUB empty,empty
  ON a%-10 GOSUB bestenliste,empty,start,tron_ende
  ON a%-16 GOSUB spieler1,spieler2,level
  ON a%-19 GOSUB empty,darstellung,bilder,sounds
	rts
;********************************
about_tron
  LOCAL magnum$,dok$,equalizer$,draku$,dummy%,dummy|
  LOCAL x1&,y1&,x2&,y2&,i|
	moveq	#-1,d0
	bsr	maus
  '
  play_sample_lang(4)
  y1&=50
  x2&=590
  y2&=350
  '
  FOR x1&=50 TO 200 STEP 1       ! Aufbau
    '
    REM Warteschleife
    FOR i|=1 TO 20
    NEXT i|
    '
    INC y1&
    DEC y2&
    DEC x2&
    BOX x1&,y1&,x2&,y2&
  NEXT x1&
  '
  COLOR 0                       ! Farbwexel
  '
  FOR x1&=200 TO 60 STEP -1      ! Abbau
    FOR i|=1 TO 20
    NEXT i|
    DEC y1&
    INC y2&
    INC x2&
    BOX x1&,y1&,x2&,y2&
  NEXT x1&
  '
  COLOR 1
  '
  '
  PUT 70,70,panda$
  DEFTEXT 1,16,0,32,13
  TEXT 220,150,"TRON 08/15"
  DEFTEXT 1,1,0,32,13
  TEXT 110,190,"Entwickelt von BlackBox"
  DEFTEXT 1,0,0,6,13
  TEXT 225,230," HELP --> Information"
  DEFTEXT 1,0,0,4,13
  dummy%=300
  TEXT 80,dummy%,magnum$
  TEXT 80,dummy%+10,dok$
  TEXT 80,dummy%+20,equalizer$
  TEXT 80,dummy%+30,draku$
	moveq	#0,d0
	bsr	maus
  DEFTEXT 1,0,0,13,13
  GRAPHMODE 1
  '
	bsr	key
  IF taste|=98
	bsr	more_info
  ENDIF
	bsr	stop_play
	bsr	clr_scn
	bsr	init_menu
	rts
;********************************
more_info
	bsr	clr_scn
  PRINT "TRON 08/15 wurde mit Gfa-Basic 3.5 und Assmebler (Devpac 2.0) entwickelt."
  PRINT "Die Vollversion umfa�t etwa 50 kB Code, 340 kB Grafik und 50 kB digitalisierte"
  PRINT "Sounds. Die Entwicklung dauerte vom 21.12.1990 bis zum"'DATE$'TIME$'"Uhr."
  PRINT "Dies ist eine Demoversion und ausdr�cklich Public-Domain!!!"
  PRINT "Wer die Vollversion des Programmes mit allen Features sein Eigen nennen"
  PRINT "m�chte, kann diese bei den Programmierern bestellen."
  PRINT "Einen Haken hat die Sache nat�rlich: Die Vollversion kostet 20.- DM."
  PRINT "Daf�r �bernehmen wir aber Diskette, Porto und Verpackung. Na, das ist"
  PRINT "doch auch was! Au�erdem wird jeder Kunde in Kenntnis gesetzt, wenn sich"
  PRINT "ein neues Produkt von BlackBox am Horizont abzeichnet. Soll hei�en:"
  PRINT "Man bekommt Nachricht was es ist, wann es erscheint und zu welchem Preis"
  PRINT "es zu erwerben ist."
  PRINT "Leute, die bereits Kunden sind, bekommen dann einen kleinen Rabatt,"
  PRINT "der aber noch festzulegen ist. Das h�ngt n�mlich alles von der Resonanz"
  PRINT "der Interessenten ab. Und solch einer bist auch DU!"
  PRINT "F�r Anregungen, Fehlermeldungen, einen Small-talk usw. sind wir 4"
  PRINT "fast jeder Zeit aufgeschlossen, sofern es entweder per Brief oder, wenn"
  PRINT "per Telefon, dann so etwa zwischen 12.00 und 22.00 Uhr ist."
  PRINT "So. Das war's dann auch schon. Bis dann!!!"
  PRINT "                                Das BlackBox-Team"
  PRINT "Bitte eine Taste oder Mausknopf dr�cken!"
	bsr	key
	rts
;********************************
bestenliste
  LOCAL i|,x|
  '
  DEFLINE 1,5,2
  PUT 185,30,score_pic$
  '
  FOR i|=1 TO 10
    '
    x|=33        ! X-Koordinate
    IF i|=10
      SUB x|,1
    ENDIF
    PRINT AT(x|,8+i|);i|
    PRINT AT(36,8+i|);score$(i|-1)
    PRINT AT(47,8+i|);score%(i|-1)
  NEXT i|
  '
  DEFLINE 1,1,2
  '
	bsr	key
	bsr	clr_scn
	bsr	init_menu
	rts
;********************************
tron_ende
	form_alert	#1,ende_str
	cmp	#1,int_out
	bne.s	.weiter
	bsr	hiscore_update
	bra	ende
.weiter
	bsr	init_menu
	rts
;********************************
spieler1
  LOCAL dummy|
  '
  DEFLINE 1,5,2
  BOX 150,180,490,220
  DEFLINE 1,1,2
  TEXT 265,206,"Name: "
  PRINT AT(40,13);
  FORM INPUT 9 AS spieler1$
  TEXT 170,206,"Spielmedium >J<oystick oder >T<astatur"
  KEYGET dummy|
  IF CHR$(dummy|)="T" OR CHR$(dummy|)="t"
	bsr	clr_scn
	bsr	init_menu
    '
    REM Hier kommt die Tastaturbelegung
    '
    DEFLINE 1,5,2
    BOX 200,55,440,105
    '
    PRINT AT(28,5);"Tastaturbelegung "
    PRINT AT(54-LEN(spieler1$),5);spieler1$
    PRINT AT(30,6);"Voreingestellte Werte:"
    '
    BOX 200,115,440,300
    '
    scan_codes(o1|)
    PRINT AT(27,9);"Oben:",text$
    scan_codes(u1|)
    PRINT AT(27,10);"Unten:",text$
    scan_codes(l1|)
    PRINT AT(27,11);"Links:",text$
    scan_codes(r1|)
    PRINT AT(27,12);"Rechts:",text$
    scan_codes(lo1|)
    PRINT AT(27,13);"Links oben:",text$
    scan_codes(ro1|)
    PRINT AT(27,14);"Rechts oben:",text$
    scan_codes(lu1|)
    PRINT AT(27,15);"Links unten:",text$
    scan_codes(ru1|)
    PRINT AT(27,16);"Rechts unten:",text$
    scan_codes(su1|)
    PRINT AT(27,18);"Speed up:",text$
    '
    BOX 200,310,440,345
    DEFLINE 1,1,2
    '
	bclr	#key1,flag
    PRINT AT(32,21);"Einstellung �ndern?"
    '
    KEYGET dummy|
    IF CHR$(dummy|)="j" OR CHR$(dummy|)="J"
      '
      PRINT AT(27,6);SPC(28)
      PRINT AT(41,9);SPC(14)
      PRINT AT(41,10);SPC(14)
      PRINT AT(41,11);SPC(14)
      PRINT AT(41,12);SPC(14)
      PRINT AT(41,13);SPC(14)
      PRINT AT(41,14);SPC(14)
      PRINT AT(41,15);SPC(14)
      PRINT AT(41,16);SPC(14)
      PRINT AT(41,18);SPC(14)
      PRINT AT(27,21);SPC(28)
      '
      PRINT AT(38,6);"AUSWAHL"
      PRINT AT(27,9);"Oben:",
      o1|=@get_scan
      scan_codes(o1|)
      PRINT text$
      PRINT AT(27,10);"Unten:",
      u1|=@get_scan
      scan_codes(u1|)
      PRINT text$
      PRINT AT(27,11);"Links:",
      l1|=@get_scan
      scan_codes(l1|)
      PRINT text$
      PRINT AT(27,12);"Rechts:",
      r1|=@get_scan
      scan_codes(r1|)
      PRINT text$
      PRINT AT(27,13);"Links oben:",
      lo1|=@get_scan
      scan_codes(lo1|)
      PRINT text$
      PRINT AT(27,14);"Rechts oben:",
      ro1|=@get_scan
      scan_codes(ro1|)
      PRINT text$
      PRINT AT(27,15);"Links unten:",
      lu1|=@get_scan
      scan_codes(lu1|)
      PRINT text$
      PRINT AT(27,16);"Rechts unten:",
      ru1|=@get_scan
      scan_codes(ru1|)
      PRINT text$
      PRINT AT(27,18);"Speed up:",
      su1|=@get_scan
      scan_codes(su1|)
      PRINT text$
      DELAY 1
    ENDIF
  ELSE
	bset	#key1,flag
  ENDIF
  '
	bsr	clr_scn
	bsr	init_menu
	rts
;********************************
spieler2
  LOCAL dummy|
  '
  DEFLINE 1,5,2
  BOX 150,180,490,220
  DEFLINE 1,1,2
  TEXT 265,206,"Name: "
  PRINT AT(40,13);
  FORM INPUT 9 AS spieler2$
  TEXT 170,206,"Spielmedium >J<oystick oder >T<astatur"
  KEYGET dummy|
  IF CHR$(dummy|)="T" OR CHR$(dummy|)="t"
	bsr	clr_scn
	bsr	init_menu
    '
    REM Hier kommt die Tastaturbelegung
    '
    DEFLINE 1,5,2
    BOX 200,55,440,105
    '
    PRINT AT(28,5);"Tastaturbelegung "
    PRINT AT(54-LEN(spieler2$),5);spieler2$
    PRINT AT(30,6);"Voreingestellte Werte:"
    '
    BOX 200,115,440,300
    '
    scan_codes(o2|)
    PRINT AT(27,9);"Oben:",text$
    scan_codes(u2|)
    PRINT AT(27,10);"Unten:",text$
    scan_codes(l2|)
    PRINT AT(27,11);"Links:",text$
    scan_codes(r2|)
    PRINT AT(27,12);"Rechts:",text$
    scan_codes(lo2|)
    PRINT AT(27,13);"Links oben:",text$
    scan_codes(ro2|)
    PRINT AT(27,14);"Rechts oben:",text$
    scan_codes(lu2|)
    PRINT AT(27,15);"Links unten:",text$
    scan_codes(ru2|)
    PRINT AT(27,16);"Rechts unten:",text$
    scan_codes(su2|)
    PRINT AT(27,18);"Speed up:",text$
    '
    BOX 200,310,440,345
    DEFLINE 1,1,2
    '
	bclr	#key2,flag
    PRINT AT(32,21);"Einstellung �ndern?"
    '
    KEYGET dummy|
    IF CHR$(dummy|)="j" OR CHR$(dummy|)="J"
      '
      PRINT AT(27,6);SPC(28)
      PRINT AT(41,9);SPC(14)
      PRINT AT(41,10);SPC(14)
      PRINT AT(41,11);SPC(14)
      PRINT AT(41,12);SPC(14)
      PRINT AT(41,13);SPC(14)
      PRINT AT(41,14);SPC(14)
      PRINT AT(41,15);SPC(14)
      PRINT AT(41,16);SPC(14)
      PRINT AT(41,18);SPC(14)
      PRINT AT(27,21);SPC(28)
      '
      PRINT AT(38,6);"AUSWAHL"
      PRINT AT(27,9);"Oben:",
      o2|=@get_scan
      scan_codes(o2|)
      PRINT text$
      PRINT AT(27,10);"Unten:",
      u2|=@get_scan
      scan_codes(u2|)
      PRINT text$
      PRINT AT(27,11);"Links:",
      l2|=@get_scan
      scan_codes(l2|)
      PRINT text$
      PRINT AT(27,12);"Rechts:",
      r2|=@get_scan
      scan_codes(r2|)
      PRINT text$
      PRINT AT(27,13);"Links oben:",
      lo2|=@get_scan
      scan_codes(lo2|)
      PRINT text$
      PRINT AT(27,14);"Rechts oben:",
      ro2|=@get_scan
      scan_codes(ro2|)
      PRINT text$
      PRINT AT(27,15);"Links unten:",
      lu2|=@get_scan
      scan_codes(lu2|)
      PRINT text$
      PRINT AT(27,16);"Rechts unten:",
      ru2|=@get_scan
      scan_codes(ru2|)
      PRINT text$
      PRINT AT(27,18);"Speed up:",
      su2|=@get_scan
      scan_codes(su2|)
      PRINT text$
      DELAY 1
    ENDIF
  ELSE
	bset	#key2,flag
  ENDIF

	bsr	clr_scn
	bsr	init_menu
	rts
;********************************
level
  LOCAL mx&,my&,dummy|
  '
  level_x&=220
  level_y&=100
  DEFFILL 2,2,4
  '
	bsr	vorarbeit
	bsr	blo
	bsr	blu
	bsr	bro
	bsr	bru
  '
	bsr	print_level
  '
  REPEAT
    MOUSE mx&,my&,dummy|
    '
    IF mx&>level_x&+10 AND mx&<level_x&+90 AND MOUSEK=1
      IF my&>level_y&+120 AND my&<level_y&+145 AND MOUSEK=1
        not	color
        bsr	blo
        REPEAT
          IF level&>0
            DEC level&
	bsr	print_level
            DELAY 0.2
          ENDIF
        UNTIL MOUSEK=0
	not	color
	bsr	blo
      ENDIF
    ENDIF
    '
    IF mx&>level_x&+10 AND mx&<level_x&+90 AND MOUSEK=1
      IF my&>level_y&+155 AND my&<level_y&+180 AND MOUSEK=1
        color&=NOT color&
        blu
        REPEAT
          IF level&>0
            DEC level&
            print_level
            DELAY 0.05
          ENDIF
        UNTIL MOUSEK=0
        color&=NOT color&
        blu
      ENDIF
    ENDIF
    '
    IF mx&>level_x&+110 AND mx&<level_x&+190 AND MOUSEK=1
      IF my&>level_y&+120 AND my&<level_y&+145 AND MOUSEK=1
        color&=NOT color&
        bro
        REPEAT
          IF level&<99
            INC level&
            print_level
          ENDIF
          DELAY 0.2
        UNTIL MOUSEK=0
        color&=NOT color&
        bro
      ENDIF
    ENDIF
    '
    IF mx&>level_x&+110 AND mx&<level_x&+190 AND MOUSEK=1
      IF my&>level_y&+155 AND my<level_y&+180 AND MOUSEK=1
        color&=NOT color&
        bru
        REPEAT
          IF level&<99
            INC level&
            print_level
          ENDIF
          DELAY 0.05
        UNTIL MOUSEK=0
        color&=NOT color&
        bru
      ENDIF
    ENDIF
  UNTIL MOUSEK=2
  '
	bsr	clr_scn
	bsr	init_menu
	rts
;********************************
print_level
  IF level&>9
    TEXT level_x&+92,level_y&+85,level&
  ELSE
    TEXT level_x&+92,level_y&+85," "+STR$(level&)
  ENDIF
	rts
;********************************
blo
  REM Linien BLO
  PUT level_x&+11-color&,level_y&+121-color&,lslow$
  COLOR color&
  DRAW level_x&+11,level_y&+144 TO level_x&+11,level_y&+121 TO level_x&+89,level_y&+121
  DRAW level_x&+12,level_y&+143 TO level_x&+12,level_y&+122 TO level_x&+88,level_y&+122
  COLOR NOT color&
  DRAW level_x&+11,level_y&+144 TO level_x&+89,level_y&+144 TO level_x&+89,level_y&+122
  DRAW level_x&+12,level_y&+143 TO level_x&+88,level_y&+143 TO level_x&+88,level_y&+123
	rts
;********************************
blu
  REM Linien BLU
  PUT level_x&+11-color&,level_y&+156-color&,lfast$
  COLOR color&
  DRAW level_x&+11,level_y&+179 TO level_x&+11,level_y&+156 TO level_x&+89,level_y&+156
  DRAW level_x&+12,level_y&+178 TO level_x&+12,level_y&+157 TO level_x&+88,level_y&+157
  COLOR NOT color&
  DRAW level_x&+11,level_y&+179 TO level_x&+89,level_y&+179 TO level_x&+89,level_y&+157
  DRAW level_x&+12,level_y&+178 TO level_x&+88,level_y&+178 TO level_x&+88,level_y&+158
	rts
;********************************
bro
  REM Linien BRO
  PUT level_x&+111-color&,level_y&+121-color&,rslow$
  COLOR color&
  DRAW level_x&+111,level_y&+144 TO level_x&+111,level_y&+121 TO level_x&+189,level_y&+121
  DRAW level_x&+112,level_y&+143 TO level_x&+112,level_y&+122 TO level_x&+188,level_y&+122
  COLOR NOT color&
  DRAW level_x&+111,level_y&+144 TO level_x&+189,level_y&+144 TO level_x&+189,level_y&+122
  DRAW level_x&+112,level_y&+143 TO level_x&+188,level_y&+143 TO level_x&+188,level_y&+123
	rts
;********************************
bru
  REM Linien BRU
  PUT level_x&+111-color&,level_y&+156-color&,rfast$
  COLOR color&
  DRAW level_x&+111,level_y&+179 TO level_x&+111,level_y&+156 TO level_x&+189,level_y&+156
  DRAW level_x&+112,level_y&+178 TO level_x&+112,level_y&+157 TO level_x&+188,level_y&+157
  COLOR NOT color&
  DRAW level_x&+111,level_y&+179 TO level_x&+189,level_y&+179 TO level_x&+189,level_y&+157
  DRAW level_x&+112,level_y&+178 TO level_x&+188,level_y&+178 TO level_x&+188,level_y&+158
	rts
;********************************
vorarbeit
  REM BigBox
  PBOX level_x&,level_y&,level_x&+200,level_y&+200
  PBOX level_x&+1,level_y&+1,level_x&+199,level_y&+199
  COLOR 1
  DRAW level_x&+1,level_y&+199 TO level_x&+199,level_y&+199 TO level_x&+199,level_y&+2
  DRAW level_x&+2,level_y&+198 TO level_x&+198,level_y&+198 TO level_x&+198,level_y&+2
  COLOR 0
  DRAW level_x&+1,level_y&+199 TO level_x&+1,level_y&+1 TO level_x&+199,level_y&+1
  DRAW level_x&+2,level_y&+198 TO level_x&+2,level_y&+2 TO level_x&+198,level_y&+2
  COLOR 1
  '
  REM Levelbox
  BOX level_x&+50,level_y&+30,level_x&+150,level_y&+100
  DRAW level_x&+51,level_y&+99 TO level_x&+51,level_y&+31 TO level_x&+149,level_y&+31
  DRAW level_x&+52,level_y&+98 TO level_x&+52,level_y&+32 TO level_x&+148,level_y&+32
  COLOR 0
  DRAW level_x&+51,level_y&+99 TO level_x&+149,level_y&+99 TO level_x&+149,level_y&+31
  DRAW level_x&+52,level_y&+98 TO level_x&+148,level_y&+98 TO level_x&+148,level_y&+32
  COLOR 1
  PUT level_x&+72,level_y&+35,levelpic$
  '
  REM Box links oben
  BOX level_x&+10,level_y&+120,level_x&+90,level_y&+145
  REM Box links unten
  BOX level_x&+10,level_y&+155,level_x&+90,level_y&+180
  '
  REM Box rechts oben
  BOX level_x&+110,level_y&+120,level_x&+190,level_y&+145
  REM Box rechts unten
  BOX level_x&+110,level_y&+155,level_x&+190,level_y&+180
	rts
;********************************
darstellung
	Supexec	.super(pc)
	bsr	init_menu
	rts
.super
	not	$ffff8240.w
	rts
;********************************
bilder
	bchg	#bildswitch,flag
	bsr	init_menu
	rts
;********************************
sounds
	bchg	#tonswitch,flag
	bsr	init_menu
	rts
;********************************
> PROCEDURE scan_codes(code|)
  LOCAL zb$,shift$,crsr$
  zb$="ZB "
  shift$="SHIFT "
  crsr$="CRSR "
  '
  SELECT code|
  CASE 1
    text$="ESC"
  CASE 2
    text$="1"
  CASE 3
    text$="2"
  CASE 4
    text$="3"
  CASE 5
    text$="4"
  CASE 6
    text$="5"
  CASE 7
    text$="6"
  CASE 8
    text$="7"
  CASE 9
    text$="8"
  CASE 10
    text$="9"
  CASE 11
    text$="0"
  CASE 12
    text$="�"
  CASE 13
    text$="'"
  CASE 14
    text$="BACKSPACE"
  CASE 15
    text$="TAB"
  CASE 16
    text$="Q"
  CASE 17
    text$="W"
  CASE 18
    text$="E"
  CASE 19
    text$="R"
  CASE 20
    text$="T"
  CASE 21
    text$="Z"
  CASE 22
    text$="U"
  CASE 23
    text$="I"
  CASE 24
    text$="O"
  CASE 25
    text$="P"
  CASE 26
    text$="�"
  CASE 27
    text$="+"
  CASE 28
    text$="RETURN"
  CASE 29
    text$="CONTROL"
  CASE 30
    text$="A"
  CASE 31
    text$="S"
  CASE 32
    text$="D"
  CASE 33
    text$="F"
  CASE 34
    text$="G"
  CASE 35
    text$="H"
  CASE 36
    text$="J"
  CASE 37
    text$="K"
  CASE 38
    text$="L"
  CASE 39
    text$="�"
  CASE 40
    text$="�"
  CASE 41
    text$="#"
  CASE 42
    text$=shift$+"LINKS"
  CASE 43
    text$="~"
  CASE 44
    text$="Y"
  CASE 45
    text$="X"
  CASE 46
    text$="C"
  CASE 47
    text$="V"
  CASE 48
    text$="B"
  CASE 49
    text$="N"
  CASE 50
    text$="M"
  CASE 51
    text$=","
  CASE 52
    text$="."
  CASE 53
    text$="-"
  CASE 54
    text$=shift$+"RECHTS"
  CASE 56
    text$="ALT"
  CASE 57
    text$="SPACE"
  CASE 58
    text$="CAPS LOCK"
  CASE 59
    text$="F1"
  CASE 60
    text$="F2"
  CASE 61
    text$="F3"
  CASE 62
    text$="F4"
  CASE 63
    text$="F5"
  CASE 64
    text$="F6"
  CASE 65
    text$="F7"
  CASE 66
    text$="F8"
  CASE 67
    text$="F9"
  CASE 68
    text$="F10"
  CASE 71
    text$="CLEAR/HOME"
  CASE 72
    text$=crsr$+"RAUF"
  CASE 74
    text$=zb$+"-"
  CASE 75
    text$=crsr$+"LINKS"
  CASE 77
    text$=crsr$+"RECHTS"
  CASE 78
    text$=zb$+"+"
  CASE 80
    text$=crsr$+"RUNTER"
  CASE 82
    text$="INSERT"
  CASE 83
    text$="DELETE"
  CASE 84
    text$="SHFT F1"
  CASE 85
    text$="SHFT F2"
  CASE 86
    text$="SHFT F3"
  CASE 87
    text$="SHFT F4"
  CASE 88
    text$="SHFT F5"
  CASE 89
    text$="SHFT F6"
  CASE 90
    text$="SHFT F7"
  CASE 91
    text$="SHFT F8"
  CASE 92
    text$="SHFT F9"
  CASE 93
    text$="SHFT F10"
  CASE 96
    text$="<"
  CASE 97
    text$="UNDO"
  CASE 98
    text$="HELP"
  CASE 99
    text$=zb$+"("
  CASE 100
    text$=zb$+")"
  CASE 101
    text$=zb$+"/"
  CASE 102
    text$=zb$+"*"
  CASE 103
    text$=zb$+"7"
  CASE 104
    text$=zb$+"8"
  CASE 105
    text$=zb$+"9"
  CASE 106
    text$=zb$+"4"
  CASE 107
    text$=zb$+"5"
  CASE 108
    text$=zb$+"6"
  CASE 109
    text$=zb$+"1"
  CASE 110
    text$=zb$+"2"
  CASE 111
    text$=zb$+"3"
  CASE 112
    text$=zb$+"0"
  CASE 113
    text$=zb$+"."
  CASE 114
    text$=zb$+"ENTER"
  CASE 120
    text$="ALT 1"
  CASE 121
    text$="ALT 2"
  CASE 122
    text$="ALT 3"
  CASE 123
    text$="ALT 4"
  CASE 124
    text$="ALT 5"
  CASE 125
    text$="ALT 6"
  CASE 126
    text$="ALT 7"
  CASE 127
    text$="ALT 8"
  CASE 128
    text$="ALT 9"
  CASE 129
    text$="ALT 0"
  CASE 130
    text$="ALT �"
  CASE 131
    text$="ALT '"
  DEFAULT
    text$=CHR$(code|)
  ENDSELECT
	rts
;********************************
get_scan
	Cconis
	tst	d0
	beq.s	.nix
	Cconin
	swap	d0
	and.l	#$ff,d0
	rts
.nix
	clr.l	d0
	rts
;********************************
clr_scn
	Cconws	clr_scn_str(pc)
	rts
;********************************
	include	call_gem
;********************************
;decrunchroutine meines crunchers MAG-CRUN V1.0

start_iff_decrunch
;a1=sourcebuffer;a2=objectbuffer
;d0=objectfile length
iff_decrunch_init
	moveq	#0,d1
	lea	header(pc),a3
	moveq	#2,d7
.loop
	cmpm.l	(a3)+,(a1)+
	bne.s	iff_de_end
	dbf	d7,.loop

	move.l	(a1)+,d0
start_iff_de
	tst.l	d0
	beq.s	iff_de_end
	bmi.s	iff_de_end
	moveq	#0,d1
	move.b	(a1)+,d1
	bpl.s	iff_de_eq
	not.b	d1
iff_de_uneq
	move.b	(a1)+,(a2)+
	subq.l	#1,d0
	dbf	d1,iff_de_uneq
	bra.s	start_iff_de

iff_de_eq
	subq.b	#1,d1
	move.b	(a1)+,d2
.loop2
	move.b	d2,(a2)+
	subq.l	#1,d0
	dbf	d1,.loop2
	bra.s	start_iff_de
iff_de_end
	rts
;********************************
	data
header	dc.b	'MAG-CRUNV1.0'
magnum	dc.b	"Thomas J�rges      Email: thomas.a.juerges@ruhr-uni-bochum.de",0
dok	dc.b	"Peter Klasen       Email: peter_klasen@kr.maus.de",0
equalizer	dc.b	"Martin Petermann",0
draku	dc.b	"Gisbert Siegmund",0
laeuft_nicht_str	dc.b	'[3][TRON 08/15|l�uft nur in der|640*400-Aufl�sung.][OK]',0
ton_an_str	dc.b	'[1][Bitte die Lautst�rke am|Monitor auf den maximalen|Wert einstellen!][Jau]',0
ende_str	dc.b	'[2][Wirklich Spielende ?][Ja|Nein]',0
rsrc_not_found_str	dc.b	'[2][Resource-Datei nicht gefunden!|Das Programm wird|abgebrochen.][OK]',0
clr_scn_str	dc.b	27,'E',0
tron	dc.b	'TRON'
name	dc.b	'____.___',0
tron0815	dc.b	'TRON0815'
ordnerpic	dc.b	'TRON0815.PIC',0
ordnersnd	dc.b	'TRON0815.SND',0
ordnerhoch	dc.b	'..',0
port1	dc.b	'Port 1'
port2	dc.b	'Port 2'
spieler1	dc.b	'Spieler 1'
spieler2	dc.b	'Spieler 2'
inaktiv	dc.b	'Inaktiv'
aktiv	dc.b	'Aktiv'
max_samples	dc.b	0
abstand	dc.b	40
zeit	dc.b	100
o1	dc.b	104
u1	dc.b	110
l1	dc.b	106
r1	dc.b	108
ro1	dc.b	105
lo1	dc.b	103
ru1	dc.b	111
lu1	dc.b	109
su1	dc.b	107
o2	dc.b	20
u2	dc.b	47
l2	dc.b	33
r2	dc.b	34
ro2	dc.b	21
lo2	dc.b	19
ru2	dc.b	48
lu2	dc.b	46
su2	dc.b	57
level	dc.w	50
	bss
screen	ds.l	1
file_length	ds.l	1
sample	ds.l	20
score	ds.l	10
zaehler	ds.l	1
aktivkey1	ds.l	1
aktivkey2	ds.l	1
score_str	ds.l	10*(256/4)
dummy	ds.l	10*(256/4)
menu	ds.w	1
flag	ds.b	1
anzahl	ds.b	1
t1	ds.b	1
t2	ds.b	1
