	include	tos_fnkt	;meine geliebten macros

;ein kleiner test, ob es m�glich ist, ein und denselben screen
;mehrmals darzustellen. Es ist auf anhieb gelungen (sehr selten!!!)
;ich bin ausserdem �berrascht, was die 'wackelquantit�t' angeht.
;vielleicht ein neues gimmick f�r eine winzige demo?

zeile	=	250
HBL	=	$68
VBL	=	$70
VIDEO_M	=	$ffff8203
VIDEO_H	=	$ffff8201
COLOR00	=	$ffff8240
SYNC	=	$ffff820a
IER_A	=	$fffffa07
IER_B	=	$fffffa09
IPR_A	=	$fffffa0b
IPR_B	=	$fffffa0d
IMR_A	=	$fffffa13
IMR_B	=	$fffffa15
ISR_A	=	$fffffa0f
ISR_B	=	$fffffa11
;*********************************************

	init	stack
begin
	Logbase
	move.l	d0,a6
	lsr.l	#8,d0
	move	d0,screen
	Supexec	super
	Pterm0

super
	move	#$2700,sr
	bsr.s	init_vbl
	bsr	init_hbl
	bsr	dis_int
	move	#$2000,sr

	move	(COLOR00).w,color00
	move	#$0,(COLOR00).w

	bsr.s	grafik
	Bconin

	move	color00,(COLOR00).w

	move	#$2700,sr
	bsr	enab_int
	bsr.s	deinit_vbl
	bsr.s	deinit_hbl
	move	#$2000,sr
	rts

;*****************************
;subroutinen
;*****************************
grafik
	move	#100,d7
plot_aussen
	move	#639,d6
plot_innen
	move	d6,d0
	move	d7,d1
	calc_coordinates
	do_plot
	dbf	d6,plot_innen
	dbf	d7,plot_aussen
	rts
init_vbl
	move.l	(VBL).w,old_vbl
	move.l	#new_vbl,(VBL).w
	rts
init_hbl
	move.l	(HBL).w,old_hbl
	move.l	#new_hbl,(HBL).w
	rts
deinit_vbl
	move.l	old_hbl,(HBL).w
	rts
deinit_hbl
	move.l	old_vbl,(VBL).w
	rts
dis_int
	move.b	(IMR_A).w,imra
	move.b	#%00000000,(IMR_A).w
	move.b	(IMR_B).w,imrb
	move.b	#%01000000,(IMR_B).w
	rts
enab_int
	move.b	imra,(IMR_A).w
	move.b	imrb,(IMR_B).w
	rts

;*******************************
;interrupts
;*******************************
new_vbl
	clr	hbl_counter
	move	#$2000,sr
	rte
new_hbl
	addq	#1,hbl_counter
	cmp	#zeile,hbl_counter
	beq.s	switch_line_counter
	rte
switch_line_counter
	move	#$2300,sr
	movem.l	d0/a0,-(sp)
	not	(COLOR00).w
	lea	(VIDEO_H).w,a0
	move	screen,d0
	movep.w	d0,(a0)
	move.b	#%11,(SYNC).w
	nop
	move.b	#%10,(SYNC).w
	movem.l	(sp)+,d0/a0
	rte

;*******************************
;daten
;*******************************
	bss
old_vbl	ds.l	1
old_hbl	ds.l	1
screen	ds.l	1
hbl_counter	ds.w	1
color00	ds.w	1
imra	ds.b	1
imrb	ds.b	1
