	opt	a+,c+,o+,w+
	include	include\tos_fnkt

;sucht in einer datei nach leerzeichen und ersetzt diese durch tabs.

	init	stack
start
	Cconws	string
	Cconrs	#12,name

fsfirst
	lea	name,a0
	addq	#2,a0
	Fsfirst	#0,(a0)
	tst.l	d0
	bmi	no_name

	lea	name,a0
	addq	#1,a0
	cmp.b	#0,(a0)
	beq	no_name
	addq	#1,a0
fopen
	Fopen	#2,(a0)
	move.w	d0,handle
	bmi	no_name

fgetdta
	Fgetdta
	tst.l	d0
	beq	read_error

get_size
	move.l	d0,a0
	add.l	#26,a0
	move.l	(a0),size
	
fread
	Fread	buffer,size,handle
	cmp.l	size,d0
	bne.s	read_error

	lea	buffer,a0
	move.l	size,d0
	subq	#1,d0
loop
	cmp.b	#' ',(a0)
	bne.s	go_on
	move.b	#9,(a0)
go_on
	addq	#1,a0
	dbf	d0,loop

fseek
	Fseek #0,handle,#0
	tst.l	d0
	bne.s	read_error

fwrite
	Fwrite	buffer,size,handle
	Cconws	ok
read_error
	Fclose	handle
no_name
	Pterm0

string	dc.b	$d,$a,'Filename: ',0
ok	dc.b	$d,$a,'O.K.!',0
	bss
handle	ds.w	1
name	ds.w	7
size	ds.l	1
buffer	ds.l	8000