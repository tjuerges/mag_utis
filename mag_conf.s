  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	f:\mag_utis\mag_conf.prg	;So hei�t das PRG-File.

;Dieses Programm initialisiert Drucker und Tastaturwiederholrate.

start
	init	stack(pc)
	init_gem	#2

	Kbrate	#1,#$f

	Setprt	#%000100

	scrp_write	#scrap_dir

	lea	ok(pc),a6
	bsr.s	cconws
	lea	scrap_dir(pc),a6
	bsr.s	cconws
	lea	ret(pc),a6
	bsr.s	cconws

	clr	d3
	lea	prt_init(pc),a3
loop
	move.b	(a3)+,d3
	beq.s	ende
test_prt
	Bcostat	#0
	tst	d0
	beq.s	off_line

	Bconout	d3,#0
	bra.s	loop
cconws
	Cconws	(a6)
	rts
off_line
	lea	off_line_str(pc),a6
	bsr.s	cconws
ende
	exit_gem
	clr	-(sp)
	trap	#1

	include	include\call_gem

	
	data
scrap_dir	dc.b	'C:\CLIPBRD\',0
	dc.l	0,0,0,0,0,0
prt_init	dc.b	27,'@',27,'k',1,27,'x0',0
off_line_str	dc.b	$d,$a,'Der Drucker ist nicht bereit.',$d,$a
	dc.b	'Die Druckerinstallation wird abgebrochen.'
ret	dc.b	$d,$a,0
ok	hello_world	'MAG-CONF'
	dc.b	'Konfiguration des Druckers & der',$d,$a
	dc.b	'Tastatureinstellungen.',$d,$a
	dc.b	'Clipboardpfad:',$d,$a,0
	copyright
