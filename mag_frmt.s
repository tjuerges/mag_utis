	include	tos_fnkt

;kleines formatierungsprogramm. ich glaube nicht, dass es 100%ig
;funktioniert, riesen fehler gab es noch nicht.

start
	init
	clr.w	device
	move.b	#80,track
	move.b	#9,sector
	move.b	#1,sides
	Cconws	menu
menu_loop
	Cconin
	move.b	d0,d1
	Cconws	clr_ln
	cmp.b	#'t',d1
	beq	new_track
	cmp.b	#'s',d1
	beq	new_sector
	cmp.b	#'f',d1
	beq	formatieren
	cmp.b	#'v',d1
	beq	verify
	cmp.b	#'a',d1
	beq	exec_flag
	cmp.b	#'d',d1
	beq.s	new_device
	cmp.b	#'n',d1
	beq	seiten
	cmp.b	#'b',d1
	seq	from_boot
	beq	boot_sec
	cmp.b	#'q',d1
	bne.s	menu_loop
ende
	Cconws	tsch�
	Pterm0
new_device
	tst.w	device
	beq.s	.weiter
	sub.w	#2,device
.weiter
	add.w	#1,device
	Cconws	device_str
	move.w	device,d1
	add.w	#'A',d1
	Bconout	d1,#2
	bra	menu_loop
verify
	Supexec	ver
	Cconws	ver_str
	move.w	ver_flag,d1
	and.l	#1,d1
	bsr	prt_dez
	bra	menu_loop
seiten
	tst.b	sides
	beq.s	.sides2
	sub.b	#2,sides
.sides2
	add.b	#1,sides
	Cconws	sides_string1
	move.b	sides,d1
	and.l	#1,d1
	addq	#1,d1
	bsr	prt_dez
	Cconws	sides_string2
	bra	menu_loop
ver
	and.w	#1,$444.w
	eor.w	#1,$444.w
	move.w	$444.w,ver_flag
	rts

exec_flag
	tst.b	boot_flag
	seq	boot_flag
	Cconws	flag_str
	move.b	boot_flag,d1
	and.l	#1,d1
	bsr	prt_dez
	bra	menu_loop

new_track
	move.b	track,d1
	Cconws	track_str
	Cconin
	cmp.b	#'+',d0
	beq.s	.add_track
	cmp.b	#'-',d0
	bne.s	return
	subq.b	#1,d1
	bra.s	.return
.add_track
	addq.b	#1,d1
.return
	cmp.b	min_track,d1
	bmi	menu_loop
	cmp.b	max_track,d1
	bhi	menu_loop
	move.b	d1,track
	bra.s	return

new_sector
	move.b	sector,d1
	Cconws	sector_str
	Cconin
	cmp.b	#'+',d0
	beq.s	.add_sector
	cmp.b	#'-',d0
	bne.s	return
	subq.b	#1,d1
	bra.s	.return
.add_sector
	addq.b	#1,d1
.return
	cmp.b	min_sector,d1
	bmi	menu_loop
	cmp.b	max_sector,d1
	bhi	menu_loop
	move.b	d1,sector
return
	and.l	#$ff,d1
	Cconws	neuer_wert
	bsr	prt_dez
	bra	menu_loop

formatieren
	Cconws	name_str
	Cconrs	#12,disk_name
	Cconws	format_str
	Cconws	track_nummer
	move.b	sides,d6
	and.w	#%1,d6
	move.b	track,d7
	and.l	#$ff,d7
	subq.l	#1,d7
	move.b	sector,d2
	and.l	#$ff,d2
track_format
	Cconws	right_str
	move.l	d7,d1
	addq	#1,d1
	bsr	prt_dez
	Flopfmt	#$e5e5,#1,#0,d7,d2,device,buffer
	tst.w	d0
	bne	error
	tst.b	d6
	beq.s	.nur_eine_seite
	Flopfmt	#$e5e5,#1,#1,d7,d2,device,buffer
	tst.w	d0
	bne	error
.nur_eine_seite
	Cconis
	tst.w	d0
	bmi	error
	dbf	d7,track_format

boot_sec
	Cconws	schreibe_boot
	moveq	#3,d6
	tst.b	sides
	bne.s	.ist_doppelseitig
	subq.w	#1,d6
.ist_doppelseitig
	Protobt	#0,d6,#-2,boot_block
	lea	boot_block,a0

	move.w	#$eb38,(a0)

	move.l	#'MAGN',2(a0)
	move.w	#'UM',6(a0)

	Gettime
	lsl.l	#8,d0
	or.l	d0,8(a0)

	moveq	#0,d0
	moveq	#0,d1
	move.b	track,d0
	move.b	sector,d1
	mulu	d0,d1
	and.l	#$ffff,d1
	lsl.w	#1,d1
	move.b	d1,19(a0)
	lsr.w	#8,d1
	move.b	d1,20(a0)

	move.w	#$300,22(a0)

	move.b	sector,24(a0)

	tst.b	boot_flag
	beq.s	not_executable
	lea	boot_block,a1
	moveq	#0,d1
	move.l	#$000000fe,d0
calc_check
	add.w	(a1)+,d1
	dbf	d0,calc_check
	move.l	#$1234,d2
	sub.w	d1,d2
	move.w	d2,510(a0)

not_executable
	Flopwr	#1,#0,#0,#1,device,boot_block
	tst.b	from_boot
	seq	from_boot
	bne	menu_loop

write_fat
	lea	boot_block,a0
	move.b	21(a0),fat
	move.b	#$ff,fat+1
	move.b	#$ff,fat+2

	Flopwr	#1,#0,#0,#2,device,fat
	Flopwr	#1,#0,#0,#5,device,fat
	clr.l	fat
	Flopwr	#1,#0,#0,#7,device,fat
	Flopwr	#1,#0,#0,#6,device,fat
	Flopwr	#1,#0,#0,#4,device,fat
	Flopwr	#1,#0,#0,#3,device,fat
write_disk_name
	lea	fat,a0
	lea	disk_name+1,a1
	move.b	(a1)+,d0
	and.l	#$ff,d0
	move.l	d0,d1
	subq.l	#1,d0
.copy_loop
	move.b	(a1)+,(a0)+
	dbf	d0,.copy_loop
	cmp.b	#12,d1
	beq.s	.ok
	moveq.l	#12,d2
	sub.b	disk_name+1,d2
	subq.l	#2,d2
.loop2
	move.b	#' ',(a0)+
	dbf	d2,.loop2
.ok
	move.b	#%1000,(a0)
	
	lea	fat,a0
	Gettime
	move.b	d0,22(a0)
	lsr.l	#8,d0
	move.b	d0,23(a0)
	lsr.l	#8,d0
	move.b	d0,24(a0)
	lsr.l	#8,d0
	move.b	d0,25(a0)

	Flopwr	#1,#0,#0,#8,device,fat
	lea	fat,a0
	moveq.l	#6,d0
.clr_loop
	clr.l	(a0)+
	dbf	d0,.clr_loop

	Flopwr	#1,#0,#0,#9,device,fat

	moveq	#1,d2
	moveq	#8,d1
write_dir
	add.l	d1,d2
	moveq	#1,d6
	tst.b	sides
	bne.s	.ist_zweiseitig
	subq	#1,d6
.ist_zweiseitig
	Flopwr	#1,d6,#0,d2,device,fat
	sub.l	d1,d2
	dbf	d1,write_dir

	Cconws	format_ok
	bra	menu_loop

error
	Cconws	error_str
	bra	menu_loop

	include	prt_dez.s
	include	prt_hex.s
	data
min_track	dc.b	70
min_sector	dc.b	1
max_track	dc.b	84
max_sector	dc.b	10

clr_ln	dc.b	27,'l',0
menu	dc.b	27,'EMAGNUM-Format '
version	dc.b	'0.7',$a,$a,$d
	dc.b	'	Bootsektor schreiben',$d,$a
	dc.b	'	Track',$d,$a,'	Sector',$d,$a,'	Format',$d,$a
	dc.b	'	N Anzahl der Seiten',$d,$a
	dc.b	'	Verify',$a,$d
	dc.b	'	Device',$a,$d
	dc.b	'	Ausf�hrbarer Bootsektor',$d,$a,$a,'	Quit ',$d,$a,0
tsch�	dc.b	$d,$a,'Tsch������',0
flag_str	dc.b	'1 => Bootsek. ausf�hrbar, 0 => n.ausf�hrbar'
	dc.b	'	exec_flag= ',0
ver_str	dc.b	'Bei 1 ist das Verify an, sonst aus. _fverify=',0
track_str	dc.b	'Track	+/- ',0
track_nummer	dc.b	'Formatiere Track ',0
sector_str	dc.b	'Sector	+/- ',0
neuer_wert	dc.b	'	Neuer Wert: ',0
name_str	dc.b	'Diskname: ',0
format_str	dc.b	'	Formatiere... Abbruch mit bel. Taste!',$d,$a,0
format_ok	dc.b	'	Formatieren gegl�ckt!',$d,$a,0
error_str	dc.b	'	Tja, das wars dann wohl. FEHLER!!!',$d,$a,0
device_str	dc.b	'Laufwerk=',0
right_str	dc.b	27,'Y',45,49,0
sides_string1	dc.b	'Die Disk ist ',0
sides_string2	dc.b	'-seitig.',0
schreibe_boot	dc.b	'Schreibe Bootsektor...',$d,$a,0
	bss
boot_flag	ds.b	1
from_boot	ds.b	1
sides	ds.b	1
track	ds.b	1
sector	ds.b	1
device	ds.w	1
ver_flag	ds.w	1
disk_name	ds.l	4
fat	ds.l	128
buffer	ds.l	12*256
boot_block	ds.l	128
