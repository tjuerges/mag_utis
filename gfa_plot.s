	include	tos_fnkt
	output	d:\basic\gfa_plot.inl

;die plotroutine schlechthin...
;f�r gfa-basic aufbereitet.

start
	movem.l	d0-a6,-(sp)
	move.l	60(sp),a4
	move.l	66(sp),a5
	move.l	(a5)+,a6
	move.l	(a5)+,d0
	move.l	(a5)+,d1
	calc_coordinates
	do_plot
	movem.l	(sp)+,d0-a6
	rts
