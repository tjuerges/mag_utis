	include	tos_fnkt
debug	=	0
length	=	$66600

	init
	Supexec	begin(pc)
	Pterm0

malloc:	Malloc	d7
	rts
begin:
	Setpalette	palette(pc)
	moveq	#-1,d7
	bsr.s	malloc
	cmp.l	#length+4,d0
	blo.s	error
	move.l	d0,d7
	bsr.s	malloc
	addq.l	#4,d0
	and.l	#$fffffffc,d0
	move.l	d0,a4
	Physbase
	move.l	d0,a5
	lea	32000(a5),a5

	lea	booting(pc),a6
	bsr.s	cconws
	moveq	#0,d1
	lea	program(pc),a0
	Fsfirst	d1,(a0)
	tst	d0
	beq.s	ok
error:	rts
cconws:	Cconws	(a6)
	rts
ok:	Fopen	d1,(a0)
	move	d0,d2
	bmi.s	error
	Fread	(a4),#length,d2
	Fclose	d2

	lea	copy_str(pc),a6
	bsr.s	cconws

	lea	start(pc),a0
	lea	(a5),a1
	moveq	#(program-start)/4-1,d0
.copy	move.l	(a0)+,(a1)+
	dbf	d0,.copy
	jmp	(a5)

	cnop	0,4
start:	lea	exec(pc),a0
	lea	$18.w,a1
	moveq	#$64,d0
.copy:	move.l	a0,(a1)+
	dbf	d0,.copy

	move.l	#length,d0
	lsr.l	#2,d0
	lea	(a4),a0
	lea	$1000.w,a1
.copy2:	move.l	(a0)+,(a1)+
	subq.l	#1,d0
	bne.s	.copy2
	pea	$1000.w
	ifne	debug
	illegal
	endc
	rts
exec:	rte
	cnop	0,4
palette:	dcb.w	16,0
program:	dc.b	'sg2.bin',0
booting:	dc.b	27,'E',27,"pMagnum's Starglider II loader",27,'q',$d,$a
	dc.b	'One moment please, program data',$d,$a
	dc.b	'is being loaded',$d,$a,$a
	dc.b	"Don't forget:",$d,$a
	dc.b	'Choose German language.',$d,$a
	dc.b	'Then press only return when being prompted',$d,$a
	dc.b	'for code word of the copy protection.',$d,$a,$a,0
copy_str:	dc.b	'Copying program data and',$d,$a
	dc.b	'will soon start Starglider II.',$d,$a,0
