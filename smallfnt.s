  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	;output	;So hei�t das PRG-File.

;Schaltet den kleinen 8*8-Zeichensatz ein.

	init	stack(pc)
start
	init_gem	#2
	graf_handle
	dc.w	$a000
	move.l	a1,a6
	move.l	4(a6),a6
	v_fontinit	a6
	move.l	vwk_current_handle(pc),contrl+12
	exit_gem
	Pterm0
	include	include\call_gem
	data
	bss
