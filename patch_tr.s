trap_nummer	=	13
fnkt_nummer	=	70
	include	tos_fnkt

;patched trap #13.

begin
	move.l	4(sp),d0
	lea	stack(pc),sp
	subq	#8,sp
	move.l	d0,4(sp)
	init
	move.l	a1,keep_mem

	Supexec	patch_trap(pc)
	Cconws	string(pc)
	Ptermres	#0,keep_mem

patch_trap
	patch	((trap_nummer+32)*4).w,saved_address,#new_trap
	rts

new_trap
	cmp.w	#fnkt_nummer,-2(sp)
	beq.s	is_trap
	move.l	saved_address(pc),a1
	jmp	(a1)
is_trap
	movem.l	d0-a7,rettung
	illegal
	movem.l	rettung(pc),d0-a6
	move.l	saved_address(pc),a1
	jmp	(a1)

	data
string	dc.b	'Alles o.k.!',$d,$a,0
crsr_down	dc.b	27,'Y',24+32,0+32,0
	bss
keep_mem	ds.l	1
saved_address	ds.l	1
rettung	ds.l	64
newstack	ds.l	1024
stack
