	include	tos_fnkt
;Kleines Programm, das m.H. einer Batchliste 'autolist'
;Programme im aktuellen Verzeichnis aufruft.

start:
	init
	lea	info(pc),a0
	bsr	cconws
	Fsfirst	#0,liste(pc)
	tst	d0
	bmi	error
	Fgetdta
	move.l	d0,a0
	move.l	d_length(a0),d0
	move.l	d0,laenge
	Malloc	d0
	move.l	d0,addresse
	Fopen	#0,liste(pc)
	move	d0,handle
	bmi	open_error
	move.l	addresse(pc),a0
	move.l	laenge(pc),d1
	Fread	(a0),d1,d0
	bsr	fclose
	;Dgetdrv
	;move	d0,akt_drive
	;Dgetpath	d0,akt_path(pc)

	move.l	addresse(pc),a2
	subq.l	#1,a2
loop_start:
	lea	filename(pc),a3
loop:	addq	#1,a2
	move.b	(a2),d0

	cmp.b	#':',d0
	beq.s	set_new_drive

	cmp.b	#'\',d0
	beq	set_new_path

	cmp.b	#',',d0	;Filenamen durch Kommata getrennt.
	beq.s	start_it

	cmp.b	#$d,d0	;Filenamen durch CR+LF getrennt.
	bne.s	.w0
	addq	#1,a2
	bra.s	start_it
.w0:	cmp.b	#'.',d0
	bne.s	.w1
	move.b	d0,(a3)+
	bra.s	loop
.w1:	cmp.b	#'0',d0
	bls.s	loop
	cmp.b	#'z',d0
	bhi.s	loop
	move.b	d0,(a3)+
	bra.s	loop

start_it:
	clr.b	(a3)
	bsr	pexec
	cmp.b	#'*',1(a2)
	beq.s	ende
	bra.s	loop_start
ende:	bsr	mfree
error:	;Dsetdrv	akt_drive(pc)
	;Dsetpath	akt_path(pc)
	lea	ende_text(pc),a0
	bsr.s	cconws
	Pterm0
cconws:	Cconws	(a0)
	rts

set_new_drive:
	moveq	#0,d0
	move.b	-1(a2),d0
	cmp.b	#'a',d0
	blo.s	.lo
	sub	#'a'-'A',d0
.lo:	sub	#'A',d0
	Dsetdrv	d0
	subq	#1,a3
	bra	loop

set_new_path:
	lea	(a2),a6
	lea	(a2),a5

.loop:	cmp.b	#',',(a5)	;Ende suchen.
	beq.s	.loop_ende
	cmp.b	#$d,(a5)
	beq.s	.loop_ende
	cmp.b	#'*',(a5)+
	beq.s	.loop2
	bra.s	.loop
.loop_ende:
	addq	#1,a5

.loop2:	cmp.b	#'\',-(a5)	;Letzte Pfadabtrennung suchen.
	bne.s	.loop2

	lea	(a5),a2

	subq	#1,a5
	move.l	a5,d0
	sub.l	a6,d0
	lea	path(pc),a5
.copy:	move.b	(a6)+,(a5)+
	dbf	d0,.copy
	clr.b	(a5)
	Dsetpath	path(pc)
	bra	loop


open_error:
	bsr.s	mfree
	bra	error
pexec:	lea	null(pc),a0
	Pexec	(a0),(a0),filename(pc),#0
	rts
fclose:	Fclose	handle(pc)
	rts
mfree:	move.l	addresse(pc),a0
	Mfree	(a0)
	rts

	bss
akt_path:	ds.l	64
filename:	ds.l	64
path:	ds.l	64
null:	ds.l	1
addresse:	ds.l	1
laenge:	ds.l	1
akt_drive:	ds.w	1
handle:	ds.w	1

	data
liste:	dc.b	'autolist',0
info:	hello_world	'MAG-AUTO'
	dc.b	'Es werden die Programmme in',$d,$a
	dc.b	'der Datei AUTOLIST ausgef�hrt.',$d,$a
	dc.b	'Bitte nur komplette Pfade angeben!',$d,$a,0
ende_text:	hello_world	'MAG-AUTO'
	dc.b	'Batchliste komplett ausgef�hrt!',$d,$a,0
	copyright
