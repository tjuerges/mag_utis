	output	d:\basic\iff_de.inl

;decrunchroutine meines crunchers MAG-CRUN V1.0
;vorbereitet f�r gfa-basic. auf dem stack befinden sich:
;lw #1: sourceaddresse
;lw #2: destinationaddresse

start_iff_decrunch
;a1=sourcebuffer;a2=objectbuffer
;d4=objectfile length
	movem.l	d0-a6,-(sp)
iff_decrunch_init
	move.l	66(sp),a5
	movem.l	(a5)+,a1-a2
	moveq	#0,d1
	lea	header,a3
	moveq	#2,d7
.loop
	cmpm.l	(a3)+,(a1)+
	bne.s	iff_de_end
	dbf	d7,.loop

	move.l	(a1)+,d4
start_iff_de
	tst.l	d4
	beq.s	iff_de_end
	bmi.s	iff_de_end
	moveq	#0,d0
	move.b	(a1)+,d0
	bpl.s	iff_de_eq
	not.b	d0
iff_de_uneq
	move.b	(a1)+,(a2)+
	subq.l	#1,d4
	dbf	d0,iff_de_uneq
	bra.s	start_iff_de

iff_de_eq
	subq.b	#1,d0
	move.b	(a1)+,d1
.loop2
	move.b	d1,(a2)+
	subq.l	#1,d4
	dbf	d0,.loop2
	bra.s	start_iff_de

iff_de_end
	movem.l	(sp)+,d0-a6
	rts
	even
header	dc.b	'MAG-CRUNV1.0'
	even
