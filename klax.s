DEBUG	=	0
	include	tos_fnkt

;das klax boot-programm (leicht modifiziert - ohne kopierschutz).

	lea	string(pc),a0
	bsr.s	cconws
	IFEQ	DEBUG
	Getrez
	subq.b	#2,d0
	bmi.s	ok
	lea	only_color(pc),a0
	bsr.s	cconws
	Cconin
	cmp.b	#'y',d0
	beq.s	ok
	Pterm0
	ENDC
ok
	moveq	#5,d1
	moveq	#-1,d0
loop
	dbf	d0,loop
	moveq	#-1,d0
	dbf	d1,loop
	lea	start(pc),a0
	Supexec	(a0)
cconws
	Cconws	(a0)
	rts
start
	LEA	($6EC00).l,A6
	LEA	L63786,A5
	MOVE	#(ende-L63786)/4,D7
L6377A	MOVE.L	(A5)+,(A6)+
	DBF	D7,L6377A
	JMP	($6EC00).l
L63786	LEA	($80000).l,A7
	ORI	#$700,SR
	LEA	klax.inc(pc),A0
	add.l	#$2600,a0
	LEA	($72600).l,A1
	MOVE	#$97F,D7
L637A4	MOVE.L	-(A0),-(A1)
	DBF	D7,L637A4
	LEA	($70000).l,A6
	JMP	(A6)
string	dc.b	27,'E',27,'p    KLAX: cracked by MAGNUM...',27,'q',$d,$a,0
only_color	dc.b	'Sorry... colormode is recommended!',$d,$a
	dc.b	'	Boot it for listening to musix & sounds?	(y/n)',0
	dc.b	'Thanx to ERIC SERRA for the beautiful motion picture soundtrack of the film GRAND BLEU	'
	dc.b	"HI's must go to DOK, Draku, Otti, Schotti, JM, 007, Olli&Steffi !"
	even
klax.inc	incbin	'klax.inc'
	even
ende