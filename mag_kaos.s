 TEXT 
Begin:
      LEA       L0026,A7
      CLR.L     -(A7) 
      MOVE.W    #$20,-(A7) 	;SUPER
      TRAP      #1
      ADDQ.L    #6,A7 
      MOVEA.L   $4F2.W,A0 
      move.l   8(A0),A0
      ;CMPI.W    #$104,2(A0) 
      ;BNE       L0007 
      MOVE.W    $1C(A0),D0
      LSR.W     #1,D0 
	lea	L0017(pc),a0
      MOVE.W    D0,(a0)
      LEA       L001D,A0
      BSR       L0013 
      MOVEA.L   $4F2.W,A0 
      MOVEA.L   4.W,A1
      ;CMPA.L    4(A0),A1
      ;BHI       L000A 
      LEA       L0026,A6
      CLR.W     -(A7) 
      PEA       L001C 
      MOVE.W    #$3D,-(A7) 	;FOPEN
      TRAP      #1
      ADDQ.L    #8,A7 
      MOVE.W    D0,D7 
      BMI       L000A 
      PEA       (A6)
      MOVE.L    #$C,-(A7) 
      MOVE.W    D7,-(A7)
      MOVE.W    #$3F,-(A7) 	;FREAD
      TRAP      #1
      ADDA.W    #$C,A7
      SUBI.L    #$C,D0
      BNE       L000A 
      MOVEA.L   8(A6),A5
      MOVEA.L   A5,A4 
      SUBA.L    #$8000,A4 
      MOVE.L    A5,D0 
      BTST      #0,D0 
      BNE       L0008 
      CMPA.L    A7,A4 
      BCS       L0008 
      MOVE.L    #$12345678,D0 
      MOVE.L    (A5),D1 
      MOVE.L    D0,(A5) 
      MOVE.L    (A5),D2 
      MOVE.L    D1,(A5) 
      CMP.L     D2,D0 
      BNE       L0008 
      MOVEA.L   A5,A0 
      ADDA.L    #$2FFFC,A0
      MOVE.L    (A0),D1 
      MOVE.L    D0,(A0) 
      MOVE.L    (A0),D2 
      MOVE.L    D1,(A0) 
      CMP.L     D2,D0 
      BNE       L0008 
      MOVEA.L   A6,A0 
      MOVEA.L   A5,A1 
      CMPM.L    (A0)+,(A1)+ 
      BNE.S     L0000 
      MOVEA.L   A5,A6 
      MOVE.W    D7,-(A7)
      MOVE.W    #$3E,-(A7) 	;FCLOSE 
      TRAP      #1
      ADDQ.W    #4,A7 
      MOVEQ     #0,D0 
      BSR       L000B 
      MOVE.W    D0,D6 
      BRA       L0005 
L0000:MOVEQ     #1,D0 
      BSR       L000B 
      MOVE.W    D0,D6 
      PEA       $C(A6)
      MOVE.L    #$2FFF4,-(A7) 
      MOVE.W    D7,-(A7)
      MOVE.W    #$3F,-(A7) 	;FREAD
      TRAP      #1
      ADDA.W    #$C,A7
      SUBI.L    #$2FFF4,D0
      BNE       L000A 
      MOVE.W    D7,-(A7)
      MOVE.W    #$3E,-(A7) 	;FCLOSE 
      TRAP      #1
      ADDQ.L    #4,A7 
      MOVEA.L   A6,A1 
      MOVEA.L   A6,A2 
      ADDA.L    #$30000,A2
      MOVE.B    1(A1),D0
      EXT.W     D0
      LEA       6(A1,D0.W),A1 
      MOVE.W    #$4E71,(A1)+
      SF        D1
L0001:MOVEA.L   A1,A0 
      CMPI.B    #$44,(A0)+
      BNE.S     L0002 
      CMPI.B    #$45,(A0)+
      BNE.S     L0002 
      CMPI.B    #$53,(A0)+
      BNE.S     L0002 
      CMPI.B    #$4B,(A0)+
      BNE.S     L0002 
      CMPI.B    #$20,(A0) 
      BNE.S     L0002 
      TST.B     1(A0) 
      BNE.S     L0002 
      MOVE.B    #$53,-(A0)
      MOVE.B    #$4F,-(A0)
      MOVE.B    #$41,-(A0) 	;#'A' 
      MOVE.B    #$4B,-(A0)
      BRA.S     L0003 
L0002:TST.B     D1
      BNE.S     L0003 
      MOVE.W    A1,D0 
      BTST      #0,D0 
      BNE.S     L0003 
      MOVEA.L   A1,A0 
      CMPI.L    #$31415926,(A0)+
      BNE.S     L0003 
      CMPI.W    #$426,(A0)+ 
      BNE.S     L0003 
      MOVE.B    #$60,(A0) 
      ST        D1
      MOVEA.L   A6,A1 
      ADDA.L    #$10000,A1
L0003:ADDQ.L    #1,A1 
      CMPA.L    A2,A1 
      BCS.S     L0001 
      MOVEA.L   A4,A0 
      MOVE.W    #$3FFF,D0 
L0004:CLR.L     (A0)+ 
      DBF       D0,L0004
      MOVE.W    #-1,-(A7) 
      MOVE.L    A4,-(A7)
      MOVE.L    A4,-(A7)
      MOVE.W    #5,-(A7) 	;SETSCREEN
      TRAP      #$E 
      ADDA.W    #$C,A7
      ORI.W     #$700,SR
      MOVEA.L   A5,A0 
      MOVEA.L   A6,A1 
      BSR       L0018 
      MOVE.L    A5,$42E.W 
      MOVE.L    A4,$436.W 
L0005:MOVE.L    #$5555AAAA,$51A.W 
      CMPI.B    #$1B,D6 
      BEQ.S     L0006 
      MOVE.L    #$31415926,$426.W 
      MOVE.L    A5,$42A.W 
L0006:JMP       (A5)
L0007:LEA       L001F,A0
      BRA.S     L0009 
L0008:LEA       L001E,A0
L0009:BSR       L0013 
L000A:CLR.W     -(A7) 	;PTERM0
      TRAP      #1
L000B:MOVEM.L   D3-D6,-(A7) 
      MOVE.W    D0,D6 
      LEA       L0020,A0
      TST.W     D6
      BNE.S     L000C 
      LEA       L0021,A0
      LEA       L0023,A1
      MOVE.B    #$62,(A1)+
      MOVE.B    #$65,(A1)+
      MOVE.B    #$69,(A1)+
      MOVE.B    #$20,(A1) 
L000C:BSR.S     L0013 
      MOVEQ     #$30,D0 
      ADD.B     2(A6),D0
      BSR.S     L0012 
      MOVEQ     #$2E,D0 
      BSR.S     L0012 
      MOVEQ     #$30,D0 
      ADD.B     3(A6),D0
      BSR.S     L0012 
      LEA       L0022,A0
      BSR.S     L0013 
      MOVEQ     #7,D3 
      MOVE.L    A5,D4 
      ST        D5
L000D:MOVE.L    D4,D1 
      LSL.L     #4,D4 
      MOVEQ     #$1C,D0 
      LSR.L     D0,D1 
      MOVEQ     #$30,D0 
      CMPI.B    #9,D1 
      BCS.S     L000E 
      MOVEQ     #$37,D0 
L000E:TST.B     D5
      BEQ.S     L000F 
      TST.B     D1
      BEQ.S     L0010 
      SF        D5
L000F:ADD.B     D1,D0 
      BSR.S     L0012 
L0010:DBF       D3,L000D
      LEA       L0024,A0
      TST.W     D6
      BNE.S     L0011 
      LEA       L0025,A0
L0011:BSR.S     L0013 
      MOVE.W    #8,-(A7) 	;CNECIN 
      TRAP      #1
      ADDQ.L    #2,A7 
      MOVEM.L   (A7)+,D3-D6 
      RTS 
L0012:ANDI.W    #$FF,D0 
      MOVE.W    D0,-(A7)
      MOVE.W    #2,-(A7) 	;CCONOUT
      TRAP      #1
      ADDQ.W    #4,A7 
      RTS 
L0013:MOVEA.L   A0,A1 
      MOVE.W    L0017(PC),D0
L0014:CMP.B     (A0)+,D0
      BEQ.S     L0016 
L0015:TST.B     (A0)+ 
      BNE.S     L0015 
      TST.B     (A0)
      BNE.S     L0014 
      MOVEA.L   A1,A0 
      ADDQ.L    #1,A0 
L0016:MOVE.L    A0,-(A7)
      MOVE.W    #9,-(A7) 	;CCONWS 
      TRAP      #1
      ADDQ.L    #6,A7 
      RTS 
L0017:DC.B      $00,$00 
L0018:MOVE.W    #$5FFF,D0 
      CMPA.L    A0,A1 
      BHI.S     L001B 
      BEQ.S     L001A 
      ADDA.L    #$30000,A1
      ADDA.L    #$30000,A0
L0019:MOVE.L    -(A1),-(A0) 
      MOVE.L    -(A1),-(A0) 
      DBF       D0,L0019
L001A:RTS 
L001B:MOVE.L    (A1)+,(A0)+ 
      MOVE.L    (A1)+,(A0)+ 
      DBF       D0,L001B
      RTS 
 DATA 
L001C:DC.B      '\kaos.im'
      DC.B      'g',$00 
      even
L001D:DC.B      $00,$0D,$0A,'KAOS will be loaded.',$0D,$0A,$00
	even
L001E:DC.B      $00,'Error:' 
      DC.B      ' Invalid'
      DC.B      ' address! Perhaps there is no RAM at destination address?'
      DC.B      $00,$01,'Fehler: Falsche Ladeadresse! Vielleicht ist dort kein RAM vorhanden?',$00,$00 
L001F:DC.B      $00,'Error: '
      DC.B      'TOS 1.4 '
      DC.B      'is in ROM r'
      DC.B      'equired!'
      DC.B      $00,$01,'Fehler'
      DC.B      ': TOS 1.'
      DC.B      '4 wird im ROM'
      DC.B      ' be'
      DC.B      'n�tigt!',$00 
      DC.B      $00 
L0020:DC.B      $00,'The file \KAOS.IM'
      DC.B      'G will b'
      DC.B      'e booted'
      DC.B      ': versi'
      DC.B      'on ',$00,$01,'\KA' 
      DC.B      'OS.IMG w'
      DC.B      'ird gebo'
      DC.B      'otet: V'
      DC.B      'ersion ',$00 
      DC.B      $00 
L0021:DC.B      $00,'inactive'
      DC.B      ' TOS in '
      DC.B      'RAM: ver'
      DC.B      'sion ',$00,$01,'i' 
      DC.B      'naktives'
      DC.B      ' TOS im '
      DC.B      'RAM: Ver'
      DC.B      'sion ',$00,$00 
L0022:DC.B      $00,' ' 
L0023:DC.B      ' to $',$00,$01,'  '
      DC.B      'nach $',$00,$00
L0024:DC.B      $00,$0D,$0A,$0A,'Spac'
      DC.B      'e:      '
      DC.B      ' load an'
      DC.B      'd make r'
      DC.B      'eset res'
      DC.B      'ident',$0D,$0A,'E' 
      DC.B      'sc:     '
      DC.B      '    load'
      DC.B      $0D,$0A,'^C:   '
      DC.B      '       d'
      DC.B      'on',$27,'t loa'
      DC.B      'd',$0D,$0A,$0A,$00,$01,$0D,$0A 
      DC.B      $0A,'Leertas' 
      DC.B      'te:   la'
      DC.B      'den und '
      DC.B      'resetfes'
      DC.B      't machen'
      DC.B      $0D,$0A,'Esc:  '
      DC.B      '       l'
      DC.B      'aden',$0D,$0A,'^C' 
      DC.B      ':       '
      DC.B      '   nicht'
      DC.B      ' laden',$0D,$0A
      DC.B      $0A,$00,$00 
L0025:DC.B      $00,$0D,$0A,$0A,'Space' 
      DC.B      ':       '
      DC.B      'make res'
      DC.B      'et resid'
      DC.B      'ent and '
      DC.B      'start',$0D,$0A,'E' 
      DC.B      'sc:     '
      DC.B      '    only'
      DC.B      ' start',$0D,$0A
      DC.B      '^C:     '
      DC.B      '     don'
      DC.B      $27,'t start' 
      DC.B      $0D,$0A,$0A,$00,$01,$0D,$0A,$0A 
      DC.B      'Leertast'
      DC.B      'e:   res'
      DC.B      'etfest m'
      DC.B      'achen un'
      DC.B      'd starte'
      DC.B      'n',$0D,$0A,'Esc: ' 
      DC.B      '        '
      DC.B      'nur star'
      DC.B      'ten',$0D,$0A,'^C:' 
      DC.B      '        '
      DC.B      '  nicht '
      DC.B      'starten',$0D 
      DC.B      $0A,$0A,$00,$00 
 BSS
      DS.B      800 
L0026:DS.B      196608
ZUEND: END
