	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.

debug	=	0
reg_list	reg	d3-d7/a3-a6
interrupt	=	1
;eigene flags:
super_flag	=	5
;line_a_flags:
blink_flag	=	0
blink_stat_flag	=	1
cursor_flag	=	2
wrap_flag	=	3
invers_flag	=	4
cursor_pos_flag	=	5
blitter_flag	=	6
esc_flag	=	7

debug_address	=	$300

setup_for_print	MACRO
	move.l	line_a(pc),a6
	movem	V_CUR_XY(a6),d0-d1
	move.l	V_CUR_AD(a6),a5
	move.l	V_FNT_AD(a6),a4
	ENDM
print_end	MACRO
	move	d0,V_CUR_XY(a6)
	move	d1,V_CUR_XY+2(a6)
	move.l	a5,V_CUR_AD(a6)
	ENDM
;*****************************
;*****************************
start:
	bra	begin

;alter gemdos-dispatcher
old_gemdos_dispatcher:
	move.l	old_gemdos(pc),-(sp)
	rts
	dc.l	'XBRA','MGSP'
old_gemdos:	dc.l	0
new_gemdos_dispatcher:
	move	usp,a0
	btst	#super_flag,(sp)
	beq.s	.stack_ok
	lea	6(sp),a0
	tst	(_longframe).w
	beq.s	.stack_ok
	addq.l	#2,a0
.stack_ok:
	cmp	#9,(a0)
	bne.s	old_gemdos_dispatcher
cconws:
	movem.l	reg_list,-(sp)
	ifne	interrupt
	or	#$700,sr
	endc
	ifne	debug
	not	$ffff8240.w
	endc
	move.l	2(a0),a0
;folgende registerbelegung:
;a0=*(char)	a4=fontadresse	a5=cursoraddresse	a6=line_a
;d0=x	d1=y	d3=akt. char
	setup_for_print
	btst	#esc_flag,RESERVED(a6)
	beq.s	.main_loop
	bsr	escape
.main_loop:
	moveq	#0,d3
	move.b	(a0)+,d3
	beq	.cconws_ende
	ifne	debug
	move.b	d3,(debug_address).w
	endc
	cmp.b	#32,d3
	bhi	.richtiges_zeichen
	bne.s	.kein_space
	addq	#1,a5
	addq.b	#1,d0
	cmp	max_spalte_plus(pc),d0
	blo.s	.main_loop
	btst	#wrap_flag,RESERVED(a6)
	bne.s	.kein_auto_wrap_an1
	sub	d0,a5
	add	V_CEL_WR(a6),a5
	moveq	#0,d0
	addq.b	#1,d1
	bra.s	.main_loop
.kein_auto_wrap_an1:
	sub	d0,a5
	subq	#1,a5
	add	V_CEL_WR(a6),a5
	move	V_CEL_MX(a6),d0
	bra.s	.main_loop
.kein_space:
;escape?
	cmp.b	#27,d3
	bne.s	.kein_escape
	clr.b	d3
	bsr	escape
	bra.s	.main_loop
.kein_escape:
;carriage return?
	cmp.b	#$d,d3
	bne.s	.no_cr
	sub	d0,a5
	moveq	#0,d0
	bra.s	.main_loop
.no_cr:
;linefeed?
	cmp.b	#$a,d3
	bne.s	.no_lf
	addq.b	#1,d1
	add	V_CEL_WR(a6),a5
	cmp	max_zeile_plus(pc),d1
	blo.s	.main_loop
	bsr	scroll_rauf
	move.l	d2,a5
	subq	#1,d1
	bra.s	.main_loop
.no_lf:
	cmp.b	#9,d3
	bne.s	.no_tab
	sub	d0,a5
	and.b	#%11111000,d0
	addq	#8,d0
	add	d0,a5
	bra	.main_loop
.no_tab:
	cmp.b	#7,d3
	bne.s	.no_bell
	btst	#2,(conterm).w
	beq	.main_loop
	move.l	(bell_hook).w,a3
	bne.s	.weiter

	not	$ffff8240.w
	move	#$fff,d0
.loop	dbf	d0,.loop
	not	$ffff8240.w
	bra	.main_loop
.weiter
	movem.l	d0-d2/a0-a2,-(sp)
	jsr	(a3)
	movem.l	(sp)+,d0-d2/a0-a2
	bra	.main_loop
.no_bell:
.richtiges_zeichen:
;zeilenende erreicht?
	cmp	max_spalte_plus(pc),d0
	blo.s	.no_lineend
	btst	#wrap_flag,RESERVED(a6)
	bne.s	.kein_auto_wrap_an
	sub	d0,a5
	add	V_CEL_WR(a6),a5
	moveq	#0,d0
	addq.b	#1,d1
	bra.s	.no_lineend
.kein_auto_wrap_an:
	sub	d0,a5
	subq	#1,a5
	add	V_CEL_WR(a6),a5
	move	V_CEL_MX(a6),d0
.no_lineend:
	bsr	char
	bra	.main_loop
.cconws_ende:
	print_end
	movem.l	(sp)+,reg_list
	rte


scroll_rauf:
	move.l	(_v_bas_ad).w,a1
	move.l	a1,d2
	add	V_CEL_WR(a6),d2
	btst	#blitter_flag,RESERVED(a6)
	beq.s	scroll_ohne_blitter
	move.l	d2,$ffff8a24.w	;src_add
	move.l	a1,$ffff8a32.w	;dst_add
	move	breite(pc),$ffff8a36.w	;x_count
	move	V_REZ_VT(a6),d2
	sub	V_CEL_HT(a6),d2
	move	d2,$ffff8a38.w	;y_count
	move.l	#$20002,$ffff8a20.w	;src_x/yinc
	move.l	#$20002,$ffff8a2e.w	;dst_x/yinc
	move.l	#$ffffffff,$ffff8a28.w	;endmask1&2
	move	#$ffff,$ffff8a2c.w	;endmask3
	move.l	#$203c000,$ffff8a3a.w	;hop
.blit1:	btst	#7,$ffff8a3c.w
	bne.s	.blit1

	move	V_CEL_WR(a6),d2
	mulu	V_CEL_MY(a6),d2
	add.l	a1,d2
	move.l	d2,$ffff8a32.w	;dst_add
	move	breite(pc),$ffff8a36.w	;x_count
	move	V_CEL_HT(a6),$ffff8a38.w	;y_count
	move.l	#$20002,$ffff8a2e.w	;dst_x/yinc
	move.l	#$200c000,$ffff8a3a.w	;hop
.blit2:	btst	#7,$ffff8a3c.w
	bne.s	.blit2
	rts
scroll_ohne_blitter:
scrolllist	reg	d0-d1/d3/a0/a4-a6
	movem.l	scrolllist,-(sp)
	move.l	d2,a0
	move	V_CEL_WR(a6),d0
	mulu	V_CEL_MY(a6),d0
	divu	#12*4,d0
	subq	#1,d0
.loop1:
	movem.l	(a0)+,d1-d7/a2-a6
	movem.l	d1-d7/a2-a6,(a1)
	lea	12*4(a1),a1
	dbf	d0,.loop1

	swap	d0
	subq	#1,d0
	bmi.s	.weiter
.loop2:
	move.b	(a0)+,(a1)+
	dbf	d0,.loop2

.weiter:
	move.l	a1,-(sp)
	move.l	line_a(pc),a6
	move	V_CEL_WR(a6),d0
	ext.l	d0
	divu	#13*4,d0
	subq	#1,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	moveq	#0,d7
	move.l	d1,a0
	move.l	d1,a2
	move.l	d1,a3
	move.l	d1,a4
	move.l	d1,a5
	move.l	d1,a6
.loop3:
	movem.l	d1-d7/a0/a2-a6,(a1)
	lea	13*4(a1),a1
	dbf	d0,.loop3
	swap	d0
	subq	#1,d0
	bmi.s	.weiter2
.loop4:
	clr.b	(a1)+
	dbf	d0,.loop4
.weiter2:
	move.l	(sp)+,d2
	movem.l	(sp)+,scrolllist
	rts

;*******************************
;*******************************

;alter bios-dispatcher
old_bios_dispatcher:
	move.l	old_bios(pc),-(sp)
	rts
	dc.l	'XBRA','MGSP'
old_bios:	dc.l	0
new_bios_dispatcher:
	move	usp,a0
	btst	#super_flag,(sp)
	beq.s	.bios_stack_ok
	lea	6(sp),a0
	tst	(_longframe).w
	beq.s	.bios_stack_ok
	addq.l	#2,a0
.bios_stack_ok:
	cmp	#3,(a0)		;Bconout
	bne.s	old_bios_dispatcher
	cmp	#5,2(a0)	;RAWCON: nicht CON:!!!
	bne.s	old_bios_dispatcher
	movem.l	reg_list,-(sp)
	ifne	interrupt
	or	#$700,sr
	endc
	ifne	debug
	not	$ffff8240.w
	endc
	setup_for_print	;Aktuelle Daten (Aufl�sung usw.) holen.
	moveq	#0,d3
	move	4(a0),d3	;Aktueller Buchstabe nach d3.
	bsr.s	bios_char_main
.bios_ende:
	print_end
	movem.l	(sp)+,reg_list
	rte


;*******************************
;*******************************

	dc.l	'XBRA','MGSP'
old_raw_bconout:	dc.l	0
new_raw_bconout:
	move	usp,a0
	btst	#super_flag,(sp)
	beq.s	.bios_stack_ok
	lea	6(sp),a0
	tst	(_longframe).w
	beq.s	.bios_stack_ok
	addq.l	#2,a0
.bios_stack_ok:
	ifne	interrupt
	or	#$700,sr
	endc
	ifne	debug
	not	$ffff8240.w
	endc
	setup_for_print	;Aktuelle Daten (Aufl�sung usw.) holen.
	moveq	#0,d3
	move	(a0),d3	;Aktueller Buchstabe nach d3.
	bsr.s	bios_char_main
.bios_ende:
	print_end
	moveq	#-1,d0
	rts


;*******************************
;*******************************

bios_char_main:
.main_loop:
	ifne	debug
	move.b	d3,(debug_address).w
	endc
	cmp.b	#32,d3
	bne.s	.kein_space
	addq	#1,a5
	addq.b	#1,d0
	cmp	max_spalte_plus(pc),d0
	blo.s	.main_loop
	btst	#wrap_flag,RESERVED(a6)
	bne.s	.kein_auto_wrap_an1
	sub	d0,a5
	add	V_CEL_WR(a6),a5
	moveq	#0,d0
	addq.b	#1,d1
	bra.s	.ende
.kein_auto_wrap_an1:
	sub	d0,a5
	subq	#1,a5
	add	V_CEL_WR(a6),a5
	move	V_CEL_MX(a6),d0
	bra.s	.ende
.kein_space:
.richtiges_zeichen
	bsr.s	char
.ende:
	rts

;*****************************
;*****************************

char:
;zeilenende erreicht?
	cmp	max_spalte_plus(pc),d0
	blo.s	.no_lineend
	btst	#wrap_flag,RESERVED(a6)
	bne.s	.kein_auto_wrap_an
	sub	d0,a5
	add	V_CEL_WR(a6),a5
	moveq	#0,d0
	addq.b	#1,d1
	bra.s	.no_lineend
.kein_auto_wrap_an:
	sub	d0,a5
	subq	#1,a5
	add	V_CEL_WR(a6),a5
	move	V_CEL_MX(a6),d0
.no_lineend:
;schon letzte zeile erreicht?
	cmp	max_zeile_plus(pc),d1
	blo.s	.no_screenend
.screen_end:
	bsr	scroll_rauf
	move.l	d2,a5
.no_screenend:
	move.l	a5,a3
	move	V_CEL_HT(a6),d4
	move.l	a4,a2
	add	d3,a2
	subq	#2,d4
.char_loop:
	move.b	(a2),d2
	lea	$100(a2),a2
	btst	#invers_flag,RESERVED(a6)
	beq.s	.weiter
	not.b	d2
.weiter:
	move.b	d2,(a3)
	add	BYTES_LIN(a6),a3
	dbf	d4,.char_loop
	move.b	(a2),d2
	btst	#invers_flag,RESERVED(a6)
	beq.s	.wweiter
	not.b	d2
.wweiter:
	move.b	d2,(a3)
	addq.l	#1,a5
	addq.b	#1,d0
	rts

;*****************************
;*****************************

;eine escape-sequenz wurde eingel�utet
escape:
	moveq	#0,d3
	move.b	(a0)+,d3
	bne.s	.weiter
	rts
.weiter:
	cmp.b	#'A',d3
	blo.s	esc_return
	cmp.b	#'Z',d3
	bhi.s	.esc_weiter
.esc_gross:
	lea	escape_table_gr(pc),a3
	sub.b	#'A',d3
	lsl.b	#2,d3
	move.l	(a3,d3.l),a3
	jmp	(a3)
.esc_weiter
	cmp.b	#'z',d3
	bhi.s	esc_return
	cmp.b	#'a',d3
	blo.s	esc_return
.esc_klein:
	lea	escape_table_kl(pc),a3
	sub.b	#'a',d3
	lsl.b	#2,d3
	move.l	(a3,d3.l),a3
	jmp	(a3)
esc_return:
	bclr	#esc_flag,RESERVED(a6)
	rts

_0:
	bra.s	esc_return
_A:
	tst.b	d1
	beq.s	esc_return
	subq.b	#1,d1
	sub	V_CEL_WR(a6),a5
	bra.s	esc_return
_B:
	cmp	max_zeile_plus(pc),d1
	bhs.s	esc_return
	addq.b	#1,d1
	add	V_CEL_WR(a6),a5
	bra.s	esc_return
_C:
	cmp	max_spalte_plus(pc),d0
	bhs.s	esc_return
	addq.b	#1,d0
	addq.l	#1,a5
	bra.s	esc_return
_D:
	tst.b	d0
	beq.s	esc_return
	subq.b	#1,d0
	subq.l	#1,a5
	bra.s	esc_return
_E:
	move.l	(_v_bas_ad).w,a1
	btst	#blitter_flag,RESERVED(a6)
	beq.s	_E_ohne_blitter
	move.l	a1,$ffff8a32.w	;dst_add
	move	breite(pc),$ffff8a36.w	;x/y_count
	move	V_REZ_VT(a6),$ffff8a38.w
	move.l	#$20002,$ffff8a2e.w	;dst_x/yinc
	move.l	#$ffffffff,$ffff8a28.w	;endmask1
	move	#$ffff,$ffff8a2c.w	;endmask3
	move.l	#$200c000,$ffff8a3a.w	;hop
	bra.s	_E_ende
_E_ohne_blitter:
loeschelist	reg	d3/a0/a4-a6
	movem.l	loeschelist,-(sp)
	move	V_CEL_WR(a6),d0
	mulu	max_zeile_plus(pc),d0
	divu	#13*4,d0
	subq	#1,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	moveq	#0,d7
	move.l	d1,a0
	move.l	d1,a2
	move.l	d1,a3
	move.l	d1,a4
	move.l	d1,a5
	move.l	d1,a6
.loesche_loop:
	movem.l	d1-d7/a0/a2-a6,(a1)
	lea	4*13(a1),a1
	dbf	d0,.loesche_loop
	swap	d0
	subq	#1,d0
	bmi.s	.weiter
.loop2:
	clr.b	(a1)+
	dbf	d0,.loop2
.weiter:
	movem.l	(sp)+,loeschelist
_E_ende:
	moveq	#0,d0
	moveq	#0,d1
	move.l	(_v_bas_ad).w,a5
	bra	esc_return
_H:
	moveq	#0,d0
	moveq	#0,d1
	move.l	(_v_bas_ad).w,a5
	bra	esc_return
_I:	
	tst.b	d1
	beq.s	_I_insert_line
	subq.b	#1,d1
	sub	V_CEL_WR(a6),a5
	bra	esc_return
_I_insert_line:
	move	V_CEL_WR(a6),d2
	mulu	max_zeile_plus(pc),d2
	add.l	(_v_bas_ad).w,d2
_I_scroll_ohne_blitter:
insertlist	reg	d0-d1/d3/a0/a4-a6
	movem.l	insertlist,-(sp)
	move.l	d2,a1
	sub	V_CEL_WR(a6),d2
	move.l	d2,a0
	move	V_REZ_VT(a6),d0
	sub	V_CEL_HT(a6),d0
	mulu	BYTES_LIN(a6),d0
	divu	#12*4,d0
	subq	#1,d0
_I_loop1:
	lea	-12*4(a0),a0
	movem.l	(a0),d1-d7/a2-a6
	movem.l	d1-d7/a2-a6,-(a1)
	dbf	d0,_I_loop1

	swap	d0
	subq	#1,d0
	bmi.s	_I_weiter
_I_loop2:
	move.b	-(a0),-(a1)
	dbf	d0,_I_loop2
_I_weiter:
	move.l	(_v_bas_ad).w,a1
	move.l	line_a(pc),a6
	btst	#blitter_flag,RESERVED(a6)
	bne.s	_I_clear_blitter
	move	V_CEL_WR(a6),d0
	ext.l	d0
	divu	#13*4,d0
	subq	#1,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	moveq	#0,d7
	move.l	d1,a0
	move.l	d1,a2
	move.l	d1,a3
	move.l	d1,a4
	move.l	d1,a5
	move.l	d1,a6
_I_loop3:
	movem.l	d1-d7/a0/a2-a6,(a1)
	lea	12*4(a1),a1
	dbf	d0,_I_loop3
	swap	d0
	subq	#1,d0
	bmi.s	_I_weiter2
_I_loop4:
	clr.b	(a1)+
	dbf	d0,_I_loop4
_I_weiter2:
	movem.l	(sp)+,insertlist
_I_ende:
	bra	esc_return
_I_clear_blitter:
	move.l	a1,$ffff8a32.w	;dst_add
	move	breite(pc),$ffff8a36.w	;x_count
	move	V_CEL_HT(a6),$ffff8a38.w	;y_count
	move.l	#$20002,$ffff8a2e.w	;dst_x/yinc
	move.l	#$200c000,$ffff8a3a.w	;hop
	bra.s	_I_weiter2
_J:
	bsr	_K_sub
	move.l	a5,a3
	sub	d0,a3
	add	V_CEL_WR(a6),a3
	btst	#blitter_flag,RESERVED(a6)
	beq.s	_J_ohne_blitter
	move.l	a3,$ffff8a32.w	;dst_add
	move	breite(pc),$ffff8a36.w	;x/y_count
	move	V_CEL_MY(a6),d2
	sub	d1,d2
	addq	#1,d2
	mulu	V_CEL_HT(a6),d2
	move	d2,$ffff8a38.w	;y_count
	move.l	#$20002,$ffff8a2e.w	;dst_x/yinc
	move.l	#$ffffffff,$ffff8a28.w	;endmask1
	move	#$ffff,$ffff8a2c.w	;endmask3
	move.l	#$200c000,$ffff8a3a.w	;hop
	bra.s	_J_ende
_J_ohne_blitter:
	movem.l	insertlist,-(sp)
	move.l	V_CEL_WR(a6),d0
	mulu	max_zeile_plus(pc),d0
	divu	#13*4,d0
	subq	#1,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	moveq	#0,d7
	move.l	d1,a0
	move.l	d1,a1
	move.l	d1,a2
	move.l	d1,a4
	move.l	d1,a5
	move.l	d1,a6
_J_loesche_loop:
	movem.l	d1-d7/a0-a2/a4-a6,(a3)
	lea	13*4(a3),a3
	dbf	d0,_J_loesche_loop
	swap	d0
	subq	#1,d0
	bmi.s	_J_weiter
_J_loop2:
	clr.b	(a3)+
	dbf	d0,_J_loop2
_J_weiter:
	movem.l	(sp)+,insertlist
_J_ende:
	bra	esc_return
_K_sub:
	move	V_CEL_MX(a6),d7
	sub	d0,d7
	move.l	a5,a3
	move	V_CEL_HT(a6),d4
	subq	#1,d4
_K_loop_aussen:
	move	d7,d2
_K_loop_innen:
	clr.b	(a3)+
	dbf	d2,_K_loop_innen
	add.l	d0,a3
	dbf	d4,_K_loop_aussen
	rts
_K:
	pea	esc_return(pc)
	bra.s	_K_sub
_L:
	moveq	#0,d0
	bra	esc_return
_M:
	moveq	#0,d0
	bra	esc_return
_O:
	move.l	old_gemdos(pc),(trap_1).w
	move.l	old_bios(pc),(trap_13).w
	bra	esc_return
_Y:
	moveq	#0,d2
	move.b	(a0)+,d2
	beq	esc_return
	sub.b	#32,d2
	cmp	max_zeile_plus(pc),d2
	bhi.s	.y_zu_gross
.back_y:
	move.b	d2,d1
	move.b	(a0)+,d2
	beq	esc_return
	sub.b	#32,d2
	cmp	max_spalte_plus(pc),d2
	bhi.s	.x_zu_gross
.back_x:
	move.b	d2,d0
	move.l	(_v_bas_ad).w,a5
	move	V_CEL_WR(a6),d4
	mulu	d1,d4
	add.l	d0,d4
	add.l	d4,a5	;screen_address
	bra	esc_return
.y_zu_gross:
	move	V_CEL_MY(a6),d2
	bra.s	.back_y
.x_zu_gross:
	move	V_CEL_MX(a6),d2
	bra.s	.back_x
_b:
	move.b	(a0)+,d3
	beq	esc_return
	sub.b	#32,d3
	move	d3,-$24(a6)
	bra	esc_return
_c:
	move.b	(a0)+,d3
	beq	esc_return
	sub.b	#32,d3
	move	d3,-$26(a6)
	bra	esc_return
_d:	
	bra	esc_return
_e:
	bset	#2,RESERVED(a6)
	bra	esc_return
_f:
	bclr	#2,RESERVED(a6)
	bra	esc_return
_j:
	move	d1,(sav_row).w
	move	d0,V_SAV_XY(a6)
	move	d1,V_SAV_XY+2(a6)
	move.l	a5,V_CUR_AD(a6)
	bra	esc_return
_k:
	move	V_SAV_XY(a6),d0
	move	V_SAV_XY+2(a6),d1
	move.l	V_CUR_AD(a6),a5
	clr.l	-$14e(a6)
	bra	esc_return
_l:
	sub.l	d0,a5
	moveq	#0,d0
	bra	esc_return
_o:
	bra	esc_return
_p:
	bset	#invers_flag,RESERVED(a6)
	bra	esc_return
_q:
	bclr	#invers_flag,RESERVED(a6)
	bra	esc_return
_v:
	bclr	#wrap_flag,RESERVED(a6)
	bra	esc_return
_w:
	bset	#wrap_flag,RESERVED(a6)
	bra	esc_return

line_a	dc.l	0
max_spalte_plus:	dc.w	0
max_zeile_plus:	dc.w	0
breite:	dc.w	0

escape_table_gr:
	dc.l	_A,_B,_C,_D,_E,_0,_0,_H,_I,_J,_K,_L,_M,_0,_O,_0
	dc.l	_0,_0,_0,_0,_0,_0,_0,_0,_Y,_0
escape_table_kl:
	dc.l	_0,_b,_c,_d,_e,_f,_0,_0,_0,_j,_k,_l,_0,_0,_o,_p
	dc.l	_q,_0,_0,_0,_0,_v,_w,_0,_0,_0
save_it:
	even

;***********************
;nichtresidenter teil
;***********************
setexc:
	Setexc	(a0),d0
	rts
super:
	Super	d0
	rts

begin:
	dc.w	$a000
	move.l	a0,line_a
	move.l	V_CEL_MX(a0),d0
	addq	#1,d0
	move	d0,max_zeile_plus
	swap	d0
	addq	#1,d0
	move	d0,max_spalte_plus
	move	BYTES_LIN(a0),d0
	lsr	#1,d0
	move	d0,breite
	moveq	#0,d7

	moveq	#0,d0
	bsr.s	super
	move.l	d0,-(sp)

	ifne	debug
	clr.b	(debug_address).w
	endc
	move.l	(trap_1).w,a0
	cmp.l	#'XBRA',-12(a0)
	bne.s	.weiter1
	cmp.l	#'MGSP',-8(a0)
	beq.s	.weiter1
	lea	new_gemdos_dispatcher(pc),a0
	moveq	#33,d0
	bsr.s	setexc
	move.l	d0,old_gemdos
	moveq	#-1,d7
.weiter1:
	move.l	(_sysbase).w,a0
	cmp	#$102,2(a0)
	bhs.s	.neues_tos
	move.l	(trap_14).w,a0
	cmp.l	#'XBRA',-12(a0)
	bne.s	.weiter2
	cmp.l	#'MGSP',-8(a0)
	beq.s	.weiter2
	lea	new_bios_dispatcher(pc),a0
	moveq	#45,d0
	bsr	setexc
	move.l	d0,old_bios
	moveq	#-1,d7
	bra.s	.weiter2
.neues_tos
	move.l	20+(xconout).w,a1
	cmp.l	#'XBRA',-12(a0)
	bne.s	.weiter2
	cmp.l	#'MGSP',-8(a0)
	beq.s	.weiter2
	move.l	20+(xconout).w,old_raw_bconout
	move.l	#new_raw_bconout,20+(xconout)
	moveq	#-1,d7
.weiter2:

	move.l	(sp)+,d0
	bsr	super

	tst	d7
	beq.s	schon_da

	move.l	line_a(pc),a6
	Blitmode	#-1
	btst	#0,d0	;f�r Blitter abh�ngig von der Desktopeinstellung
;	btst	#1,d0	;f�r Blitter abh�ngig von der Hardware
	beq.s	.weiter
	bset	#blitter_flag,RESERVED(a6)
.weiter:
	bclr	#wrap_flag,RESERVED(a6)

	lea	hallo(pc),a0
	bsr.s	print
	Ptermres	#0,#$100+(save_it-start),-(sp)

schon_da:
	lea	not(pc),a0
	bsr.s	print
	Cconin
	cmp.b	#'j',d0
	beq.s	loesch
	cmp.b	#'J',d0
	beq.s	loesch
enden:
	Pterm0
loesch:
	lea	loeschen(pc),a0
	bsr.s	print
	bra.s	enden
print:
	Cconws	(a0)
	rts
hallo:	hello_world	'MAG-PRINT'
not:	dc.b	'MAG-PRINT ist schon installiert.',$d,$a
	dc.b	'Zum deaktivieren: <ESC-"O">',$d,$a
	dc.b	'Soll MAG-PRINT deaktiviert werden? ',0
loeschen:	dc.b	27,'O',0
