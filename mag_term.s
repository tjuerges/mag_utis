	opt	a+,c+,o+,w+
	output	f:\mag_utis\mag_term.prg

	include	include\tos_fnkt

;sollte ein kleines terminalprogramm werden.
;kein kommentar.

DEBUG	=	0
begin
	init	stack(pc)
	lea	my_screen(pc),a6
	add.l	#128,a6
	move.l	a6,d5
	and.l	#$ffffff00,d5
	move.l	d5,screen
	lea	hallo(pc),a6
	bsr	cconws
	clr.b	protokoll_flag
;************************************
;HAUPTSCHLEIFENSTART
;************************************
main_loop
	bsr.s	test_vt52
	bsr	test_rs
	bra.s	main_loop

;************************************
;HAUPTSCHLEIFENENDE
;************************************

;************************************
;TEST, OB EIN ZEICHEN AN DER TASTATUR
;ANLIEGT
;************************************
test_vt52
	moveq	#2,d1
	bsr	status
	bmi.s	.char_found
	rts

;ES WURDE EIN ZEICHEN GEFUNDEN, WELCHES
;IM FOLGENDEN AUSGEWERTET WIRD
;************************************
.char_found
	Kbshift	#-1
	and.b	#%1101000,d0
	beq.s	.keine_sondertaste
	btst	#3,d0
	bne	is_alternate
	btst	#5,d0
	bne	is_lmb
	btst	#6,d0
	bne	is_rmb

;************************************
;ES WAR NUR EIN NORMALES ASCII-ZEICHEN
.keine_sondertaste
	moveq	#2,d1
	bsr	get
	move.l	d0,char_in
	tst.b	d0
	beq.s	is_scankey

	tst.b	protokoll_flag
	beq.s	.kein_protokoll
	move.b	d0,(a5)+
	add.l	#1,count
	move.l	protokoll_size(pc),d7
	cmp.l	count(pc),d7
	blt	forced_close_protokoll_file
.kein_protokoll
.return
	move.l	char_in(pc),d0
	IFNE	DEBUG
	moveq	#2,d1
	ELSEIF
	moveq	#1,d1
	ENDC
	bsr	put
	rts
;************************************


;************************************
;************************************
;ES WURDE KEIN ASCII-CODE F�R DIE ANLIEGENDE
;TASTE GEFUNDEN, ALSO LIEGT NUR EIN SCAN-CODE
;VOR. DIESER WIRD HIER AUSGEWERTET
;************************************
is_scankey
	swap	d0
	cmp.b	#97,d0
	beq	is_undo
	cmp.b	#98,d0
	beq	is_help
	cmp.b	#59,d0
	beq	is_f1
	cmp.b	#60,d0
	beq	is_f2
	cmp.b	#61,d0
	beq	is_f3
	cmp.b	#62,d0
	beq	is_f4
	cmp.b	#63,d0
	beq	is_f5
	cmp.b	#64,d0
	beq	is_f6
	cmp.b	#65,d0
	beq	is_f7
	cmp.b	#66,d0
	beq	is_f8
	cmp.b	#67,d0
	beq	is_f9
	cmp.b	#68,d0
	beq	is_f0
	rts
;************************************
;ALTERNATE ABWICKLUNG
;************************************
is_alternate
	moveq	#2,d1
	bsr	get
	swap	d0
	cmp.b	#31,d0
	beq	screen_toggle
	cmp.b	#19,d0
	beq	config_rs232
	cmp.b	#21,d0
	beq	zmodem_send
	cmp.b	#$2d,d0
	beq.s	zmodem_receive
	cmp.b	#25,d0
	beq	protokoll
	rts
;************************************
zmodem_receive
	Pexec	($0).w,rz_cmd(pc),rz_prg(pc),#0
	tst	d0
	bmi.s	.error
	rts
.error
	lea	zmodem_error(pc),a6
	bsr	cconws
	lea	sz_prg(pc),a6
	bsr	cconws
	lea	ret(pc),a6
	bsr	cconws
	rts
;************************************
zmodem_send
	lea	zmodem_filename(pc),a6
	bsr	cconws
	Cconrs	#78,zmodem_file
	move.l	sz_base(pc),a0
	Pexec	($0).w,zmodem_file+1(pc),sz_prg(pc),#0
	tst	d0
	bmi.s	.error
	rts
.error
	lea	zmodem_error(pc),a6
	bsr	cconws
	lea	sz_prg(pc),a6
	bsr	cconws
	lea	ret(pc),a6
	bsr	cconws
	rts
;************************************
screen_toggle
	lea	screen(pc),a5
	Physbase
	move.l	d0,screen
	Setscreen	#-1,(a5),(a5)
	rts
;************************************
config_rs232
	lea	config_str(pc),a6
	bsr	cconws
	rts
protokoll
	lea	protokoll_str(pc),a6
	bsr	cconws
	tst.b	protokoll_flag
	seq	protokoll_flag
	bne	close_protokoll_file
open_protokoll_file
	lea	protokoll_on(pc),a6
	bsr	cconws
	lea	protokoll_gr��e(pc),a6
	bsr	cconws
	lea	hex_zahl(pc),a0
	bsr	read_hex
	lea	protokoll_size(pc),a1
	addq	#2,a0
	moveq	#10,d0
	bsr	ascii_hex
	tst.l	(a1)
	beq.s	.weiter
	lea	file_name_str(pc),a6
	bsr	cconws
	Cconrs	#12,file_name
	lea	ret(pc),a6
	bsr	cconws
	tst.b	file_name+1
	beq.s	.weiter
	Malloc	protokoll_size(pc)
	move.l	d0,adresse
	bmi.s	.error
	move.l	adresse(pc),a5
	clr.l	count
	rts
.weiter
	clr.b	protokoll_flag
	lea	protokoll_in_error(pc),a6
	bsr	cconws
	rts
.error
	lea	malloc_fehler_str(pc),a6
	bsr	cconws
	rts
close_protokoll_file
	lea	protokoll_off(pc),a6
	bsr	cconws
	Fcreate	#0,file_name+2(pc)
	move.w	d0,handle
	move.l	adresse,a0
	Fwrite	(a0),count,handle
	Fclose	handle
	rts
forced_close_protokoll_file
	clr.b	protokoll_flag
	lea	protokoll_is_full(pc),a6
	bsr	cconws
	bra.s	close_protokoll_file

;************************************
is_lmb
	rts
;************************************
is_rmb
	rts
;************************************
is_undo
	Fclose	handle(pc)
	Pterm0
;************************************
is_help
	lea	help_string(pc),a6
	bsr	cconws
	moveq	#$d,d0
	moveq	#2,d1
	bsr	put
	moveq	#$a,d0
	moveq	#2,d1
	bsr	put
	rts
;************************************
is_f1
	rts
;************************************
is_f2
	rts
;************************************
is_f3
	rts
;************************************
is_f4
	rts
;************************************
is_f5
	rts
;************************************
is_f6
	rts
;************************************
is_f7
	rts
;************************************
is_f8
	rts
;************************************
is_f9
	rts
;************************************
is_f0
	rts
;ENDE DER ALTERNATE-SECTION
;************************************
;************************************


;************************************
;TEST, OB EIN ZEICHEN AN DER RS232 ANLIEGT
;************************************
test_rs
	moveq	#1,d1
	bsr.s	status
	bmi.s	.is_char
	rts
.is_char
	moveq	#1,d1
	bsr.s	get
	moveq	#2,d1
	bsr.s	put
	rts
;************************************
;************************************


;************************************
;SUBROUTINEN
;************************************
cconws
	bsr	screen_toggle
	Cconws	(a6)
	bsr	screen_toggle
	rts
;************************************
status
	Bconstat	d1
	tst.l	d0
	rts
;************************************
get
	Bconin	d1
	rts
;************************************
put
	Bconout	d0,d1
	rts
;************************************

;************************************
;INCLUDES
;************************************
	include	sub\read_hex.s
	include	sub\asc_hex.s

;************************************
;DATA
;************************************
	data
hallo	dc.b	27,'EDies ist Mag_term von Thomas J�rges.',$d,$a
	dc.b	'Version 0.90	(Testversion, fast fertig)',$d,$a
	dc.b	"HELP f�r 'ne kurze Hilfe dr�cken.",$d,$a,$a
	dc.b	'Ich brauche wohl nicht zu erw�hnen, es ist Public Domain.',$d,$a,$a
	dc.b	'Dieses kleine Terminalprogramm soll nicht der absolute Hammer',$d,$a
	dc.b	'sein. Es ist nur ein klitzekleines Hilfsprogramm, um mal eben',$d,$a
	dc.b	"ein paar Daten durch's Telefon zu schicken.",$d,$a,$a,0
help_string	dc.b	'UNDO	Quit',$d,$a
	dc.b	'ALT-p	Protokoll ein/aus',$d,$a
	dc.b	'ALT-r	RS-232 Konfiguration',$d,$a
	dc.b	'ALT-x	ZModem empfangen ein/aus',$d,$a
	dc.b	'ALT-z	ZModem senden ein/aus',$d,$a,0
file_name_str	dc.b	$d,$a,'Filename: ',0
protokoll_gr��e	dc.b	$d,$a,'Maximale Gr��e des Protokolles in Bytes: $',0
protokoll_is_full	dc.b	$d,$a,27,'pProtokoll-Puffer ist voll. Speichere in das File ab...',27,'q',$d,$a,0
protokoll_str	dc.b	'Protokoll ist ',0
protokoll_off	dc.b	'aus.',$d,$a,0
protokoll_on	dc.b	'an.',$d,$a,0
protokoll_in_error	dc.b	'Fehler. Schalte Protokollfunktion aus.',$d,$a,0
malloc_fehler_str	dc.b	'Fehler bei der Speicherallokierung. Abbruch der Protokollfunktion!',$d,$a,0
ret	dc.b	$d,$a,0
zmodem_filename	dc.b	'Name des zu sendenden Files: ',0
zmodem_error	dc.b	'Kann es nicht finden: ',0
config_str	dc.b	'RS-232 Konfiguration:',$d,$a,0
rz_cmd	dc.b	sz_prg-rz_cmd-1,'',0

sz_prg	dc.b	'sz.prg',0
rz_prg	dc.b	'rz.prg',0
	copyright
;************************************
;BSS
;************************************
	bss
screen	ds.l	1
rz_base	ds.l	1
sz_base	ds.l	1
zmodem_file	ds.l	20
hex_zahl	ds.l	3
file_name	ds.l	4
adresse	ds.l	1
count	ds.l	1
protokoll_size	ds.l	1
char_in	ds.l	1
char_out	ds.w	1
handle	ds.w	1
protokoll_flag	ds.b	1
	even
my_screen	ds.l	8128
