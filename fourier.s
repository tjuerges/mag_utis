	include	tos_fnkt	;meine geliebten macros

;sollte mal eine fourier-analyse bzw. -synthese werden.
;es hat allerdings noch nie richtig funktioniert!

	jmp	(begin).l

sample	incbin	sample
ende

nn	=	ende-sample
nnhalb	=	nn/2
nnmal2	=	nn*2
logn	=	7

begin
	init	stack
start
	lea	satz1,a1
	bsr	menue
	bsr	tastin
	cmp.b	#'a',d0
	bcc.s	istgross
	add.b	#'a'-'A',d0
istgross
	cmp	#'e',d0
	beq	emessung
	cmp	#'h',d0
	beq	hintransf
	cmp	#'r',d0
	beq	ruecktran
	cmp	#'f',d0
	beq	continue
	cmp	#'a',d0
	beq	ausgabe
	cmp	#'q',d0
	beq	quit
	bra.s	start

quit
	Pterm0

menue
	bsr	clbild
	Cconws	(a1)
	rts

read_file

	move.l	datenre,a0
	move.l	sample,a1
	clr.l	d1
	move.l	#nn-1,d0
.loop
	move.b	(a1)+,d1
	move	d1,(a0)+
	dbf	d0,.loop
	rts

continue
	lea	satz3,a1
	bsr.s	menue
conti
	bsr.s	messung
	bsr	transform
	bsr	betrag
	bsr	ausgabe
	bsr	keypress
	tst.b	d0
	beq.s	conti
	bsr	tastin
	move	#2,datentyp
	bra	start

hintransf
	cmp	#1,datentyp
	bne	start
	move	#0,vorrueck
	bsr	transform
	bsr	betrag
	move	#2,datentyp
	bra	start

ruecktran
	cmp	#2,datentyp
	bne	start
	lea	satz2,a1
	bsr	menue
	bsr	liesint
	clr	d4
	bsr	loeschek
	bsr	liesint
	move	d2,d4
	move	#nn,d2
	bsr	loeschek
	move	#1,vorrueck
	bsr	transform
	bsr	normiere
	move	#1,datentyp
	bra	start

liesint
	clr.l	d2
wdh
	bsr	tastin
	cmp.b	#$d,d0
	beq	zahlin
	sub	#$30,d0
	bmi.s	wdh
	cmp	#9,d0
	bcc.s	wdh
	mulu	#10,d2
	add	d0,d2
	add	#$30,d0
	bsr	asciiout
	bra.s	wdh
zahlin
	divu	#25,d2
	asl	#1,d2
	move	#8,d1
	bsr	prtmsp
	rts

loeschek
	lea	datenre,a0
	lea	(datenim).l,a1
nextclr
	move	d4,d5
	neg	d5
	add	#nnmal2-2,d5
	clr	(a0,d4)
	clr	(a1,d4)
	clr	(a0,d5)
	clr	(a1,d5)
	addq	#2,d4
	cmp	d2,d4
	bcs.s	nextclr
	rts

ausgabe
;	move	#0,daout
	lea	(datbetr).l,a0
	move.l	a0,a1
	add.l	#nn,a1
showit
;	move.b	#$40,opset
	move	#9,d1
high
;	move	(a0)+,daout
	cmp.l	a1,a0
	dbcc	d1,high
;	move.b	#$40,opreset
	move	#49,d1
low
;	move	(a0)+,daout
	cmp.l	a1,a0
	dbcc	d1,low
	cmp.l	a0,a1
	bcc.s	showit
;	move	#$ff00,daout
	rts

normiere
	lea	datenre,a0
	lea	(datenim).l,a1
nextnorm
	move	(a0),d0
	asr	#8,d0
	move	d0,(a0)+
	cmp.l	a0,a1
	bne.s	nextnorm
	rts

shuffle
	clr	d2
	move	#nnhalb,d5
	move	#1,d6
	move.b	#logn,d4
shuff1
	move	d0,d1
	and	d5,d1
	asr	d4,d1
	or	d1,d2
	asr	#1,d5
	move	d0,d1
	and	d6,d1
	asl	d4,d1
	or	d1,d2
	asl	#1,d6
	subq.b	#2,d4
	bpl.s	shuff1
	rts

transform
	lea	datenre,a0
	lea	(datenim).l,a1
	moveq	#1,d0
ford0
	bsr.s	shuffle
	cmp	d0,d2
wird
	bls	notausch
	move	d0,d1
	asl	#1,d1
	asl	#1,d2
	move	(a0,d1),d3
	move	(a0,d2),(a0,d1)
	move	d3,(a0,d2)
	move	(a1,d1),d3
	move	(a1,d2),(a1,d1)
	move	d3,(a1,d2)
notausch
	addq	#1,d0
	cmp	#nn,d0
	bcs.s	ford0

ffttrans
	lea	datenre,a0
	lea	(datenim).l,a1
	lea	sinus,a2
	moveq	#1,d0
while
	cmp	#nn,d0
	bcc	trend
	clr	d1
form
	move	#804,d5
	muls	d1,d5
	divs	d0,d5
	clr	d4
	cmp	#402,d5
	bcs	intab
	sub	#804,d5
	neg	d5
	moveq	#1,d4
intab
	asl	#1,d5
	move	(a2,d5),wi
	cmp	#1,vorrueck
	beq	minsin
	neg	wi
minsin
	sub	#804,d5
	neg	d5
	move	(a2,d5),wr
	btst	#0,d4
	beq	intab1
	neg	wr
intab1
	move	d1,d2
repeat1
	move	d2,d3
	add	d0,d3
	asl	#1,d3
	move	(a0,d3),d4
	move	(a1,d3),d5
	move	d4,d6
	muls	wr,d6
	move	d5,d7
	muls	wi,d7
	sub.l	d7,d6
	asr.l	#8,d6
	move	d6,tr
	muls	wr,d5
	muls	wi,d4
	add.l	d4,d5
	asr.l	#8,d5
	move	d5,ti
	asl	#1,d2
	move	(a0,d2),d4
	move	(a1,d2),d5
	move	d4,d6
	move	d5,d7
	sub	tr,d6
	sub	ti,d7
	move	d6,(a0,d3)
	move	d7,(a1,d3)
	add	tr,d4
	add	ti,d5
	move	d4,(a0,d2)
	move	d5,(a1,d2)
	asr	#1,d2
	add	d0,d2
	add	d0,d2
	cmp	#nn,d2
	bcs.s	repeat1
	addq	#1,d1
	cmp	d1,d0
	bhi	form
	asl	#1,d0
	bra	while
trend
	rts

betrag
	lea	datenre,a0
	lea	(datenim).l,a1
	lea	(datbetr).l,a2
	clr	d0
nextdate
	clr.l	d1
	move	(a0,d0),d1
	muls	d1,d1
	clr.l	d2
	move	(a1,d0),d2
	muls	d2,d2
	add.l	d2,d1
	move.l	d1,d3
	beq	istnull
	add.l	#$10000,d3
	asr.l	#1,d3
repxly
	move.l	d3,d2
	move.l	d1,d4
	asr.l	#8,d3
	divu	d3,d4
	and.l	#$ffff,d4
	asl.l	#8,d4
	add.l	d2,d4
	asr.l	#1,d4
	move.l	d4,d3
	cmp.l	d2,d3
	bcs.s	repxly
	asr.l	#8,d3
istnull
	move	d3,(a2,d0)
	addq	#2,d0
	cmp	#nn,d0
	bcs.s	nextdate
	rts

keypress
	Cconis
	rts
asciiout
	and.l	#$ff,d0
	Cconout	d0
	rts
crout
	move.b	#$d,d0
	bsr.s	asciiout
	move.b	#$a,d0
	bsr.s	asciiout
	rts
tastin
	Cconin
	and.l	#$ff,d0
	rts
clbild
	move.b	#27,d0
	bsr.s	asciiout
	move.b	#'E',d0
	bsr.s	asciiout
	rts
prtmsp
	move.b	#' ',d0
.loop
	bsr.s	asciiout
	dbf	d1,.loop
	rts

	data
sinus	dc.w	0,1,2,3,4,5,6,7,8,9,$a,$b,$c,$e,$f
	dc.w	$10,$11,$12,$13,$14,$15,$16,$17
	dc.w	$18,$19,$1a,$1b,$1c,$1d,$1e,$1f
	dc.w	$20,$21,$22,$23,$24,$25,$26,$27
	dc.w	$28,$29,$2a,$2b,$2c,$2d,$2e,$2f
	dc.w	$30,$31,$32,$33,$34,$35,$36,$37
	dc.w	$38,$39,$3a,$3a,$3b,$3c,$3d,$3e
	dc.w	$3f,$40,$41,$42,$43,$44,$45,$46
	dc.w	$47,$48,$49,$4a,$4b,$4c,$4d,$4e
	dc.w	$4f,$50,$51,$52,$53,$53,$54,$55
	dc.w	$56,$57,$58,$59,$5a,$5b,$5c,$5d
	dc.w	$5e,$4f,$60,$61,$61,$62,$63,$64
	dc.w	$65,$66,$67,$68,$69,$6a,$6b,$6c
	dc.w	$6c,$6d,$6e,$6f,$70,$71,$72,$73
	dc.w	$74,$75,$75,$76,$77,$78,$79,$7a
	dc.w	$7b,$7c,$7c,$7d,$7e,$7f,$80,$81
	dc.w	$82,$83,$83,$84,$85,$86,$87,$88
	dc.w	$89,$89,$8a,$8b,$8c,$8d,$8e,$8e
	dc.w	$8f,$90,$91,$92,$93,$93,$94,$95
	dc.w	$96,$97,$97,$98,$99,$9a,$9b,$9b
	dc.w	$9c,$9d,$9e,$9f,$9f,$a0,$a1,$a2
	dc.w	$a2,$a3,$a4,$a5,$a6,$a6,$a7,$a8
	dc.w	$a9,$a9,$aa,$ab,$ac,$ac,$ad,$ae
	dc.w	$ae,$af,$b0,$b1,$b1,$b2,$b3,$b4
	dc.w	$b4,$b5,$b6,$b6,$b7,$b8,$b8,$b9
	dc.w	$ba,$bb,$bb,$bc,$bd,$bd,$be,$bf
	dc.w	$bf,$c0,$c1,$c1,$c2,$c3,$c3,$c4
	dc.w	$c4,$c5,$c6,$c6,$c7,$c8,$c8,$c9
	dc.w	$ca,$ca,$cb,$cb,$cc,$cd,$cd,$ce
	dc.w	$ce,$cf,$d0,$d0,$d1,$d1,$d2,$d2
	dc.w	$d3,$d4,$d4,$d5,$d5,$d6,$d6,$d7
	dc.w	$d7,$d8,$d8,$d9,$da,$da,$db,$db
	dc.w	$dc,$dc,$dd,$dd,$de,$de,$df,$df
	dc.w	$e0,$e0,$e1,$e1,$e2,$e2,$e2,$e3
	dc.w	$e3,$e4,$e4,$e5,$e5,$e6,$e6,$e7
	dc.w	$e7,$e7,$e8,$e8,$e9,$e9,$ea,$ea
	dc.w	$ea,$eb,$eb,$ec,$ec,$ec,$ed,$ed
	dc.w	$ed,$ee,$ee,$ef,$ef,$ef,$f0,$f0
	dc.w	$f0,$f1,$f1,$f1,$f2,$f2,$f2,$f3
	dc.w	$f3,$f3,$f4,$f4,$f4,$f4,$f5,$f5
	dc.w	$f5,$f6,$f6,$f6,$f6,$f7,$f7,$f7
	dc.w	$f8,$f8,$f8,$f8,$f8,$f9,$f9,$f9
	dc.w	$f9,$fa,$fa,$fa,$fa,$fb,$fb,$fb
	dc.w	$fb,$fb,$fb,$fc,$fc,$fc,$fc,$fc
	dc.w	$fd,$fd,$fd,$fd,$fd,$fd,$fd,$fe
	dc.w	$fe,$fe,$fe,$fe,$fe,$fe,$fe,$fe
	dc.w	$fe,$ff,$ff,$ff,$ff,$ff,$ff,$ff
	dc.w	$ff,$ff,$100,$100,$100,$100,$100,$100
	dc.w	$100,$100,$100,$100,$100,$100,$100,$100
	dc.w	$100,$100,$100


satz1	dc.b	'Fast Fourier Transformation mit 256 Punkten',$d,$a
	dc.b	'A-nalyse',$d,$a
	dc.b	'S-ynthese',$d,$a
	dc.b	'R-Sample von Disk einlesen',$d,$a
	dc.b	'D-ruckerausgabe',$d,$a
	dc.b	'Q-uit',$d,$a,0
satz2	dc.b	'R�cktransformation der Fourier-Komponenten',$d,$a
	dc.b	'Frequenzbereich (0-3200 Hz): ',0
	even

	bss
tr	ds.w	1
ti	ds.w	1
wr	ds.w	1
wi	ds.w	1
datentyp	ds.w	1
vorrueck	ds.w	1
datenre	ds.w	nn
datenim	ds.w	datenim-datenre+nnmal2
datbetr	ds.w	datenim-datenre+nnmal2
