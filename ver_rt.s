	opt	a+,c+,o+,w+
	include	include\tos_fnkt

;eine demonstration, welche geschwindigkeitsunterschiede sich
;mit und ohne blitterbenutzung ergeben.
;hier vertikales bildschirmscrolling ohne blitter

	Supexec	start(pc)
	Pterm0
start:
	Physbase

	move.l	sp,stack
	move.l	d0,sp
	add	#80,sp
	move.l	usp,a0
	move.l	a0,user
	move.l	sp,usp

	move.w	#400,rettung
	move.w	#$2700,sr
page_loop:
	move.l	usp,sp

	REPT	(32000-80)/60
	movem.l	(sp)+,d0-a6
	movem.l	d0-a6,-140(sp)
	ENDR

	subq	#1,rettung
	bne	page_loop

	move.l	stack(pc),sp
	move.l	user,a0
	move.l	a0,usp
	move.w	#$2300,sr
	rts

stack	ds.l	1
screen	ds.l	1
user	ds.l	1
rettung	ds.w	1