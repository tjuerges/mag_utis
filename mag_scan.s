  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	;include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	d:\basic\mag_scan.inl	;So hei�t das PRG-File.
max_fnkt	=	255

dispatcher:
	movem.l	d1-a6,-(sp)
	moveq	#0,d0
	move	60(sp),d0
	cmp	#max_fnkt,d0
	bhs.s	return
	lsl.l	#2,d0
	lea	sprung_tabelle(pc),a0
	lea	erste_funktion(pc),a6
	adda.l	(a0,d0.l),a6
	jsr	(a6)
return:
	movem.l	(sp)+,d1-a6
	rts

sprung_tabelle:
	dc.l	0,test2-erste_funktion

supexec:
	Supexec	(a6)
	rts

erste_funktion:
	lea	.test(pc),a6
	bra.s	supexec
.test
	not	$ffff8240.w
	rts
test2:
	rts
