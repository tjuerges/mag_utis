	include	tos_fnkt

;liest das tos aus und speichert es unreloziert auf disk ab.

start
	init	stack(pc)
	Supexec	begin(pc)
	Pterm0

begin
	move.l	#(192*256)-1,d0
	lea	($fc0000).l,a0
	lea	buffer(pc),a1
loop
	move.l	(a0)+,(a1)+
	dbf	d0,loop
write_it
	Fcreate	#0,filename(pc)
	move.w	d0,handle
	bmi.s	error
	Fwrite	buffer(pc),#192*1024,handle(pc)
	tst.l	d0
	bmi.s	error
	Fclose	handle(pc)
error
	rts

	data
filename	dc.b	'KAOS.IMG',0
	bss
handle	ds.w	1
buffer	ds.l	192*256
