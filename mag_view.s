	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	output	f:\mag_utis\mag_view.ttp

;zeigt degas-, doodle- oder mag-crunch-bilder an.

start:
	move.l	4(sp),a5
	init	stack(pc)
	move.l	a5,a0
	add.l	#$81,a0
	lea	name(pc),a4
	tst.b	(a0)
	beq	written_by
	moveq	#0,d0
	bsr	cursconf
	clr.l	buffer
loop:
	tst.b	(a0)
	beq.s	fertig
	move.b	(a0)+,(a4)+
	bra.s	loop
fertig:
	Fsfirst	#0,name(pc)
	tst	d0
	bne	open_error
	Fgetdta
	move.l	d0,a1
	move.l	$1a(a1),length
	cmp.l	#32000,length
	bne	kein_pic
	bsr	fopen	
	move	d0,handle
	bmi	open_error
return:
	Logbase
	move.l	d0,screen
	lea	bild(pc),a0
	move.l	#32000,d0
	bsr	fread
	bsr	fclose
	bsr	main
	bsr	cnecin
	tst.b	col_flag
	beq.s	set_scr
	moveq	#0,d2
	moveq	#15,d1
	lea	old_palette(pc),a5
old_pal_loop:
	move	(a5)+,d0
	bsr	setcolor
	addq	#1,d2
	dbf	d1,old_pal_loop
set_scr:
	tst.b	flag
	beq.s	open_error
	move	old_res(pc),d0
	bsr	setscreen	
free_error:
open_error:
close_error:
	moveq	#1,d0
	bsr	cursconf
	bsr	mfree
	Pterm0
written_by:
	Cconws	blabla(pc)
	bsr	cnecin
	bra.s	close_error
kein_pic:
	lea	name(pc),a0
.loop
	tst.b	(a0)+
	bne.s	.loop
	subq.l	#4,a0
	cmp.b	#'M',(a0)
	beq.s	kann_sein
	cmp.b	#'m',(a0)
	bne weiter
kann_sein:
	cmp.b	#'C',1(a0)
	beq.s	kann_sein2
	cmp.b	#'c',1(a0)
	bne weiter
kann_sein2:
	cmp.b	#'R',2(a0)
	beq.s	ist_es
	cmp.b	#'r',2(a0)
	bne	weiter
ist_es:
	bsr	fopen	
	move	d0,handle
	beq.s	open_error
	move.l	length(pc),d0
	Malloc	d0
	move.l	d0,buffer
	beq	open_error
	move.l	buffer(pc),a0
	move.l	length(pc),d0
	bsr	fread
start_iff_decrunch
;a1=sourcebuffer;a2=objectbuffer
;d4=objectfile length
	bsr.s	clear_regs
iff_decrunch_init
	move.l	buffer(pc),a1
	lea	bild(pc),a2
	moveq	#0,d1
	lea	header(pc),a3
	moveq	#2,d7
.loop
	cmpm.l	(a3)+,(a1)+
	bne	free_error
	dbf	d7,.loop

	move.l	(a1)+,d4
	cmp.l	#32000,d4
	bne	open_error
	moveq	#0,d7
start_iff_de
	tst.l	d4
	beq	einsprung1
	bmi	einsprung1
	moveq	#0,d0
	addq.l	#1,d7
	cmp.l	#32000,d7
	beq	einsprung1
	move.b	(a1)+,d0
	bpl.s	iff_de_eq
	not.b	d0
iff_de_uneq
	move.b	(a1)+,(a2)+
	subq.l	#1,d4
	dbf	d0,iff_de_uneq
	bra.s	start_iff_de
iff_de_eq
	subq.b	#1,d0
	move.b	(a1)+,d1
.loop2
	move.b	d1,(a2)+
	subq.l	#1,d4
	dbf	d0,.loop2
	bra.s	start_iff_de
clear_regs
	moveq	#0,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	moveq	#0,d7
	sub.l	a0,a0
	sub.l	a1,a1
	sub.l	a2,a2
	sub.l	a3,a3
	sub.l	a4,a4
	sub.l	a5,a5
	sub.l	a6,a6
	rts
einsprung1:
	bsr	getrez
	move	d0,res
	bra	einsprung
weiter:
	bsr	fopen
	move	d0,handle
	beq	open_error
	lea	res(pc),a0
	moveq	#2,d0
	bsr	fread
	lea	palette(pc),a0
	moveq	#32,d0
	bsr	fread
	tst.l	d0
	bmi	open_error
einsprung:
	move.b	#1,flag
	bsr	getrez
break:
	move	d0,old_res
	moveq	#16,d1
	clr.l	d2
	move	res(pc),d2
	beq.s	test
	addq	#1,d2
test:
	lsr	d2,d1
	move	d1,loop_wert
	move	res(pc),d0
	bsr	setscreen	
	moveq	#0,d2
	moveq	#15,d1
	lea	old_palette(pc),a0
	lea	palette(pc),a5
palette_loop:
	moveq	#-1,d0
	bsr	setcolor
	move	d0,(a0)+
	move	(a5)+,d0
	bsr  	setcolor
	addq	#1,d2
	dbf	d1,palette_loop
	move.b	#1,col_flag
	bra	return
main:
	move.l	screen(pc),a6
	move.l	#399,d3
.loop:
	bsr.s	zeige_zeile
	dbf	d3,.loop
	bra.s	ende2
ende:
	addq.l	#4,sp
ende2:
	rts

zeige_zeile:
	move.l	d3,d1
	mulu	#80,d1
	move.l	a6,a5
	move.l	a6,a4
	add.l	d1,a5

	lea	bild(pc),a0
	add.l	d1,a0
	move.l	d3,d1
.loop2:
	moveq	#19,d6
.loop:
	move.l	(a0)+,(a4)+
	dbf	d6,.loop
	Bconstat	#2
	tst.b	d0
	bne.s	ende
	bsr.s	clr_line
	lea	-80(a0),a0
	dbf	d1,.loop2

	moveq	#19,d6
	lea	-80(a0),a0
.loop3:
	move.l	(a0)+,(a5)+
	dbf	d6,.loop3
	rts

clr_line:
	moveq	#19,d5
	lea	-80(a4),a4
.loop
	clr.l	(a4)+
	dbf	d5,.loop
	rts
setscreen:
	Setscreen	d0,#-1,#-1
	rts
setcolor:
	Setcolor	d0,d2
	rts
fread:
	Fread	(a0),d0,handle(pc)
	rts
cnecin:
	Cnecin
	rts
getrez:
	Getrez
	rts
cursconf:
	Cursconf	d0
	rts
fopen:
	Fopen	#0,name(pc)
	rts
fclose:
	Fclose	handle(pc)
	rts
mfree:
	tst.l	buffer
	beq.s	.nix
	Mfree	buffer(pc)
.nix
	rts

	data
header:	dc.b	'MAG-CRUNV1.0'
blabla:	dc.b	$d,$a,'MAG-VIEW, (c) Magnum -BlackBox-.',$d,$a
	dc.b	'Es werden entweder ungepackte',$d,$a
	dc.b	'Degas-Elite Bilder, Bilder im',$d,$a
	dc.b	'Doodle-Format oder Bilder,',$d,$a
	dc.b	'die mit MAG-CRUNCH V1.0 ge-',$d,$a
	dc.b	'cruncht wurden, angezeigt.',$d,$a,0
	copyright
 	bss
handle:	ds.w	1
res:	ds.w	1
old_res:	ds.w	1
loop_wert:	ds.w	1
old_palette:	ds.l	8
palette:	ds.l	8
screen:	ds.l	1
length:	ds.l	1
buffer:	ds.l	1
col_flag:	ds.b	1
flag:	ds.b	1
name:	ds.l	64
bild:	ds.l	8000
