	include	tos_fnkt

;eine line-routine von magnum. entnommen aus:
;david f. rogers 'procedural elements for computer graphics'
;besonderheit: schnell. dies wird hier demonstriert.
OR	=	0
XOR	=	1
CLEAR	=	0

	init	stack
start
	Logbase
	move.l	d0,a6
	Supexec	loop(pc)
	Pterm0
loop
	bsr	rand
	move	d0,d3
	divu	#399,d3
	swap	d3
	and.l	#$ffff,d3
	bsr	rand
	move	d0,d1
	divu	#399,d1
	swap	d1
	and.l	#$ffff,d1
	bsr	rand
	move	d0,d2
	divu	#639,d2
	swap	d2
	and.l	#$ffff,d2
	bsr	rand
	divu	#639,d0
	swap	d0
	and.l	#$ffff,d0
	bsr.s	line
	Cconis
	tst	d0
	beq.s	loop
	Cconin
	rts

;Dieser Linealgorithmus berechnet und zeichnet eine Linie nach dem
;'Generalized integer Bresenham's algorithm'.
;Es werden erwartet: d0=X1, d1=Y1, d2=X2, d3=Y2
line
line_regs	reg	d0-d7/a0
	movem.l	line_regs,-(sp)

	moveq	#0,d5
	moveq	#0,d7

	cmp	d0,d2
	bhi.s	.x2_gr_x1
	bne.s	.weiter
	cmp	d1,d3
	bne.s	.weiter
	;HIER FEHLT DIE PLOTROUTINE,
	;DA EIN PIXEL GEZEICHNET WERDEN M��TE.
	movem.l	(sp)+,line_regs
	rts
.weiter
	or	#$ff00,d5
	move	d0,d6
	sub	d2,d6
	bra.s	.weiter_dx
.x2_gr_x1
	or	#$0100,d5
	move	d2,d6
	sub	d0,d6
.weiter_dx
	move	d6,d2

	cmp	d1,d3
	bhi.s	.y2_gr_y1
	or.b	#$ff,d5
	move	d1,d6
	sub	d3,d6
	bra.s	.weiter_dy
.y2_gr_y1
	or.b	#$01,d5
	move	d3,d6
	sub	d1,d6
.weiter_dy
	move	d6,d3

	cmp	d2,d6
	blo.s	.weiter_dx_dy
	move	d2,d3
	move	d6,d2
	moveq	#1,d7
.weiter_dx_dy

	move	d3,d6
	add	d6,d6
	sub	d2,d6
	move	d6,d4

	swap	d7
	move	d2,d7
	add	d2,d2	;dx*2
	add	d3,d3	;dy*2
;Plotroutine, die die ST-High-Aufl�sung benutzt.
;Erwartet werden: d0=X, d1=Y, a6=SCREEN
.plot
plot_regs	reg	d2-d3
	movem.l	plot_regs,-(sp)
	move	d0,d2
	lsr	#4,d2
	and	#15,d0
	eor	#15,d0
	move	d1,d3
	lsl	#6,d1
	lsl	#4,d3
	add	d3,d1
	add	d2,d2
	add.l	d1,d2
	lea	(a6,d2.l),a0
	moveq	#0,d1
	bset	d0,d1
	movem.l	(sp)+,plot_regs
	move	d5,d0	;d0.w=s1

	moveq	#80,d5
	tst.b	d0
	beq.s	.d5_positiv
	moveq	#-80,d5
.d5_positiv

	btst	#16,d7
	beq.s	for_flach

for_steil
	eor	d1,(a0)
	;and	d1,(a0)
	;or	d1,(a0)
.while
	tst	d4
	bmi.s	.end_if

	tst	d0
	bmi.s	.s1_neg
	ror	#1,d1
	bcc.s	.weiter_i
	addq	#2,a0
	bra.s	.weiter_i
.s1_neg
	rol	#1,d1
	bcc.s	.weiter_i
	subq	#2,a0
.weiter_i
	sub	d2,d4
.end_if

	add.l	d5,a0

	add	d3,d4

	dbf	d7,for_steil
.line_ende
	movem.l	(sp)+,line_regs
	rts

for_flach
	eor	d1,(a0)
	;and	d1,(a0)
	;or	d1,(a0)
.while
	tst	d4
	bmi.s	.end_if

	add.l	d5,a0

	sub	d2,d4
.end_if

	tst	d0
	bmi.s	.s1_neg2
	ror	#1,d1
	bcc.s	.weiter_i2
	addq.l	#2,a0
	bra.s	.weiter_i2
.s1_neg2
	rol	#1,d1
	bcc.s	.weiter_i2
	subq.l	#2,a0
.weiter_i2

	add	d3,d4

	dbf	d7,for_flach
.line_ende
	movem.l	(sp)+,line_regs
	rts
rand	include	rand.s