	opt	a+,c+,o+,w+
	include	include\tos_fnkt

;der screen kann rauf und runter gescrollt werden.

start
	Physbase
	move.l	d0,p_base

main_loop
	Bconin

	swap	d0
	cmp.b	#$10,d0
	beq.s	ende
	cmp.b	#$4d,d0
	beq.s	rauf
	cmp.b	#$4b,d0
	beq.s	runter
	cmp.b	#$48,d0
	beq.s	d_rauf
	cmp.b	#$50,d0
	beq.s	d_runter
	bra.s	main_loop

ende
	Setscreen #-1,p_base(pc),#-1

	Pterm0

d_rauf
	add.l	#$7c00,physbase
	bra.s	rauf
d_runter
	sub.l	#$7c00,physbase
	bra.s	runter
rauf
	add.l	#256,physbase
	bsr.s	setscreen
	bra.s	main_loop
runter
	sub.l	#256,physbase
	bsr.s	setscreen
	bra	main_loop
setscreen
	Setscreen	#-1,physbase(pc),#-1
	rts

	bss
p_base	ds.l	1
physbase	ds.l	1

