	opt	a+,c+,o+,w+
	include	include\gem
	include	include\tos_fnkt

;eine weitere demo, die ein fenster �ffnet und nur darauf wartet,
;dass das fenster wieder geschlossen wird.

	init	stack(pc)
start
	init_gem	#2
	graf_mouse	#0
	wind_get	#0,#4
wtype	equ	%000000101111
	movem	int_out+2(pc),d0-d3
	wind_calc	#wtype,#1,d0,d1,d2,d3
	movem	int_out+2(pc),d0-d3
	movem	d0-d3,xstart
	wind_create	#wtype,d0,d1,d2,d3,w_handle
	wind_set	w_handle,#2,#windowname
	movem	xstart(pc),d0-d3
	graf_growbox	#0,#0,#0,#0,d0,d1,d2,d3
	movem	xstart(pc),d0-d3
	add	#10,d0
	add	#10,d1
	sub	#20,d2
	sub	#20,d3
	wind_open	w_handle,d0,d1,d2,d3
	bsr	recalcwindow
	bsr	setupmode
	bsr	drawwindow
waitforevent
	lea	messagebuf(pc),a0
	clr.l	(a0)
	clr.l	4(a0)
	clr.l	8(a0)
	clr.l	12(a0)
break
	evnt_multi	#MU_MESAG!MU_TIMER,,,,,,,,,,,,,,a0,#1000
	lea	messagebuf(pc),a0
	move	(a0),d0
	cmp	#20,d0
	beq	updateit
	cmp	#22,d0
	beq	quit
	cmp	#23,d0
	beq	fullit
	cmp	#27,d0
	beq	sizeit
	cmp	#28,d0
	beq	moveit

	lea	messagebuf(pc),a0
	Random
	and.l	#$ffff,d0
	divu	#640,d0
	move	d0,8(a0)
	Random
	and.l	#$ffff,d0
	divu	#400,d0
	add	#30,d0
	move	d0,10(a0)
	Random
	and.l	#$ffff,d0
	divu	#640,d0
	swap	d0
	move	d0,d2
	Random
	and.l	#$ffff,d0
	divu	#400,d0
	swap	d0
	move	d0,d1
	move	d2,d0
	bra	jump

moveit	move	6(a0),d0
	cmp	w_handle(pc),d0
	bne	waitforevent
changedwindow
	move	12(a0),d0
	cmp	#40,d0
	bls.s	.wok
	moveq	#40,d0
.wok
	move	14(a0),d1
	cmp	#50,d1
	bls.s	jump
	moveq	#50,d1
jump
	wind_set	w_handle,#WF_CURRXYWH,8(a0),10(a0),d0,d1
	movem	xwidth(pc),d4-d5
	bsr	recalcwindow
	cmp	xwidth(pc),d4
	bcs	waitforevent
	cmp	ywidth(pc),d5
	bcs	waitforevent
	bne.s	forceupdate
	cmp	xwidth(pc),d4
	beq	waitforevent
forceupdate
	bsr	drawwindow
	bra	waitforevent
sizeit	move	6(a0),d0
	cmp	w_handle(pc),d0
	bne	waitforevent
	bra.s	changedwindow
updateit
	move	6(a0),d0
	cmp	w_handle(pc),d0
	bne	waitforevent
	movem	8(a0),d0-d3
hier
	add	d0,d2
	add	d1,d3
	vs_clip	#1,d0,d1,d2,d3
	bsr	drawwindow
	bra	waitforevent
fullit
	Malloc	#-1
	move.l	d0,hex_zahl
	lea	hex_zahl(pc),a0
	lea	buffer(pc),a1
	moveq	#4,d0
	bsr	hex_ascii
	form_alert	#1,#myalert
	bra	waitforevent

	include	sub\hex_asc.s

quit	movem	xstart(pc),d0-d3
	graf_shrinkbox	#0,#0,#0,#0,d0,d1,d2,d3
	wind_close	w_handle
	wind_delete	w_handle
	exit_gem
	Pterm0
recalcwindow
	wind_get	w_handle,#WF_WORKXYWH
	movem	int_out+2(pc),d0-d3
	movem	d0-d3,xstart
	rts
drawwindow
	v_hide_c
	vsf_interior	#2
	movem	xstart(pc),d0-d3
	add	d0,d2
	subq	#1,d2
	add	d1,d3
	subq	#1,d3
	vr_recfl	d0,d1,d2,d3
	vsf_interior	#4
	movem	xstart(pc),d0-d3
	asr	#1,d2
	asr	#1,d3
	add	d2,d0
	add	d3,d1
	v_ellipse	d0,d1,d2,d3
	v_show_c	#0
	rts
setupmode
	lea	intin(pc),a0
	move.l	#%01101100010000000101010011100000,(a0)+
	move.l	#%01000101000100000000000000000000,(a0)+
	move.l	#%00110010010100100100001101010010,(a0)+
	move.l	#%01001010110100100011101001001100,(a0)+
	move.l	#%00001101100000000000101010000000,(a0)+
	move	#%0000100010000000,(a0)+
	clr	(a0)+
	clr.l	(a0)+
	clr.l	(a0)+
	vsf_udpat	#1
	vsf_color	#1
	rts

	include	include\call_gem

	data
windowname	dc.b	' Ein kleines GEM-Beispiel in Assembler! ',0

myalert	dc.b	'[1][ Freier Speicher: | $'
buffer	dc.b	30,30,30,30,30,30,30,30,' Bytes |'
	dc.b	' Programmiert mit: | DevpacST Version 2.00D |'
	dc.b	' vom Magnum. ][ OK ]',0

	bss
xstart	ds.w	1
ystart	ds.w	1
xwidth	ds.w	1
ywidth	ds.w	1

w_handle	ds.w	1
ws_handle	ds.w	1
ap_id	ds.w	1
messagebuf	ds.l	4

hex_zahl	ds.l	1
