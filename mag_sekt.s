	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	gem	;Vdi- & Aes-Macros einbinden.

	output	mag_sekt.acc	;So heisst das Programm sp�ter.

name	MACRO
	dc.b	'SEKTOR'
	ENDM

GEM_200	=	0

start
	lea	end_stack(pc),sp	;Als .ACC einen eigenen Stack.
	appl_init	;Applikation beim AES anmelden.
	bmi	fehler
	clr.b	flag
	cmp	#$0200,global
	blo.s	.no_unregister
	bset	#GEM_200,flag
.no_unregister
	tst.b	d7
	beq.s	.kein_kaos
	bset	#GEM_200,flag
.kein_kaos
	graf_handle
	menu_register	appl_id(pc),#mag_sekt_str,menu_id
	bmi	fehler

	lea	buffer(pc),a5
	moveq	#1,d1
	moveq	#2,d2
	move	#MU_MESAG,d3
	clr.l	d7
loop
	evnt_multi	d3,,,,,,,,,,,,,,,a5
	cmp	int_out(pc),d3	;War es ein MU_MESAG-Event?
	bne.s	loop	;Nein...

	move	menu_id(pc),d0
	cmp	8(a5),d0	;Ist die Menu-ID des betroffenen ACC meine?
	bne.s	loop	;Nee...
	cmp	#AC_OPEN,(a5)	;...
	bne.s	loop
auswahl
	lea	mag_sekt_alert(pc),a6
	btst	#GEM_200,flag(pc)
	beq.s	.kein_gem20
	lea	mag_sekt_alert_spezial(pc),a6
.kein_gem20
	form_alert	d1,a6	;Doch, dann...
	cmp	int_out(pc),d1	;Los geht's!
	beq.s	los
	btst	#GEM_200,flag(pc)	;Gem-Version >=2.00?
	beq.s	loop	;Nee.
	;Falls doch, dann gibt's n�mlich menu_unregister zum Abmelden
	;des ACC!
	cmp	int_out(pc),d2	;Mittlerer Knopf gedr�ckt?
	bne	loop	;Nee, also passiert gar nix.
	menu_unregister	menu_id(pc)
fehler
	moveq	#-1,d4
.loop	evnt_timer	d4
	bra.s	.loop

los
	Floprd	d1,d7,d7,d1,d7,sektor(pc)	;Sektor einlesen.

	lea	stars(pc),a0	;Bootprogramm kopieren und
	lea	sektor(pc),a1
	move	#$601c,(a1)	;den Branch einsetzen.
	lea	$1e(a1),a1
	move	#((stars_end-stars)/4)-1,d0
.loop	move.l	(a0)+,(a1)+
	dbf	d0,.loop

	lea	sektor(pc),a1	;Checksumme berechnen.
	lea	510(a1),a0
	move	#254,d0
	clr	d4
.chkloop
	add	(a1)+,d4
	dbf	d0,.chkloop
	move	#$1234,d0
	sub	d4,d0
	move	d0,(a0)

	Flopwr	d1,d7,d7,d1,d7,sektor(pc)	;Und wieder zur�ckschreiben.
	bra	auswahl

	include	call_gem	;AES- & VDI-Aufrufe einbinden.

	data
stars	incbin	'mag_star.bin'
	cnop	0,4
stars_end
mag_sekt_str	dc.b	'  Mag-Sektor',0
mag_sekt_alert	dc.b	'[2][MAG-'
	name
	dc.b	' - �Magnum/BlackBox-|Sternen-Bootsektor'
	dc.b	' installieren?][Ja|Abbruch]',0
mag_sekt_alert_spezial	dc.b	'[2][MAG-'
	name
	dc.b	' - �Magnum/BlackBox-|'
	dc.b	'Sternen-Bootsektor installieren?][Ja|Quit ACC|Abbruch]',0
	copyright

	bss
sektor	ds.l	128
buffer	ds.l	4
menu_id	ds.w	1
flag	ds.b	1
stack	ds.l	64
end_stack
