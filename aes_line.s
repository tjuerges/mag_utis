  	;opt	a+,c+,o+,w+,m+
	include	include\tos_fnkt
	include	include\gem

line_regs	reg	d0-d7
regs	reg	d0-d4/a0-a1/a6
plot_regs	reg	d0-d3
start
	bra	los
	dc.l	'XBRA','LINE'
old_trap	dc.l	0
new_trap
	cmp	#115,d0
	beq	vdi_trap
	move.l	old_trap(pc),-(sp)
	rts
vdi_trap
	movem.l	regs,-(sp)
	move.l	d1,a0
	move.l	(a0)+,a1
	cmp	#6,(a1)
	beq.s	linie
	movem.l	(sp)+,regs
	move.l	old_trap,-(sp)
	rts
linie
	moveq	#0,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	move	2(a1),d4
	subq	#2,d4
	move.l	4(a0),a0
	move.l	$44e.w,a6
	move	(a0)+,d0
	move	(a0)+,d1
	bra.s	einsprung
loop
	move	d2,d0
	move	d3,d1
einsprung
	move	(a0)+,d2
	move	(a0)+,d3
	;cmp	d0,d2
	;beq	hor_line
	;cmp	d1,d3
	;beq	ver_line

	movem.l	line_regs,-(sp)

	moveq	#0,d5
	moveq	#0,d7

	cmp	d0,d2
	bhi.s	.x2_gr_x1
	bne	.weiter
	cmp	d1,d3
	beq	.line_ende
.weiter
	or	#$ff00,d5
	move	d0,d6
	sub	d2,d6
	bra.s	.weiter_dx
.x2_gr_x1
	or	#$0100,d5
	move	d2,d6
	sub	d0,d6
.weiter_dx
	move	d6,d2

	cmp	d1,d3
	bhi.s	.y2_gr_y1
	or.b	#$ff,d5
	move	d1,d6
	sub	d3,d6
	bra.s	.weiter_dy
.y2_gr_y1
	or.b	#$01,d5
	move	d3,d6
	sub	d1,d6
.weiter_dy
	move	d6,d3

	cmp	d2,d6
	blo.s	.weiter_dx_dy
	move	d2,d3
	move	d6,d2
	moveq	#1,d7
.weiter_dx_dy

	move	d3,d6
	add	d6,d6
	sub	d2,d6
	move	d6,d4

	swap	d7
	move	d2,d7
.for
	swap	d7

	movem.l	plot_regs,-(sp)
	move	d0,d2
	lsr	#3,d2
	and	#7,d0
	eori	#7,d0
	move	d1,d3
	lsl	#6,d1
	lsl	#4,d3
	add	d3,d1
	add.l	d1,d2
	bset	d0,(a6,d2.l)
	movem.l	(sp)+,plot_regs
.while
	tst	d4
	bmi.s	.wend

	tst	d7
	beq.s	.i_n_1
	tst	d5
	bmi.s	.s1_neg
	addq	#1,d0
	bra.s	.weiter_i
.s1_neg
	subq	#1,d0
	bra.s	.weiter_i
.i_n_1
	tst.b	d5
	bmi.s	.s2_neg
	addq	#1,d1
	bra.s	.weiter_i
.s2_neg
	subq	#1,d1
.weiter_i

	move	d2,d6
	add	d6,d6
	sub	d6,d4
	bra.s	.while
.wend

	tst	d7
	beq.s	.i_n_12
	tst.b	d5
	bmi.s	.s2_neg2
	addq	#1,d1
	bra.s	.weiter_i2
.s2_neg2
	subq	#1,d1
	bra.s	.weiter_i2
.i_n_12
	tst	d5
	bmi.s	.s1_neg2
	add	#1,d0
	bra.s	.weiter_i2
.s1_neg2
	subq	#1,d0
.weiter_i2

	move	d3,d6
	add	d6,d6
	add	d6,d4

	swap	d7
	dbf	d7,.for
.line_ende
	movem.l	(sp)+,line_regs
return
	dbf	d4,loop
	movem.l	(sp)+,regs
	rte
hor_line
	movem.l	plot_regs,-(sp)
	cmp	d0,d2
	bhs.s	.weiter
	move	d0,d4
	move	d2,d0
	move	d4,d2
.weiter
	move	d2,d4
	sub	d0,d4
	move	d4,d5
	divu	#32,d4
	subq	#1,d4
	move	d0,d2
	lsr	#3,d2
	and	#7,d0
	eori	#7,d0
	move	d1,d3
	lsl	#6,d1
	lsl	#4,d3
	add	d3,d1
	add.l	d1,d2
	bset	d0,(a6,d2.l)
	movem.l	(sp)+,plot_regs
	bra.s	return

ver_line
	movem.l	plot_regs,-(sp)
	cmp	d1,d3
	bhs.s	.weiter
	move	d1,d4
	move	d3,d1
	move	d4,d3
.weiter
	move	d0,d2
	lsr	#3,d2
	and	#7,d0
	eori	#7,d0
	move	d1,d3
	lsl	#6,d1
	lsl	#4,d3
	add	d3,d1
	add.l	d1,d2
	bset	d0,(a6,d2.l)
	movem.l	(sp)+,plot_regs
	bra.s	return


los
	init	stack(pc)
	Supexec	super(pc)
	Ptermres	#0,#$100+los-start

super
	move.l	$88.w,old_trap
	move.l	#new_trap,$88.w
	rts

info	dc.b	'LINE-Patch ist installiert.',$d,$a,0
