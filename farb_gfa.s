	include	tos_fnkt

;eine interrupt-routine, die ab einer einstellbaren zeile die farben
;umstellt. zur einbindung in gfa-programme vorgesehen.

int_strt
	movem.l	d1-a6,-(sp)
	tst.b	d0
	bne.s	int_on
int_off
	Jdisint	#8
	lea	vbl+2,a0
	move.l	(a0),a0
	Setexc	(a0),#28
int_ende
	moveq	#0,d0
	movem.l	(sp)+,d1-a6
	rts

int_on
	lea	wert,a3
	lea	old_colors,a4
	lea	new_colors,a5
	move.b	d1,(a3)
	move.l	a0,(a4)
	move.l	a1,(a5)

	lea	int_loop,a0
	lea	new_vbl,a1
	lea	vbl+2,a2
	Xbtimer	(a0),(a3),#8,#1
	Setexc	(a1),#28
	move.l	d0,(a2)
	bra.s	int_ende

new_vbl
	move.l	a0,-(sp)
	lea	wert,a0
	move.b	(a0),$fffffa21.w
	move.l	(sp)+,a0
	move.b	#8,$fffffa1b.w
vbl	jmp	$12345678.l


int_loop
	clr.b	$fffffa1b.w
	movem.l	d0/a0-a1,-(sp)

	lea	new_colors,a0
	move.l	(a0),a0
	lea	$ffff8260.w,a1
	moveq	#15,d0
.loop
	move	(a0)+,-(a1)
	dbf	d0,.loop

	lea	old_colors,a1
	move.l	(a1),$45a.w

	bclr	#0,$fffffa0f.w
	movem.l	(sp)+,d0/a0-a1
	rte

old_vbl	dc.l	0
old_colors	dc.l	0
new_colors	dc.l	0
wert	dc.b	0
	dc.b	"F�r'n Dirk Tewissen vom Thomas J�rges. 26.11.1990"
	cnop	0,2
