	include	tos_fnkt

;Der ST/STE/TT/blabla legt nach einem Absturz Informationen �ber denselben
;ab einer bestimmten Speicheraddresse ab. Dieses Programm wertet
;diese Informationen ein wenig aus.

_longf:	equ	0
fs:	equ	1

begin:	init	stack(pc)
	bsr	clr_scn
start:	Supexec	afterall(pc)
	lea	ausdruck(pc),a0
	bsr	print
.loop:	bsr.s	get_char
	cmp.b	#'a',d0
	blo.s	.no_lower
	sub.b	#'a'-'A',d0
.no_lower:
	swap	d0
	cmp.b	#98,d0
	bne.s	ende
	bsr	clr_scn
	lea	information(pc),a0
	bsr	print
	bsr.s	get_char
	bsr	clr_scn
	bra.s	start
ende:
	Pterm0	;feieraaahm!!!!!

get_char:
	Cnecin
	rts

afterall:
	tst	(_longframe).w
	beq.s	.weiter
	bset	#_longf,flag
.weiter:
	lea	start_str(pc),a0	;copyrigth und was isses?
	bsr	print
	move.l	(proc_lives).w,d1	;daten g�ltig?
	bsr	prt_hex
	moveq	#44,d0
	bsr	prt_char
	moveq	#32,d0
	bsr	prt_char
	cmp.l	#$12345678,d1
	beq.s	g�ltig	;ja, g�ltig!
daten_ung�ltig:
	lea	un(pc),a0
	bsr	print
g�ltig:
	lea	g�(pc),a0
	bsr	print

;***********************************
register_ausgabe:
	lea	gleich(pc),a0	;a3=*'=$'
	lea	(proc_dregs).w,a1	;daten=(a1)

	moveq	#'D',d2
	moveq	#'0',d3
	
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_ret
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_ret

	moveq	#'A',d2
	moveq	#'0',d3
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_ret
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_tab
	bsr	reg_print_tab
	lea	a7_info(pc),a0
	bsr	print
	move.l	(a1)+,d2
;***********************************
;************exception-nummer ausgeben:
	lea	ex_info(pc),a0
	bsr	print
	and.l	#$ff000000,d2
	rol.l	#8,d2
	move.l	d2,d1
	move.b	d2,exe_nummer
	bsr	prt_dez
	move.b	#':',d0
	bsr	prt_char
;*************exception-text folgt:
	lea	tabelle(pc),a2
	add	d2,d2
	add	d2,d2
	move.l	(a2,d2.l),a0
	bsr	prt_tab
	bsr	print
	move.b	#'.',d0
	bsr	prt_char
	bsr	prt_ret
;*******************************
user_stack_ausgabe:
	clr	d3
	lea	so_stack(pc),a0
	bsr	print
	move.l	(a1),d1
	bsr	prt_hex
;***************statusregister ausgeben (mit FLAGS)
	lea	status_reg(pc),a0
	bsr	print
	lea	(proc_stk).w,a1
	move	(a1)+,d1
	and.l	#$ffff,d1
	bsr	prt_hexw
	bsr	statusregister
pc_ausgabe:
	lea	prgc(pc),a0
	bsr	print
	move.l	(a1)+,d1
	move.l	d1,d2
	and.l	#$ffffff,d1
	bsr	prt_hex
	bsr	prt_ret
;*******************************
super_stack_ausgabe:
	lea	so_super_stack(pc),a0
	bsr	print
	lea	(proc_stk).w,a1
	moveq	#1,d2
.stack_loop:
	moveq	#7,d4
.loop:
	move	(a1)+,d1
	bsr	prt_hexw
	bsr	prt_tab
	dbf	d4,.loop
	bsr	prt_ret
	dbf	d2,.stack_loop
;*****************address-/bus-error?
	lea	(proc_stk).w,a1
	btst	#_longf,flag	;Ist ein MC68020 oder besser am Werk?
	bne.s	.MC68020
	cmp.b	#2,exe_nummer
	beq	.add_bus_err
	cmp.b	#3,exe_nummer
	beq	.add_bus_err
	rts

.MC68020:
	lea	stack_format(pc),a0
	bsr	print
	lea	6(a1),a1
	move.b	(a1),d1
	lsr.b	#4,d1
	and.l	#$f,d1
	move	d1,-(sp)
	bsr	prt_hexw
	lea	stack_info(pc),a0
	bsr	print
	lsl	#2,d1
	lea	stack_format_tabelle(pc),a0
	move.l	(a0,d1.l),a0
	bsr	print
	bsr	prt_ret
	move	(a1)+,d1
	and	#$fff,d1
	lea	vektor_offset(pc),a0
	bsr	print
	bsr	prt_hexw

	move	(sp)+,d2	;Stack Frame Format
	subq	#2,d2
	bmi	.kein_add_bus_err
	bne.s	.fpcp_mid
	bsr	.prt_inst_add
	bra	.kein_add_bus_err
.fpcp_mid:
	subq	#7,d2
	bmi	.kein_add_bus_err
	bne.s	.short_bus
	bsr	.prt_inst_add
	lea	int_regs(pc),a0
	bsr	print
	move.l	(a1)+,d1
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	move.l	(a1)+,d1
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	swap	d1
	bsr	prt_hexw
	bra	.kein_add_bus_err
.short_bus:
	subq	#1,d2
	bmi	.kein_add_bus_err
	bne.s	.long_bus
	bsr	.help_bus_cycle
	bra	.kein_add_bus_err
.long_bus:
	subq	#1,d2
	bmi	.kein_add_bus_err
	bne	.kein_add_bus_err
	bsr	.help_bus_cycle
	bsr	prt_tab
	move.l	(a1)+,d1
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	swap	d1
	bsr	prt_hexw
	lea	stage_b_add(pc),a0
	bsr	print
	move.l	(a1)+,d1
	bsr	prt_hex
	lea	int_regs(pc),a0
	bsr	print
	move.l	(a1)+,d1
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	swap	d1
	bsr	prt_hexw
	lea	data_input_buffer(pc),a0
	bsr	print
	move.l	(a1)+,d1
	bsr	prt_hex
	lea	int_regs(pc),a0
	bsr	print
	bsr	prt_ret
	moveq	#1,d7
.loop_long:
	moveq	#7,d6
.loop_long1:
	move	(a1)+,d1
	bsr	prt_hexw
	bsr	prt_tab
	dbf	d6,.loop_long1
	bsr	prt_ret
	dbf	d7,.loop_long
	moveq	#5,d7
.loop_long2:
	move	(a1)+,d1
	bsr	prt_hexw
	bsr	prt_tab
	dbf	d7,.loop_long2
.kein_add_bus_err:
	bsr	prt_ret
	rts

.prt_inst_add:
	lea	inst_add(pc),a0
	bsr	print
	move.l	(a1)+,d1
	bsr	prt_hex
	rts

.help_bus_cycle:
	lea	int_regs(pc),a0
	bsr	print
	move	(a1)+,d1
	bsr	prt_hexw
	lea	special_status_word(pc),a0
	bsr	print
	move	(a1)+,d1
	bsr	prt_hexw
	lea	inst_pipe_b(pc),a0
	bsr	print
	move	(a1)+,d1
	bsr	prt_hexw
	lea	inst_pipe_c(pc),a0
	bsr	print
	move	(a1)+,d1
	bsr	prt_hexw
	lea	data_cycle_fa(pc),a0
	bsr	print
	move.l	(a1)+,d1
	bsr	prt_hex
	lea	int_regs(pc),a0
	bsr	print
	move.l	(a1)+,d1
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	swap	d1
	bsr	prt_hexw
	lea	data_output_buffer(pc),a0
	bsr	print
	move.l	(a1)+,d1
	bsr	prt_hex
	lea	int_regs(pc),a0
	bsr	print
	move.l	(a1)+,d1
	swap	d1
	bsr	prt_hexw
	bsr	prt_tab
	swap	d1
	bsr	prt_hexw
	rts
;**********super-statuswort ausgeben
.add_bus_err:
	lea	add_bus_str(pc),a0
	bsr	print
	move	(a1)+,d1
	and.l	#$ffff,d1
	move	d1,ssw
	bsr	prt_hexw
	bsr	prt_tab
;*************function code ausgeben
	lea	func_code_str(pc),a0
	bsr	print
	move	ssw(pc),d1
	and	#%111,d1
	bsr	prt_hexw
	bsr	prt_ret
;*************exception gruppe feststellen
	lea	exception_gruppe_str(pc),a0
	bsr	print
	bsr	prt_ret
	move	ssw(pc),d1
	lea	exception_gruppe1_str(pc),a0
	and	#%1000,d1
	bne.s	.ist_gruppe_1
	lea	exception_gruppe2_str(pc),a0
.ist_gruppe_1:
	bsr	print
;***************read-/write-zyklus?
	lea	read_write_str1(pc),a0
	bsr	print
	lea	read(pc),a0
	move	ssw(pc),d1
	and	#%10000,d1
	bne.s	.ist_read
	lea	write(pc),a0
.ist_read:
	bsr	print
	lea	read_write_str2(pc),a0
	bsr	print
;*************zugriffsadresse
	lea	zugriffs_add(pc),a0
	bsr	print
	move.l	(a1)+,d1
	bsr	prt_hex
	move.b	#'.',d0
	bsr	prt_char
;***************letztes bekanntes befehlswort
	lea	befehl_reg(pc),a0
	bsr	print
	move	(a1)+,d1
	and.l	#$ffff,d1
	bsr	prt_hexw
	move.b	#'.',d0
	bsr	prt_char
	bsr	prt_ret
	rts
;****************das waren die hard-infos
;****************

;*******************************
reg_print_tab:
	move.b	d7,-(sp)
	clr.b	d7
	bra.s	reg_print
reg_print_ret:
	move.b	d7,-(sp)
	move.b	#-1,d7
reg_print:
	move.b	d2,d0
	bsr	prt_char
	move.b	d3,d0
	bsr	prt_char
	addq.b	#1,d3
	bsr	print	;(In A0 befindet sich: *'=$')
	move.l	(a1)+,d1	;prt (a1)
	bsr	prt_hex
	tst.b	d7
	beq.s	.tab
	bsr	prt_ret
.ende:
	move.b	(sp)+,d7
	rts
.tab:
	bsr	prt_tab
	bra.s	.ende
;*******************************
statusregister:
	lea	status_flags(pc),a2
	btst	#15,d1
	beq.s	.keint1
	move.b	#'T',(a2)+
	btst	#_longf,flag(pc)
	beq.s	.keint1
	move.b	#'1',(a2)+
.keint1:
	btst	#_longf,flag(pc)
	beq.s	.kein_t2
	btst	#14,d1
	beq.s	.kein_t2
	move.b	#'T',(a2)+
	move.b	#'2',(a2)+
.kein_t2:
	btst	#13,d1
	beq.s	.kein_s
	move.b	#'S',(a2)+
.kein_s:
	btst	#_longf,flag(pc)
	beq.s	.kein_m
	btst	#12,d1
	beq.s	.kein_m
	move.b	#'M',(a2)+
.kein_m:
	move.b	#' ',(a2)+
	move.b	#'I',(a2)+
	move.b	#'P',(a2)+
	move.b	#'L',(a2)+
	move	d1,d4
	and	#$0700,d4
	lsr	#8,d4
	add.b	#$30,d4
	move.b	d4,(a2)+

	move.b	#' ',(a2)+
	btst	#4,d1
	beq.s	.kein_x
	move.b	#'X',(a2)+
.kein_x:
	btst	#3,d1
	beq.s	.kein_n
	move.b	#'N',(a2)+
.kein_n:
	btst	#2,d1
	beq.s	.kein_z
	move.b	#'Z',(a2)+
.kein_z:
	btst	#1,d1
	beq.s	.kein_v
	move.b	#'V',(a2)+
.kein_v:
	btst	#0,d1
	beq.s	.kein_c
	move.b	#'C',(a2)+
.kein_c:
	clr.b	(a2)
	lea	status_flags_str(pc),a0
	bsr.s	print
	lea	status_flags(pc),a0
	bsr.s	print
	rts

;*******************************
print:	Cconws	(a0)
	rts
prt_ret:
	moveq	#$d,d0
	bsr.s	prt_char
	moveq	#$a,d0
	bsr.s	prt_char
	rts
prt_char:
	Cconout	d0
	rts
prt_tab:
	moveq	#9,d0
	bsr.s	prt_char
	rts
clr_scn:
	move.l	a0,-(sp)
	lea	clr_screen(pc),a0
	bsr.s	print
	move.l	(sp)+,a0
.ende:	rts

;prt_hexw gibt ein datum in hex
;auf dem bilschirm aus. d1=datum(4 g�ltige stellen)
prt_hexw:
	movem.l	a0/d1-d3,-(sp)
	moveq	#'$',d0
	bsr.s	prt_char
	lea	buffer(pc),a0
	and.l	#$ffff,d1
	moveq	#1,d3
	move	d1,d2
	ror.w	#8,d2
.hex_prt_loop:
	move.b	d2,d1
	lsr.b	#4,d1
	cmp.b	#9,d1
	ble.s	.hex_prt_t1
	add.b	#$37,d1
	move.b	d1,(a0)+
	bra.s	.hex_prt_zweit
.hex_prt_t1:
	add.b	#$30,d1
	move.b	d1,(a0)+
.hex_prt_zweit:
	move.b	d2,d1
	and.b	#$0f,d1
	cmp.b	#9,d1
	ble.s	.hex_prt_t2
	add.b	#$37,d1
	move.b	d1,(a0)+
	bra.s	.hex_prt_schl
.hex_prt_t2:
	add.b	#$30,d1
	move.b	d1,(a0)+
.hex_prt_schl:
	lsr	#8,d2
	dbf	d3,.hex_prt_loop
	clr.b	(a0)
	lea	buffer(pc),a0
	bsr	print
	movem.l	(sp)+,a0/d1-d3
	rts
;prt_hex gibt ein datum in hex
;auf dem bilschirm aus. d1=datum(8 g�ltige stellen)
prt_hex:
	movem.l	a0/d1-d3,-(sp)
	moveq	#'$',d0
	bsr	prt_char
	lea	buffer(pc),a0
	moveq	#3,d3
	move.l	d1,d2
	ror.w	#8,d2
	swap	d2
	ror.w	#8,d2
.hex_prt_loop:
	move.b	d2,d1
	lsr.b	#4,d1
	cmp.b	#9,d1
	ble.s	.hex_prt_t1
	add.b	#$37,d1
	move.b	d1,(a0)+
	bra.s	.hex_prt_zweit
.hex_prt_t1:
	add.b	#$30,d1
	move.b	d1,(a0)+
.hex_prt_zweit:
	move.b	d2,d1
	and.b	#$0f,d1
	cmp.b	#9,d1
	ble.s	.hex_prt_t2
	add.b	#$37,d1
	move.b	d1,(a0)+
	bra.s	.hex_prt_schl
.hex_prt_t2:
	add.b	#$30,d1
	move.b	d1,(a0)+
.hex_prt_schl:
	lsr.l	#8,d2
	dbf	d3,.hex_prt_loop
	clr.b	(a0)
	lea	buffer(pc),a0
	bsr	print
	movem.l	(sp)+,a0/d1-d3
	rts
;prt_dez gibt ein datum in dez
;auf dem bilschirm aus. d1=datum(6 g�ltige stellen)
prt_dez:
	movem.l	a0/d1-d5,-(sp)
	lea	buffer(pc),a0
	moveq	#0,d2
	moveq	#0,d5
	and.l	#$fffff,d1
	tst.l	d1
	beq.s	.ok
	move.l	d1,d4
	move.l	#100000,d5
	moveq	#0,d3
.loop1:
	moveq	#0,d2
.loop2:
	cmp.l	d5,d4
	bmi.s	.is_lower
	moveq	#1,d3
	addq	#1,d2
	sub.l	d5,d4
	bne.s	.loop2
.is_lower:
	tst.b	d2
	bne.s	.ok
	tst.b	d3
	beq.s	.is_zero
.ok:
	add	#'0',d2
	move.b	d2,(a0)+
.is_zero:
	divu	#10,d5
	bne.s	.loop1
	clr.b	(a0)
	lea	buffer(pc),a0
	bsr	print
	movem.l	(sp)+,a0/d1-d5
	rts
;*******************************
	data
tabelle:
	dc.l	reset_ssp,reset_pc,bus,address,illegal,division
	dc.l	chk,trapv,privileg,trace,line_a,line_f,reserved
	dc.l	copro_protocol,illegal_format,not_init_int,reserved
	dc.l	reserved,reserved,reserved,reserved,reserved
	dc.l	reserved,reserved,wrong_int,level1_iav,level2_iav
	dc.l	level3_iav,level4_iav,level5_iav,level6_iav,level7_iav
	dc.l	trap0,trap1,trap2,trap3,trap4,trap5,trap6,trap7,trap8
	dc.l	trap9,trap10,trap11,trap11,trap12,trap13,trap14,trap15
	dc.l	fpcp_branch,fpcp_inexact,fpcp_divide,fpcp_underflow
	dc.l	fpcp_operand,fpcp_overflow,fpcp_nan,reserved
	dc.l	pmmu_configuration,pmmu_illegal,pmmu_access,reserved
	dc.l	reserved,reserved,reserved,reserved
	rept	192
	dc.l	userdefined
	endr
stack_format_tabelle:
	dc.l	short,throwaway,instruction_excep
	dc.l	sf_reserved,sf_reserved,sf_reserved,sf_reserved,sf_reserved
	dc.l	mc_68010_bf,fpcp_mid_ins,mc68020_sbf,mc68020_lbf
	dc.l	sf_reserved,sf_reserved,sf_reserved,sf_reserved
short:	dc.b	"Normaler Stack Frame (4 Worte)",0
throwaway:	dc.b	'Wechsel vom MASTER-Status in den Interrupt-Status (4 Worte)',0
instruction_excep:	dc.b	'CHK, CHK2, cpTRAPcc, TRAPcc, TRAPV, TRACE, DIV by 0 (6 Worte)',0
mc_68010_bf:	dc.b	'MC68010 Busfehler (29 Worte)',0
fpcp_mid_ins:	dc.b	'FPCP Mid-Instruction Exception (10 Worte)',0
mc68020_sbf:	dc.b	'Short Bus Cycle Fault (16 Worte)',0
mc68020_lbf:	dc.b	'Long Bus Cycle Fault (46 Worte)',0
sf_reserved:	dc.b	'Reserviert, bisher nicht belegt',0

reset_ssp:	dc.b	'MPU: Reset-SSP',0
reset_pc:	dc.b	'MPU: Reset-PC',0
bus:	dc.b	'MPU: Bus-Fehler',0
address:	dc.b	'MPU: Adress-Fehler',0
illegal:	dc.b	'MPU: Illegale Instruktion',0
division:	dc.b	'MPU: Division durch Null',0
chk:	dc.b	'MPU: CHK/CHK2-Instruktion',0
trapv:	dc.b	'MPU: cpTRAPcc-, TRAPcc-, TRAPV-Instruktion',0
privileg:	dc.b	'MPU: Privilegverletzung',0
trace:	dc.b	'MPU: TRACE',0
line_a:	dc.b	'MPU: Line-$Axxx',0
line_f:	dc.b	'MPU: Line-$Fxxx',0
illegal_format:	dc.b	'MPU: Illegales Stack Format (nur MC68010 & MC68020)',0
not_init_int:	dc.b	'MPU: Uninitialisierter Interrupt',0
wrong_int:	dc.b	'MPU: Falscher Interrupt',0
reserved:	dc.b	'Reserviert f�r sp�tere Anwendung',0
copro_protocol:	dc.b	'FPCP: Protokoll Verletzung',0
level1_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #1',0
level2_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #2',0
level3_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #3',0
level4_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #4',0
level5_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #5',0
level6_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #6',0
level7_iav:	dc.b	'MPU: Interrupt-Auto-Vektor #7',0
trap0:	dc.b	'MPU: Trap #0',0
trap1:	dc.b	'MPU: Trap #1',0
trap2:	dc.b	'MPU: Trap #2',0
trap3:	dc.b	'MPU: Trap #3',0
trap4:	dc.b	'MPU: Trap #4',0
trap5:	dc.b	'MPU: Trap #5',0
trap6:	dc.b	'MPU: Trap #6',0
trap7:	dc.b	'MPU: Trap #7',0
trap8:	dc.b	'MPU: Trap #8',0
trap9:	dc.b	'MPU: Trap #9',0
trap10:	dc.b	'MPU: Trap #10',0
trap11:	dc.b	'MPU: Trap #11',0
trap12:	dc.b	'MPU: Trap #12',0
trap13:	dc.b	'MPU: Trap #13',0
trap14:	dc.b	'MPU: Trap #14',0
trap15:	dc.b	'MPU: Trap #15',0
fpcp_branch:	dc.b	'FPCP: Verzweigung oder Set mit NAN-Input',0
fpcp_inexact:	dc.b	'FPCP: Ungenaues Ergebnis',0
fpcp_divide:	dc.b	'FPCP: Division durch Null',0
fpcp_underflow:	dc.b	'FPCP: Unterlauf',0
fpcp_operand:	dc.b	'FPCP: Operande(n) Fehlerhaft (kommt auf den ausgef�hrten Befehl an)',0
fpcp_overflow:	dc.b	'FPCP: �berlauf',0
fpcp_nan:	dc.b	'FPCP: Operand/Ergebnis=NAN (keine IEEE-Zahl)',0
pmmu_configuration:	dc.b	'PMMU: Konfiguration fehlerhaft',0
pmmu_illegal:	dc.b	'PMMU: Illegale Operation',0
pmmu_access:	dc.b	'PMMU: Illegaler Zugriff auf die PMMU',0
userdefined:	dc.b	'MPU: Benutzerdefinierter Exceptionvektor',0

;ret:	dc.b	$a,$d,0
;tab:	dc.b	9,0
de:	dc.b	'D',0
aa:	dc.b	'A',0
prgc:	dc.b	9,'PC'
gleich:	dc.b	'=',0
stat:	dc.b	9,'SR=',0
add_bus_str:	dc.b	'SSW=',0
so_stack:	dc.b	'USP=',0
so_super_stack:	dc.b	'Daten auf dem Supervisor-Stack:',$d,$a,0
ex_info:	dc.b	'Exception #',0
a7_info:	dc.b	'A7=SSP',$d,$a,0
func_code_str:	dc.b	'FCL: ',0
start_str:
	hello_world	'AFTERALL'
	dc.b	'- Hilft nach Bomben manchmal weiter.',$d,$a
	dc.b	'Die Daten sind, da ($proc_lives)=',0
g�:	dc.b	'g�ltig.',$d,$a,$a,0
un:	dc.b	'un',0
stack_format:	dc.b	'Stackformat des 680-10/12/20: ',0
stack_info:	dc.b	$d,$a,'Information �ber den Stack Frame: ',0
vektor_offset:	dc.b	'Exception Table Vector Offset=',0
inst_add:	dc.b	$d,$a,'Instruction Address=',0
int_regs:	dc.b	$d,$a,'Internal Register(s):',9,0
special_status_word:	dc.b	$d,$a,'Special Status Word=',0
inst_pipe_b:	dc.b	$d,$a,'Instruction Pipe Stage B=',0
inst_pipe_c:	dc.b	$d,$a,'Instruction Pipe Stage C=',0
data_cycle_fa:	dc.b	$d,$a,'Data Cycle Fault Address=',0
data_output_buffer:	dc.b	$d,$a,'Data Output Buffer=',0
stage_b_add:	dc.b	$d,$a,'Stage B Address=',0
data_input_buffer:	dc.b	$d,$a,'Data Input Buffer=',0
read_write_str1:	dc.b	'Es wurde in einem ',0
read:	dc.b	'Lese',0
write:	dc.b	'Schreib',0
read_write_str2:	dc.b	'zyklus abgebrochen.',$d,$a,0
exception_gruppe_str:	dc.b	'Ursache des Abbruchs war ein Element aus folgender Gruppe:',0
exception_gruppe1_str:	dc.b	'RESET oder Address-/Bus-Fehler',$d,$a,0
exception_gruppe2_str:	dc.b	'Trace, Interrupt, Illegal, Line $Axxx, Line $Fxxx, Privilegverletzung',$d,$a
	dc.b	'TRAP, TRAPV, CHK, Division durch Null',$d,$a,0
zugriffs_add:	dc.b	'Es wurde zugegriffen auf Adresse $',0
befehl_reg:	dc.b	$d,$a,'Inhalt des Befehlsregisters: $',0
status_reg:	dc.b	9,'SR=',0
status_flags_str:	dc.b	9,'FLAGS: ',0
clr_screen:	dc.b	27,'E',0
ausdruck:	dc.b	'<Help> Information, andere Tasten beenden das Programm.',0
information:	freeware	'AFTERALL'
	copyright
	bss
status_flags:	ds.l	4
buffer:	ds.l	10
ssw:	ds.w	1
exe_nummer:	ds.b	1
flag:	ds.b	1