  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	f:\mag_utis\mag_crun.prg	;So hei�t das PRG-File.

;Mein superduper Cruncher MAG-CRUN V1.0.
;Features: rasend schnell, recht effektiv bei Grafiken.
;Und... er funktioniert.

not_save	=	0
lohnt_nicht	=	1

	init	stack(pc)
start
	init_gem	#2
	lea	file_name(pc),a0
	lea	init_name(pc),a1
.loop
	move	(a1)+,d0
	beq.s	.weiter
	move	d0,(a0)+
	bra.s	.loop
.weiter
	lea	path(pc),a0
	move.l	#'\*.*',(a0)+
	clr.b	(a0)
	clr.b	flag
	bsr	arrow

men�	;Hier startet MAIN f�r ein normales Programm.
;Und die immer wiederkehrende Initialisierung.
auswahl	lea	menu_str(pc),a0
	move	#3,d0
	bsr.s	form_alert
	move	int_out(pc),d0
	subq	#1,d0
	add	d0,d0
	add	d0,d0
	lea	tabelle(pc),a0
	move.l	(a0,d0.w),a0
	jsr	(a0)
	bra.s	auswahl
ende	addq	#4,sp
	exit_gem
	Pterm0

form_alert
	form_alert	d0,a0
	rts

start_iff_crunch	;Hier geht 's zur Sache Leute!
	bsr	load_file	;Zuerst File einladen.
	move.l	rfile_length(pc),d0
	bsr	malloc
	move.l	d0,buffer2
	beq	no_mem
	bsr	bee
iff_crunch_init
	bsr	clear_regs

	move.l	buffer1(pc),a1	;sourcebuffer
	move.l	buffer2(pc),a2	;dest.buffer
	move.l	rfile_length(pc),d6	;Maximale Anzahl, bei der abgebrochen wird.
	lea	header(pc),a3	;12 Bytes langer Header,
	movem.l	(a3)+,d1-d3	;damit der Wiedererkennungswert
	movem.l	d1-d3,(a2)	;recht hoch bleibt.
	lea	12(a2),a2
	move.l	d6,(a2)	;Die ersten 4 Bytes des gecrunchten
;Files sind die org. Filel�nge, damit ich sp�ter auch Bescheid wei�,
;wie lange ich decrunchen mu�.
	add.l	#16,d7	;destfilel�nge+=4
outer_crunch_loop
	moveq	#0,d2
	moveq	#1,d3
	moveq	#1,d4

inner_crunch_loop
;d1 enth�lt das Datenbyte, d2 die Anzahl,
;d6 den Rest, der noch zu crunchen ist,
;d7 die zu schreibende L�nge.
;Bei gleichen Bytes: zuerst die Anzahl der Datenbytes, dann das Datenbyte
;Bei Byte-run: zuerst die Anzahl der folgenden Bytes, dann die Bytes
	move.b	(a1),d1	;Databyte #1 nach d1.
	cmp.b	1(a1),d1	;databyte #1==databyte #2
	bne.s	not_equal_bytes	;Nein, dann nach <-!

equal_bytes
	tst.b	d3	;data-2==data-1==data-->d3:=0^d4<>0
	beq.s	.schon_initialisiert	;D.h. schon initialisiert.
	tst.b	d4
	bne.s	.weiter
	move.b	d2,(a3)
.weiter
	moveq	#0,d2
	clr.b	d3
	moveq	#1,d4
	move.l	a2,a3
	addq.l	#1,a2
	move.b	d1,(a2)+
	addq.l	#2,d7
	cmp.l	rfile_length(pc),d6
	bhs.s	.schon_initialisiert
	bset	#lohnt_nicht,flag
	bra.s	ende_2
.schon_initialisiert
	addq.b	#1,d2
	addq.l	#1,a1

	subq.l	#1,d6
	beq.s	end_crunch
	cmp.b	#127,d2
	bne.s	inner_crunch_loop

	move.b	d2,(a3)
	bra.s	outer_crunch_loop

;*********Bytes sind ungleich!**********
not_equal_bytes
	tst.b	d4
	beq.s	.schon_initialisiert
	tst.b	d3
	bne.s	.weiter
	move.b	d2,(a3)
.weiter
	clr.b	d2
	clr.b	d4
	moveq	#1,d3
	move.l	a2,a3
	addq.l	#1,a2
	addq.l	#1,d7
	cmp.l	rfile_length(pc),d6
	bhs.s	.schon_initialisiert
	bset	#lohnt_nicht,flag
	bra.s	ende_2
.schon_initialisiert
	subq.b	#1,d2
	move.b	(a1)+,(a2)+
	addq.l	#1,d7
	subq.l	#1,d6
	beq.s	end_crunch
	cmp.b	#-128,d2
	bne.s	inner_crunch_loop

	move.b	d2,(a3)
	bra	outer_crunch_loop

end_crunch
	move.b	d2,(a3)
	move.l	d7,wfile_length
ende_2	bsr	arrow
	bsr	prozent
	btst	#not_save,flag(pc)
	bne.s	.weiter
	bsr	write_file
.weiter	rts
;*******************************
;*******************************
start_iff_decrunch
;a1=sourcebuffer, a2=objectbuffer
;d4=objectfile length
	bsr.s	clear_regs
	bsr	load_file
	bsr	bee
iff_decrunch_init
	move.l	buffer1(pc),a1
	moveq	#0,d1
	lea	header(pc),a3
	moveq	#2,d7
.loop
	cmpm.l	(a3)+,(a1)+
	bne	wrong_format
	dbf	d7,.loop

	move.l	(a1)+,d4
	move.l	d4,wfile_length
	move.l	d4,d0
	bsr	malloc
	move.l	d0,buffer2
	move.l	d0,a2
	beq	no_buf2
start_iff_de
	tst.l	d4
	beq.s	iff_de_end
	bmi.s	iff_de_end
	clr	d0
	move.b	(a1)+,d0
	bpl.s	iff_de_eq
	not.b	d0
iff_de_uneq
	move.b	(a1)+,(a2)+
	subq.l	#1,d4
	dbf	d0,iff_de_uneq
	bra.s	start_iff_de

iff_de_eq
	subq.b	#1,d0
	move.b	(a1)+,d1
.loop2
	move.b	d1,(a2)+
	subq.l	#1,d4
	dbf	d0,.loop2
	bra.s	start_iff_de
iff_de_end
	bsr	arrow
	bsr	write_file
	bra	men�

;*******************************
;*******************************
;*******************************
clear_regs
	moveq	#0,d0
	moveq	#0,d1
	moveq	#0,d2
	moveq	#0,d3
	moveq	#0,d4
	moveq	#0,d5
	moveq	#0,d6
	moveq	#0,d7
	sub.l	a0,a0
	sub.l	a1,a1
	sub.l	a2,a2
	sub.l	a3,a3
	sub.l	a4,a4
	sub.l	a5,a5
	sub.l	a6,a6
	rts
;*******************************
prozent
	bclr	#not_save,flag
	btst	#lohnt_nicht,flag(pc)
	bne.s	.gained_nothing
	lea	s_length(pc),a6
	move	#5,d0
.loop1	move.b	#' ',(a6)+
	dbf	d0,.loop1
	lea	o_length(pc),a6
	move	#5,d0
.loop2	move.b	#' ',(a6)+
	dbf	d0,.loop2
	move.l	rfile_length(pc),d1
	lea	s_length(pc),a6
	bsr	prt_ldez
	sub.l	d7,d1
	bmi.s	.gained_nothing
	lea	o_length(pc),a6
	bsr	prt_ldez
	lea	form_prozent(pc),a0
	move	#1,d0
	bsr	form_alert
	rts
.gained_nothing
	bset	#not_save,flag
	clr	d0
	lea	gn(pc),a0
	bsr	form_alert
	bsr	free_mem
	rts
;*******************************
wrong_format
	clr	d0
	lea	falsches_format(pc),a0
	bsr	form_alert
	rts
;*******************************
no_buf2
	bsr.s	free_mem
	clr	d0
	lea	kein_speicher(pc),a0
	bsr	form_alert
	bra	men�
;*******************************
free_mem
	move.l	a6,-(sp)
	lea	buffer1(pc),a6
	tst.l	(a6)
	beq.s	.weiter
	move.l	(a6),a6
	bsr.s	mfree
	clr.l	buffer1
.weiter
	lea	buffer2(pc),a6
	tst.l	(a6)
	beq.s	.weiter2
	move.l	(a6),a6
	bsr.s	mfree
	clr.l	buffer2
.weiter2
	move.l	(sp)+,a6
	rts
;*******************************
mfree
	Mfree	(a6)
	rts
;*******************************
malloc
	Malloc	d0
	rts
;*******************************
arrow
	move.l	d0,-(sp)
	move	#ARROW,d0
	bsr.s	graf_mouse
	move.l	(sp)+,d0
	rts
;*******************************
bee
	move.l	d0,-(sp)
	move	#BUSYBEE,d0
	bsr.s	graf_mouse
	move.l	(sp)+,d0
	rts
;*******************************
graf_mouse
	graf_mouse	d0
	rts
;*******************************
;******Fehler*******************
;*******************************
file_zu_lang_error
read_error
	move	#1,d0
	lea	laenge_err(pc),a0
	bsr	form_alert
	bsr	free_mem
	move.l	#men�,(sp)
	rts
;*******************************
open_error
not_found
write_error
	bsr.s	form_error
	bsr	free_mem
	move.l	#men�,(sp)
	rts
;*******************************
no_mem
	bsr.s	form_error
	move.l	#men�,(sp)
	rts
;*******************************
	include	sub\form_err.s
;*******************************
;*******************************
;*******************************

;*******************************
;Filebehandlung*****************
;*******************************
load_file
	lea	lese(pc),a2
	lea	path(pc),a1
	lea	file_name(pc),a0
	bsr	fsel_input
	Fsfirst	#0,file_name(pc)
	tst.l	d0
	bmi.s	not_found
	Fopen	#0,file_name(pc)
	move	d0,handle
	bmi	open_error
	Fgetdta
	move.l	d0,a0

	moveq	#-1,d0
	bsr	malloc
	move.l	$1a(a0),d1
	lsl.l	#1,d1
	cmp.l	d0,d1
	bhi	file_zu_lang_error

	move.l	$1a(a0),rfile_length
	move.l	$1a(a0),d0
	bsr	malloc
	move.l	d0,buffer1
	beq	no_mem
	move.l	d0,a0
	Fread	(a0),rfile_length(pc),handle(pc)
	cmp.l	rfile_length(pc),d0
	bne	read_error

	Fclose	handle(pc)
	rts
;*******************************
write_file
	lea	schreibe(pc),a2
	lea	path(pc),a1
	lea	file_name(pc),a0
	bsr	fsel_input
	Fcreate	#0,file_name(pc)
	move	d0,handle
	bmi	open_error
	move.l	buffer2(pc),a0
	Fwrite	(a0),wfile_length(pc),handle(pc)
	cmp.l	wfile_length(pc),d0
	bne	write_error
	Fclose	handle(pc)
	bsr	free_mem
	rts
;*******************************
;Stellt eine Fileselector-Box dar und �bergibt am Ende
;das Ergebnis des Knopfes, der gedr�ckt wurde.
;Erwartet in a1 den Pfad und in a0 einen vordefinierten Dateinamen
;Ab TOS 1.04 kann in a2 ein Text f�r die FS-Box �bergeben werden.
fsel_input
	movem.l	a0-a2,-(sp)
	cmp	#$104,global
	blo.s	.weiter
	fsel_exinput	a1,a0,a2
	bra.s	.go
.weiter
	fsel_input	a1,a0
.go
	tst	int_out+2
	beq.s	.abbruch
	clr.l	d0
	cmp.b	#':',1(a1)
	bne.s	.kein_laufwerk_wechseln
	move.b	(a1),d0
	sub.b	#'A',d0
	Dsetdrv	d0
.kein_laufwerk_wechseln
	lea	(a1),a2
.loop
	cmp.b	#0,(a2)+
	bne.s	.loop
.loop2
	cmp.b	#'\',-(a2)
	bne.s	.loop2
	addq	#1,a2
	move.b	(a2),d0
	clr.b	(a2)
	Dsetpath	(a1)
	move.b	d0,(a2)
	movem.l	(sp)+,a0-a2
	move	int_out+2(pc),d0
	rts
.abbruch
	bsr	free_mem
	move.l	#men�,(sp)
	rts
;*******************************
;*******************************
;*******************************

;*******************************
;*******************************
;*******************************
;prt_dez gibt ein Datum in dezimaler Schreibweise
;auf dem Bilschirm aus. d1=Datum(6 g�ltige Stellen),
;a6=*Zielbuffer.
prt_ldez
	movem.l	d0-d7/a6,-(sp)
	moveq	#0,d2
	moveq	#0,d5
	tst.l	d1
	beq.s	.ok
	move.l	d1,d4
	move.l	#10000000,d5
	moveq	#0,d3
.loop1
	moveq	#0,d2
.loop2
	cmp.l	d5,d4
	bmi.s	.is_lower
	moveq	#1,d3
	addq.b	#1,d2
	sub.l	d5,d4
	bne.s	.loop2
.is_lower
	tst.b	d2
	bne.s	.ok
	tst.b	d3
	beq.s	.is_zero
.ok
	add.b	#'0',d2
	move.b	d2,(a6)+
.is_zero
	cmp.l	#100000,d5
	bhs.s	.long_div
	divu	#10,d5
	bne.s	.loop1
.ende
	movem.l	(sp)+,d0-d7/a6
	rts
;****************long division**********
;in d5 der divident, d6=divisor
.long_div
	moveq	#1,d7
	moveq	#10,d6
	moveq	#0,d0
	cmp.l	d6,d5
	blo.s	.l574ba
.l574a8
	add.l	d6,d6
	add.l	d7,d7
	cmp.l	d6,d5
	bhs.s	.l574a8
	bra.s	.l574ba
.l574b2
	cmp.l	d6,d5
	blo.s	.l574ba
	or.l	d7,d0
	sub.l	d6,d5
.l574ba
	lsr.l	#1,d6
	lsr.l	#1,d7
	bne.s	.l574b2
	move.l	d0,d5
	bra.s	.loop1
;*******************************
;*******************************
;*******************************

;*******************************
	include	include\call_gem
;*******************************

	data
tabelle	dc.l	start_iff_crunch,start_iff_decrunch,ende
lese	dc.b	'Quellfile einlesen:',0
schreibe	dc.b	'Zielfile schreiben:',0
header	dc.b	'MAG-CRUNV1.0'
init_name	dc.b	'NONAME.MCR'
	cnop	0,4
laenge_err	dc.b	'[3][Das eingelesen/geschrieben|File hat nicht die|'
	dc.b	'korrekte L�nge. Breche ab.][ OK ]',0
gn	dc.b	'[1][Es wurde nichts eingespart.][ OK ]',0
kein_speicher	dc.b	'[3][Es steht nicht mehr gen�gend|'
	dc.b	'Systemspeicher zur Verf�gung.|Breche ab.][Abbruch]',0
menu_str	dc.b	'[2][Mag-Crunch - ',189,'Magnum/BlackBox-|Was darf es sein?][Crunch|Decrunch|Abbruch]',0
falsches_format	dc.b	'[3][Das Sourcefile hat|nicht das richtige|Format.|'
	dc.b	'Falsche Version?][Abbruch]',0
form_prozent	dc.b	'[1][L�nge des Source-files: '
s_length	dc.b	'000000 Bytes|Eingesparte Bytes: '
o_length	dc.b	'000000][ OK ]',0
	copyright
	bss
evnt_buff	ds.l	4
drive	ds.w	1
handle	ds.w	1
buffer1	ds.l	1
buffer2	ds.l	1
rfile_length	ds.l	1
wfile_length	ds.l	1
file_name	ds.l	4
path	ds.l	64
flag	ds.b	1
