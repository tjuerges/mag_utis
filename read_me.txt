Wenn es Schwierigkeiten beim Abspeichern von Daten gibt:
--------------------------------------------------------

Bevor MERLIN.PRG gestartet wird, sollte man das Programm PTCHMRLN.PRG starten.
Dies hat den Zweck, da� eine kleine 'Unsauberkeit' des Programms ausgeb�gelt
wird. Die Programmierer haben wohl, als sie dieses Programm als
Shareware-Version herausgaben, eine Kleinigkeit vergessen:
Es wird bei (fast?) jedem Schreibzugriff versucht, die Datei 'dummy.xxx'
anzulegen. Ist sie noch nicht vorhanden, so macht dies auch nix. Denn dann wird
sie einfach angelegt. Ist sie aber schon auf dem aktuellen Laufwerk vorhanden,
so wird von der GEMDOS-Funktion Fcreate die Fehlermeldung 'Datei schon
vorhanden' (oder so �hnlich) zur�ckgegeben. Dies wertet MERLIN als Fehler und
weigert sich fortan, irgendwelche Daten abzuspeichern.

Ich habe mir schon das Handbuch bestellt, darin aber keine Erkl�rung oder
L�sung f�r dieses �beraus bl�de Verhalten des Programms gefunden. Deshalb
mein kleines Patch-Programm.

Trotzdem viel Spa� mit Merlin...
	Magnum