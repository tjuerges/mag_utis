	opt	a+,c+,o+,w+
	include	include\tos_fnkt

;zeigt an, wie oft ein bestimmtes nibble in einem file vertreten ist.

start
	init	stack(pc)
	bsr	open_file
	bsr	read_file
	bsr	close_file
	bsr	statistik
error
	Cconin
	Pterm0


;****************************************
;**************Subroutinen***************
;****************************************
statistik
	Cconws	file_length_str(pc)
	move.l	length(pc),d1
	bsr	prt_ldez
	bsr.s	prt_ret

	moveq	#0,d1
	lea	null,a6
	move.l	address(pc),a0
	move.l	length(pc),d7
	lsr.l	#2,d7
.loop
	move.l	(a0)+,d0
.test_nibbles
	moveq	#7,d6
.test_nibbles_loop
	move.b	d0,d1
	and.b	#$0f,d1
	lsl.l	#2,d1
	addq.l	#1,(a6,d1.l)
	lsr.l	#4,d0
	dbf	d6,.test_nibbles_loop

	subq.l	#1,d7
	bne.s	.loop

	bsr	auswertung
	bsr	free_mem
	rts
prt_ret
	Cconws	ret(pc)
	rts
auswertung
	Cconws	auswertung_str(pc)
	lea	null_str(pc),a5
	lea	null(pc),a6
	bsr	sortieren
	moveq	#15,d7
	moveq	#0,d6
	move.l	length(pc),d5
	lsl.l	#1,d5
.loop
	Cconws	(a5)
	addq	#2,a5
	Cconout	#9
	move.l	(a6)+,d1
	add.l	d1,d6
	bsr	prt_ldez
;	Cconout	#9
;	Cconws	etwa(pc)
;prozent-ausgabe. vorkommastellen
;	move.l	d1,d2
;	mulu	#100,d2
;	bsr	long_div
;	move.l	d2,d1
;	bsr	prt_dez
;	Cconout	#'%'
	bsr.s	prt_ret
	dbf	d7,.loop
	move.l	d6,d1
	bsr.s	prt_ret
	bsr.s	prt_ret
	Cconws	gesamtnibble(pc)
	bsr	prt_ldez
	bsr	prt_ret
	bsr	prt_ret
	bsr	prt_ret
	rts
	include	sub\long_div.s
open_file
	Cconws	filename_str(pc)
	Cconrs	#78,filename
	bsr	prt_ret
	bsr	get_length
	addq	#2,a6
	Fopen	#0,filename+2(pc)
	lea	handle(pc),a2
	move	d0,(a2)
	bmi	open_error
	rts
sortieren
	rts
free_mem
	move.l	address(pc),a5
	Mfree	(a5)
	rts
get_length
	Fsfirst	#0,filename+2(pc)
	tst	d0
	bmi.s	open_error
	Fgetdta
	move.l	d0,a0
	move.l	$1a(a0),d0
	lea	length(pc),a1
	move.l	d0,(a1)
	rts

read_file
	lea	address(pc),a4
	Malloc	length(pc)
	move.l	d0,(a4)
	beq.s	read_error
	move.l	d0,a5
	Fread	(a5),length(pc),handle(pc)
	cmp.l	length(pc),d0
	bne.s	read_error
	rts

close_file
	Fclose	handle(pc)
	rts

open_error
	Cconws	open_e(pc)
return
	pea	error(pc)
	rts
read_error
	Cconws	read_e(pc)
	Fclose	handle(pc)
	bra.s	return

	include	sub\prt_ldez.s

ret	dc.b	$d,$a,0
file_length_str	dc.b	'Filel�nge: ',0
filename_str	dc.b	'Filename: ',0
open_e	dc.b	'Fehler beim Datei �ffnen!',$d,$a,0
read_e	dc.b	'Lesefehler!',$d,$a,0
auswertung_str	dc.b	'Die Verteilung der Nibbleh�ufigkeit liegt so:',$d,$a,0
gesamtnibble	dc.b	'Gesamtanzahl aller Nibble: ',0
etwa	dc.b	'ungef�hr ',0
null_str	dc.b	'0',0
	dc.b	'1',0
	dc.b	'2',0
	dc.b	'3',0
	dc.b	'4',0
	dc.b	'5',0
	dc.b	'6',0
	dc.b	'7',0
	dc.b	'8',0
	dc.b	'9',0
	dc.b	'A',0
	dc.b	'B',0
	dc.b	'C',0
	dc.b	'D',0
	dc.b	'E',0
	dc.b	'F',0

filename	ds.l	80
null	ds.l	1
eins	ds.l	1
22	ds.l	1
33	ds.l	1
44	ds.l	1
55	ds.l	1
66	ds.l	1
77	ds.l	1
88	ds.l	1
99	ds.l	1
aa	ds.l	1
bb	ds.l	1
cc	ds.l	1
dd	ds.l	1
ee	ds.l	1
ff	ds.l	1
length	ds.l	1
address	ds.l	1
handle	ds.w	1
	