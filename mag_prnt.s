	opt	a+,c+,o+,w+
	;include	include\tos_fnkt

	;V0.99	3-2-1991
	;---------------
	;Ersetzt die sehr langsamen Ausgaberoutinen des TOS
	;durch meine etwas schnelleren.
	;Ich benutze hier die XBRA-Methode, um anderen Programmen
	;auch noch eine Chance zu geben.
	;BIOS & GEMDOS werden gepatcht.
	;->Danke<- an Gis f�r Deine Hilfe (Wie immer...)!
	;V0.99-�  4-2-1991
	;-----------------
	;Versuche zur Geschwindigkeitssteigerung sind
	;leider kl�glich gescheitert. So was bl�des! Wie
	;macht z.B. Quick-ST das nur?
	;Einbindung der TOS-Cursorposition, Einbindung in die
	;TOS-con_state-Routine.

patch_con_state	=	1
patch_bios	=	1
patch_gemdos	=	1
interrupt	=	1
debug	=	0
eigener_font	=	0
offset	=	256
breite	=	16
hoehe	=	8
esc_flag	=	%10
wrap_flag	=	1
cursor_pos	=	$2ac2

start:
	bra	begin
	dc.b	'MAG-PRINT - der Screenrunner.'
	even

;*******************************
;*******************************

;alter bios-dispatcher
	ifne	patch_bios
old_bios_dispatcher:
	move.l	old_bios,a0
	jmp	(a0)
	dc.l	'XBRA','MGNM'
old_bios:	dc.l	0
new_bios_dispatcher:
	btst	#5,(sp)
	bne.s	bios_ist_ja_super
	move	usp,a0
	bra.s	bios_stack_ok
bios_ist_ja_super:
	lea	6(sp),a0
bios_stack_ok:
	move	(a0),d0
	cmp.b	#3,d0
	bne.s	old_bios_dispatcher
	cmp.b	#2,2(a0)
	bne.s	old_bios_dispatcher
	addq.l	#2,a0
	bra.s	new_cconout
	endc
;*******************************
;*******************************
	ifne	patch_bios!patch_gemdos
new_crawio:
	cmp.b	#$ff,3(a0)
	beq.s	old_gemdos_dispatcher
new_cconout:
	move.b	3(a0),d0
	lea	character,a0
	move.b	d0,(a0)
	bra.s	von_new_cconout
;alter gemdos-dispatcher
old_gemdos_dispatcher:
	move.l	old_gemdos,a0
	jmp	(a0)
	endc
;*******************************
;*******************************
	ifne	patch_con_state
;neue con_state-Funktion
new_raw_out:
	move.l	a0,-(sp)
	lea	character,a0
	move.b	d1,(a0)
	pea	come_back_to_raw
	move	sr,-(sp)
	bra.s	von_new_raw_out
come_back_to_raw:
	move.l	(sp)+,a0
	rts
	endc
;**********************
;einsprung
;**********************
	ifne	patch_gemdos
	dc.l	'XBRA','MGNM'
	endc
	ifne	patch_gemdos!patch_bios
old_gemdos:	dc.l	0
	endc
	ifne	patch_gemdos
new_gemdos_dispatcher:
	btst	#5,(sp)
	bne.s	ist_ja_super
	move	usp,a0
	bra.s	stack_ok
ist_ja_super:
	lea	6(sp),a0
stack_ok:
	move	(a0),d0
	cmp.b	#9,d0
	beq.s	cconws
	cmp.b	#2,d0
	beq.s	new_cconout
	cmp.b	#6,d0
	beq.s	new_crawio
	bra.s	old_gemdos_dispatcher
cconws:
	move.l	2(a0),a0
	endc
von_new_cconout:
von_new_raw_out:
	ifne	debug
	not	$ffff8240.w
	endc
	ifne	interrupt
	elseif
	move	#$2700,sr
	endc
	movem.l	d2-d4/a2-a4,-(sp)
	move.l	$44e.w,a1		;screenadresse
	move.l	fontaddress,a2		;im TOS-Format!!!
	move.b	(cursor_pos).w,d0
	move.b	(cursor_pos+1).w,d1
	subq.b	#1,d0
	subq.b	#1,d1
	btst	#esc_flag,flag
	bne	escape
main_loop:
	moveq	#0,d3
	move.b	(a0)+,d3
	beq	ende
	cmp.b	#32,d3
	bcc.b	no_tab
;escape?
	cmp.b	#$1b,d3
	beq	escape
;linefeed?
	cmp.b	#$a,d3
	bne.s	no_lf
	addq.b	#1,d1
	bra.s	main_loop
no_lf:
;carriage return?
	cmp.b	#$d,d3
	bne.s	no_cr
	moveq	#0,d0
	bra.s	main_loop
no_cr:
	cmp.b	#9,d3
	bne.s	no_tab
	and.b	#%11111000,d0
	addq.b	#8,d0
	bra.s	main_loop
no_tab:
;zeilenende erreicht?
	cmp.b	#80,d0
	bcs.s	no_lineend
	btst	#wrap_flag,flag
	beq.s	kein_auto_wrap_an
	moveq	#0,d0
	addq.b	#1,d1
	bra.s	no_lineend
kein_auto_wrap_an:
	moveq	#79,d0
no_lineend:
;schon letzte zeile erreicht?
	cmp.b	#25,d1
	bcs	no_screenend
screen_end:
	lea	16*80(a1),a4
	move.l	#$20002,$ffff8a20.w	;src_xinc&src_yinc
	move.l	a4,$ffff8a24.w	;src_add
	move.l	#$ffffffff,$ffff8a28.w	;endmask1&2
	move.l	#$ffff0002,$ffff8a2c.w	;endmask3&dst_xinc
	move	#2,$ffff8a30.w	;dst_yinc
	move.l	a1,$ffff8a32.w	;dst_add
	move.l	#$280180,$ffff8a36.w	;x_count&y_count
		; ||	(durch eine var sind windows m�glich!!!)
	move	#$203,$ffff8a3a.w	;hop
;	clr.b	$ffff8a3d.w	;skew
	move	#$c000,$ffff8a3c.w	;line_num

	lea	384*80(a1),a4
	move.l	#$20002,$ffff8a20.w	;src_xinc
	move.l	a4,$ffff8a24.w	;src_add
	move.l	#$ffffffff,$ffff8a28.w	;endmask1
	move.l	#$ffff0002,$ffff8a2c.w	;endmask3
	move	#2,$ffff8a30.w	;dst_yinc
	move.l	a4,$ffff8a32.w	;dst_add
	move.l	#$280010,$ffff8a36.w	;x_count&y_count
		; ||	(durch eine var sind windows m�glich!!!)
	move	#$200,$ffff8a3a.w	;hop
;	clr.b	$ffff8a3d.w	;skew
	move	#$c000,$ffff8a3c.w	;line_num
	subq.b	#1,d1

no_screenend:
;berechnung der ersten pixeladresse
	moveq	#0,d4
	move	d1,d4
	lsl	#4,d4

	move.l	d4,d2			;d4*80 =
	lsl	#6,d2
				;d4->dn;(dn<<6)+(d4<<4)
	lsl	#4,d4
	add.l	d2,d4

	move	d0,d2
	and.l	#$fff,d2
	add.l	d4,d2

;berechnung der char-adresse
	move.l	a1,a3			;screenadresse retten
	move.l	a2,a4			;fontimageadresse retten
	lsl	#4,d3
	add.l	d3,a4
	add.l	d2,a3
char_loop:
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3
	move.b	(a4)+,(a3)		;a1=a3=screen, a2=a4=font
	lea	80(a3),a3

	addq.b	#1,d0
	bra	main_loop
ende:
	addq.b	#1,d0
	addq.b	#1,d1
	move.b	d0,(cursor_pos).w
	move.b	d1,(cursor_pos+1).w
	movem.l	(sp)+,d2-d4/a2-a4
	ifne	interrupt
	elseif
	move	#$2300,sr
	endc
	rte

;eine escape-sequenz wurde eingel�utet
escape:
	bset	#esc_flag,flag

	moveq	#0,d3
	move.b	(a0)+,d3
	beq.s	ende
	cmp.b	#'A',d3
	bcs.s	esc_return
	cmp.b	#'z'+1,d3
	bcc.s	esc_return

	cmp.b	#'a'-1,d3
	bhi.s	esc_klein
esc_gross:
	lea	escape_table_gr,a5
	sub.b	#'A',d3
	lsl	#2,d3
	move.l	(a5,d3.l),a4
	jmp	(a4)
esc_klein:
	lea	escape_table_kl,a5
	sub.b	#'a',d3
	lsl	#2,d3
	move.l	(a5,d3.l),a4
	jmp	(a4)
esc_return:
	bclr	#esc_flag,flag
	bra	main_loop

;falls der untere bildschirmrand erreicht wurde

_0:
	bra.s	esc_return
_A:
	tst.b	d1
	beq.s	esc_return
	subq.b	#1,d1
	bra.s	esc_return
_B:
	addq.b	#1,d1
	bra.s	esc_return
_C:
	addq.b	#1,d0
	bra.s	esc_return
_D:
	tst.b	d0
	beq.s	esc_return
	subq.b	#1,d0
	bra.s	esc_return
_E:
	move.l	#$20002,$ffff8a20.w	;src_xinc
	move.l	a1,$ffff8a24.w	;src_add
	move.l	#$ffffffff,$ffff8a28.w	;endmask1
	move.l	#$ffff0002,$ffff8a2c.w	;endmask3
	move	#2,$ffff8a30.w	;dst_yinc
	move.l	a1,$ffff8a32.w	;dst_add
	move.l	#$280190,$ffff8a36.w	;x_count
	move	#$200,$ffff8a3a.w	;hop
;	clr.b	$ffff8a3d.w	;skew
	move	#$c000,$ffff8a3c.w	;line_num
	moveq	#0,d0
	moveq	#0,d1
	bra.s	esc_return
_H:
	moveq	#0,d0
	moveq	#0,d1
	bra.s	esc_return
_I:	
	bra.s	esc_return
_J:
	bra.s	esc_return
_K:
	bra.s	esc_return
_L:
	bra.s	esc_return
_M:
	bra.s	esc_return
_O:
	ifne	patch_gemdos
	move.l	old_gemdos,$84.w
	endc
	ifne	patch_bios
	move.l	old_bios,$b4.w
	endc
	bra	esc_return
_Y:
	tst.b	(a0)
	beq	ende
	move.b	(a0)+,d1
	sub.b	#33,d1
	tst.b	(a0)
	beq	ende
	move.b	(a0)+,d0
	sub.b	#33,d0
	bra	esc_return
_b:
	move.b	(a0)+,$ffff8242.w
	bra	esc_return
_c:
	move.b	(a0)+,$ffff8240.w
	bra	esc_return
_d:	
	bra	esc_return
_e:
	bra	esc_return
_f:
	bra	esc_return
_j:
	addq.b	#1,d0
	addq.b	#1,d1
	move.b	d0,(cursor_pos).w
	move.b	d1,(cursor_pos+1).w
	subq.b	#1,d0
	subq.b	#1,d1
	bra	esc_return
_k:
	move.b	(cursor_pos).w,d0
	move.b	(cursor_pos+1).w,d1
	subq.b	#1,d0
	subq.b	#1,d1
	clr	(cursor_pos).w
	bra	esc_return
_l:
	moveq	#0,d0
	bra	esc_return
_o:
	bra	esc_return
_p:
	move.l	fontaddress_revers,a2
	move.l	a2,fontaddress
	bra	esc_return
_q:
	move.l	fontaddress_normal,a2
	move.l	a2,fontaddress
	bra	esc_return
_v:
	bset	#wrap_flag,flag
	bra	esc_return
_w:
	bclr	#wrap_flag,flag
	bra	esc_return

font_revers:	ds.l	1024
fontaddress:	ds.l	1
fontaddress_normal:	ds.l	1
fontaddress_revers:	ds.l	1

character:	ds.w	1
flag:	ds.b	1
	even

escape_table_gr:
	dc.l	_A,_B,_C,_D,_E,_0,_0,_H,_I,_J,_K,_L,_M,_0,_O,_0
	dc.l	_0,_0,_0,_0,_0,_0,_0,_0,_Y,_0
escape_table_kl:
	dc.l	_0,_b,_c,_d,_e,_f,_0,_0,_0,_j,_k,_l,_0,_0,_o,_p
	dc.l	_q,_0,_0,_0,_0,_v,_w,_0,_0,_0

	ifne	eigener_font
font_normal:	incbin	f:\font\mac.fnt
	elseif
font_normal:	ds.l	1024
	endc
save_it:
	even

;***********************
;nichtresidenter teil
;***********************
supexec:
	pea	(a0)
	move	#$26,-(sp)
	trap	#14
	addq.l	#6,sp
	rts
setexc:
	pea	(a0)
	move	d0,-(sp)
	move	#5,-(sp)
	trap	#13
	addq.l	#8,sp
	rts
begin:
	ifne	eigener_font
	move.l	#font_normal,fontaddress_normal
	elseif
	dc.w	$a000
	move.l	8(a1),a1
	move.l	$4c(a1),fontaddress_normal
	endc

	lea	invert_font,a0
	bsr.s	supexec

	move.l	#font_normal,fontaddress
	move.l	#font_normal,fontaddress_normal
	move.l	#font_revers,fontaddress_revers
	ifne	patch_gemdos
	lea	new_gemdos_dispatcher,a0
	moveq	#33,d0
	bsr.s	setexc
	move.l	d0,old_gemdos
	endc

	ifne	patch_bios
	lea	new_bios_dispatcher,a0
	moveq	#45,d0
	bsr.s	setexc
	move.l	d0,old_bios
	endc

	clr.b	character+1
	bset	#wrap_flag,flag

	pea	hallo
	move	#9,-(sp)
	trap	#1
	addq.l	#6,sp

	clr	-(sp)
	move.l	#$100+(save_it-start),-(sp)
	move	#$31,-(sp)
	trap	#1

invert_font:
	ifne	patch_con_state
	move.l	#new_raw_out,$4a8.w
	endc
	move.l	fontaddress_normal,a0
	lea	font_revers,a1
	lea	font_normal,a2
	move.l	#255,d0
invert_font_loop:
	move.b	(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$100(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$200(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$300(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$400(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$500(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$600(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$700(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$800(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$900(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$a00(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$b00(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$c00(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$d00(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$e00(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	move.b	$f00(a0),d1
	move.b	d1,(a2)+
	not.b	d1
	move.b	d1,(a1)+
	addq.l	#1,a0
	dbf	d0,invert_font_loop
	rts
hallo:	dc.b	'MAG-PRINT ist installiert.',$d,$a,0
