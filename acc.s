  	;opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	gem	;Vdi- & Aes-Macros einbinden.

debug	=	1
	ifne	debug
	output	d:\mag_.acc	;So heisst das Programm sp�ter.
	elseif
	output	f:\mag_utis\mag_.acc	;So heisst das Programm sp�ter.
	endc

name:	MACRO
	dc.b	'BOXES'
	ENDM

start:
	lea	end_stack(pc),sp	;Als .ACC einen eigenen Stack.
	appl_init	;Applikation beim AES anmelden.
	bmi	fehler
	move.b	#1,flag
	cmp	#$0200,global
	blo.s	.no_unregister
	bset	#1,flag
.no_unregister:
	cmp.l	#'KAOS',int_in
	bne.s	.kein_kaos
	bset	#1,flag
.kein_kaos:
	graf_handle
	menu_register	appl_id(pc),#mag_acc_str,menu_id
	bmi	fehler

loop:
	lea	buffer(pc),a0
	evnt_multi	#MU_MESAG,,,,,,,,,,,,,,,a0
	cmp	#MU_MESAG,intout(pc)	;War es ein MU_MESAG-Event?
	bne.s	loop	;Nein...

	cmp	#AC_OPEN,(a0)	;...
	bne.s	loop
	move	menu_id(pc),d0
	cmp	8(a0),d0	;Ist die Menu-ID des betroffenen ACC meine?
	bne.s	loop	;Nee...

	lea	mag_acc_alert(pc),a6
	btst	#1,flag(pc)
	beq.s	.kein_gem20
	lea	mag_acc_alert_spezial(pc),a6
.kein_gem20:
	form_alert	#1,a6	;Doch, dann...
	cmp	#1,int_out	;Los geht's!
	beq.s	los
	btst	#1,flag(pc)	;Gem-Version >=2.00?
	beq	loop	;Nee.
	;Falls doch, dann gibt's n�mlich menu_unregister zum Abmelden
	;des ACC!
	cmp	#2,int_out	;Mittlerer Knopf gedr�ckt?
	bne	loop	;Nee, also passiert gar nix.
	menu_unregister	menu_id(pc)
fehler:
	evnt_timer	#-1
	bra.s	fehler

;*************** Eigentliche Routine ****************
los:
	bra	loop

	include	include\call_gem	;AES- & VDI-Aufrufe einbinden.

	data
mag_acc_str:	dc.b	'  Mag-'
	name
	dc.b	0
mag_acc_alert:	dc.b	'[2][MAG-'
	name
	dc.b	' - �Magnum/BlackBox-|][Ja|Abbruch]',0
mag_acc_alert_spezial:	dc.b	'[2][MAG-'
	name
	dc.b	' - �Magnum/BlackBox-|Funktion ausf�hren?][Ja|Quit ACC|Abbruch]',0
	copyright

	bss
flag:	ds.b	1
	even
buffer:	ds.l	4
menu_id:	ds.w	1
stack:	ds.l	64
end_stack:
