  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.

debug	=	0
	ifne	debug
	output	d:\mag_yell.acc	;So heisst das Programm sp�ter.
	elseif
	output	f:\mag_utis\mag_yell.acc	;So heisst das Programm sp�ter.
	endc

__SAVEREGS	set	1

;FLAGS:
GEM_200	=	0
KAOS_142	=	1
an	=	2	;Zeigt an, da� etwas gespielt werden darf.
laeuft	=	3	;Zeigt an, da� ein Sample gerade abgespielt wird.
wert	=	60000	;In Millisekunden
timer_a_data	=	82	;Timerwert.

name:	MACRO
	dc.b	'Yell'
	ENDM
_appl_init:	MACRO
	move.l	#$a0000,control
	move.l	#$10000,control+4
	bsr	CALL_AES
	cmp.l	#'KAOS',int_in
	seq	d7
	move	(int_out).l,appl_id
	ENDM
_menu_register:	MACRO
	move.l	#$230001,control
	move.l	#$10001,control+4
	move	\1,int_in
	move.l	\2,addr_in
	bsr	CALL_AES
	move	(int_out).l,\3
	ENDM
_graf_handle:	MACRO
	move.l	#$4d0000,control
	move.l	#$50000,control+4
	bsr	CALL_AES
	move	(int_out).l,contrl+12
	move	(int_out).l,wk_current_handle
	ENDM

start:
	lea	(end_stack).l,sp	;Als .ACC einen eigenen Stack.
	_appl_init	;Applikation beim AES anmelden.
	bmi	fehler
	clr.b	flag
	cmp	#$0200,global
	blo.s	.no_unregister
	bset	#GEM_200,flag
.no_unregister:
	tst.b	d7
	beq.s	.kein_kaos
	bset	#KAOS_142,flag
.kein_kaos:
	_graf_handle
	_menu_register	(appl_id).l,#mag_acc_str,menu_id
	bmi	fehler
	Random
	and.l	#$ffff,d0
	divu	#wert,d0
	swap	d0
	and.l	#$ffff,d0
	move.l	d0,zufallswert
	bset	#an,flag
;***************************************
loop:
	lea	(buffer).l,a0
	clr.l	d0
	move	#MU_MESAG,d1

	btst	#an,(flag).l
	beq.s	.weiter2
	or	#MU_TIMER,d1
	move.l	(zufallswert).l,d0

.weiter2:
	evnt_multi	d1,,,,,,,,,,,,,,d0,a0

	cmp	#MU_MESAG,intout	;War es ein MU_MESAG-Event?
	beq.s	.message	;Ja...
;***************************************
	btst	#an,(flag).l
	beq.s	loop
	btst	#laeuft,(flag).l
	bne.s	loop
	lea	malloc_address(pc),a0
	tst.l	(a0)
	beq.s	.weiter_mfree
	move.l	(a0),a0
	Mfree	(a0)
.weiter_mfree:
	cmp	#MU_TIMER,intout
	beq	play_sample
	bra	loop
;***************************************
.message:
	cmp	#AC_OPEN,(a0)	;...
	bne	loop
	move	(menu_id).l,d0
	cmp	8(a0),d0	;Ist die Menu-ID des betroffenen ACC meine?
	bne	loop	;Nee...
;***************************************
	lea	mag_acc_alert(pc),a6
	btst	#GEM_200,(flag).l
	bne.s	.gem_200
	btst	#KAOS_142,(flag).l
	beq.s	.kein_gem200
.gem_200:
	lea	mag_acc_alert_spezial(pc),a6
;***************************************
.kein_gem200:
	form_alert	#1,a6	;Doch, dann...
	cmp	#1,int_out	;Los geht's!
	beq.s	los
	btst	#GEM_200,(flag).l	;Gem-Version >=2.00?
	bne.s	.gem_200_2	;Jaa.
	btst	#KAOS_142,(flag).l	;Nein, aber vielleicht KAOS?
	beq	loop	;Nein...
.gem_200_2:
	;Falls doch, dann gibt's n�mlich menu_unregister zum Abmelden
	;des ACC!
	cmp	#2,int_out	;Mittlerer Knopf gedr�ckt?
	bne	loop	;Nee, also passiert gar nix.
	menu_unregister	(menu_id).l
	bra	fehler
;***************************************
fehler:
	evnt_timer	#-1
	bra.s	fehler
;*************** Eigentliche Routine ****************
los:
	bchg	#an,flag
	bra	loop
;***************************************
play_sample:
	Random
	and.l	#$ffff,d0
	divu	#wert,d0
	swap	d0
	and.l	#$ffff,d0
	move.l	d0,zufallswert
	and.l	#$ff,d0
	clr.l	d1
	move.b	samples(pc),d1
	divu	d1,d0
	swap	d0
	move	d0,d7

	lea	filename(pc),a6
	Fsfirst	#$17,(a6)
	tst.l	d0
	bne	.fsnext_error
	subq	#1,d7
	beq.s	.fertig
.loop:	Fsnext
	tst.l	d0
	bne	.fsnext_error
	subq	#1,d7
	bne.s	.loop
.fertig:
	Fgetdta
	move.l	d0,a6
	move.l	d_length(a6),a5	;In a5 die L�nge festhalten.

	Malloc	a5
	move.l	d0,a4	;In a4 die Adresse des angeforderten Speicherblocks
	;abspeichern.
	beq	loop	;Fehler? Dann ab zur Hauptschleife!

	lea	filename(pc),a0
	lea	name(pc),a1
.loop_n:
	cmp.b	#'*',(a0)
	beq.s	.loop_n_ende
	move.b	(a0)+,(a1)+
	bra.s	.loop_n
.loop_n_ende:
	lea	d_fname(a6),a0
.loop_o:
	move.b	(a0)+,(a1)+
	bne.s	.loop_o

	Fopen	#0,name(pc)
	move	d0,d7	;In d7 das Handle sichern.
	beq	.fopen_error

	Fread	(a4),a5,d7
	cmp.l	a5,d0
	bne.s	.fread_error

	Fclose	d7

	move.l	a4,malloc_address
	move.l	a4,int_position
	move.l	a5,rest

	Supexec	klick_aus(pc)
	Dosound	sounds(pc)
	bset	#laeuft,flag
	Xbtimer	play_sample_interrupt(pc),#timer_a_data,#1,#0
	bra	loop

.fread_error:
	Fclose	d7
.fopen_error:
	Mfree	(a4)
.fsnext_error:
	clr.l	malloc_address
	bra	loop
;***************************************
play_sample_interrupt:
	movem.l	d0-d2/a0,-(sp)
	move.l	#$07007f00,(giselect).w
	subq.l	#1,rest
	bmi.s	.sound_ende
	move.l	(int_position).l,a0	;aktuelle position der sample daten
	clr.l	d0
	move.l	#$08000000,d0
	move.l	#$09000000,d1
	move.l	#$0a000000,d2
	move.b	(a0)+,d0	;daten holen
	add	d0,d0
	move.l	a0,int_position
	lea	tabelle(pc),a0
	move	(a0,d0),d1
	lea	512(a0),a0
	move	(a0,d0),d2
	lea	512(a0),a0
	move	(a0,d0),d0
	lea	(giselect).w,a0
	movem.l	d0-d2,(a0)
.int_ganz_fertig:
	bclr	#TIMER_A,(isra).w
	movem.l	(sp)+,d0-d2/a0
	rte	
.sound_ende:
	bclr	#TIMER_A,(imra).w
	move.b	klick(pc),(conterm).w
	bclr	#laeuft,flag
	bra.s	.int_ganz_fertig
;***************************************
klick_aus:
	move.b	(conterm).w,klick
	and.b	#%1010,(conterm).w
	rts
;***************************************
	include	include\call_gem	;AES- & VDI-Aufrufe einbinden.
;***************************************
	data
	dc.b	'FILEPATH:'
filename:	dc.b	'C:\MAG_YELL\*.SAM',0
	dc.b	'# OF SAMPLES:'
samples:	dc.b	14
sounds:	dc.b	0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,%111111,8,0,9,0
	dc.b	10,0,11,0,12,0,13,0,$82,0
tabelle:	include	tab1.s
mag_acc_str:	dc.b	'  Mag-'
	name
	dc.b	0
mag_acc_alert:	dc.b	'[2][MAG-'
	name
	dc.b	' - �Magnum/BlackBox-|An-/abschalten?][An/Aus|Abbruch]',0
mag_acc_alert_spezial:	dc.b	'[2][MAG-'
	name
	dc.b	' - �Magnum/BlackBox-|An-/abschalten?][An/Aus|Quit ACC|Abbruch]',0
;***************************************
	copyright
;***************************************
	bss
name:	ds.l	128
buffer:	ds.l	4
malloc_address:	ds.l	1
length:	ds.l	1
rest:	ds.l	1
int_position:	ds.l	1
zufallswert:	ds.l	1
menu_id:	ds.w	1
klick:	ds.b	1
flag:	ds.b	1
;***************************************
	even
stack:	ds.l	64
end_stack:
