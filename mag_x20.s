  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	MAG_X20.prg	;So hei�t das PRG-File.
;Dieses Programm ist bei der Einbindung fremder, PC-relativer
;Programme in eigene Assembler Sourcen via 'incbin' (z.B. DEVPAC).
;Es wird eine Datei mit Endung '.X20' erzeugt. Diese Datei besitzt
;allerdings keinen Programm-Header, ist also nicht relozierbar und
;mu� daher absolut!!!!!!! PC-relativ sein.
	init	stack(pc)
start:
	init_gem	#NDC
begin:	form_alert	#2,#alert
	cmp	#2,intout
	beq	abbruch
	lea	name(pc),a0
	clr.b	(a0)
	lea	path(pc),a1
	clr.b	(a1)
	lea	laden(pc),a2
	bsr	fsel_input
	tst	d0
	beq	start

	Fsfirst	#$17,(a0)
	tst	d0
	beq.s	.1
	bsr	form_error
	bra	start

.1:	Fgetdta
	move.l	d0,a3
	lea	d_length(a3),a3
	move.l	(a3),d6

	Malloc	d6
	move.l	d0,a4
	beq	start

	Fopen	#0,(a0)
	move	d0,d7
	bpl.s	.2
	bsr	form_error
	bra	free

.2:	Fread	(a4),d6,d7
	cmp.l	d0,d6
	beq.s	.3
	bsr	form_error
	bra.s	close

.3:	bsr	fclose

	lea	(a0),a5
.loop:	cmp.b	#'.',(a5)+
	beq.s	.exit
	cmp.l	a0,a1
	bne.s	.loop
	lea	(a0),a5
.loop2:	tst.b	(a5)+
	bne.s	.loop2
.exit:	
	move.b	#'X',(a5)+
	move.b	#'2',(a5)+
	move.b	#'0',(a5)
	lea	speichern(pc),a2
	bsr	fsel_input
	tst	d0
	beq.s	free

	Fcreate	#0,(a0)
	move	d0,d7
	bne.s	.4
	bsr.s	form_error
	bra.s	free

.4:	lea	$1c(a4),a5
	move.l	2(a4),d5
	Fwrite	(a5),d5,d7
close:	bsr.s	fclose
free:	Mfree	(a4)
	bra	begin
abbruch:
	exit_gem
	Pterm0

fclose:	Fclose	d7
	rts

	include	sub\form_err.s
	include	sub\fsel_inp.s
	include	include\call_gem

	data
alert	dc.b	'[2][MAG-Make_X20 - �Magnum/BlackBox-][Starten|Abbruch]',0
laden:	dc.b	'Programm einladen:',0
speichern:	dc.b	'Bin�re Datei abspeichern:',0
	bss
name:	ds.l	4
path:	ds.l	64
