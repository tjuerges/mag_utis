;dieser programmteil reloziert programme, die 'einfach so'
;in den speicher geladen wurden.

start_offset=$4000
laenge=30000

start:
	move.l	#surce,a0
	move.l	#buffer,a3
	bsr	load

	move.l	#buffer,a0
	move.l	2(a0),d0	;Text Segment
	add.l	6(a0),d0	;Daten
;	add.l	10(a0),d0	;BSS
	lea	28(a0),a2
	add.l	d0,a2
	add.l	14(a0),d0	;Symboltabelle
	lea	28(a0),a0
	move.l	a0,a1
	add.l	d0,a1

	moveq	#0,d0
	moveq	#0,d1
	move.l	#start_offset,d2
*				a0 text
*				a1 symbol
;	move	#$4E71,(a0)	;NOP bei Turbo C
;	move	#$4E71,2(a0)	;NOP
;	move	#$3E7C,-4(a0)	;move #$1000,sp
;	move	#$1000,-2(a0)

	move.b	(a1)+,d1
	lsl.l	#8,d1
	move.b	(a1)+,d1
	lsl.l	#8,d1
	move.b	(a1)+,d1
	lsl.l	#8,d1
	move.b	(a1)+,d1
	tst.l	d1
	beq	rel_end
rel_loop:
	add.l	d2,(a0,d1)
rel_loop2:
	move.b	(a1)+,d0
	beq.s	rel_end
	cmp	#1,d0
	bne.s	add_norm
	add.l	#254,d1
	bra.s	rel_loop2
add_norm:
	add.l	d0,d1
	bra.s	rel_loop

rel_end:move.l	a2,a3

	bsr	save
ende:
	illegal			;back to Seka
	clr	-(sp)
	trap	#1

super:	lea	buffer+28(pc),a0
	move.l	#$C000A,a1
	move	#20000/4,d0
copy_loop:
	move.l	(a0)+,(a1)+
	dbf	d0,copy_loop

*	move.l	#$96000000+laenge,$C0000	;send memory
*	move.l	#start_offset,$C0006
*	trap	#3

	move.l	#$C000A+10000,a0
	move.l	#$C000A,a1
	move.l	#laenge-10001,d0
copy_l2:move.b	(a0)+,(a1)+
	dbf	d0,copy_l2

	move.l	#$96000000+laenge-10000,$C0000	;send memory
	move.l	#start_offset+10000,$C0006
	trap	#3

	move.l	#$7D000,a7_mem
	move.l	#$7F000,ssp_mem
	move.l	#$7E000,usp_mem
	move	#$2700,sr_mem
	move.l	#$1000,pc_mem	;put_status
	lea	reg_mem(pc),a1
	move.l	a1,d2
	moveq	#4,d0
	trap	#4

*	moveq	#5,d0		;go
*	trap	#4

*	moveq	#$A,d0
*	move.l	#$1000,a1
*	trap	#4
	rts

save:	move	#0,-(sp)	; Create
	move.l	#target,-(sp)
	move	#$3C,-(sp)
	trap	#1
	addq.l	#8,sp
	move	d0,handle

	move.l	#buffer+28-4,-(sp); Write
	sub.l	#buffer+28-4,a3
	move.l	a3,-(sp)
	move	handle,-(sp)
	move	#$40,-(sp)
	trap	#1
	add.l	#12,sp

	move	handle,-(sp)	; Close
	move	#$3E,-(sp)
	trap	#1
	addq.l	#4,sp
	movem.l	(sp)+,a0-a3
	rts

load:	movem.l	d0-d7/a0-a6,-(sp)

	move	#0,-(sp)	; Open
	move.l	a0,-(sp)
	move	#$3D,-(sp)
	trap	#1
	addq.l	#8,sp
	move	d0,handle

	move.l	a3,-(sp)	; Read
	move.l	#100000,-(sp)
	move	handle,-(sp)
	move	#$3F,-(sp)
	trap	#1
	add.l	#12,sp

	move	handle,-(sp)	; Close
	move	#$3E,-(sp)
	trap	#1
	addq.l	#4,sp

	movem.l	(sp)+,d0-d7/a0-a6
	rts

*****************************************************
status_daten:
reg_mem:blk.l	15
a7_mem:dc.l	1	;muss zusammenbleiben
pc_mem:	dc.l	0
sr_mem:	dc.w	0
usp_mem:dc.l	0
ssp_mem:dc.l	0
zugriff_mem:dc.l0
befehl_mem:dc.w	0
ssr_mem:dc.w	0		;+68020
exeption_nummer:dc.w0
msp_mem:dc.l	0		;68020
saved_vektor_offset:dc.w0	;68020
dc.w0
blk.l	8

**************************************************
erweiterte_status_daten:
daten_ausgabe_puffer:dc.w0	;68020
daten_einlese_puffer:dc.w0	;68020
				;befehls_einlese_puffer?
internal_error_mem:blk.w16	;68020
blk.w	14
blk.l	32
**************************************************
handle:	dc.w	0

surce:	dc.b	"c:\tc\messer\am.prg",0

target:	dc.b	"c:\am.exe",0
even

buffer:	blk.b	1	; Data
	END

