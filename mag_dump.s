	include	tos_fnkt

;sollte mal ein speichermonitor & disassembler werden.
;ist bisher nur ein monitor... seufz.

	init	stack(pc)
start:	Cursconf	#10,#4
	Supexec	begin(pc)
	Pterm0

begin:	move.b	(conterm).w,klick
	or.b	#8,(conterm).w
	move.b	#9,buffer	initialisierung des �blichen krams
	move.b	#16,base
	move.b	#'$',base_char
	move	#1,length_flag
	clr.b	richtung
	lea	begin_str(pc),a0	hallihallo!
	bsr	print
main_loop:
	bsr.s	menu
	bra.s	main_loop
menu:	bsr	crsr_on
	bsr	clr_ln

	bsr	cconin	;zeichen holen.
	move.b	d0,d2	zeichen retten
	cmp.b	#'Q',d0	Quit?
	beq	ende
	cmp.b	#'b',d0	Basis?
	beq	new_base
	cmp.b	#'B',d0	byte
	beq.s	byte_flag
	cmp.b	#'W',d0	word
	beq.s	word_flag
	cmp.b	#'L',d0 longword
	beq.s	long_flag
	cmp.b	#'m',d0	memdump
	beq	mem_dump
	cmp.b	#'a',d0	asciidump
	beq	ascii_dump
	cmp.b	#'A',d0	neue adresse
	beq.s	adresse_einlesen
	cmp.b	#'d',d0	disassemble
	beq	disass
	cmp.b	#'f',d0
	beq.s	zweiteiler

	swap	d0	jetzt scancodes testen
	cmp.b	#98,d0	HELP?
	beq.s	hilfe	ja.
	lea	wie_bitte(pc),a0
	bsr	print
	rts
;ende der ein-zeichen-befehle
zweiteiler:
	bsr	cconin	;noch eins holen!
	lsl	#8,d2
	or	d0,d2	2.b->1.w
;testen ob das kommando g�ltig ist.
	cmp	#'fm',d2	freier speicher?
	beq	free_memory
	rts

hilfe:
	lea	hilfe_str(pc),a0	hilfe!
	bsr	print
	rts
byte_flag:
	move	#1,length_flag
	rts
word_flag:
	move	#2,length_flag
	rts
long_flag:
	move	#4,length_flag
	rts
adresse_einlesen:
	move.b	base_char(pc),d7
	bsr	bconout
	lea	hex_zahl(pc),a0
	clr.l	(a0)
	clr.l	4(a0)
	clr.l	8(a0)
	bsr	read_hex
	tst.b	1(a0)
	beq.s	.keine_neue_zahl
	lea	address(pc),a1
	moveq	#8,d0
	addq	#2,a0
	bsr	ascii_hex
.keine_neue_zahl:
	rts
new_base:
	bsr	cconin
	cmp.b	#'%',d0
	bne	.go1
	move.b	d0,base_char
	move.b	#2,base
.go1:
	cmp.b	#'&',d0
	bne	.go2
	move.b	d0,base_char
	move.b	#8,base
.go2:
	cmp.b	#'#',d0
	bne	.go3
	move.b	d0,base_char
	move.b	#10,base
.go3:
	cmp.b	#'$',d0
	bne.s	.no_char
	move.b	d0,base_char
	move.b	#$10,base
.no_char:
	lea	base_str(pc),a0
	bsr	print
	move.b	base(pc),d1
	and	#$ff,d1
	bsr	prt_hexw
	rts
free_memory:
	Malloc	#-1
	move.l	d0,d1
	move	#9,d7
	bsr	bconout
	bsr	prt_ldez
	lea	free_memory_str(pc),a0
	bsr	print
	rts

;bytes (a0) in ascii (a1) wandeln (m.H. einer Tabelle)
;d0 = anzahl der bytes
;ABER modifiziert zur benutzung zus�tzlicher trennzeichen zur ab
;grenzung der einzelnen einheiten
hex_ascii:
	movem.l	d1-d2/a2,-(sp)
	move	length_flag(pc),d2
	lea	.hexasc_table(pc),a2
	moveq	#0,d1
	subq.l	#1,d0
.hexast_loop:
	move.b	(a0),d1
	lsr.b	#4,d1
	move.b	(a2,d1.l),(a1)+

	move.b	(a0)+,d1
	and.b	#$0f,d1
	move.b	(a2,d1.l),(a1)+

	subq	#1,d2
	bne.s	.go_on
	move.b	#'.',(a1)+
	move	length_flag(pc),d2
.go_on:
	dbf	d0,.hexast_loop
	move.b	#$a,(a1)+
	move.b	#$d,(a1)+
	clr.b	(a1)
	movem.l (sp)+,d1-d2/a2
	rts
.hexasc_table:
	dc.b	$30,$31,$32,$33,$34,$35,$36,$37
	dc.b	$38,$39,$41,$42,$43,$44,$45,$46

mem_dump:
	bsr	crsr_off
	bsr	prt_ret
	move.l	address(pc),a6
.mem_dump:
	move.l	a6,a0
	move.l	a0,d1
	bsr	prt_hex
	moveq	#16,d0
	lea	buffer+1(pc),a1
	bsr.s	hex_ascii

	tst.b	richtung
	beq.s	.go_on
	move.b	#27,-2(a1)
	move.b	#$49,-1(a1)
	move.b	#$d,(a1)
	clr.b	1(a1)
.go_on:
	Cconws	buffer(pc)
	tst.b	richtung
	bne.s	.minus
	add	#32,a6
.minus:
	sub	#16,a6
	Cconis
	tst	d0
	beq.s	.mem_dump
	bsr	cconin
	cmp.b	#$d,d0
	bne	.dump_weiter
	bsr	cconin
.dump_weiter:
	swap	d0
	cmp.b	#80,d0	vorw�rts
	sne	richtung
	beq	.mem_dump
	cmp.b	#72,d0	r�ckw�rts
	seq	richtung
	beq	.mem_dump
	move.l	a6,address
	rts

ascii_dump:
	bsr	crsr_off
	lea	home_insert(pc),a4
	lea	ret(pc),a5
	move.l	address(pc),a6
.ascii_dump:
	move.l	a6,a0
	move.l	a0,d1
	bsr	prt_hex
	Bconout	#9
	moveq	#31,d2
.loop:
	moveq	#0,d0
	move.b	(a0)+,d0
	Bconout	d0,#5
	dbf	d2,.loop
.go_on:
	tst.b	richtung
	beq.s	.vorw�rts
	Cconws	(a4)
.vorw�rts:
	Cconws	(a5)
	tst.b	richtung
	bne.s	.minus
	add	#64,a6
.minus:
	sub	#32,a6
	Cconis
	tst	d0
	beq	.ascii_dump
	bsr	cconin
	cmp.b	#$d,d0
	bne.s	.dump_weiter
	bsr	cconin
.dump_weiter:
	swap	d0
	cmp.b	#80,d0	vorw�rts
	sne	richtung
	beq	.ascii_dump
	cmp.b	#72,d0	r�ckw�rts
	seq	richtung
	beq	.ascii_dump
	move.l	a6,address
	rts

disass:
	bsr	crsr_off
	bsr	prt_ret
	lea	home_insert(pc),a4
	lea	ret(pc),a5
	move.l	address(pc),a6
.loop:
	tst.b	richtung
	beq.s	.vorw�rts
	Cconws	(a4)
.vorw�rts:
	move.l	a6,d1
	bsr	prt_hex
	Bconout	#9
	cmp	#$4e76,(a6)
	beq	.trap_v
	cmp	#$4e75,(a6)
	beq	.rts
	cmp	#$4e77,(a6)
	beq	.rtr
	cmp	#$4e73,(a6)
	beq	.rte
	cmp	#$4e70,(a6)
	beq	.reset
	cmp	#$4e71,(a6)
	beq	.nop
	cmp	#$4afc,(a6)
	beq	.illegal
	cmp	#$4e72,(a6)
	beq	.stop
	Cconws	const(pc)
	move	(a6),d1
	bsr	prt_hexw
.return:
	Cconws	(a5)
.go_on:
	tst.b	richtung
	bne.s	.minus
	addq	#4,a6
.minus:
	subq	#2,a6
	Cconis
	tst	d0
	beq	.loop
	bsr	cconin
	cmp.b	#$d,d0
	bne.s	.disass_weiter
	bsr	cconin
.disass_weiter:
	swap	d0
	cmp.b	#80,d0	vorw�rts
	sne	richtung
	beq	.loop
	cmp.b	#72,d0	r�ckw�rts
	seq	richtung
	beq	.loop
	move.l	a6,address
	rts
.prt_str:
	bsr	print
	bra.s	.return
.trap_v:
	lea	trapv(pc),a0
	bra.s	.prt_str
.rts:
	lea	rts(pc),a0
	bra.s	.prt_str
.rtr:
	lea	rtr(pc),a0
	bra.s	.prt_str
.rte:
	lea	rte(pc),a0
	bra.s	.prt_str
.reset:
	lea	reset(pc),a0
	bra.s	.prt_str
.stop:	  
	lea	stop(pc),a0
	bsr	print
	addq	#2,a6
	move	(a6),d1
	bsr	prt_hexw
	bra	.return
.illegal:
	lea	illegal(pc),a0
	bra.s	.prt_str
.nop:
	lea	nop(pc),a0
	bra.s	.prt_str

ende:	move.b	klick(pc),(conterm).w
	rts

	include	read_hex.s	krimskrams
	include	prt_hexw.s
	include	prt_ldez.s	|
	include	prt_hex.s	|
	include	asc_hex.s	-
bconout:
	Bconout	d7
	rts

cconin:	Cconin
	rts

clr_ln:	lea	clrln(pc),a0
print:
	Cconws	(a0)
	rts
prt_ret:
	lea	ret(pc),a0
	bra.s	print
crsr_on:
	moveq	#1,d0
crsr:	Cursconf	d0
	rts
crsr_off:
	moveq	#1,d0
	bra.s	crsr

	data
wie_bitte:	dc.b	9,'Kein Befehl!',0
ret:	dc.b	$d,$a,0
clrln:	dc.b	$a,27,'l',0
home_insert:	dc.b	27,'I',27,'I',$d,0
begin_str:	dc.b	'MAG-DUMP, (c) Magnum -BlackBox-.'
version:	dc.b	' V0.30',$d,$a,'HELP->Hilfe',$d,$a,$d,0
hilfe_str:	dc.b	'Q->Quit',$d,$a
	dc.b	'fm->freier Speicherplatz	m->Speicherdump',$d,$a
	dc.b	'b<c>->neue Basis bestimmen mit <c>=$#&%, sonst die alte ausgeben lassen',$d,$a
	dc.b	'L->Langwort-	W->Wort-	B->Bytejustierung',$d,$a
	dc.b	'd->Disassemblieren	a->ASCII-Dump	A->neue Adresse bestimmen',$d,$a,0
free_memory_str:	dc.b	' Bytes freier Speicher.',0
base_str:	dc.b	' Basis ist $',0
illegal:	dc.b	'illegal',0
stop:	dc.b	'stop',9,'#$',0
rte:	dc.b	'rte',0
rtr:	dc.b	'rtr',0
rts:	dc.b	'rts',0
reset:	dc.b	'reset',0
trapv:	dc.b	'trapv',0
nop:	dc.b	'nop',0
const:	dc.b	'dc.w',9,'$',0
	copyright

	bss
buffer:	ds.l	20
hex_zahl:	ds.l	3
address:	ds.l	1
length_flag:	ds.w	1
base:	ds.b	1
base_char:	ds.b	1
richtung:	ds.b	1
klick:	ds.b	1