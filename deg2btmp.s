	include	tos_fnkt

;konvertiert ein degas.pi? bild in ein 32000'er bild (doodle format).

start
	init	stack
	Cconws	string
	Cconrs	#30,magnum
	Fopen	#0,magnum+2
	move.w	d0,handle
	bmi	open_error
	Fread	buffer,#32066,handle
	Fclose	handle
	tst.w	d0
	bmi	close_error
	Cconws	string2
	Cconrs	#8,magnum2
	lea	magnum2,a0
	move.b	1(a0),d0
	and.l	#$ff,d0
	addq	#2,d0
	add.l	d0,a0
	move.b	#'.',(a0)+
	move.b	#'d',(a0)+
	move.b	#'o',(a0)+
	move.b	#'o',(a0)+
	clr.b	(a0)
	Fcreate	#0,magnum2+2
	move.w	d0,handle
	bmi.s	open_error
	Fwrite	buffer+34,#32000,handle
	Fclose	handle
	Cconws	taste
	Cconin
open_error
close_error
	Pterm0

	data
string	dc.b	'DEG_BTMP wandelt Bilder im ungepackten DEAGS Format ins DOO',$d,$a
	dc.b	'Format um. Einzugeben ist der volle Pfadname, wenn sich das Bild',$d,$a
	dc.b	'nicht im aktuellen Directory befindet. Beim Namen des DOO-Files',$d,$a
	dc.b	'sollte die Endung weggelassen werden. Es wird automatisch `.DOO`',$d,$a
	dc.b	'angeh�ngt.	MAGNUM',$d,$a
	dc.b	'Filename des DEGAS Bildes: ',0
string2	dc.b	$d,$a,'Filename des DOO Bildes: ',0
taste	dc.b	$d,$a,'O.K.	Taste...',0
	bss
handle	ds.w	1
magnum	ds.l	8
magnum2	ds.l	9
buffer	ds.l	8017