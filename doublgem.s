	include	tos_fnkt	;meine geliebten macros

;ein kleiner test, ob es m�glich ist, ein und denselben screen
;mehrmals darzustellen. Es ist auf anhieb gelungen (sehr selten!!!)
;ich bin ausserdem �berrascht, was die 'wackelquantit�t' angeht.
;vielleicht ein neues gimmick f�r eine winzige demo?

zeile	=	250
HBL	=	$68
VBL	=	$70
VIDEO_M	=	$ffff8203
VIDEO_H	=	$ffff8201
COLOR00	=	$ffff8240
SYNC	=	$ffff820a
IER_A	=	$fffffa07
IER_B	=	$fffffa09
IPR_A	=	$fffffa0b
IPR_B	=	$fffffa0d
IMR_A	=	$fffffa13
IMR_B	=	$fffffa15
ISR_A	=	$fffffa0f
ISR_B	=	$fffffa11
;*********************************************

start
	bra.s	begin
new_vbl
	clr	hbl_counter
	move	#$2000,sr
old_vbl
	jmp	($12345678).l
new_hbl
	addq	#1,hbl_counter
	cmp	#zeile,hbl_counter
	beq.s	switch_line_counter
	rte
switch_line_counter
	move	#$2300,sr
	movem.l	d0/a0,-(sp)
	lea	(VIDEO_H).w,a0
	move	screen,d0
	movep.w	d0,(a0)
	move.b	#%11,(SYNC).w
	nop
	move.b	#%10,(SYNC).w
	movem.l	(sp)+,d0/a0
	rte

;*******************************
;daten
;*******************************
screen	ds.l	1
hbl_counter	ds.w	1

begin
	Logbase
	move.l	d0,a6
	lsr.l	#8,d0
	move	d0,screen
	Supexec	super
	Ptermres	#0,#begin-start+$100

super
	move	#$2700,sr
	bsr.s	init_vbl
	bsr	init_hbl
	move	#$2000,sr
	rts

init_vbl
	move.l	(VBL).w,old_vbl+2
	move.l	#new_vbl,(VBL).w
	rts
init_hbl
	move.l	#new_hbl,(HBL).w
	rts
