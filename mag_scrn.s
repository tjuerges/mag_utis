	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	output	f:\mag_utis\mag_scrn.prg
;Leitet die aktuelle Hardcopy vom Bildschirm, die zum Drucker
;gehen sollte, in eine IFF-Datei Namens 'screen#.iff' um.

_v_bas_ad	=	$44e
savptr	=	$4a2
_prt_cnt	=	$4ee
dump_vec	=	$502

regs	reg	d0-d1/a0

	bra	initialisierung
hardcopy
	tst	(_prt_cnt).w	;Ist das Programm schon bei der Arbeit?
	bne.s	ist_schon_aktiv
	movem.l	regs,-(sp)	;Register retten, man wei� ja nie.
	bsr.s	dojob	;Do it...
	movem.l	(sp)+,regs
	move	#-1,(_prt_cnt).w	;Abmelden, Hardcopy fertig.
ist_schon_aktiv
	rts	;Und Tsch��.

dojob
	sub.l	#46,(savptr).w	;Tip aus dem Profibuch.
	Kbshift	#-1	;Tastaturstatus feststellen.
	add.l	#46,(savptr).w
	and.b	#3,d0	;Alt&Help gedr�ckt?
	bne.s	ist_schon_aktiv	;Nee: Kleiner Programmiertechnischer Kniff.

	addq.b	#1,nummer	;Doch eine Hardcopy anfertigen.
	cmp.b	#$39,nummer	;Ist die Nummer des Files >9
	ble.s	.name_ok	;Nein, also ist der Name o.k.
	move.b	#$30,nummer	;Neue Nummer 0.
.name_ok
	Fcreate	#0,filename(pc)	;File neu anlegen.
	move	d0,d1	;Filehandle retten.
	bmi.s	abbruch	;Filehandle negativ: Abbruch, da es einen Fehler gab.
	lea	iff_header(pc),a0	;Aktuelle Position der Daten.
	move.l	#iff_header_ende-iff_header,d0	;L�nge der Daten.
	bsr.s	fwrite	;Und die Daten schreiben.
	move.l	(_v_bas_ad).w,a0	;Aktuelle Position der Daten.
	move.l	#32000,d0	;L�nge der Daten.
	bsr.s	fwrite	;Und nochmals schreiben.
abbruch
	Fclose	d1	;File schliessen.
	rts	;Und aus der Hardcopy-Funktion zur�ckkehren.
fwrite
	Fwrite	(a0),d0,d1	;Schreibt in das File mit dem Handle
	;in d1, dem Datenpointer in a0 und der Filel�nge in d0.
	rts

;Nun folgt der Kopf der IFF-Grafik-Datei:
iff_header	dc.l	'FORM',(iff_header_ende-iff_header-4)+32000
	dc.l	'ILBM','BMHD'
	dc.l	bmhd_ende-bmhd_start
bmhd_start	dc.w	640,400,0,0
	dc.b	1,0,0,0
	dc.w	2
	dc.b	10,11
	dc.w	640,400
bmhd_ende	dc.l	'BODY',32000
iff_header_ende

;Filename der Bildschirmhardcopy:
filename	dc.b	'screen'

;Nummer der Bildschirmhardcopy:
nummer	dc.b	'0'-1

;Fileendung:
	dc.b	'.iff',0
	copyright

	even
initialisierung
	init	stack(pc)	;Stack usw. f�r Ptermres initialisieren.
	Supexec	super(pc)	;Initialisierung im Supervisormodus.
	Cconws	text(pc)	;Meldung ausgeben.
	Ptermres	#0,#$100+initialisierung-hardcopy	;Programm resident halten.
super
	move.l	#hardcopy,(dump_vec).w	;Neue Routine eintragen.
	rts

text	dc.b	$d,$a,'MAG-SCRN, (c) Magnum -BlackBox-.',$d,$a
	dc.b	'Die Screen-Hardcopies werden nun',$d,$a
	dc.b	'auf (Hard-)Disk im aktuellen',$d,$a
	dc.b	'Verzeichnis als IFF-Dateien abge-',$d,$a
	dc.b	'speichert.',$d,$a,0
