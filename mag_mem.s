	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	include	include\gem
	output	f:\mag_utis\mag_mem.prg

;zeigt den freien speicher an.

start:
	move.l	4(sp),a0
	init	stack(pc)
begin:
	Malloc	#-1
	add.l	$c(a0),d0
	add.l	$14(a0),d0
	add.l	$1c(a0),d0
	move.l	d0,d1
	lea	zahl(pc),a0
	bsr.s	copy_ldez
	form_alert	#1,#alert
	Pterm0

	include	include\call_gem
	include	sub\copyldez.s
	data
alert	dc.b	'[0][Freier Speicher:|'
zahl	dc.b	'0000000 Bytes.][Ende]',0
	copyright
