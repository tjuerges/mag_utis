; ATARI.S       Equate and macro file for atari ST      RMS
;               Extended and Improved: 8/11/88 - RJZ
;               Combined and Extended and Improved: 09/16/88 - RMS
;

save_rs         EQU ^^RSCOUNT ;RS-Counter merken

; File header structure
                RSRESET
FILE_ID:        RS.W 1
TSIZE:          RS.L 1
DSIZE:          RS.L 1
BSIZE:          RS.L 1
SSIZE:          RS.L 1
XXX1:           RS.L 1
XXX2:           RS.L 1
XXX3:           RS.W 1
HEADSIZE        EQU ^^RSCOUNT

; base page structure
                RSRESET
LOWTPA:         RS.L 1
HITPA:          RS.L 1
TBASE:          RS.L 1
TLEN:           RS.L 1
DBASE:          RS.L 1
DLEN:           RS.L 1
BBASE:          RS.L 1
BLEN:           RS.L 1
DTA:            RS.L 1
PARENT:         RS.L 1
XXXX:           RS.L 1
ENVIR:          RS.L 21
CMDLINE:        RS.B 128

; defines
TEXTSZ          EQU TLEN
DATASZ          EQU DLEN
BSSSZ           EQU BLEN
BPSZ            EQU ^^RSCOUNT
MYDTA           EQU DTA
                RSSET save_rs ;RS-Counter wieder zur�cksetzen

CR              EQU $0d
LF              EQU $0a
TAB             EQU $09

****
****  This is a preliminary equates file for the Atari ST;
****  not all the equates may be here, there may be typos
****  we haven't caught, and not all the macros for Gemdos
****  and the BIOS have been defined.
****

;
; Atari ST hardware locations
;
memconf         EQU $ffff8001 ;memory configuration
vbasehi         EQU $ffff8201
vbasemid        EQU $ffff8203 ;Video base address
vcounthi        EQU $ffff8205
vcountmid       EQU $ffff8207
vcountlo        EQU $ffff8209 ;Video display counter
syncmode        EQU $ffff820a ;video sync mode
vbaselo         EQU $ffff820d ;Video base address (lo; STE only)
linewid         EQU $ffff820f ;Width of a scan-line (Words, minus 1; STE only)
palette         EQU $ffff8240 ;color registers 0..15
rezmode         EQU $ffff8260 ;Resolution mode (0=320x200,1=640x200,2=640x400)
hscroll         EQU $ffff8265 ;Horizontal scroll count (0..15; STE only)

diskctl         EQU $ffff8604 ;disk controller data access
fifo            EQU $ffff8606 ;DMA mode control
dmahi           EQU $ffff8609
dmamid          EQU $ffff860b
dmalo           EQU $ffff860d ;DMA base address

cmdreg          EQU $80     ;1770/FIFO command register select
trkreg          EQU $82     ;1770/FIFO track register select
secreg          EQU $84     ;1770/FIFO sector register select
datareg         EQU $86     ;1770/FIFO data register select

; GI ("psg") sound chip:
giselect        EQU $ffff8800 ;(W) sound chip register select
giread          EQU $ffff8800 ;(R) sound chip read-data
giwrite         EQU $ffff8802 ;(W) sound chip write-data
gitoneaf        EQU 0       ;channel A fine/coarse tune
gitoneac        EQU 1
gitonebf        EQU 2       ;channel B
gitonebc        EQU 3
gitonecf        EQU 4       ;channel C
gitonecc        EQU 5
ginoise         EQU 6       ;noise generator control
gimixer         EQU 7       ;I/O control/volume control register
giaamp          EQU 8       ;channel A, B, C amplitude
gibamp          EQU 9
gicamp          EQU $0a
gifienvlp       EQU $0b     ;envelope period fine, coarse tune
gicrnvlp        EQU $0c
giporta         EQU $0e     ;GI register# for I/O port A
giportb         EQU $0f     ;Centronics output register

; Bits in "giporta":
xrts            EQU 8       ;RTS output
dtr             EQU $10     ;DTR output
strobe          EQU $20     ;Centronics strobe output
gpo             EQU $40     ;"general purpose" output

; 68901 ("mfp") sticky chip:
mfp             EQU $fffffa00 ;mfp base
gpip            EQU mfp+1   ;general purpose I/O
aer             EQU mfp+3   ;active edge reg
ddr             EQU mfp+5   ;data direction reg
iera            EQU mfp+7   ;interrupt enable A & B
ierb            EQU mfp+9
ipra            EQU mfp+$0b ;interrupt pending A & B
iprb            EQU mfp+$0d
isra            EQU mfp+$0f ;interrupt inService A & B
isrb            EQU mfp+$11
imra            EQU mfp+$13 ;interrupt mask A & B
imrb            EQU mfp+$15
vr              EQU mfp+$17 ;interrupt vector base
tacr            EQU mfp+$19 ;timer A control
tbcr            EQU mfp+$1b ;timer B control
tcdcr           EQU mfp+$1d ;timer C & D control
tadr            EQU mfp+$1f ;timer A data
tbdr            EQU mfp+$21 ;timer B data
tcdr            EQU mfp+$23 ;timer C data
tddr            EQU mfp+$25 ;timer D data
scr             EQU mfp+$27 ;sync char
ucr             EQU mfp+$29 ;USART control reg
rsr             EQU mfp+$2b ;receiver status
tsr             EQU mfp+$2d ;transmit status
udr             EQU mfp+$2f ;USART data

; 6850 registers:
keyctl          EQU $fffffc00 ;keyboard ACIA control
keybd           EQU $fffffc02 ;keyboard data
midictl         EQU $fffffc04 ;MIDI ACIA control
midi            EQU $fffffc06 ;MIDI data

; BIOS Variables
etv_timer       EQU $0400   ;vector for timer interrupt chain
etv_critic      EQU $0404   ;vector for critical error chain
etv_term        EQU $0408   ;vector for process terminate
etv_xtra        EQU $040c   ;5 reserved vectors
memvalid        EQU $0420   ;indicates system state on RESET
memcntlr        EQU $0424   ;mem controller config nibble
resvalid        EQU $0426   ;validates 'resvector'
resvector       EQU $042a   ;[RESET] bailout vector
phystop         EQU $042e   ;physical top of RAM
_membot         EQU $0432   ;bottom of available memory;
_memtop         EQU $0436   ;top of available memory;
memval2         EQU $043a   ;validates 'memcntlr' and 'memconf'
flock           EQU $043e   ;floppy disk/FIFO lock variable
seekrate        EQU $0440   ;default floppy seek rate
_timr_ms        EQU $0442   ;system timer calibration (in ms)
_fverify        EQU $0444   ;nonzero: verify on floppy write
_bootdev        EQU $0446   ;default boot device
palmode         EQU $0448   ;nonzero ==> PAL mode
defshiftmd      EQU $044a   ;default video rez (first byte)
sshiftmd        EQU $044c   ;shadow for 'shiftmd' register
_v_bas_ad       EQU $044e   ;pointer to base of screen memory
vblsem          EQU $0452   ;semaphore to enforce mutex in vbl
nvbls           EQU $0454   ;number of deferred vectors
_vblqueue       EQU $0456   ;pointer to vector of deferred vfuncs
colorptr        EQU $045a   ;pointer to palette setup (or NULL)
screenpt        EQU $045e   ;pointer to screen base setup (|NULL)
_vbclock        EQU $0462   ;count of vblank interrupts
_frclock        EQU $0466   ;count of unblocked vblanks (not blocked by vblsem)
hdv_init        EQU $046a   ;hard disk initialization
swv_vec         EQU $046e   ;video change-resolution bailout
hdv_bpb         EQU $0472   ;disk "get BPB"
hdv_rw          EQU $0476   ;disk read/write
hdv_boot        EQU $047a   ;disk "get boot sector"
hdv_mediach     EQU $047e   ;disk media change detect
_cmdload        EQU $0482   ;nonzero: load COMMAND.COM from boot
conterm         EQU $0484   ;console/vt52 bitSwitches (%%0..%%2)
trp14ret        EQU $0486   ;saved return addr for _trap14
criticret       EQU $048a   ;saved return addr for _critic
themd           EQU $048e   ;memory descriptor (MD)
_____md         EQU $049e   ;(more MD)
savptr          EQU $04a2   ;pointer to register save area
_nflops         EQU $04a6   ;number of disks attached (0, 1+)
constate        EQU $04a8   ;state of conout() parser
save_row        EQU $04ac   ;saved row# for cursor X-Y addressing
sav_context     EQU $04ae   ;pointer to saved processor context
_bufl           EQU $04b2   ;two buffer-list headers
_hz_200         EQU $04ba   ;200hz raw system timer tick
_drvbits        EQU $04c2   ;bit vector of "live" block devices
_dskbufp        EQU $04c6   ;pointer to common disk buffer
_autopath       EQU $04ca   ;pointer to autoexec path (or NULL)
_vbl_list       EQU $04ce   ;initial _vblqueue (to $4ee)
_prt_cnt        EQU $04ee   ;screen-dump flag (non-zero abort screen dump)
_prtabt         EQU $04f0   ;printer abort flag
_sysbase        EQU $04f2   ;-> base of OS
_shell_p        EQU $04f6   ;-> global shell info
end_os          EQU $04fa   ;-> end of OS memory usage
exec_os         EQU $04fe   ;-> address of shell to exec on startup
scr_dump        EQU $0502   ;-> screen dump code
prv_lsto        EQU $0506   ;-> _lstostat()
prv_lst         EQU $050a   ;-> _lstout()
prv_auxo        EQU $050e   ;-> _auxostat()
prv_aux         EQU $0512   ;-> _auxout()
                END
