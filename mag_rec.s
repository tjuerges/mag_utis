	opt	a+,c+,o+,w+,m+
	include	include\tos_fnkt
	include	include\gem

	output	mag_rec.acc
;ein gem-rekorder.

	lea	stack(pc),sp
	clr	schalter
start
	appl_init
	menu_register	appl_id(pc),#menu_name,menu_id
	move	menu_id(pc),d7
warte
	evnt_mesag	#puffer
	cmp	#AC_OPEN,puffer
	bne.s	warte
	cmp	puffer+8(pc),d7
	bne.s	warte
	tst	schalter
	beq.s	.weiter
	bra	save_data
.weiter
	moveq	#1,d1
	lea	start_alert(pc),a0
	bsr	form_alert
	cmp	#3,d6
	beq.s	warte
	cmp	#1,d6
	beq	record
	cmp	#2,d6
	beq.s	play
	bra.s	warte
play
	Fopen	#0,file_name(pc)
	move	d0,handle
	bmi.s	warte
	Fread	play_record(pc),#8000,handle(pc)
	move.l	d0,d6
	bsr	fclose
	cmp.l	#8000,d6
	bne	warte
play_loop
	appl_tplay	#1000,#100,#play_record
	bra	warte

record
	move	#-1,schalter
	appl_trecord	#1000,#play_record
	tst	schalter
	beq	warte
save_data
	clr	schalter
	moveq	#1,d1
	lea	record_alert(pc),a0
	bsr.s	form_alert
	cmp	#2,d6
	beq	warte
	Fcreate	#0,file_name(pc)
	move	d0,handle
	bmi	warte
	Fwrite	play_record(pc),#8000,handle(pc)
	bsr.s	fclose
	bra	warte
fclose
	Fclose	handle(pc)
	rts
form_alert
	form_alert	d1,a0
	move	int_out(pc),d6
	rts

	include	include\call_gem
	data
menu_name	dc.b	'  Event-Recorder',0
start_alert	dc.b	'[1][ MAG-REC, | (c) Magnum -BlackBox-. |'
	dc.b	'GEM-Event-Recorder V1.0 |'
	dc.b	' Events aufnehmen oder abspielen ? ]'
	dc.b	'[ Record | Play | Abbruch ]',0
record_alert	dc.b	'[3][ Der Event-Puffer ist voll. |'
	dc.b	' Wie soll fortgefahren werden ? ]'
	dc.b	'[ Stop | Abbruch ]',0
file_name	dc.b	'recorder.dat',0
	copyright
	bss
puffer	ds.l	4
schalter	ds.w	1
menu_id	ds.w	1
handle	ds.w	1
test	ds.l	2
play_record	ds.l	2000
my_stack	ds.l	64
stack
