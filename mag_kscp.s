	include	tos_fnkt	;gemdos-, bios- & xbios-macros einbinden.
	include	gem	;vdi- & aes-macros einbinden.

dauer=$2800

	init	stack(pc)
start
	init_gem	#2
	v_hide_c

	dc.w	$a000
	move.l	a0,a6

	bsr	randomize
	Logbase
	move.l	d0,a5

	lea	xor_pixel(pc),a3

clr_screen:
	v_clrwk

	bsr	random
	move	d0,d6
	and	#$ff,d6
	move	d0,d7
	and	#$7f,d7

big_loop:
	move	#dauer,d3
loop:
	bsr	put_pixel

	btst	#0,CUR_MS_STAT(a6)
	bne.s	clr_screen
	btst	#1,CUR_MS_STAT(a6)
	bne.s	ende
	dbf	d3,loop
	bra.s	clr_screen
ende:
	v_show_c	#1
	exit_gem
	Pterm0

daten:	dc.w	$0102,$0408,$0306,$090c

put_pixel:
	bsr	random
	move	d0,d5
	moveq	#7,d4
kalei_pixel_loop:
	move.b	d5,d0
	and	#7,d0
	move.b	daten(pc,d0.w),d0
	btst	#0,d0
	beq.s	no_bit0
	addq	#1,d7
	bra.s	no_bit2
no_bit0:
	btst	#2,d0
	beq.s	no_bit2
	subq	#1,d7
no_bit2:
	btst	#1,d0
	beq.s	no_bit1
	addq	#1,d6
	bra.s	no_bit3
no_bit1:
	btst	#3,d0
	beq.s	no_bit3
	subq	#1,d6
no_bit3:
	tst	d6
	bpl.s	offset_a_plus
	moveq	#3,d6
offset_a_plus:
	tst	d7
	bpl.s	offset_b_plus
	moveq	#3,d7
offset_b_plus:
	add	d7,d0
	cmp	#198,d0
	bmi.s	put_kalei_pixel
	subq	#2,d6
	subq	#2,d7
	bra.s	no_bit3
put_kalei_pixel:
	move	#199,d0
	sub	d7,d0
	move	#121,d1
	add	d6,d1
	jsr	(a3)

	move	#200,d0
	add	d7,d0
	move	#121,d1
	add	d6,d1
	jsr	(a3)

	move	#199,d0
	sub	d7,d0
	move	#518,d1
	sub	d6,d1
	jsr	(a3)

	move	#200,d0
	add	d7,d0
	move	#518,d1
	sub	d6,d1
	jsr	(a3)

	moveq	#1,d0
	add	d6,d0
	move	#319,d1
	sub	d7,d1
	jsr	(a3)

	moveq	#1,d0
	add	d6,d0
	move	#320,d1
	add	d7,d1
	jsr	(a3)

	move	#398,d0
	sub	d6,d0
	move	#319,d1
	sub	d7,d1
	jsr	(a3)

	move	#398,d0
	sub	d6,d0
	move	#320,d1
	add	d7,d1
	jsr	(a3)

	ror	#3,d5
	dbf	d4,kalei_pixel_loop
	rts

xor_pixel
	move	d0,d2
	add	d0,d0
	add	d0,d0
	add	d2,d0
	lsl	#4,d0
	move.b	d1,d2
	not.b	d2
	lsr	#3,d1
	add	d1,d0
	bchg	d2,(a5,d0.w)
	rts

	include	call_gem
	include	randomiz.s
	include	rand.s
