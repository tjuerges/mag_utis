  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	;output	;So hei�t das PRG-File.

	bra	start
begin
	dc.l	'XBRA','MGXY'
old_mouse_vec	dc.l	0
new_mouse_vec
	movem.l	d0-a6,-(sp)

	movem	d0-d1,-(sp)

	lea	text_x(pc),a0
	move.b	#' ',(a0)
	move.b	#' ',1(a0)
	move.b	#' ',2(a0)
	clr.l	d1
	move	(sp)+,d1
	bsr.s	prt_dez

	lea	text_y(pc),a0
	move.b	#' ',(a0)
	move.b	#' ',1(a0)
	move.b	#' ',2(a0)
	clr.l	d1
	move	(sp)+,d1
	bsr.s	prt_dez
	pre_trap
	Cconws	text(pc)
	post_trap
	movem.l	(sp)+,d0-a6
	move.l	old_mouse_vec(pc),-(sp)
	rts

;prt_dez gibt ein datum in dez
;auf dem bilschirm aus. d1=datum(6 g�ltige stellen)
prt_dez
	movem.l	d1-d5,-(sp)
	moveq	#0,d2
	moveq	#0,d5
	and.l	#$fffff,d1
	tst.l	d1
	beq.s	.ok
	move.l	d1,d4
	move.l	#100000,d5
	moveq	#0,d3
.loop1
	moveq	#0,d2
.loop2
	cmp.l	d5,d4
	bmi.s	.is_lower
	moveq	#1,d3
	addq	#1,d2
	sub.l	d5,d4
	bne.s	.loop2
.is_lower
	tst.b	d2
	bne.s	.ok
	tst.b	d3
	beq.s	.is_zero
.ok
	add.w	#'0',d2
	move.b	d2,(a0)+
.is_zero
	divu	#10,d5
	bne.s	.loop1

	movem.l	(sp)+,d1-d5
	rts
text	dc.b	27,'Y',32+0,32+60,'X: '
text_x	dc.b	'   '
	dc.b	' Y:'
text_y	dc.b	'   '
	rept	10
	dc.b	0
	endr
	even

ende

start
	graf_handle
	move	intout(pc),contrl+12
	vex_curv	#new_mouse_vec
	move.l	contrl+18(pc),old_mouse_vec
	Ptermres	#0,#ende-begin+$100

	include	include\call_gem
