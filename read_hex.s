;liest eine bis zu 8stellige hexzahl ein
;a0=>eingabebuffer(3 LW)
read_hex
	move.l	a0,-(sp)

	Cconrs	#8,(a0)
	addq	#1,a0
	move.b	(a0)+,d0
	beq.s	.fertig
	ext	d0
	cmp.b	#8,d0
	beq.s	.fertig
	moveq	#7,d1
	subq	#1,d0
.loop
	move.b	(a0,d0),(a0,d1)
	clr.b	(a0,d0)
	subq	#1,d1
	dbf	d0,.loop
.fertig
	move.l	(sp)+,a0
	rts
