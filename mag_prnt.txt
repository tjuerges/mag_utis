	;V0.99	3-2-1991
	;---------------
	;Ersetzt die sehr langsamen Ausgaberoutinen des TOS
	;durch meine etwas schnelleren.
	;Ich benutze hier die XBRA-Methode, um anderen Programmen
	;auch noch eine Chance zu geben.
	;BIOS & GEMDOS werden gepatcht.
	;->Danke<- an Gis f�r Deine Hilfe (Wie immer...)!
	;V0.99-�  4-2-1991
	;-----------------
	;Versuche zur Geschwindigkeitssteigerung sind
	;leider kl�glich gescheitert. So was bl�des! Wie
	;macht z.B. Quick-ST das nur?
	;Einbindung der TOS-Cursorposition, Einbindung in die
	;TOS-con_state-Routine.
	;V0.9901  24-5-1991
	;------------------
	;Ich habe die Arbeit am Speeder wieder aufgenommen.
	;Mein Vorhaben steht fest: Er soll eine angenehme Ge-
	;schwindigkeit bei allen Aufl�sungen bieten.
	;Einbindung einiger LINE-A-Variablen.
	;27.5.1991
	;---------
	;HURRA!!!
	;Ich habe es geschafft! Endlich:
	;1. Absturzfrei!
	;2. Sauschnell
	;3. Sehr kompatibel mit anderen s/w-Aufl�sungen
	;4. Besteht jeden H�rtetest. (Glaube ich)
	;
	;Ich habe (schon wieder) etwas optimiert. Nun werden die
	;wichtigsten Sachen in Registern gehalten und so f�r Adressie-
	;rung keine Zeit verschwendet. Ausserdem wird auf eine Reihe
	;von Vars Registerrelativ zugegriffen. Ergebnis: Code wird
	;kleiner.
	;
	;Ich habe diese Version(3d) wieder weiterbenutzt, da Version 4
	;kein bisschen Geschwindigkeitszuwachs brachte.
	;31.5.1991
	;---------
	;Ich berechne (nat�rlich nach einem Tip von Gis) die Bildschirm-
	;adresse f�r jedes Zeichen nicht neu.
	;Au�erdem habe ich bei der Addition wieder LEA benutzt.
	;1.6.1991
	;--------
	;Wiederum einige �nderungen. Das korrekte Funktionieren
	;kann ich jetzt garantieren. Wurde auch Zeit. Es werden
	;weitere LINE-A-Variablen benutzt.
	;Die Schnelligkeit beim Scrolling kann sich m.M. nach sehen-
	;lassen.(V 9.0)
	;Kompatibilit�t d�rfte auch kein Problem sein. (�hem... voraus-
	;gesetzt nat�rlich, man hat einen Blitter)
	;3.6.1991
	;--------
	;Ein paar neue Escape-Codes habe ich implementiert. Die Geschwin-
	;digkeit finde ich inzwischen zufriedenstellend.
	;Achja, bevor ich es vergesse: Jetzt wird bei der Installation
	;�berpr�ft, ob ein Blitter vorhanden ist. (Es war Gis Wunsch!)
	;
	;11.6.1991
	;---------
	;Jetzt sind alle Escape-Sequenzen bis auf Cusor-an/aus implemen-
	;tiert. Volle funktionst�chtigkeit garantiert.
	;Das Cursor-Ausschalten funktioniert jetzt richtig.
	;Au�erdem wird die LINE-A-Variable RESERVED (Cursor-Flag) benutzt.
	;->Speicherplatz gespart und alles immer auf dem richtigen Wert!
