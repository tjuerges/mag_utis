	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	win.tos	;So hei�t das PRG-File.
  	opt	a+,c+,o+,w-,m+	;Optimierungen etc. anschalten.

;win.s	text-windows

;8.3.92   4h + Backmem-Y-Clipping, Quad-Format, redraw_char
;10.3.92  3h + redraw_line, maus-handler, win_move
;11.3.92  3h + var rel Offset, multiple Windows, redraw Update
;12.3.92  2h - top und bottom redraw
;13.3.92  2 1/2h + win_titel + Maus select + top_window
;18.3.92  1h - X-Offset + Rand-Problematik weggelassen
;22.3.92  3h - Maus relativ + win_move Cleanup + neue Var Struktur
;23.3.92  2h + Aufl�sungsunabh�ngig
;24.3.92  4h + Disk-Routinen + read_transform + Datenstruktur entw.
;29.3.92  1h   Text display + Tabs
;31.3.92  2h + maus_window_handling + win_resize
;1.4.92   2h - linkes X-Clipping f�r Virt-Screen

;->> Display TEXT, Cursor Funktionen
;->> label verwaltung ( "Drachenbuch" oder Skip_Lists ? )
;->> go_koord, hard_go_koord, x_offset-Problem (c_x, s_x, Range???)
;->> vbl-sync, scroll rechts links
;->> gr�sserer Virtueller Screen (mit Scroll)
;->> print to different Windows (Print to back-Window)
;->> kleinere/gr�ssere Zeichens�tze -> Blitter ideal geeignet

;"HARDWARE" f�r alle ger�teabh�ngige Routinen

;d0 a0 ;Parameter �bergabe
;d1 a1 ;k�nnen durch Unterprogramm ver�ndert werden
;d6 aktuelle Zeilen-Nummer (text-appl spezifisch)
;a4 Aktuelle Zeilen-Adresse (text-appl spezifisch)
;a5 Basisregister f�r Windows
;a6 geplantes Basisregister f�r globale Variablen


LABELS	=	0


;window Daten-Struktur
w_x=0
w_y=2
w_width=4
w_height=6
w_x_off=8
w_y_off=10
w_max_width=12
w_max_height=14
c_x=16
c_y=18
w_backmem=20
w_backmem_width=24
w_backmem_start=26
w_backmem_line=28
w_backmem_cur=30
w_flags=34
;next 35/36
w_titel_length=36
w_titel=38	;80 Zeichen + 0 Byte

auto_scroll=0	;w_flags

x_reg=$64	;die Adressen werden vom MAUS.PRG versorgt
y_reg=$66	;nur TEST!!! unbenutzte Interruptvektoren
button=$74

;Vereinbarung X und Y kommen zusammen vor
; und k�nnen zusammen als Langwort benutzt werden

push:	macro
	movem.l	d0-a6,-(sp)
	endm

pull:	macro
	movem.l	(sp)+,d0-a6
	endm
newline:	macro
	moveq	#13,d0
	bsr	print
	moveq	#13,d0
	bsr	print
	endm

	init	stack(pc)
start:
	bsr	init_proc
	IFNE	LABELS
;read
;transform	;CR LF -> Zeilenende	;64K Byte Buffer verwenden
;		;Kommentare trennen
;transform2	;labels rausziehen -> TAB, -1 Marker und Nummer zur�cklas
;copy part -> window
;scroll up/down
;
;4 verschiedene Zust�nde
;00= Constant (=Register-Token)	d0-d7,a0-a7,sp Opcode-Angepasst
;		ccr,sr,tr,usp kommen nicht vor! sind im Befehls-Token
;    Bit 0-2 und 9-11 enthalten die Reg Nummer
;    Bit 7=0 Dat-Reg  Bit 7=1 Adr-Reg
;(endg�ltige Entscheidung, was ausmaskiert wird, muss sowiso ass-Routine)
;  = Konstante,	Bit 0-7  1 Byte incl. Vorzeichen, Bit 9-11 = Bit 0-2

;    oder Strukur-Token
;01= Label/(Makro-Aufruf)
;10= Befehls-Token
;
;16000 Labels Bit 13-0
;
;
;L
;LB
;B
;
;move #hugo+leo+2,(a0)+
;BSL+L+CC
;=
; Befehl	23 = move.w
; Struktur	14 = #,()+
; Label		1 = hugo
; Operation	5 = "+"
; Label		2 = leo
; Operation	5 = "+"
; Constant	2 = 2
; Constant	128 = A0
;
; 256 Wort-Tabelle f�r Register (0-2 auf 9-11) und Constant (dito)
	ENDC

test_restart:
	bsr	print_cache		;textausgabe beenden

	lea	window2(pc),a5		;a5 Basisregister
	move.l	#big_backmem2,w_backmem(a5)
	bsr	make_random_window
	bsr	reset_backmem
	lea	source(pc),a0
	bsr	win_titel
	bsr	draw_window
	bsr	go_koord
	;
	move.l	a5,win_list+4
	;

	move	#1000,d7
fill_loop:
	moveq	#9+1,d0
	bsr	randomx
	add	#'A',d0
	bsr	print
	dbf	d7,fill_loop
	bsr	print_cache
	newline

	lea	window(pc),a5		;a5 Basisregister
	move.l	#big_backmem,w_backmem(a5)
;	bsr.s	make_random_window
	clr.l	w_x(a5)
	move	screen_height(pc),d0
	subq	#2,d0
	move	d0,w_max_height(a5)
	move	#60,w_max_width(a5)
	clr.l	w_width(a5)
	bsr	window_init
	bsr	reset_backmem

	move	#0,-(sp)
	pea	target_str(pc)
	move	#$47,-(sp)
	trap	#1
	addq	#8,sp
	lea	target_str(pc),a0
	bsr	win_titel

	bsr	draw_window
	bsr	go_koord
	;
	move.l	a5,win_list
	clr.l	win_list+8
	;

	bsr	read_transform	;text einlesen und in Form bringen
	bset	#auto_scroll,w_flags(a5)
	move	w_height(a5),d7
	subq	#1,d7
	bmi.s	no_print
	lea	text\l,a3
	lea	text_tab\l,a4
	moveq	#0,d4
line_loop:
	move.b	(a4)+,d4
	beq.s	no_print
	and	#$7F,d4		;Bit 7 l�schen
	move.l	a3,a0
	bsr	string
	moveq	#13,d0
	bsr	print
	add.l	d4,a3
;	dbf	d7,line_loop
	bra.s	line_loop
no_print:

test_loop:
	move	#$0B,-(sp)
	trap	#1
	addq	#2,sp
	tst	d0
	bne.s	key_pressed
	;
	IFNE	LABELS
	;
	moveq	#9+1,d0
	bsr.s	randomx
	add	#'0',d0
	cmp.b	#"9"+1,d0
	bne.s	no_cr
	moveq	#13,d0
no_cr:
	bsr.s	print
	ELSEIF
	lea	test_str(pc),a0
;	bsr.s	string
	ENDC
;	bsr.s	print_cache
	bsr.s	maus_window_handling
	bra.s	test_loop

key_pressed:
	move	#7,-(sp)
	trap	#1
	addq	#2,sp
	cmp.b	#27,d0
;	bne.s	test_restart
	bne.s	test_loop
ende:
	bsr	exit_proc
	IFND	__LK
	illegal
	ELSEIF
	clr	-(sp)
	trap	#1
	ENDC


***********************************************
maus_window_handling:
	bsr	read_mouse
	move.l	maus_s_x(pc),s_x
	bsr	hard_go_koord

	btst	#1,maus_but+1(pc)
	beq.s	not_pressed
	tst	win_routine
	bmi.s	new_action
				;hier normal bei gedr�ckter Maus-Taste
	move.l	win_routine(pc),a0
	lea	temp_window(pc),a4
	movem.l	(a5),d0-d2
	movem.l	d0-d2,(a4)
	jmp	(a0)

new_action:
	movem	maus_s_x(pc),d0-d1
	bsr	find_window	;sub
	tst	d0
	beq.s	not_pressed	;kein WIndow -> Desktop-Hintergrund

	move.l	maus_s_x(pc),d2
	sub.l	w_x(a0),d2	;d0 sollte immer >0 sein / a0!
	move.l	d2,mem_maus_x

	cmp	#8,d0		;oberer Rand
	beq.s	top_grab
	cmp	#5,d0		;oberer Rand
	beq.s	resize_grab
not_pressed:
	move	#-1,win_routine
	rts

top_grab:
;	move.l	a0,a5		;aktuelles Window wechseln
	bsr	top_window	;sub
	move.l	#win_move,win_routine
	rts

resize_grab:
;	move.l	a0,a5		;aktuelles Window wechseln
	bsr	top_window	;sub
	move.l	#win_resize,win_routine
	rts

******************
win_move:
	move	maus_s_x(pc),d0
	sub	mem_maus_x(pc),d0
	bpl.s	win_linker_rand_ok
	moveq	#0,d0			;win �bern linken Rand
win_linker_rand_ok:

	move	d0,d1
	add	w_width(a5),d1
	addq	#2,d1
	sub	screen_width(pc),d1
	bls.s	win_rechts_ok
	sub	d1,d0
win_rechts_ok:
	move	d0,w_x(a5)

	move	maus_s_y(pc),d0
;	sub	mem_maus_y(pc),d0	;erweiterungsf�hig
	move	d0,d1
	add	w_height(a5),d1
	addq	#2,d1
	sub	screen_height(pc),d1
	bls.s	win_unten_ok
	sub	d1,d0
win_unten_ok:
	move	d0,w_y(a5)
	bra.s	back_update

*****************
win_resize:
	move	maus_s_x(pc),d0
	sub	w_x(a5),d0
	subq	#1,d0
	bpl.s	win_width_ok1
	moveq	#0,d0			;win width <0
win_width_ok1:
	move	d0,d1
	sub	w_max_width(a5),d1
	bls.s	win_width_ok2
	sub	d1,d0
win_width_ok2:
	move	d0,w_width(a5)

	move	maus_s_y(pc),d0
	sub	w_y(a5),d0
	subq	#1,d0
	bpl.s	win_height_ok1
	moveq	#0,d0
win_height_ok1:
	move	d0,d1
	sub	w_max_height(a5),d1
	bls.s	win_height_ok2
	sub	d1,d0
win_height_ok2:
	move	d0,w_height(a5)
	bra	back_update


back_update:
	movem.l	(a5),d0-d2
	cmp.l	(a4),d0
	bne.s	win_hat_sich_veraendert
	cmp.l	4(a4),d1
	bne.s	win_hat_sich_veraendert
	cmp.l	8(a4),d2
	bne.s	win_hat_sich_veraendert
	rts
win_hat_sich_veraendert:
	bsr	draw_window		;neues zeichnen

	move	w_x(a5),d2		;altes l�schen
	sub	w_x(a4),d2
	bhi.s	m_rechts
	blo.s	m_links
	move	w_width(a5),d2
	sub	w_width(a4),d2
	blo.s	m_links
	bra.s	m_vert
****************
m_rechts:
	move	w_x(a4),d3
	move	w_y(a4),d4
	move	w_height(a4),d5
	addq	#1,d5
m_r_loop:
	move	d3,d0
	move	d4,d1
	add	d5,d1
	bsr	redraw_line
	dbf	d5,m_r_loop
	bra.s	m_vert
***************
m_links:
	neg	d2
	move	w_x(a5),d3
	add	w_width(a5),d3
	addq	#2,d3		;Rand
	move	w_y(a4),d4
	move	w_height(a4),d5
	addq	#1,d5
m_l_loop:
	move	d3,d0
	move	d4,d1
	add	d5,d1
	bsr	redraw_line
	dbf	d5,m_l_loop
*****************
m_vert:
	move	w_y(a5),d5
	sub	w_y(a4),d5
	blo.s	m_hoch
	bhi.s	m_runter
	move	w_height(a5),d5
	sub	w_height(a4),d5
	blo.s	m_hoch
	bra.s	m_end
*************
m_runter:
	move	w_x(a4),d3
	move	w_y(a4),d4
	move	w_width(a4),d2
	addq	#2,d2
	subq	#1,d5
m_u_loop:
	move	d3,d0
	move	d4,d1
	add	d5,d1
	bsr	redraw_line
	dbf	d5,m_u_loop
	bra.s	m_end
**************
m_hoch:
	neg	d5
	move	w_x(a4),d3
	move	w_y(a5),d4
	add	w_height(a5),d4
	addq	#2,d4
	move	w_width(a4),d2
	addq	#2,d2
	subq	#1,d5
m_h_loop:
	move	d3,d0
	move	d4,d1
	add	d5,d1
	bsr	redraw_line
	dbf	d5,m_h_loop
***********
m_end:	rts

win_routine:	dc.l	-1
mem_maus_x:dc.w	0
mem_maus_y:dc.w	0

top_window:				;input a0 zu toppendes Window
	lea	win_list(pc),a1
	moveq	#-2,d1
wtop_next_window:
	move.l	(a1)+,d0
	beq.s	wtop_end
	addq	#1,d1
	cmp.l	d0,a0
	bne.s	wtop_next_window
	tst	d1
	bmi.s	wtop_end		;ist schon oberstes Window
	movem.l	a0-a1,-(sp)
	bsr	print_cache
	movem.l	(sp)+,a0-a1
	move.l	a0,a5			;DRAKU??? �berhaupt?
wtop_shift:
	move.l	-8(a1),-(a1)
	dbf	d1,wtop_shift
	move.l	a0,-(a1)		;a0 als oberstes W. eintragen
	bsr	draw_window
wtop_end:
	rts

*********************************
find_window:				;input d0/d1 x/y
;10 /  8 / 9				;output d0 Status a0 Win-Adresse
; 2 / 16 / 1
; 6 /  4 / 5
	movem.l	d3-d4/a2,-(sp)
	lea	win_list(pc),a2
find_next_window:
	move.l	(a2)+,d3
	beq.s	found_no_window
	move.l	d3,a0

	move	d1,d4			;y Range Test
	sub	w_y(a0),d4
	blo.s	find_next_window
	subq	#2,d4
	cmp	w_height(a0),d4
	bpl.s	find_next_window

	move	d0,d3			;x Range Test
	sub	w_x(a0),d3
	blo.s	find_next_window
	subq	#2,d3
	cmp	w_width(a0),d3
	bpl.s	find_next_window
*************************************window gefunden
	moveq	#8,d0
	addq	#1,d4
	bmi.s	find_win_cont		;oben
	moveq	#4,d0
	cmp	w_height(a0),d4
	beq.s	find_win_cont		;unten
	moveq	#0,d0
find_win_cont:
	moveq	#2,d4
	addq	#1,d3
	bmi.s	find_win_cont2		;links
	moveq	#1,d4
	cmp	w_width(a0),d3
	beq.s	find_win_cont2		;rechts
	moveq	#0,d4
find_win_cont2:
	add	d4,d0
	bne.s	find_win_end
	moveq	#16,d0			;innerhalb
find_win_end:
	movem.l	(sp)+,d3-d4/a2
	rts
found_no_window:
	moveq	#0,d0
	bra.s	find_win_end

****************************************************
maus_init:
	pea	super_maus_init(pc)
	move	#38,-(sp)
	trap	#14
	addq	#6,sp
	move	screen_width,d0
	lsl	#1,d0
	move	d0,maus_x		;z.B. 160
	add	d0,d0
	subq	#1,d0
	move	d0,maus_max_x
	move	screen_height(pc),d0
	lsl	#2,d0
	move	d0,maus_y		;z.B. 100
	add	d0,d0
	subq	#1,d0
	move	d0,maus_max_y
	clr.l	maus_x_rel
	bra.s	comp_maus_s

super_maus_init:
	move	sr,d0
	or	#$700,sr
	clr.l	(x_reg).w
	move	#0,(button).w
	move	d0,sr
	rts

read_mouse:
	pea	super_maus(pc)
	move	#38,-(sp)
	trap	#14
	addq	#6,sp

	move	maus_x(pc),d0
	add	maus_x_rel(pc),d0
	bpl.s	mx_drueber
	moveq	#0,d0
mx_drueber:
	cmp	maus_max_x(pc),d0
	bls.s	mx_drunter
	move	maus_max_x(pc),d0
mx_drunter:
	move	d0,maus_x

	move	maus_y(pc),d0
	add	maus_y_rel(pc),d0
	bpl.s	my_drueber
	moveq	#0,d0
my_drueber:
	cmp	maus_max_y(pc),d0
	bls.s	my_drunter
	move	maus_max_y(pc),d0
my_drunter:
	move	d0,maus_y

comp_maus_s:
	move	maus_x(pc),d0
	lsr	#2,d0
	move	d0,maus_s_x
	move	maus_y(pc),d0
	lsr	#3,d0
	move	d0,maus_s_y
	rts

super_maus:
	move	sr,d0
	or	#$700,sr
	move.l	(x_reg).w,maus_x_rel
	move	(button).w,maus_but
	clr.l	(x_reg).w
	move	d0,sr
	rts

******************************************
redraw_char:
	movem.l	d2-d4/a2,-(sp)
	moveq	#1,d2			;nur 1 Zeichen
	bra.s	redraw_entry
redraw_line:
	movem.l	d2-d4/a2,-(sp)
redraw_entry:
	lea	target_str(pc),a1
	movem	d0-d1,s_x
	bsr	hard_go_koord
	move	#1,last_window	;ung�ltig_machen
*********************************
redraw_loop:
	lea	win_list(pc),a2
	move.b	#' ',(a1)	;Hintergrund
redraw_next_window:
	move.l	(a2)+,d3
	beq.s	redraw_end
	move.l	d3,a0

	move	d1,d4			;y Range Test
	sub	w_y(a0),d4
	blo.s	redraw_next_window
	subq	#2,d4
	cmp	w_height(a0),d4
	bpl.s	redraw_next_window

	move	d0,d3			;x Range Test
	sub	w_x(a0),d3
	blo.s	redraw_next_window
	subq	#2,d3
	cmp	w_width(a0),d3
	bpl.s	redraw_next_window
**************************************window gefunden
	addq	#1,d4
	bmi.s	redraw_rand_oben
	cmp	w_height(a0),d4
	beq	redraw_rand_unten

	addq	#1,d3
	bmi	redraw_rand_links
	cmp	w_width(a0),d3
	beq	redraw_rand_rechts

	move	#1,last_window	;ung�ltig machen

;	add	w_y_off(a0),d4	;nur bei auto-Y-Slider!!!
	add	w_x_off(a0),d3

	add	w_backmem_start(a0),d4
	cmp	w_height(a0),d4	;oder w_max_height???????
	bcs.s	rc_1
	sub	w_height(a0),d4
rc_1:	mulu	w_backmem_width(a0),d4
	add	d3,d4
	move.l	w_backmem(a0),a0
	move.b	(a0,d4.l),(a1)
redraw_end:
	addq	#1,a1
	addq	#1,d0
	subq	#1,d2
	bne.s	redraw_loop

	clr.b	(a1)
	lea	target_str(pc),a0
	bsr	hard_string

	movem.l	(sp)+,d2-d4/a2
	rts

last_window:dc.w	0

*****************
redraw_rand_oben:
	movem.l	d0-d1/a5,-(sp)
	move.l	a0,a5
	clr.b	(a1)
	lea	target_str(pc),a0
	bsr	hard_string
	bsr	rev_on1

	addq	#1,d3
	bpl.s	nicht_linke_ecke
	moveq	#5,d0
	bsr	hard_print_special
	bra.s	r_rand_oben_cont

nicht_linke_ecke:
	cmp	last_window(pc),a5
	beq.s	no_assemble_oben
	lea	line_str(pc),a0
	bsr	assemble_top
	move	a5,last_window
no_assemble_oben:
	lea	line_str(pc),a0
	move.b	(a0,d3.w),d0
	bsr	hard_print

r_rand_oben_cont:
	bsr	rev_off1
	lea	target_str-1(pc),a1
	movem.l	(sp)+,d0-d1/a5
	bra.s	redraw_end	
*****************
redraw_rand_unten:
	movem.l	d0-d1/a5,-(sp)
	move.l	a0,a5
	lea	line_str(pc),a0
	bsr	assemble_bottom
	lea	line_str(pc),a0
	move.b	2(a0,d3.w),(a1)
	movem.l	(sp)+,d0-d1/a5
	bra	redraw_end
**************
redraw_rand_links:
	move.b	#'|',(a1)
	bra	redraw_end	
redraw_rand_rechts:
	move.b	#'|',(a1)
	bra	redraw_end	

**********************
reset_backmem:
	clr	w_backmem_start(a5)
	clr	w_backmem_line(a5)
	move.l	w_backmem(a5),a0
	move.l	a0,w_backmem_cur(a5)
	move	w_backmem_width(a5),d0
	lsr	#2,d0
	mulu	w_height(a5),d0
	subq	#1,d0
	move.l	#$20202020,d1
rb_1:	move.l	d1,(a0)+
	dbf	d0,rb_1
	rts

print_cache:
	lea	line_cache(pc),a0
	move	(a0),d0
	beq.s	nichts_da
	clr	(a0)+		;cache pointer l�schen
	clr.b	(a0,d0.w)	;ende des Strings markieren

	move	tc_x(pc),d0
	sub	w_x_off(a5),d0
	bmi.s	nichts_da	;ABBRUCH sollte eigentlich nie vorkommen
	move.l	c_x(a5),-(sp)
	move	d0,c_x(a5)
	move	tc_y(pc),c_y(a5)
	bsr	go_koord
	bsr	hard_string
	move.l	(sp)+,c_x(a5)
nichts_da:
	rts

make_space_str:			;IN: d0 L�nge des Strings / OUT: a0
	lea	space_str+256(pc),a0
	sub	d0,a0
	rts

string:
	movem.l	d3/a3,-(sp)
	move.l	a0,a3
string_loop:
	move.b	(a3)+,d0
	beq.s	string_end
	cmp.b	#9,d0
	beq.s	tab_vera
	bsr.s	print
	bra.s	string_loop
tab_vera:
	move	c_x(a5),d3
	not	d3
	and	#7,d3
tab_loop:
	moveq	#32,d0
	bsr.s	print
	dbf	d3,tab_loop
	bra.s	string_loop
string_end:
	movem.l	(sp)+,d3/a3
	rts

print:
	cmp.b	#13,d0			;CR
	beq.s	next_line

	move.l	w_backmem_cur(a5),a0	;backmem
	move.b	d0,(a0)+

	move	c_x(a5),d1
	sub	w_x_off(a5),d1
	blo.s	print_ex		;nicht sichtbar
	cmp	w_width(a5),d1
	bhs.s	print_ex

	lea	line_cache(pc),a1	;***
	tst	(a1)
	bne.s	print1
	move.l	c_x(a5),tc_x
print1:	add	(a1)+,a1
	move.b	d0,(a1)
	addq	#1,line_cache

;	bsr.s	hard_print

print_ex:
	move	c_x(a5),d0
	addq	#1,d0
	cmp	w_max_width(a5),d0
	bhs.s	next_line		;wrap around
	move.l	a0,w_backmem_cur(a5)	;backmem_cur_adr + 1
	move	d0,c_x(a5)
	rts

next_line:
	bsr	print_cache	;***
	clr	c_x(a5)

	move	w_backmem_start(a5),d0	;w_backmem
	add	c_y(a5),d0
	cmp	w_height(a5),d0
	bcs.s	next_line_1
	sub	w_height(a5),d0
next_line_1:
	mulu	w_backmem_width(a5),d0
	move.l	w_backmem(a5),a0
	add	d0,a0
	move.l	a0,w_backmem_cur(a5)

	btst	#auto_scroll,w_flags(a5)
	bne.s	nach_unten

	move	c_y(a5),d0
	addq	#1,d0
	cmp	w_height(a5),d0
	bhs	go_koord

nach_unten:
	add	w_backmem_width(a5),a0	;backmem nur nach Carriage Ret
	move	w_backmem_line(a5),d0
	addq	#1,d0
	cmp	w_height(a5),d0
	bcs.s	nach_unten_1
	move.l	w_backmem(a5),a0
	moveq	#0,d0
nach_unten_1:
	move.l	a0,w_backmem_cur(a5)
	move	d0,w_backmem_line(a5)

	move	c_y(a5),d0
	addq	#1,d0
	cmp	w_height(a5),d0
	bhs.s	do_scroll
	move	d0,c_y(a5)
	bra	go_koord

do_scroll:
	bsr.s	hard_win_scroll

	move	w_backmem_start(a5),d0	;backmem
	addq	#1,d0
	cmp	w_height(a5),d0
	bne.s	scroll_backmem
	moveq	#0,d0
scroll_backmem:
	move	d0,w_backmem_start(a5)

	move	w_height(a5),d0
	subq	#1,d0
	move	d0,c_y(a5)
	bsr	go_koord
	move	w_width(a5),d0
	bsr	make_space_str
	bsr	hard_string

	move	w_backmem_width(a5),d0	;backmem line clear
	lsr	#2,d0
	subq	#1,d0
	move.l	#$20202020,d1
	move.l	w_backmem_cur(a5),a0
scroll_1:
	move.l	d1,(a0)+
	dbf	d0,scroll_1

	bra	go_koord

hard_win_scroll:
	movem.l	d0-a6,-(sp)
;				;cache bei 68020 - 040 l�schen fehlt!!!
	move.l	logbase(pc),a6
	lea	scroll_patch(pc),a1
	move	w_width(a5),d2

	moveq	#1+1,d0		;Titelzeile + Up-Scroll(+1) & d0.l l�schen
	add	w_y(a5),d0
	mulu	screen_line_offset(pc),d0
	add.l	d0,a6
	moveq	#1,d0
	add	w_x(a5),d0
	add.l	d0,a6

	moveq	#0,d4	;TEST

	btst	#0,d0
	beq.s	scroll_start_even
	move	#$1d5E,(a1)+		;"move.b (a6)+,-?(a6)"
	moveq	#-1,d0
	sub	screen_line_offset(pc),d0
	move	d0,(a1)+	;-1 wegen a8)+,-?(a6
;	move	#-16*screen_width-1,(a1)+	;-1 wegen a8)+,-?(a6
	subq	#1,d2
	addq	#1,d4	;TEST
scroll_start_even:
	move	d2,d0
	lsr	#1,d0
	bclr	#0,d0
	beq.s	scroll_start_long
	move	#$3d5E,(a1)+		;"move.w (a6)+,-?(a6)"
	moveq	#-2,d5
	sub	screen_line_offset(pc),d5
	move	d5,(a1)+		;-2 wegen a8)+,-?(a6
;	move	#-16*screen_width-2,(a1)+	;-2 wegen a8)+,-?(a6
	subq	#2,d2
	addq	#2,d4	;TEST
scroll_start_long:
	tst	d0		;jetzt Anzahl der Register*2 in d0
	beq.s	scroll_middle_end
	move	#$3F7F,d3		;13 Register / Full Load

scroll_middle_loop:
	sub	#52/2,d0
	bcs.s	scroll_w_weniger
	move	#$4CDE,(a1)+		;"movem.l (a6)+,?
	move	d3,(a1)+
	move	#$48EE,(a1)+		;"movem.l ?,-?(a6)
	move	d3,(a1)+
	moveq	#-52,d5
	sub	screen_line_offset(pc),d5
	move	d5,(a1)+
;	move	#-16*screen_width-52,(a1)+
	add	#52,d4	;TEST
	bra.s	scroll_middle_loop
scroll_w_weniger:
	add	#52/2,d0
	beq.s	scroll_middle_end
	lea	reg_tab(pc),a0
	move	(a0,d0.w),d3
	move	#$4CDE,(a1)+		;"movem.l (a6)+,?
	move	d3,(a1)+
	move	#$48EE,(a1)+		;"movem.l ?,-?(a6)
	move	d3,(a1)+
	moveq	#0,d1
	sub	screen_line_offset(pc),d1
;	move	#-16*screen_width,d1
	add	d0,d0
	sub	d0,d1
	move	d1,(a1)+
	add	d0,d4	;TEST
scroll_middle_end:
	btst	#0,d2
	beq.s	scroll_end_even
	move	#$1d5E,(a1)+		;"move.b (a6)+,-?(a6)"
	moveq	#-1,d5
	sub	screen_line_offset(pc),d5
	move	d5,(a1)+		;-1 wegen a8)+,-?(a6
;	move	#-16*screen_width-1,(a1)+	;-1 wegen a8)+,-?(a6
;	subq	#1,d2
	addq	#1,d4	;TEST
scroll_end_even:
	move	#$4DEE,(a1)+		;"lea ?(a6),a6"
	move	screen_real_width(pc),d3
	sub	d4,d3		;TEST	;d2=w_width	
	move	d3,(a1)+

	move	#$51CF,(a1)+		;"dbf d7,?"
	move.l	a1,d0
	sub.l	#scroll_patch,d0
	neg	d0
	move	d0,(a1)+

	move	#$4E75,(a1)		;"rts"

	move	w_height(a5),d7
	subq	#1,d7
	bcs.s	no_scroll
	beq.s	no_scroll
	lsl	#4,d7
	subq	#1,d7
	bsr.s	scroll_patch
no_scroll:
	movem.l	(sp)+,d0-a6
	rts
scroll_patch:
	dcb.w	50,0

reg_tab:dc.w	0,$0001,$0003,$0007,$000F,$001F,$003F,$007F
	dc.w	$017F,$037F,$077F,$0F7F,$1F7F,$3F7F

;scroll_loop:
;	movem.l	(a6)+,d0-d6/a0-a5	;52 Bytes
;	movem.l	d0-d6/a0-a5,-1280-52(a6)
;	move.w	(a6)+,-1280-2(a6)
;	lea	80-1(a6),a6
;	dbf	d7,scroll_loop
;	rts

h_scroll:
	move	w_width(a5),d0
	subq	#1,d0
	beq.s	no_h_scroll
	lea	h_scroll_tab+512(pc),a2
	sub	d0,a2
	sub	d0,a2

	move.l	logbase(pc),a1
	move	w_y(a5),d1
	addq	#1,d1	;oberer Rand
	mulu	screen_line_offset(pc),d1
	add.l	d1,a1

	moveq	#1,d1
	add	w_x(a5),d1
	add.l	d1,a1		;Ziel
	lea	1(a1),a0	;Quelle

	jsr	(a2)
no_h_scroll:
	rts
h_scroll_tab:			;nach links scrollen
	dcb.w	256,$12D8	;move.b	(a0)+,(a1)+
	add.l	d1,a0
	add.l	d1,a1
	subq	#1,d0
	beq.s	h_scroll_end
	jmp	(a2)
h_scroll_end:
	rts
nop
	move.b	(a0)+,(a1)+	;nach links
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+
	move.b	(a0)+,(a1)+

init_proc:
	bsr	randomize
	move	#3,-(sp)
	trap	#14
	addq	#2,sp
	move.l	d0,logbase

	dc.w	$A000
;	move.l	a0,line_a
;	move	-$2E(a0),char_height
	move	-$2C(a0),d0		;V_CEL_MX
	addq	#1,d0
	move	d0,screen_width
	move	d0,screen_virt_width
	move	-$2A(a0),d0		;V_CEL_MY
	addq	#1,d0
	move	d0,screen_height
	move	d0,screen_virt_height
	move	-$28(a0),screen_line_offset	;V_CEL_WR
	move	-$2(a0),screen_real_width	;BYTES_LIN

	moveq	#27,d0		;Automatischer Zeilen�berlauf aus
	bsr	hard_print
	moveq	#'w',d0
	bsr	hard_print
	bsr	maus_init	;nach Line-A!
;special
	move	#-1,win_routine	;inaktivieren
	rts

exit_proc:
	moveq	#27,d0		;Automatischer Zeilen�berlauf ein
	bsr	hard_print
	moveq	#'v',d0
	bsr	hard_print
	rts

make_random_window:
	move	screen_width(pc),d0
	subq	#3,d0
	bsr	randomx
	addq	#1,d0
	move	d0,w_max_width(a5)
	clr	w_width(a5)

	move	screen_height(pc),d0
	subq	#3,d0
	bsr	randomx
	addq	#1,d0
	move	d0,w_max_height(a5)
	clr	w_height(a5)

	bsr.s	window_init

	move	screen_width(pc),d0
	sub	w_width(a5),d0
	subq	#2,d0
	bsr	randomx
	move	d0,w_x(a5)

	move	screen_height(pc),d0
	sub	w_height(a5),d0
	subq	#2,d0
	bsr	randomx
	move	d0,w_y(a5)

;	move	#5,w_off_x(a5)		;TEST
	rts

window_init:
	move	w_width(a5),d0
	bne.s	win_width_cont
	move	w_max_width(a5),d0
	move	d0,w_width(a5)
win_width_cont:
	addq	#3,d0
	and	#$FFFC,d0
	move	d0,w_backmem_width(a5)

	move	w_height(a5),d0
	bne.s	win_height_cont
	move	w_max_height(a5),d0
	move	d0,w_height(a5)
win_height_cont:
	clr.l	c_x(a5)
	rts

win_titel:
	moveq	#-1,d0
	move.l	a0,a1
w_tit_count:
	addq	#1,d0
	tst.b	(a1)+
	bne.s	w_tit_count
	sub	#80,d0
	bls.s	copy_titel
	add	d0,a0		;String darf max 80 Zeichen gro� sein
copy_titel:
	lea	w_titel(a5),a1
	move.l	a1,d0
copy_tit_loop:
	move.b	(a0)+,(a1)+
	bne.s	copy_tit_loop
	sub.l	d0,a1
	subq.l	#1,a1
	move	a1,w_titel_length(a5)
	rts

draw_window:
	move.l	a3,-(sp)
	move.l	w_x(a5),s_x
	bsr	hard_go_koord
	bsr	rev_on1
	moveq	#5,d0
	bsr	hard_print_special

	lea	line_str(pc),a0
	bsr	assemble_top		;sub
	clr.b	(a0)
	lea	line_str(pc),a0
	bsr	hard_string
	bsr	rev_off1
*********************
	;
	move.l	w_backmem(a5),a2
	add	w_x_off(a5),a2
	move	w_backmem_start(a5),d3
	;
	move	w_height(a5),d2
	beq.s	d_height_end
	subq	#1,d2
	
d_height_loop:
	;
	move	w_backmem_width(a5),d0
	mulu	d3,d0
	lea	(a2,d0.l),a3
	;
	addq	#1,s_y
	bsr	hard_go_koord
	lea	line_str(pc),a0
	move.b	#'|',(a0)+
	move	w_width(a5),d1
	sub	w_x_off(a5),d1
	bls.s	d_width_innen_end
	subq	#1,d1
d_width_innen_loop:
	move.b	(a3)+,(a0)+
	dbf	d1,d_width_innen_loop
d_width_innen_end:
	move.b	#'|',(a0)+
	clr.b	(a0)
	lea	line_str(pc),a0
	bsr	hard_string
	;
	addq	#1,d3
	cmp	w_height(a5),d3
	bcs.s	d_height_1
	moveq	#0,d3
d_height_1:
	;
	dbf	d2,d_height_loop
d_height_end:
*****************
	addq	#1,s_y
	bsr	hard_go_koord
	lea	line_str(pc),a0
	bsr.s	assemble_bottom		;sub
	clr.b	(a0)
	lea	line_str(pc),a0
	bsr	hard_string
	move.l	(sp)+,a3
	rts

assemble_top:
	movem.l	d2-d3/a1,-(sp)
	move	w_width(a5),d1
	beq.s	d_width_end
	lea	w_titel(a5),a1
	move	w_titel_length(a5),d2
	beq.s	ass_top_no_titel
	cmp	d1,d2
	bls.s	ass_t_short
	sub	d1,d2
	add	d2,a1
	move	d1,d2
ass_top_no_titel:
	moveq	#-1,d3
	bra.s	ass_t_cont

ass_t_short:				;Titel-String zentrieren
	move	d1,d3
	lsr	#1,d3
	move	d2,d0
	lsr	#1,d0
	sub	d0,d3
;	addq	#1,d3		;wegen Abstand-Spaces

ass_t_cont:
	subq	#1,d1
	moveq	#'-',d0
d_width_loop:
	subq	#1,d3
	bgt.s	ass_top_fill		;'-' vorher
	beq.s	ass_top_abstand		;' ' vorher
	subq	#1,d2
	bmi.s	ass_top_fill		;'-' nachher
	bne.s	ass_top_nolast
	moveq	#1,d3			;' ' nachher initiieren
ass_top_nolast:
	move.b	(a1)+,(a0)+
	dbf	d1,d_width_loop
	bra.s	d_width_end
ass_top_abstand:
	move.b	#" ",(a0)+
	dbf	d1,d_width_loop
	bra.s	d_width_end
ass_top_fill:
	move.b	d0,(a0)+
	dbf	d1,d_width_loop
d_width_end:
	move.b	#'.',(a0)+
	movem.l	(sp)+,d2-d3/a2
	rts

assemble_bottom:
	move.b	#"'",(a0)+
	move	w_width(a5),d1
	beq.s	d_width_unten_end
	subq	#1,d1
	moveq	#'-',d0
d_width_unten_loop:
	move.b	d0,(a0)+
	dbf	d1,d_width_unten_loop
d_width_unten_end:
	move.b	#"'",(a0)+
	rts
***************

read_transform:
	movem.l	d0-a6,-(sp)
	move.l	#source,filename
	lea	buffer(pc),a0
	move.l	#65000,d0
	bsr	load
	tst.l	d0
	bmi	read_transform_error
	beq.s	read_transform_error

	lea	buffer(pc),a0
	lea	buffer(pc),a1		;buffer end
	add.l	d0,a1
	lea	text\l,a2
	lea	text_tab\l,a3

	move.l	a2,a4		;letzte sinnvolle Position
	move.l	a2,a5		;Start einer Zeile
	bra.s	r_trans_loop
neue_zeile:
	move.l	a2,d0
	sub.l	a5,d0
	move.b	d0,(a3)+
	move.l	a2,a4		;letzte sinnvolle Position
	move.l	a2,a5		;Start einer Zeile
r_trans_loop:
	cmp.l	a1,a0
	bhs.s	read_transform_end
	move.b	(a0)+,d0
	cmp.b	#13,d0
	beq.s	zeilen_ende
	cmp.b	#";",d0
	beq.s	kommentar_cutoff
	move.b	d0,(a2)+

	cmp.b	#9,d0
	beq.s	r_trans_loop
	cmp.b	#" ",d0
	beq.s	r_trans_loop
	move.l	a2,a4
	bra.s	r_trans_loop

zeilen_ende:
	move.l	a4,a2
	cmp.b	#10,(a0)
	bne.s	z_equalizer
	addq	#1,a0
z_equalizer:
	move.l	a2,d0
	btst	#0,d0
	beq.s	nz_ist_gerade
	clr.b	(a2)+
nz_ist_gerade:
	clr	(a2)+
	bra.s	neue_zeile

kommentar_cutoff:
	cmp.l	a1,a0
	bhs.s	read_transform_end
	move.b	(a0)+,d0
	cmp.b	#13,d0
	bne.s	kommentar_cutoff
	bra.s	zeilen_ende

read_transform_end:
	clr.b	(a3)		;Ende-Marke
	move.l	a2,text_ende
	movem.l	(sp)+,d0-a6
	rts

read_transform_error:
	lea	text\l,a2
	lea	text_tab\l,a3
	bra.s	read_transform_end

;endo
***********************************
go_koord:
	move.l	a0,-(sp)
	moveq	#1,d0
	swap	d0
	add.l	w_x(a5),d0
	add.l	c_x(a5),d0
	addq	#1,d0
	move.l	d0,s_x

	lea	pos_str+2(pc),a0
	moveq	#32,d0
	add	s_y(pc),d0
	move.b	d0,(a0)+
	moveq	#32,d0
	add	s_x(pc),d0
	move.b	d0,(a0)
	pea	pos_str(pc)
	move	#9,-(sp)
	trap	#1
	addq	#6,sp
	move.l	(sp)+,a0
	rts

hard_go_koord:
	movem.l	d0-d1/a0-a1,-(sp)
	lea	pos_str+2(pc),a0
	moveq	#32,d0
	add	s_y(pc),d0
	move.b	d0,(a0)+
	moveq	#32,d0
	move	s_x(pc),d1
	add	d1,d0
	move.b	d0,(a0)
	move	screen_x_off(pc),d0
	sub	d1,d0
	move	d0,s_x_special		;special f�r left Virt-Clipping
	pea	pos_str(pc)
	move	#9,-(sp)
	trap	#1
	addq	#6,sp
	movem.l	(sp)+,d0-d1/a0-a1
	rts

hard_print:
	subq	#1,s_x_special
	bpl.s	no_hard_print
	movem.l	d0-d2/a0-a2,-(sp)
	move	d0,-(sp)
	move	#2,-(sp)
	trap	#1
	addq	#4,sp
	movem.l	(sp)+,d0-d2/a0-a2
no_hard_print:
	rts

hard_print_special:
	subq	#1,s_x_special
	bpl.s	no_hard_print_special
	movem.l	d0-d2/a0-a2,-(sp)
	move	d0,-(sp)
	move	#5,-(sp)
	move	#3,-(sp)
	trap	#13
	addq	#6,sp
	movem.l	(sp)+,d0-d2/a0-a2
no_hard_print_special:
	rts

hard_string:
	movem.l	d0-d2/a0-a2,-(sp)
	subq	#1,s_x_special
	bmi.s	hs_no_clipping
hard_string_clipping:
	tst.b	(a0)+
	beq.s	hard_string_end
	subq	#1,s_x_special
	bpl.s	hard_string_clipping
hs_no_clipping:
	pea	(a0)
	move	#9,-(sp)
	trap	#1
	addq	#6,sp
hard_string_end:
	movem.l	(sp)+,d0-d2/a0-a2
	rts

rev_on1:
	movem.l	d0-d2/a0-a2,-(sp)
	pea	rev_on_str(pc)
	move	#9,-(sp)
	trap	#1
	addq	#6,sp
	movem.l	(sp)+,d0-d2/a0-a2
	rts
rev_off1:
	movem.l	d0-d2/a0-a2,-(sp)
	pea	rev_off_str(pc)
	move	#9,-(sp)
	trap	#1
	addq	#6,sp
	movem.l	(sp)+,d0-d2/a0-a2
	rts

*------------------------------------------------------------------------
*NAME: RANDOM
*COMPUTES AN INTEGER RANDOM NUMBER RANGE 0-65535
*INPUT:
*	--
*OUTPUT:
*	D0 IS RANDOM NUMBER
*	VARIABLE SEED=D0
*REGISTERS CHANGED:
*	D0
*	VARIABLE "SEED"
*				Bit #0 has a very good 50% chance to win!
*------------------------------------------------------------------------
random:
	move.l	d1,-(sp)
	move	seed(pc),d0
	addq	#1,d0
	mulu	#75,d0
	move.l	d0,d1
	swap	d1

	ori	#$10,ccr 	;set extend bit

	subx	d1,d0
	addx	d1,d0
	sub	d1,d0		;fragt einfach Volker...
	bne.s	ra1
	move	#$ffb5,d0
ra1:
	and.l	#$ffff,d0
	move	d0,seed
	move.l	(sp)+,d1
	rts
seed:dc.w	0
*------------------------------------------------------------------------
*NAME: RANDOM_MAX
*COMPUTES RANDOM NUMBER RANGE 0-D0
*INPUT:
*	D0 GIVES HIGHEST POSSIBLE NUMBER +1
*OUTPUT:
*	D0 IS RANDOM NUMBER
*REGISTERS CHANGED:
*	D0
*RANDOM_MAX USES SUBROUTINE RANDOM !
*------------------------------------------------------------------------
randomx:
	tst	d0
	beq.s	rnd_max_end
	addq	#1,d0		;0 - d0   war Falsch (0-(d0-1))
	moveq	#-1,d1
	move	d0,d2
*compute mask to erase unsignificant bits
make_mask:
	asl	#1,d2
	bcs.s	mask_ok
	lsr	#1,d1
	bra.s	make_mask
mask_ok:			;d1 is mask
	move	d0,d2
maxloop:
	bsr.s	random		;get RANDOM number
	and	d1,d0		;mask out lowest bits
	cmp	d2,d0
	bcc.s	maxloop 	;d0 too high
*
rnd_max_end:
	rts

*------------------------------------------------------------------------
*NAME: INIT RANDOM
*INITIALISES VARIABLE "SEED" FOR RANDOM ROUTINE
*THIS IS REQUIRED AT EVERY GAME START, ELSE ALL GAMES WILL LOOK THE SAME!
*INPUT:
*	--
*OUTPUT:
*	VAR. SEED
*REGISTERS CHANGED:
*	--
*------------------------------------------------------------------------
randomize:
init_RANDOM:
	pea	rand_super(pc)	;im Supervisor aufrufen
	move	#38,-(sp)
	trap	#14
	addq	#6,sp
	rts

rand_super:
;	move	vhposr,seed	;ACHTUNG ! HARDWARESPEZIFISCH !
	move.b	$FFFF8207.w,seed
	move.b	$FFFF8209.w,seed+1
	rts

*****DISK*******************************************

save:	;VAR filename
	;A0 = Startadresse
	;D0 = L�nge des Blockes

	movem.l	d1-a6,-(sp)
	move.l	a0,a3
	move.l	d0,d3

	move	#0,-(sp)	; Create
	move.l	filename,-(sp)
	move	#$3C,-(sp)
	trap	#1
	addq.l	#8,sp
	tst.l	d0
	bmi	f_error
	move	d0,handle

	move.l	a3,-(sp)	; Write
	move.l	d3,-(sp)
	move	handle(pc),-(sp)
	move	#$40,-(sp)
	trap	#1
	add.l	#12,sp
	tst.l	d0
	bmi.s	f_error

	move	handle(pc),-(sp)	; Close
	move	#$3E,-(sp)
	trap	#1
	addq.l	#4,sp
	tst.l	d0
	bmi.s	f_error
	moveq	#0,d0
	bra.s	disk_end	;Stack reparieren

load:	;VAR filename
	;A0 = Startadresse
	;D0 = max. L�nge

	movem.l	d1-a6,-(sp)
	move.l	a0,a3
	move.l	d0,d3
	move.l	filename(pc),a0
	move	#0,-(sp)	; Open
	move.l	a0,-(sp)
	move	#$3D,-(sp)
	trap	#1
	addq.l	#8,sp
	tst.l	d0
	bmi.s	f_error
	move	d0,handle

	move.l	a3,-(sp)	; Read
	move.l	d3,-(sp)
	move	handle(pc),-(sp)
	move	#$3F,-(sp)
	trap	#1
	add.l	#12,sp
	tst.l	d0
	bmi.s	f_error
	clr.b	(a3,d0)		;Ende mit 0 markieren
	move.l	d0,d3

	move	handle(pc),-(sp)	; Close
	move	#$3E,-(sp)
	trap	#1
	addq.l	#4,sp
	tst.l	d0
	bmi.s	f_error
	move.l	d3,d0		;Filel�nge
disk_end:
	movem.l	(sp)+,d1-a6
	rts

f_error:
	neg	d0
	move	d0,-(sp)
	lea	open_error(pc),a0
	bsr	string
	move.l	filename(pc),a0
	bsr	string
	newline
	move	(sp)+,d0
	lea	error_tab(pc),a0
err_l:	tst.b	(a0)
	beq.s	err_un
	cmp.b	(a0)+,d0
	bne.s	err_l0
	bsr	string
	newline
	bra.s	err_end
err_l0:	tst.b	(a0)+
	bne.s	err_l0
	bra.s	err_l
err_un:	lea	error_unbekannt(pc),a0
	bsr	string
	newline
err_end:
	moveq	#-1,d0
	bra.s	disk_end

open_error:	dc.b	"Fehler bei File: ",0
error_tab:
	dc.b	32,"ungueltige Funktionsnummer",0
	dc.b	33,"Datei nicht gefunden",0
	dc.b	34,"Pfadname nicht gefunden",0
	dc.b	35,"zuviele offene Dateien",0
	dc.b	36,"Zugriff nicht moeglich",0
	dc.b	37,"ungueltige Handle-Nummer",0
	dc.b	39,"nicht genuegend Speicher",0
	dc.b	40,"ungueltige Speicherblockadresse",0
	dc.b	46,"ungueltige Laufwerksbezeichnung",0
	dc.b	49,"keine weiteren Dateien",0
	dc.b	0	; Ende der Liste
error_unbekannt:dc.b	"Unbekannte Disk-Fehlernummer!",0
	even
	IFNE	LABELS
make_name:	;filename IN >>> tname+filename_ende
	push
	move.l	filename(pc),a0
	move.l	f_path(pc),a1
	lea	f_name_h(pc),a2	;Dest.
n_first:move.b	(a1)+,(a2)+
	bne.s	n_first
	subq	#1,a2		;0 zur�ckholen

	move.l	a0,a1
n_mid:	move.b	(a0)+,d0
	beq.s	n_last
	cmp.b	#'\',d0
	bne.s	n_mid
	move.l	a0,a1		;steg gefunden
	bra.s	n_mid

n_last:	move.b	(a1)+,(a2)+
	bne.s	n_last		;auch die Null wird kopiert

	lea	f_name_h(pc),a0
	lea	f_name(pc),a1	;Dest.
	move.l	a1,filename
n_copy:	move.b	(a0)+,(a1)+
	bne.s	n_copy

	moveq	#32,d0
	bsr.s	print
	moveq	#'>',d0
	bsr.s	print
	moveq	#32,d0
	bsr.s	print
	lea	f_name(pc),a0
	bsr.s	string

	pull
	rts
	ENDC

	;include	include\call_gem

******************************
	bss
temp_window:	ds.w	20	;6 Worte werden nur verwendet

window2:	ds.w	20
	ds.b	80
window:
	ds.w	20		;17 benutzt
	ds.b	80
;******************************************************
;w_x:		dc.w	0	;window start
;w_y:		dc.w	0
;w_width / w_height =0 --> w_max_* werden beim init. eingetragen!!!
;w_width:	dc.w	0	;innerhalb / aktuelle Gr�sse
;w_height:	dc.w	0
;w_x_off:	dc.w	0
;w_y_off:	dc.w	0 ; gibt es nur bei Y-auto-Slider !!!
;Verbieten, da� x*y mehr als 32000 gross ist (=256*128 Zeichen)!!!
;w_max_* =0 --> screen max Werte(-2 f�r Rand) werden eingetragen???
;w_max_width:	dc.w	0	;innerhalb / kann breiter als Screen sein
;w_max_height:	dc.w	0	;darf nicht h�her als Screen sein!!!
;				;nur bei Y-auto-Slider!!!!!!!
;c_x:		dc.w	0	;cursor im window rel zum 0 Punkt
;c_y:		dc.w	0
;w_backmem:	dc.l	0	;pointer auf inhaltsspeicher
;w_backmem_width:dc.w	0	;(w_max_width+3) DIV 4
;w_backmem_start:dc.w	0	;Startline / wegen Clipping Technik
;w_backmem_line:dc.w	0	;phys line / wegen Clipping Technik
;w_backmem_cur:	dc.l	0	;aktuelle Cursor-Position
;b_width=w_max_width
;b_height_flag=0     --> w_max_height wird eingetragen
;		         h�chstens jedoch #screen_height-2 !!!
;             =1     --> w_max_height wird eingetragen,
;			 auch wenn es h�her als der screen ist
;			 (f�r schnelle Nachschlag-Tabellen???)
;	off 39
;w_titel:	dcb.b	80,0
;		dc.b	0
;********************************************************************
q:			;Basis-Register a6
logbase:	ds.l	1
;line_a:	ds.l	1
filename:	ds.l	1
handle:	ds.w	1

screen_width:	ds.w	1
screen_height:	ds.w	1
screen_virt_width:	ds.w	1
screen_virt_height:	ds.w	1
screen_x_off:	ds.w	1
screen_y_off:	ds.w	1
screen_real_width:	ds.w	1	;HARDWARE f�r scroll
;char_height:	ds.w	1
screen_line_offset:	ds.w	1

win_list:	ds.l	3

maus_but:	ds.w	1	;Bit 1 linke Taste / Bit 0 rechte Taste
maus_x:		ds.w	1
maus_y:		ds.w	1
maus_max_x:	ds.w	1
maus_max_y:	ds.w	1
maus_x_rel:	ds.w	1	;rel Bewegung von Maustreiber
maus_y_rel:	ds.w	1
maus_s_x:	ds.w	1	;in Cursor-Pos umgerechnet
maus_s_y:	ds.w	1

s_x:	ds.w	1	;screen koord
s_y:	ds.w	1
s_x_special:	ds.w	1	;Virt-Clipping Vorberechnung

esc_str:	ds.b	8
target_str:	ds.b	256
line_str:	ds.b		256

tc_flag:	ds.w	1
tc_x:	ds.w	1
tc_y:	ds.w	1
line_cache:	ds.w	1	;offset
		ds.b	256	;161 max bei SuperMono

big_backmem:	ds.w	1500	;Data unendlich (!) Speicherverw. fehlt!!
big_backmem2:	ds.w	1500	;Data unendlich (!) Speicherverw. fehlt!!
buffer:	ds.w	128*256
text_tab:ds.w	16000		;16000 Zeilen-Starts
text_ende:	ds.l	1
text: ds.w	1

	data
*********************
source:	dc.b	'MAG_SPED.S',0
rev_on_str:	dc.b	27,'p',0
rev_off_str:	dc.b	27,'q',0
pos_str:dc.b	27,'Y',' ',' ',0

test_str:dc.b	"Hello World! 0123456789",13,0
text1:	dc.b	"Window 1",0
	even			;!!! auf space_str word/longword access
space_str:	dcb.b	256,' '	;Space String
		dc.b	0	;Ende des Strings
	end


         1         2         3         4         5         6
123456789012345678901234567890123456789012345678901234567890123456789
abcdefghijklmnopqrstuvwxyz����ABCDEFGHIJKLMNOPQRSTUVWXYZ���0123456789
7         8         9        10
0123456789012345678901234567890
.,+-!"�$%&/()=?'#^*~|;:_<>@\[] {}`

������� & CO werden schon vorher in Zahlen unter 128 gewandelt (von PRINT)
128*128 ZEICHENM�GLICHKEITEN = 16384'er Wort-Tabelle
verschiedene Bereiche:	acemnorsuvwxz.:-=~  kurz
			ABC 0123           mittel 4 Zeilen fehlen
			Q�pqgjy�|[]{}$,;  �berl�nge
.----test.s-----.
|Hallo		^
|add a0,	|
|		x
|>		|
|		v
'<-x----------->'
