  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	;include	include\gem	;Vdi- & Aes-Macros einbinden.
	output	mag_xcpt.prg	;So hei�t das PRG-File.
begin
	bra	init
	dc.l	'XBRA','MGXC'
	dc.l	0
bus
	move.l	#2,offset
	move.b	#'2',text1
	move.l	bus-4(pc),addresse
	bra	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
address
	move.l	#2,offset
	move.b	#'3',text1
	move.l	address-4(pc),addresse
	bra	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
illegal
	move.l	#4,offset
	move.b	#'4',text1
	move.l	illegal-4(pc),addresse
	bra	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
div0
	clr.l	offset
	move.b	#'5',text1
	move.l	div0-4(pc),addresse
	bra	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
chk
	clr.l	offset
	move.b	#'6',text1
	move.l	chk-4(pc),addresse
	bra	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
trapv
	clr.l	offset
	move.b	#'7',text1
	move.l	trapv-4(pc),addresse
	bra.s	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
privileg
	move.l	#2,offset
	move.b	#'8',text1
	move.l	privileg-4(pc),addresse
	bra.s	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
trace
	clr.l	offset
	move.b	#'9',text1
	move.l	trace-4(pc),addresse
	bra.s	weiter
	dc.l	'XBRA','MGXC'
	dc.l	0
linea
	move.l	#4,offset
	move.b	#'A',text1
;	move.l	linea-4(pc),addresse
;	bra.s	weiter
;	dc.l	'XBRA','MGXC'
;	dc.l	0
;linef
;	move.l	#4,offset
;	move.b	#'B',text1
;	move.l	linef-4(pc),addresse

weiter
	movem.l	d0/a0,-(sp)
	lea	text(pc),a0
	bsr.s	bconout
	pre_trap
	Bconin	#2
	post_trap
	cmp.b	#'f',d0
	beq.s	fortfahren
	cmp.b	#'F',d0
	beq.s	fortfahren
	movem.l	(sp)+,d0/a0
	cmp.b	#'3',text1
	bhi.s	.no_bus
	addq.l	#8,sp
.no_bus
	move.l	addresse(pc),2(sp)
	rte
fortfahren
	movem.l	(sp)+,d0/a0
	cmp.b	#'3',text1
	bhi.s	.no_bus
	addq.l	#8,sp
.no_bus
	move.l	d0,-(sp)
	move.l	offset(pc),d0
	add.l 	d0,6(sp)
	move.l	(sp)+,d0
	rte


bconout
	pre_trap
.bbconout
	move.b	(a0)+,d0
	beq.s	.ende
	ext	d0
	Bconout	d0,#2
	bra.s	.bbconout
.ende
	post_trap
	rts
addresse	dc.l	0
offset	dc.l	0
text	dc.b	27,'EException  #$'
text1	dc.b	'4.',$d,$a,'(F)ortfahren | (T)OS-Exception?',$d,$a,0
	copyright
ende
	even
init
	Super	#0
	move.l	d0,d7

	lea	bus(pc),a0
	moveq	#8/4,d0
	bsr	setexc
	move.l	d0,-4(a0)
	lea	address(pc),a0
	moveq	#$c/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
	lea	illegal(pc),a0
	moveq	#$10/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
	lea	div0(pc),a0
	moveq	#$14/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
	lea	chk(pc),a0
	moveq	#$18/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
	lea	trapv(pc),a0
	moveq	#$1c/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
	lea	privileg(pc),a0
	moveq	#$20/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
	lea	trace(pc),a0
	moveq	#$24/4,d0
	bsr.s	setexc
	move.l	d0,-4(a0)
;	lea	linea(pc),a0
;	moveq	#$28/4,d0
;	bsr.s	setexc
;	move.l	d0,-4(a0)
;	lea	linef(pc),a0
;	moveq	#$2c/4,d0
;	bsr.s	setexc
;	move.l	d0,-4(a0)
	Super	d7
	Ptermres	#ende-begin+$100,#0
setexc
	Setexc	(a0),d0
	rts