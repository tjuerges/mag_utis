	include	tos_fnkt

;ein versuch, interlace auf dem st mit 71/2 hz zu verwirklichen.

anfang
	bra.s	start
	dc.l	'XBRA','LACE'
old	dc.l	0
new
	movem.l	d0-d1/a0-a1,-(sp)
	lea	$ffff8201.w,a0
	lea	old_screen(pc),a1
	movep	(a0),d0	;d0=video
	move	(a1),d1	;d1=next_screen
	move	d0,(a1)	;next_screen=old_video
	movep	d1,(a0)	;video=next_screen
	movem.l	(sp)+,d0-d1/a0-a1
	move.l	old(pc),-(sp)
	rts
old_screen	dc.w	1
ende

start
	init	stack(pc)
	Malloc	#32256
	tst.l	d0
	beq.s	kein_freier_speicher
	add.l	#128,d0
	and.l	#$ffffff00,d0
break
	lsr.l	#8,d0
	move	d0,old_screen
	Supexec	begin(pc)
	Ptermres	#0,#$100+ende-anfang
begin
	move	#$2700,sr
	move.l	$70.w,old
	move.l	#new,$70.w
	move	#$2300,sr
	rts
kein_freier_speicher
	Cconws	no_mem(pc)
	Cconin
	Pterm0
no_mem	dc.b	'Nicht gen�gend freier Speicherplatz vorhanden',$d,$a,0
