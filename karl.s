	include	tos_fnkt

;testprogramm f�r einen selbstentpackenden cruncher auf basis
;von MAG-CRU.

	Fopen	#0,name
	move.w	d0,handle
	bmi	error
	Fread	buffer,#1000000,handle
	tst.l	d0
	bmi	error
	move.l	d0,l�nge
	Fclose	handle

	bsr	init_base_page

	Fcreate	#0,wname
	move.w	d0,handle
	bmi.s	error
	move.l	l�nge,d0
	add.l	#buffer-base_page,d0
	move.l	d0,text_length
	Fwrite	base_page,d0,handle
	tst.l	d0
	bmi.s	error
	Fclose	handle
	Cconws	fertig
	Pterm0
error
	Cconws	err_str
	Cconin
	Pterm0


init_base_page
	rts


base_page	dc.w	$601a
text_length	dc.l	0
data_length	dc.l	0
bss_length	dc.l	0
symbol_length	dc.l	0
res	dc.l	0,0
flag	dc.w	0

	init
	move.l	a0,a1
	lea	buffer,a2
	move.l	a2,8(a1)
	move.l	2(a2),d4
	move.l	d4,$c(a1)
	add.l	a2,d4
	move.l	d4,$10(a1)
	move.l	6(a2),d5
	move.l	d5,$14(a1)
	add.l	d5,d4
	move.l	d4,$18(a1)
	move.l	$a(a2),d5
	move.l	d5,$1c(a1)

	move.l	#10000,d2
.loop
	Setcolor	#0,#0
	Setcolor	#1,#0
	dbf	d2,.loop
	Cconin
	lea	0.w,a0
	Pexec	(a0),(a0),(a1),#4
	move.l	d0,d1
;prt_dez gibt ein datum in dez
;auf dem bilschirm aus. d1=datum(6 g�ltige stellen)
prt_dez
	movem.l	d1-d5,-(sp)
	and.l	#$fffff,d1
	tst.l	d1
	beq.s	.ok
	move.l	d1,d4
	move.l	#100000,d5
	moveq	#0,d3
.loop1
	moveq	#0,d2
.loop2
	cmp.l	d5,d4
	bcs.s	.is_lower
	moveq	#1,d3
	addq	#1,d2
	sub.l	d5,d4
	bne.s	.loop2
.is_lower
	tst.b	d2
	bne.	.ok
	tst.b	d3
	beq.s	.is_zero
.ok
	add.w	#'0',d2
	Cconout	d2
.is_zero
	divu	#10,d5
	bne.s	.loop1

	movem.l	(sp)+,d1-d5
	Cconin
	Pterm0
buffer	ds.l	1000


handle	ds.w	1
l�nge	ds.l	1
fertig	dc.b	'O.K....................................',$d,$a,0
err_str	dc.b	'FEHLER!',$a,$d,0
name	dc.b	't.tos',0
wname	dc.b	'tt.tos',0