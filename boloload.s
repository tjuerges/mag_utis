	opt	a+,c+,o+,w+
	include	include\tos_fnkt
	output	g:\daddeln\bolo\boloload.prg
;patcht das gemdos, soda� ein zugriff auf laufwerk a: auf das
;aktuelle laufwerk und verzeichnis umgebogen wird.
;ich habe es f�r ultima V ben�tigt (lief n�mlich nicht auf hd!!!).

	init	stack(pc)
begin:
	Cconws	hallo(pc)
	Super	#0
	move.l	$84.w,old_jmp
	lea	$300.w,a1
.back
	lea	(a1),a2
	lea	start(pc),a0
	moveq	#ende-start-1,d1
.loop
	move.b	(a0)+,(a1)+
	dbf	d1,.loop
	lea	new_trap-start(a2),a2
	move.l	a2,$84.w
	Super	d0
	Pexec	env(pc),cmd(pc),bolo(pc),#0
	Pterm0
start:
	dc.l	'XBRA','PDSK'
old_jmp:	dc.l	0
new_trap:
	move.l	usp,a0
	btst	#5,(sp)
	beq.s	.user
	lea	6(sp),a0
.user:
	cmp	#$3d,(a0)
	beq.s	.ist_ok
	cmp.	#$4e,(a0)
	beq.s	.ist_ok
	cmp.	#$3c,(a0)
	bne.s	old_trap
.ist_ok
	lea	2(a0),a0
	move.l	a0,d0
	move.l	(a0),a0
	cmp.b	#'A',(a0)
	beq.s	.gefunden
	cmp.b	#'a',(a0)
	bne.s	old_trap
.gefunden
	cmp.b	#':',1(a0)
	bne.s	old_trap
	cmp.b	#'\',2(a0)
	bne.s	old_trap
	move.l	d0,a0
	addq.l	#3,(a0)
old_trap:
	lea	old_jmp(pc),a0
	move.l	(a0),-(sp)
	rts
	dc.l	0
ende:
	data
cmd	dc.l	0
env	dc.l	0
bolo	dc.b	'bolo.prg',0
hallo:	dc.b	27,'ELoading BOLO from harddisk...',$d,$a
	dc.b	9,'Written by Magnum.',$d,$a,$a,0
