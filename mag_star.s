	opt	a+,c+,o+,w+
	output	mag_star.prg
durchlaeufe	equ 100
anzahl          EQU 200
max_x           EQU 639
max_y           EQU 399
max_ebenen      EQU 10
dauer		EQU 100
debug           EQU 0

start:	pea     super(pc)
        move    #$26,-(a7)
        trap    #$e
        addq.w  #6,a7
        clr.l   -(a7)
	trap    #$1


super:		lea     put_pixel(PC),A3
                lea     clr_pixel(PC),A4
                lea     random(PC),A5
                bsr     set_up

loop:		move.l  $0466.w,D7
warte:          cmp.l   $0466.w,D7
                beq.s   warte
                ;ori     #$0700,SR
                lea     starx(PC),A0
                lea     stary(PC),A1
                lea     starsp(PC),A2
                move.w  #anzahl-1,D7

innen:
                move.w  (A0),D4         ;d4=xold
                move.w  (A1),D5         ;d5=yold
                move.w  D4,D0
                move.w  D5,D1
                add.w   (A2),D0
                move.w  D0,(A0)
                cmp.w   #max_x,D0
                bls.s   weiter_x
                move.w  #max_y,D1
                jsr     (A5)
                move.w  D0,(A1)
                moveq   #max_ebenen,D1
                jsr     (A5)
                addq.w  #1,D0
                move.w  D0,(A2)
                move.w  D0,(A0)
                move.w  (A1),D1
weiter_x:
                jsr     (A4)            ;clr_pixel
                jsr     (A3)            ;put_pixel
weiter_xx:
                addq.w  #2,A0
                addq.w  #2,A1
                addq.w  #2,A2
                dbra    D7,innen

                move.l  #$010002,-(SP)
                trap    #13
                addq.w  #4,SP
                tst.w   D0
                bne.s   taste_gedrueckt

                subq.w	#$1,durchgang
		bne.s	loop
taste_gedrueckt:
		rts

seed:           DC.W 0

random:		movem.l D1-D2,-(SP)
                moveq   #0,D0
                move.w  -2(A5),D0
                addq.w  #1,D0
                mulu    #75,D0          ;74 TZ
                move.l  D0,D1
                swap    D1
                ori     #$10,CCR        ;set extend bit
                subx.w  D1,D0
                addx.w  D1,D0
                sub.w   D1,D0           ;fragt einfach Volker...
                bne.s   ra1
                move.w  #$FFB5,D0
ra1:
                and.l   #$FFFF,D0
                move.w  D0,-2(A5)
                movem.l (SP)+,D1-D2
                divu    D1,D0
                swap    D0
                cmp.w   D1,D0
                bhs.s   random
                rts
put_pixel:
plot_regs       REG D0-D2
                movem.l plot_regs,-(SP)
                move.w  D1,D2
                add.w   D1,D1
                add.w   D1,D1
                add.w   D2,D1
                lsl.w   #4,D1
                move.b  D0,D2
                not.b   D2
                lsr.w   #3,D0
                add.w   D1,D0
                bset    D2,0(A6,D0.w)
                movem.l (SP)+,plot_regs
                rts
clr_pixel:
                movem.l D2/D4-D5,-(SP)
                move.w  D5,D2
                add.w   D5,D5
                add.w   D5,D5
                add.w   D2,D5
                lsl.w   #4,D5
                move.b  D4,D2
                not.b   D2
                lsr.w   #3,D4
                add.w   D5,D4
                bclr    D2,0(A6,D4.w)
                movem.l (SP)+,D2/D4-D5
                rts
set_up:
                bclr    #0,$ffFF8241.w
                move.b  $ffff8207.w,-1(A5)
                move.b  $ffff8209.w,-2(A5)
                pea     clr(PC)
                move.w  #9,-(SP)
                trap    #1
                addq.w  #6,SP
                move.w  #2,-(SP)
                trap    #14
                addq.w  #2,SP
                movea.l D0,A6
                lea     starx(PC),A0
                lea     stary(PC),A1
                lea     starsp(PC),A2
                move.w  #anzahl-1,D7
loop1:
                move.w  #max_x,D1
                jsr     (A5)
                move.w  D0,(A0)+
                move.w  #max_y,D1
                jsr     (A5)
                move.w  D0,(A1)+
                moveq   #max_ebenen,D1
                jsr     (A5)
                addq.w  #1,D0
                move.w  D0,(A2)+
                dbra    D7,loop1
                rts

                DATA
durchgang:	dc.w	200
clr:            DC.B 27,'E',0

                BSS
starx:          DS.W anzahl
stary:          DS.W anzahl
starsp:         DS.W anzahl
                END
