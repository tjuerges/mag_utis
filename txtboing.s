	include	tos_fnkt

;eine kleine demo
start
	init
	Supexec	begin
return
	Pterm0
begin
	Cursconf	#0
	moveq.b	#0,d1
	moveq.b	#0,d2
	moveq.b	#0,d3
	moveq.b	#0,d4
	moveq	#0,d6
	lea	hallo,a0
	bsr.s	cconws
	Cconrs	#79,string
	lea	clr,a0
	bsr.s	cconws
	tst.b	string+1
	beq	linien_boing

text_boing
	move.b	#$ff,flag
	moveq.b	#32,d1
	moveq.b	#32,d2
	move.b	string+1,d5
	and.l	#$ff,d5
	lea	string+2,a0
	bra.s	loop1

cconws
	Cconws	(a0)
	rts
print
	Bconout	#27
	Bconout	#'Y'
	Bconout	d2
	Bconout	d1
	bsr.s	cconws
	rts
loop1
	move.l	$466.w,count
	bsr.s	print
	tst.b	flag
	bne.s	.ok
	subq.b	#2,d2
.ok
	addq.b	#1,d1
	addq.b	#1,d2
	bsr	key_test

	tst.b	flag
	beq.s	.ist_negativ
	cmp.b	#55,d2
	bne.s	.go_on
	clr.b	flag
	bra.s	.go_on
.ist_negativ
	cmp.b	#32,d2
	bne.s	.go_on
	move.b	#$ff,flag
.go_on
	moveq.b	#80,d6
	sub.b	d5,d6
	move.b	d1,d7
	sub.b	#32,d7
	cmp.b	d7,d6
	bhi.s	loop1
	subq.b	#1,d1
loop2
	bsr	print
	tst.b	flag
	bne.s	.ok
	subq.b	#2,d2
.ok
	subq.b	#1,d1
	addq.b	#1,d2
	bsr	key_test

	tst.b	flag
	beq.s	.ist_negativ
	cmp.b	#55,d2
	bne.s	.go_on
	clr.b	flag
	bra.s	.go_on
.ist_negativ
	cmp.b	#32,d2
	bne.s	.go_on
	move.b	#$ff,flag
.go_on
	addq.l	#1,d6
	cmp.b	#32,d1
	bne.s	loop2
	bra	loop1

linien_boing
	dc.w	$a000
	move.l	a0,a6
	move	#1,COLBIT0(a6)
	move	#2,WMODE(a6)
	move	#0,LSTLIN(a6)
	clr	PATMSK(a6)
	clr	LNMASK(a6)
	move.l	$466.w,count
.loop
	bsr.s	key_test
	Random
	and.l	#$ffff,d0
	move.l	d0,d7
	lsr	#8,d7
	lsr	#1,d7
	lsr	#8,d0
	add	d7,d0
	clr	X1(a6)
	move	d0,Y1(a6)
	move	#639,X2(a6)
	dc.w	$a004
	addq.l	#1,d6
	bra.s	.loop

key_test
	Cconis
	tst.b	d0
	beq.s	no_key
ende
	move.l	count,d2
	move.l	$466.w,d1
	sub.l	d2,d1
	Bconout	#$20
	bsr.s	prt_ldez
	lea	komm1,a0
	bsr	cconws
	move.l	d6,d1
	bsr.s	prt_ldez
	lea	kommentar,a0
	bsr	cconws
	bsr.s	cconin
	bsr.s	cconin
	addq	#4,sp
no_key
	rts

cconin
	Cconin
	rts

	include	prt_ldez.s

	data
hallo	dc.b	'Bitte einen Text eingeben oder nur RETURN!',$d,$a
	dc.b	'Das Programm wird mit einem Tastendruck abgebrochen.',$d,$a
	dc.b	'Es sind auch ESC-Sequenzen m�glich.',$d,$a
	dc.b	'Text: ',0
clr	dc.b	27,'E',0
komm1	dc.b	' Bildschirmaufbauten (Ein Bildschirmaufbau dauert 1/72stel Sekunde).',$d,$a,0
kommentar	dc.b	' (Linien/Strings) wurden w�hrend dieser Zeit ausgegeben.',$d,$a,0
	bss
count	ds.l	1
string	ds.l	21
flag	ds.b	1