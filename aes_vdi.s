                PART 'INIT'
;************************************************************************
;* �-soft GEM-Library                                                   *
;* �1989/90 �-soft, written by Markus Fritze           18.01.1990 13:27 *
;************************************************************************
;************************************************************************
;* Globale Variablen:                                                   *
;************************************************************************
reglist         REG d1-d2/a0-a2
_StackSize      EQU 4096    ;4k Stack f�r das Programm

;************************************************************************
;* Initialisierung der Library (GEMDOS & AES)                           *
;************************************************************************
_gem_init:movea.l 4(sp),a6              ;Basepageadresse holen
        movea.w #$0100+_StackSize,a5    ;Gr��e der Basepage + Stackgr��e (1k)
        adda.l  12(a6),a5               ;+ Gr��e des TEXT-Segments
        adda.l  20(a6),a5               ;+ Gr��e des DATA-Segments
        adda.l  28(a6),a5               ;+ Gr��e des BSS-Segments
        move.l  a5,d1                   ;= Gesamtl�nge des Programms
        and.b   #$fe,d1                 ;L�nge nun gerade
        add.l   a6,d1                   ;+ Programmstart (Basepageadresse)
        movea.l d1,sp                   ;Stack endet dort
        move.l  a5,-(sp)                ;Programml�nge
        move.l  a6,-(sp)                ;Adresse der Basepage
        move.l  a6,basepage             ;Basepageadresse merken
        move.l  #$4a0000,-(sp)          ;Funktionsnummer + Dummyword (0)
        trap    #1                      ;Mshrink(0,Basepageadr,Prgl�nge)
        lea     12(sp),sp               ;Nur noch den Stack korrigieren

        move.l  #appl_init,d0
        bsr.s   _aes                    ;Programm als GEM-Applikation anmelden
        move.w  d0,ap_id
        moveq   #-1,d7                  ;-1: appl_init() Fehler (kann beim
        addq.w  #1,d0                   ;aktuellen GEM nie (!) auftreten!)
        beq.s   _pterm                  ;keine ID f�r das Programm => Ende

        move.l  #graf_handle,d0
        bsr.s   _aes                    ;Handle der Bildschirm-Workstation ermitteln
        move.w  d0,vdi_handle           ;Handle f�r VDI-Aufrufe merken
        movem.w int_out+2(pc),d1-d4
        movem.w d1-d4,gr_hwchar

        lea     int_in(pc),a0
        movea.l a0,a1                   ;Zeiger merken (s.u.)
        moveq   #10,d0
_gem_init2:move.w #1,(a0)+              ;1,1,1,1,1,1,1,1,1,1,2
        dbra    d0,_gem_init2
        addq.w  #1,-(a0)
        move.l  #v_opnvwk,d0
        bsr.s   _vdi                    ;Workstation �ffnen
        moveq   #-39,d7                 ;-39: kein Speicher f�r VDI-Workstation
        move.w  12+control(pc),vdi_handle
        beq.s   _do_pterm               ;kein Speicherplatz f�r eine Workstation
                ENDPART
;************************************************************************
;* Hier kommt das Hauptprogramm...                                      *
;* D0 enth�lt den Exit-Code des Programms                               *
;************************************************************************

                PART 'EXIT'
;************************************************************************
;* Verlassen des Programms                                              *
;************************************************************************
_gem_exit:move.w d0,d7                  ;Exit-Status merken

        moveq   #v_clsvwk,d0
        bsr.s   _vdi                    ;Workstation schliessen

_do_pterm:move.l #appl_exit,d0
        bsr.s   _aes                    ;Applikation wieder abmelden

_pterm: move.w  d7,-(sp)                ;Fehlercode
        move.w  #$4c,-(sp)
        trap    #1                      ;Pterm(Fehlercode)
                ENDPART
                PART 'CALL AES&VDI'
;************************************************************************
;* Mein eigener kleiner AES-Aufruf                                      *
;* D0=Die ersten 4 Eintr�ge im control-Array (Bytebreite!)              *
;*    Bit 24-31 = Der Opcode bzw. die Funktionsnummer                   *
;*    Bit 16-23 = Anzahl der Eintr�ge in int_in                         *
;*    Bit 8-15  = Anzahl der Eintr�ge in int_out                        *
;*    Bit 0-7   = Anzahl der Eintr�ge in addr_in                        *
;* Die Anzahl der Eintr�ge in addr_out ist stets 0.                     *
;* Ausnahme:Bei rsrc_gaddr() sollte dort eine 1 stehen (Atari h�lt sich *
;*          allerdings selbst nicht dran)                               *
;************************************************************************
_aes:   movem.l reglist,-(sp)           ;besser retten, man kann nie wissen
        lea     control(pc),a0
        clr.l   (a0)+
        clr.l   (a0)+                   ;control-Array l�schen
        clr.w   (a0)
        movep.l d0,-7(a0)               ;und die neuen Daten eintragen
        cmpi.b  #112,-7(a0)             ;Ist es rsrc_gaddr()?
        bne.s   _aes1                   ;Nein! =>
        move.b  #1,1(a0)                ;Anzahl der Eintr�ge in addr_out=1
_aes1:  lea     _aes_pb(pc),a0
        move.l  a0,d1
        move.w  #200,d0
        trap    #2                      ;AES aufrufen
        moveq   #0,d0
        move.w  int_out(pc),d0          ;int_out[0]
        movem.l (sp)+,reglist
        rts

_aes_pb:        DC.L control ;Der AES-Parameterblock
                DC.L global
                DC.L int_in
                DC.L int_out
                DC.L addr_in
                DC.L addr_out

;************************************************************************
;* Und das gleiche nochmal f�r die VDI-Funktionen                       *
;* D0 = Die Eingabeparameter                                            *
;*      Bit 24-31 = Opcode der Funktion                                 *
;*      Bit 16-23 = L�nge des Intin-Arrays                              *
;*      Bit 8-15  = Identifikationsnummer f�r Unter-Opcodes             *
;*      Bit 0-7   = Anzahl der Eintr�ge in ptsin                        *
;* Die Ger�tekennung wird der Variable "gr_handle" entnommen.           *
;************************************************************************
_vdi:   movem.l reglist,-(sp)
        lea     control+2(pc),a0
        clr.w   (a0)+                   ;alle Parameter l�schen
        clr.l   (a0)+
        clr.w   (a0)
        move.l  d0,-(sp)
        tst.b   (sp)                    ;kein Opcode vorhanden? => Kurzformat
        addq.l  #4,sp
        bne.s   _vdi1
        move.b  d0,1-8(a0)              ;Funktionsnummer in den Bits 0-7
        bra.s   _vdi2                   ;alle anderen Parameter sind 0!
_vdi1:  move.b  d0,3-8(a0)              ;Anzahl der Eintr�ge in ptsin
        lsr.w   #8,d0
        move.w  d0,10-8(a0)             ;Unter-Opcode
        swap    d0
        move.b  d0,7-8(a0)              ;Anzahl der Eintr�ge in intin
        lsr.w   #8,d0
        move.w  d0,0-8(a0)              ;Funktionsnummer
_vdi2:  move.w  vdi_handle(pc),12-8(a0) ;VDI-Handle
        lea     _vdi_pb(pc),a0
        move.l  a0,d1
        moveq   #$73,d0
        trap    #2
        moveq   #0,d0
        move.w  int_out(pc),d0          ;int_out[0]
        movem.l (sp)+,reglist
        rts

_vdi_pb:        DC.L control ;Der VDI-Parameterblock
                DC.L int_in
                DC.L ptsin
                DC.L int_out
                DC.L ptsout

;************************************************************************
;* und noch ein wenig BSS-Bereich                                       *
;************************************************************************
basepage:       DS.L 1      ;Adresse der Basepage
control:        DS.W 16
global:         DS.W 15
addr_in:        DS.L 3
addr_out:       DS.L 1
int_in:         DS.W 128
int_out:        DS.W 128
ptsin:          DS.W 128
ptsout:         DS.W 128
ap_id:          DS.W 1      ;ID der Applikation
vdi_handle:     DS.W 1      ;Die Handle der Bildschirm-Workstation
gr_hwchar:      DS.W 1      ;Breite (in Punkten) eines Zeichens
gr_hhchar:      DS.W 1      ;H�hen (in Punkten) eines Zeichens
gr_hwbox:       DS.W 1      ;Ma�e eines Rechtecks, in das ein Standardzeichen
gr_hhbox:       DS.W 1      ;komplett hineinpassen w�rde
                ENDPART
                PART 'AES/VDI-Library-EQUs'
;************************************************************************
;* Die AES-"Library"                                                    *
;************************************************************************
appl_init       EQU $0a000100
appl_read       EQU $0b020101
appl_write      EQU $0c020101
appl_find       EQU $0d000101
appl_tplay      EQU $0e020101
appl_trecord    EQU $0f010101
appl_bvset      EQU $10020100 ;ab GEM 2.0
appl_yield      EQU $11000100 ;ab GEM 2.0
appl_exit       EQU $13000100

evnt_keybd      EQU $14000100
evnt_button     EQU $15030500
evnt_mouse      EQU $16050500
evnt_mesag      EQU $17000101
evnt_timer      EQU $18020100
evnt_multi      EQU $19100701
evnt_dclick     EQU $1a020100

menu_bar        EQU $1e010101
menu_icheck     EQU $1f020101
menu_ienable    EQU $20020101
menu_tnormal    EQU $21020101
menu_text       EQU $22010102
menu_register   EQU $23010101
menu_unregister EQU $24010100 ;ab GEM 2.0

objc_add        EQU $28020101
objc_delete     EQU $29010101
objc_draw       EQU $2a060101
objc_find       EQU $2b040101
objc_offset     EQU $2c010301
objc_order      EQU $2d020101
objc_edit       EQU $2e040201
objc_change     EQU $2f080101

form_do         EQU $32010101
form_dial       EQU $33090100
form_alert      EQU $34010101
form_error      EQU $35010100
form_center     EQU $36000501
form_keybd      EQU $37030301
form_button     EQU $38020201

graf_rubberbox  EQU $46040300
graf_rubbox     EQU $46040300 ;dies ist der richtige Name!
graf_dragbox    EQU $47080300
graf_movebox    EQU $48060100
graf_mbox       EQU $48060100 ;dies ist der richtige Name!
graf_growbox    EQU $49080100
graf_shrinkbox  EQU $4a080100
graf_watchbox   EQU $4b040101
graf_slidebox   EQU $4c030101
graf_handle     EQU $4d000500
graf_mouse      EQU $4e010101
graf_mkstate    EQU $4f000500

srcp_read       EQU $50000101
srcp_write      EQU $51000101
scrp_clear      EQU $52000100 ;ab GEM 2.0

fsel_input      EQU $5a000202
fsel_exinput    EQU $5b000203 ;ab TOS 1.4

wind_create     EQU $64050100
wind_open       EQU $65050100
wind_close      EQU $66010100
wind_delete     EQU $67010100

wind_get        EQU $68020500
wind_set        EQU $69060100
wind_find       EQU $6a020100
wind_update     EQU $6b010100
wind_calc       EQU $6c060500
wind_new        EQU $6d000000 ;ab TOS 1.4

rsrc_load       EQU $6e000101
rsrc_free       EQU $6f000100
rsrc_gaddr      EQU $70020100
rsrc_saddr      EQU $71020101
rsrc_obfix      EQU $72010101

shel_read       EQU $78000102
shel_write      EQU $79030102
shel_get        EQU $7a010101
shel_put        EQU $7b010101
shel_find       EQU $7c000101
shel_envrn      EQU $7d000102
shel_rdef       EQU $7e000102 ;ab GEM 2.0
shel_wdef       EQU $7f000002 ;ab GEM 2.0

xgrf_stepcalc   EQU $82060600 ;ab GEM 2.0
xgrf_2box       EQU $83090100 ;ab GEM 2.0

; Die Messages des AES
MN_SELECTED     EQU 10      ;Men�eintrag angew�hlt
WM_REDRAW       EQU 20      ;Teil eines Windows mu� neu gezeichnet werden
WM_TOPPED       EQU 21      ;Window ist nun das aktuelle Fenster
WM_CLOSED       EQU 22      ;Window soll geschlossen werden
WM_FULLED       EQU 23      ;Window soll auf max.Gr��e gebracht werden
WM_ARROWED      EQU 24      ;Scollbalken angeklickt
WM_HSLID        EQU 25      ;Der horizontale Slider wurde bewegt
WM_VSLID        EQU 26      ;Der vertikale Slider wurde bewegt
WM_SIZED        EQU 27      ;Die Gr��e des Windows wurde ge�ndert
WM_MOVED        EQU 28      ;Das Window wurde bewegt
WM_NEWTOP       EQU 29      ;Das Fenster wurde aktiviert (siehe WM_TOPPED)
WM_UNTOPPED     EQU 30      ;Der Fenster wird gleich inaktiv (erst ab GEM 2.0!)
AC_OPEN         EQU 40      ;Ein Acc wurde angeklickt.(30 bzw. 31 ist FALSCH!)
AC_CLOSE        EQU 41      ;Prg wurde beendet, ACC soll seinen Speicher freigeben.
CT_UPDATE       EQU 50
CT_MOVE         EQU 51      ;Funktion unbekannt
CT_NEWTOP       EQU 52

; WM_ARROWED angeklickt (nun die genauere Definition)
WA_UPPAGE       EQU 0       ;Balken oberhalb des Sliders
WA_DNPAGE       EQU 1       ;Balken unterhalb des Sliders
WA_UPLINE       EQU 2       ;Pfeil nach oben
WA_DNLINE       EQU 3       ;Pfeil nach unten
WA_LFPAGE       EQU 4       ;Balken links vom Slider
WA_RTPAGE       EQU 5       ;Balken rechts vom Slider
WA_LFLINE       EQU 6       ;Pfeil nach links
WA_RTLINE       EQU 7       ;und Pfeil nach rechts

; Die Objekttypen des Resource-Baumes
G_BOX           EQU 20
G_TEXT          EQU 21
G_BOXTEXT       EQU 22
G_IMAGE         EQU 23
G_PROGDEF       EQU 24
G_IBOX          EQU 25      ;Bitte in entsprechender Literatur nachschlagen
G_BUTTON        EQU 26
G_BOXCHAR       EQU 27
G_STRING        EQU 28
G_FTEXT         EQU 29
G_FBOXTEXT      EQU 30
G_ICON          EQU 31
G_TITLE         EQU 32

; Object-Flags bei einem Objekt
NONE_FLG        EQU $00
SELECTABLE_FLG  EQU $01
DEFAULT_FLG     EQU $02
EXIT_FLG        EQU $04
EDITABLE_FLG    EQU $08
RBUTTON_FLG     EQU $10     ;Bitte ebenfalls in der Literatur nachlesen
LASTOB_FLG      EQU $20
TOUCHEXIT_FLG   EQU $40
HIDETREE_FLG    EQU $80
INDIRECT_FLG    EQU $0100

; Objekt-Status bei einem Objekt
NORMAL          EQU $00
SELECTED        EQU $01
CROSSED         EQU $02
CHECKED         EQU $04     ;s.o.
DISABLED        EQU $08
OUTLINED        EQU $10
SHADOWED        EQU $20

; Die einzelnen Bits bei evnt_multi()
MU_KEYBD        EQU $01     ;Tastaturereignis
MU_BUTTON       EQU $02     ;Maustastenereignis
MU_M1           EQU $04     ;1.Mausereignis
MU_M2           EQU $08     ;2.Mausereignis
MU_MESAG        EQU $10     ;Message des AES
MU_TIMER        EQU $20     ;Timer-Ereignis

; form_dial()
FMD_START       EQU $00     ;Bildschirmhintergrund reservieren
FMD_GROW        EQU $01     ;Ausdehnende Box zeichnen
FMD_SHRINK      EQU $02     ;Schrumpfende Box zeichnen
FMD_FINISH      EQU $03     ;Bildschirmbereich wieder freigeben

; graf_mouse()
ARROW           EQU $00
TEXT_CRSR       EQU $01
HOURGLASS       EQU $02
BUSYBEE         EQU $02
POINT_HAND      EQU $03
FLAT_HAND       EQU $04
THIN_CROSS      EQU $05
THICK_CROSS     EQU $06
OUTL_CROSS      EQU $07
USER_DEF        EQU $ff
M_OFF           EQU $0100
M_ON            EQU $0101

; Parameter bei wind_create()
WI_NAME         EQU $01
WI_CLOSE        EQU $02
WI_FULL         EQU $04
WI_MOVE         EQU $08
WI_INFO         EQU $10
WI_SIZE         EQU $20
WI_UPARROW      EQU $40
WI_DNARROW      EQU $80
WI_VSLIDE       EQU $0100
WI_LFARROW      EQU $0200
WI_RARROW       EQU $0400
WI_HSLIDE       EQU $0800

; Parameter bei wind_get() & wind_set()
WF_KIND         EQU 1
WF_NAME         EQU 2
WF_INFO         EQU 3
WF_WORKXYWH     EQU 4
WF_CURRXYWH     EQU 5
WF_PREVXYWH     EQU 6
WF_FULLXYWH     EQU 7
WF_HSLIDE       EQU 8
WF_VSLIDE       EQU 9
WF_TOP          EQU 10
WF_FIRSTXYWH    EQU 11
WF_NEXTXYWH     EQU 12
WF_NEWDESK      EQU 14
WF_HSLSIZE      EQU 15
WF_VSLSIZE      EQU 16

; wind_update()
END_UPDATE      EQU 0
BEG_UPDATE      EQU 1
END_MCTRL       EQU 2
BEG_MCTRL       EQU 3

;************************************************************************
;* Die VDI-"Library"                                                    *
;* Wer n�heres dazu wissen m�chte, sollte sich im Profibuch von Sybex   *
;* die Informationen dazu besorgen.                                     *
;************************************************************************
;1. Kontrollfunktionen
v_opnwk         EQU $010b0000 ;Open workstation
v_clswk         EQU $02     ;Close workstation
v_opnvwk        EQU $640b0000 ;Open virtual screen workstation
v_clsvwk        EQU $65     ;Close virtual screen workstation
v_clrwk         EQU $03     ;Clear workstation
v_updwk         EQU $04     ;Update workstation
vst_load_fonts  EQU $77010000 ;Load fonts
unload_fonts    EQU $78010000 ;Unload fonts
vs_clip         EQU $81010002 ;Set clipping rectangle
;2. Ausgabefunktionen
v_pline         EQU $06000000 ;Polyline (Byte 0=count(n))
v_pmarker       EQU $07000000 ;Polymarker (Byte 0=count(n))
v_gtext         EQU $08000001 ;Text (Byte 2=n)
v_fillarea      EQU $09000000 ;Filled area (Byte 0=count(n))
v_cellarray     EQU $0a000002 ;Cell array (Byte 2=n)
v_contourfill   EQU $67010001 ;Contour fill
vr_recfl        EQU $72000002 ;Fill rectangle
v_bar           EQU $0b000102 ;Bar
v_arc           EQU $0b020204 ;Arc
v_pieslice      EQU $0b020304 ;Pie
v_circle        EQU $0b000403 ;Circle
v_ellipse       EQU $0b000502 ;Ellipse
v_ellarc        EQU $0b020602 ;Elliptical arc
v_ellpie        EQU $0b020702 ;Elliptical pie
v_rbox          EQU $0b000802 ;Rounded rectangle
v_rfbox         EQU $0b000902 ;Filled rounded rectangle
v_justified     EQU $0b020a02 ;Justified graphics text (Byte 2=n+2)
;3. Attribut-Funktionen
vswr_mode       EQU $20010000 ;Set writing mode
vs_color        EQU $0e040000 ;Set color representation
vsl_type        EQU $0f010000 ;Set polyline line type
vsl_udsty       EQU $71010000 ;Set user-defined line style pattern
vsl_width       EQU $10000001 ;Set polyline line width
vsl_color       EQU $11010000 ;Set polyline color index
vsl_ends        EQU $6c020000 ;Set polyline end styles
vsm_type        EQU $12010000 ;Set polymarker type
vsm_height      EQU $13000001 ;Set polymarker height
vsm_color       EQU $14010000 ;Set polymarker color index
vst_height      EQU $0c000001 ;Set character height, absolute mode
vst_point       EQU $6b010000 ;Set character height, points mode
vst_rotation    EQU $0d010000 ;Set character baseline vector
vst_font        EQU $15010000 ;Set text face
vst_color       EQU $16010000 ;Set graphic text color index
vst_effects     EQU $6a010000 ;Set graphic text special effects
vst_alignment   EQU $27020000 ;Set graphic text alignment
vsf_interior    EQU $17010000 ;Set fill interior index
vsf_style       EQU $18010000 ;Set fill style index
vsf_color       EQU $19010000 ;Set fill color index
vsf_perimeter   EQU $68010000 ;Set fill perimeter visibility
vsf_udpat       EQU $70000000 ;Set user-defined fill pattern (Byte 2=16*planes)
;4. Rasteroperationen
vro_cpyfm       EQU $6d010004 ;Copy raster, opaque
vrt_cpyfm       EQU $79030004 ;Copy raster, transparent
vr_trnfm        EQU $6e000000 ;Transform form
v_get_pixel     EQU $69000001 ;Get pixel
;5. Eingabefunktionen
vsin_mode       EQU $21020000 ;Set input mode
vrq_locator     EQU $1c000001 ;Input locator, request mode
vsm_locator     EQU $1c000001 ;Input locator, sample mode
vrq_valuator    EQU $1d010000 ;Input valuator, request mode
vsm_valuator    EQU $1d010000 ;Input valuator, sample mode
vrq_choice      EQU $1e010000 ;Input choice, request mode
vsm_choice      EQU $1e010000 ;Input choice, sample mode
vrq_string      EQU $1f020001 ;Input string, request mode
vsm_string      EQU $1f020001 ;Input string, sample mode
vsc_form        EQU $6f250000 ;Set mouse form
vex_timv        EQU $76     ;Exchange timer interrupt vector
v_show_c        EQU $7a010000 ;Show cursor
v_hide_c        EQU $7b     ;Hide cursor
vq_mouse        EQU $7c     ;Sample mouse button state
vex_butv        EQU $7d     ;Exchange button change vector
vex_motv        EQU $7e     ;Exchange mouse movement vector
vex_curv        EQU $7f     ;Exchange cursor change vector
vq_key_s        EQU $80000000 ;Sample keyboard state information
;6. Auskunftsfunktionen
vq_extnd        EQU $66010000 ;Extended inquire function
vq_color        EQU $1a020000 ;Inquire color representation
vql_attributes  EQU $23     ;Inquire current polyline attributes
vqm_attributes  EQU $24     ;Inquire current polymarker attributes

vqf_attributes  EQU $25     ;Inquire current fill area attributes
vqt_attributes  EQU $26     ;Inquire current graphic text attributes
vqt_extent      EQU $74     ;Inquire text extent (Byte 2=n)
vqt_width       EQU $75010000 ;Inquire character cell width
vqt_name        EQU $82010000 ;Inquire face name and index
vq_cellarray    EQU $1b000002 ;Inquire cell array
vqin_mode       EQU $73010000 ;Inquire input mode
vqt_fontinfo    EQU $83000000 ;Inquire current face information
vqt_justified   EQU $84020002 ;Inquire just.graph.text (Byte 2=n+2) GEM 2.0!
;7. Escapes
vq_chcells      EQU $05000100 ;Inquire addressable alpha character cells
v_exit_cur      EQU $05000200 ;Exit alpha mode
v_enter_cur     EQU $05000300 ;Enter alpha mode
v_curup         EQU $05000400 ;Alpha cursor up
v_curdown       EQU $05000500 ;Alpha cursor down
v_curright      EQU $05000600 ;Alpha cursor right
c_curleft       EQU $05000700 ;Alpha cursor left
v_curhome       EQU $05000800 ;Home alpha cursor
v_eeos          EQU $05000900 ;Erase to end of alpha screen
v_eeol          EQU $05000a00 ;Erase to end of alpha text line
v_curaddress    EQU $05020b00 ;Direct alpha cursor address
v_curtext       EQU $05000c00 ;Output cursor addressable alpha text (Byte 2=n)
v_rvon          EQU $05000d00 ;Reverse video on
v_rvoff         EQU $05000e00 ;Reverse video off
vq_curaddress   EQU $05000f00 ;Inquire current alpha cursor address
vq_tabstatus    EQU $05001000 ;Inquire tablet status
v_hardcopy      EQU $05001100 ;Hardcopy
v_dspcur        EQU $05001201 ;Place graphic cursor at location
v_rmcur         EQU $05001300 ;Remove last graphic cursor
v_form_adv      EQU $05001400 ;Form advance
v_output_window EQU $05001500 ;Output window
v_clear_disp_list EQU $05001600 ;Clear display list
v_bit_image     EQU $05051702 ;Output bit image file (Byte 2=n+5)
vq_scan         EQU $05001800 ;Inquire printer scan
v_alpha_text    EQU $05001900 ;Output alpha text (Byte 2=n)
vs_palette      EQU $05013c00 ;Select palette
v_sound         EQU $05023d00 ;Generate specified tone
vs_mute         EQU $05013e00 ;Set/clear tone muting flag
vt_resolution   EQU $05025100 ;Set tablet axis resolution in lines/inch
vt_axis         EQU $05025200 ;Set table axis resolution in lines
vt_origin       EQU $05025300 ;Set tablet x and y origin
vq_tdimensions  EQU $05005400 ;Return tablet x and y dimensions
vt_alignment    EQU $05025500 ;Set tablet alignment
vsp_film        EQU $05025b00 ;Set camera file type and exposure time
vqp_filename    EQU $05015c00 ;Inquire camera file name
vsc_expose      EQU $05015d00 ;Disable or enable file exposure for frame preview
v_meta_extents  EQU $05006202 ;Update metafile extents
v_write_meta    EQU $05006300 ;Write metafile item (Byte 0=num_ptsin, Byte 2=num_intin)
vm_pagesize     EQU $05036300 ;Physical page size
wm_coords       EQU $05056300 ;Coordinate window
vm_filename     EQU $05006400 ;Change gem vdi filename (Byte 2=n)
v_offset        EQU $05016500 ;Set line offset
v_fontinit      EQU $05026600 ;Init system font
;v_escape2000(times) -> (klappt mit dem Konzept leider nicht...)
                ENDPART
                END
