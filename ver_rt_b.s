	include	include\tos_fnkt

;eine demonstration, welche geschwindigkeitsunterschiede sich
;mit und ohne blitterbenutzung ergeben.
;hier vertikales bildschirmscrolling mit blitter.

	Supexec	start(pc)
	Pterm0
start:
	Physbase
	move.l	d0,a6
	lea	80(a6),a5
	move.w	#398,d0
	move.w	#$2700,sr
	init_blit	#2,#2,a5,#$ffff,#$ffff,#$ffff,#2,#2,a6,#40,#400,#2,#3,#0,#192
rt_loop:
	blit	a5,a6,#40,#400,#3,#0
	dbf	d0,rt_loop
	move.w	#$2300,sr
	rts