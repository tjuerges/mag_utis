	opt	a+,c+,o+,w+,m+
	output	h:\sw2.acc
	include	include\tos_reg
	include	include\tos_fnkt
	include	include\aes
col0	=	$ffff8240
col1	=	$ffff8242
col2	=	$ffff8244
col3	=	$ffff8246

;sollte ein acc werden, dass bei anclick den bildschirm invwertiert.
;es sollte auch neben den men�eintrag ein haken gesetzt werden, der
;anzeigt, ob die farbe st�ndig neu gesetzt wird. dazu sind einige klimm-
;z�ge n�tig. allzu gut gelungen ist es mir nicht...

start
	lea	stack(pc),sp	;jedem seinen stack
	appl_init	;die brauchen wir
	tst	d0
	seq	tos_version

	menu_register	appl_id(pc),#title,menu_id	;hier f�r...
	bmi	error	;war die menu_id <0 =>error

	Getrez
	cmp	#2,d0
	sne	resolution

	Cconws	init_ok(pc)

main_loop
	evnt_mesag	#mesg_buf	;warten auf godot

	cmp	#40,mesg_buf
	bne.s	main_loop	;wir sind's nicht, ab nach oben.

	tst.b	tos_version
	beq	.nicht_1_4
	wind_get	#0,#14
	move	int_out(pc),start_objc
	beq	main_loop
	move.l	int_out+2(pc),desktop_addresse
	graf_mkstate
	illegal
	move	int_out+8(pc),d7
	objc_find	start_objc(pc),#255,int_out+2(pc),int_out+4(pc),desktop_addresse(pc)
	move	int_out(pc),objc_nummer
	bmi	main_loop
.nicht_1_4
	Supexec	userint_suchen(pc)	;de- bzw. installieren
	tst.b	tos_version
	beq	main_loop
	tst	menu_flag
	bmi	main_loop
	menu_icheck	objc_nummer(pc),menu_flag(pc),desktop_addresse(pc)
	menu_bar	#0,desktop_addresse(pc)
	;haken mu� sein.
	bra	main_loop	;und wieder von vorn...

userint_suchen	;aber nur im supermode
	move.l	(_vblqueue).w,a4
	addq	#4,a4
	move	(nvbls).w,d4
	subq	#2,d4
loop
	move.l	(a4)+,a3
	cmp.l	#0,a3
	beq.s	int_gefunden
	cmp.l	#'SW 2',-8(a3)
	beq	schon_inst
	dbf	d4,loop
	bra	error

int_gefunden	;immer noch im supermode
	tst.b	resolution
	beq.s	.ist_sw
	lea	magnum_c(pc),a6
	move	(col0).w,old0
	move	(col1).w,old1
	move	(col2).w,old2
	move	(col3).w,old3
	bra.s	.weiter
.ist_sw
	lea	magnum(pc),a6
.weiter
	subq	#4,a4
	move	#0,(vblsem).w
	move.l	a6,(a4)

	move	#1,(vblsem).w
	move	#1,menu_flag
	rts	;jetzt nicht mehr

schon_inst	;s-bit noch immer gesetzt
	and	#%10,d7
	beq.s	toggle	;wenn installiert und keine l-shft taste...toggle
got_one	;sonst abschalten
	subq.l	#4,a4
	clr.l	(a4)
	tst.b	resolution
	beq.s	.old_sw
	move	old0(pc),magnum_c1+2
	move	old1(pc),magnum_c2+2
	move	old2(pc),magnum_c3+2
	move	old3(pc),magnum_c4+2
.old_sw
	clr	menu_flag
	rts	;raus aus dem superdupermode

toggle	;umschalten statt umlegen...
	clr	(vblsem).w
	tst.b	resolution
	bne.s	toggle_farbe
	bchg	#0,magnum+3
	bchg	#0,magnum1+3
ret_toggle
	move	#1,(vblsem).w
	move	#-1,menu_flag
	rts	;endg�ltiges verlassen des s-mode
toggle_farbe
	move	(col0).w,d0
	move	(col1).w,d1
	move	(col2).w,d2
	move	(col3).w,d3
	move	old0(pc),magnum_c+2
	move	old0(pc),magnum_c1+2
	move	old1(pc),magnum_c2+2
	move	old2(pc),magnum_c3+2
	move	old3(pc),magnum_c4+2
	move	d0,old0
	move	d1,old2
	move	d2,old2
	move	d3,old3
	bra.s	ret_toggle

error	;kann mal passieren
	Cconws	fehler(pc)
	rts	;raus aus dem supermode

;hier f�ngt's los...
begin
	dc.l	'SW 2'
	dc.l	'MGNM'
magnum
	cmp	#0,(col0).w
	beq.s	int_weiter
magnum1
	move	#0,(col0).w
int_weiter
	rts
	dc.l	'SW 2'
	dc.l	'MGNM'
magnum_c
	cmp	#0,(col0).w
	beq.s	int_weiter
magnum_c1
	move	#0,(col0).w
magnum_c2
	move	#$700,(col1).w
magnum_c3
	move	#$204,(col2).w
magnum_c4
	move	#$777,(col3).w
	rts
ende
;das war's schon

	data
title	dc.b	'  SW 2  ',0
fehler	dc.b	'SW 2 konnte nicht initialisiert',$d,$a
	dc.b	'werden. Abbruch...',$d,$a,0
init_ok	dc.b	$d,$a,'SW2.ACC o.k.!',$d,$a
	dc.b	'Geschrieben von MAGNUM',$d,$a,0

	bss
mesg_buf	ds.l	4
desktop_addresse	ds.l	1
start_objc	ds.w	1
old0	ds.w	1
old1	ds.w	1
old2	ds.w	1
old3	ds.w	1
acc_id	ds.w	1
menu_id	ds.w	1
objc_nummer	ds.w	1
menu_flag	ds.w	1
resolution	ds.b	1
tos_version	ds.b	1
new_stack	ds.l	64
stack
