  	opt	a+,c+,o+,w+,m+	;Optimierungen etc. anschalten.
	include	include\tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	include\gem	;Vdi- & Aes-Macros einbinden.

debug	=	0
	ifne	debug
	output	d:\mag_clos.acc	;So heisst das Programm sp�ter.
	elseif
	output	f:\mag_utis\mag_clos.acc	;So heisst das Programm sp�ter.
	endc

GEM_200	=	0
an	=	1

start
	lea	end_stack(pc),sp	;Als .ACC einen eigenen Stack.
	appl_init	;Applikation beim AES anmelden.
	bmi	fehler
	move.b	#2,flag
	cmp	#$0200,global
	blo.s	.no_unregister
	bset	#GEM_200,flag
.no_unregister
	tst.b	d7
	beq.s	.kein_kaos
	bset	#GEM_200,flag
.kein_kaos
	graf_handle
	menu_register	appl_id(pc),#mag_close_str,menu_id
	bmi	fehler

	lea	int_out(pc),a5
loop
	lea	buffer(pc),a0
	move.l	#1000,d0	;Alle 2 Sekunden Meldung vom AES erwarten.
	evnt_multi	#MU_MESAG!MU_TIMER,,,,,,,,,,,,,,d0,a0
	cmp	#MU_MESAG,(a5)	;War es ein MU_MESAG-Event?
	beq.s	.message_event	;Ja...
	cmp	#MU_TIMER,(a5)
	bne.s	loop
	btst	#an,flag(pc)	;Ist Mag-Close aktiviert?
	beq.s	loop	;Nein.
	cmp	#2,6(a5)	;Ist die rechte Maustaste gedr�ckt?
	bne.s	loop	;Nein.
	bsr	close_window	;Doch, also schliessen, was zu schliessen ist.
	bra.s	loop	;Fertig

.message_event
	move	menu_id(pc),d0
	cmp	8(a0),d0	;Ist die Menu-ID des betroffenen ACC meine?
	bne.s	loop	;Nee...
	cmp	#AC_OPEN,(a0)	;...
	bne.s	loop

	lea	mag_close_alert(pc),a6
	btst	#GEM_200,flag(pc)
	beq.s	.kein_gem20
	lea	mag_close_alert_spezial(pc),a6

.kein_gem20
	form_alert	#1,a6	;Doch, dann...
	cmp	#1,(a5)	;An-/abschalten?
	beq.s	switch_it
	btst	#GEM_200,flag(pc)	;Gem-Version >=2.00?
	beq	loop	;Nee.
	;Falls doch, dann gibt's n�mlich menu_unregister zum Abmelden
	;des ACC!
	cmp	#2,(a5)	;Mittlerer Knopf gedr�ckt?
	bne	loop	;Nee, also passiert gar nix.
	menu_unregister	menu_id
fehler
	evnt_timer	#-1
	bra.s	fehler

switch_it
	bchg	#an,flag	;Aktivit�ts-Flag umschalten.
	beq.s	mag_close_aus	;Falls es an war, d.h. Bit=1, ist's nun aus.

mag_close_an
	;Mag-Close war angeschaltet, nun ist's aus.
	bsr.s	test_gem
	move.b	#'n',(a6)
	bra	loop	;Fertig.

mag_close_aus
	bsr.s	test_gem
	move.b	#'b',(a6)
	bra	loop

test_gem
	lea	byte(pc),a6
	btst	#GEM_200,flag(pc)
	beq.s	.weiter
	lea	byte_spezial(pc),a6
.weiter
	rts

close_window
	;Doch gedr�ckt und in int_out+2/+4 sind die aktuellen
	;Maus-Koordinaten.
	movem	2(a5),d5-d6

	wind_find	d5,d6,d7	;Ist ein Window an dieser Stelle?
	bmi	.fertig	;Kein Window!
	beq	.fertig	;Oberstes Window ist das Desktop selbst.
	;Das hat n�mlich die Kennunug 0 und dieses Fenster sollte man
	;nicht schlie�en.

	;Doch ein vern�nftiges Window:
	wind_get	#0,#WF_TOP	;Handle des obersten Windows erfragen.
	tst	(a5)	;Ist �berhaupt ein Window zu oberst?
	beq.s	.fertig	;Nein, dann zur�ck in die Schleife.

	cmp	2(a5),d7	;Handle des obersten Windows
	bne.s	.fertig	;mit dem Handle des Windows an der
	;Mausposition vergleichen: Sind sie verschieden, so passiert nichts.

	wind_get	d7,#WF_CURRXYWH
	tst	(a5)
	beq.s	.fertig

	move	#WM_CLOSED,(a0)
	move	appl_id(pc),2(a0)
	clr	4(a0)
	move	d7,6(a0)
	clr.l	8(a0)
	clr.l	12(a0)
	appl_write	#0,#16,a0
.fertig
	rts
	include	include\call_gem	;AES- & VDI-Aufrufe einbinden.

	data
mag_close_str	dc.b	'  Mag-Close',0
mag_close_alert	dc.b	'[2][MAG-CLOSE - �Magnum/BlackBox-|a'
byte	dc.b	'bschalten?][Ja|Abbruch]',0
mag_close_alert_spezial	dc.b	'[2][MAG-CLOSE - �Magnum/BlackBox-|a'
byte_spezial	dc.b	'bschalten?][Ja|Quit ACC|Abbruch]',0
	copyright

	bss
buffer	ds.l	4
menu_id	ds.w	1
flag	ds.b	1
	even
stack	ds.l	64
end_stack
