;ATARI screen emulator for 640*800 pixels.
	include	include\tos_fnkt

main:
	bra	start
begin:
	dc.l	'XBRA','MGDB'
old_trap2:	dc.l	0
my_trap2:
	cmp	#$73,d0	;VDI function?
	bne.s	normal_vdi	;No
	move.l	d1,a0	;Yes
	move.l	12(a0),intout_ptr	;Get the intout-pointer.
	move.l	(a0),a0	;Get the contrl-pointer.
	cmp	#1,(a0)		;Patch only VDI function v_opnwk.
	bne.s	normal_vdi
	move.l	2(sp),back	;Save return address. Doesn't matter
	;if MC680x0 or so is used. The stack frames are the same
	;up to the 6th word.
	move.l	#patch_v_opnwk_call,2(sp)	;New return address after rte.
normal_vdi:
	move.l	old_trap2(pc),-(sp)
	rts

patch_v_opnwk_call:
	move.l	line_a(pc),a0	;Get LINE_A variable-pointer.
	move	V_REZ_VT(a0),d0	;Actual Y_resolution.
	lsl	#1,d0		;*2
	move	d0,V_REZ_VT(a0)	;and write it back to LINE_A
	move	d0,DEV_TAB+2(a0)	;and to the workstation table.
	move.l	intout_ptr(pc),a1	;Remember the VDI-call and its variables.
	move	d0,2(a1)	;Setup new Y-size.
	move.l	back(pc),-(sp)
	rts
back:	dc.l	0
intout_ptr:	dc.l	0
line_a:	dc.l	0
max_screen:	dc.w	0
min_screen:	dc.w	0
max_height:	dc.w	0
min_height:	dc.w	0
old_y:	dc.w	0

number_of_shifter_lines:	dc.w	0
	;E.g. number_of_shifter_lines=(pixel_lines * bytes_per_row)/256.
	;The number of hardware lines being scrolled up|down
	;when the mouse exceeds the max|min koordinates.
	;This strange calculation is necessary because the
	;ST-SHIFTER has got only a 256-Bytes resolution when
	;addressing a new screen area. Therefore you have to
	;find out the smallest common multiple(right term?)
	;of hardware addressable lines and display lines:
	;e.g. (bytes_per_row * N)/256

pixel_lines:	dc.w	0
	;E.g. pixel_lines=16
	;Number of pixel lines being scrolled up|down
	;when mousemovement initiated it.

	dc.l	'XBRA','MGDB'
old_vbl:	dc.l	0
new_vbl:
	movem.l	d0-d1/a0,-(sp)
	move.l	line_a(pc),a0
	move	CUR_Y(a0),d0
	cmp	old_y(pc),d0	;Has the mouse been moved?
	beq.s	no_movement	;Nope!
	lea	(dbasehi).w,a0	;Yep! Fetch the hardware address at once.
	move	d0,old_y	;Save actual mouse_y coordinate.
	cmp	max_height(pc),d0	;Is the mouse in the hot area?
	bhs.s	down			;mouse_y>=max_height (e.g. 399)
	cmp	min_height(pc),d0	;mouse_y<=min_height
	bhi.s	no_movement

	;Scoll backwards...
	sf	(flock).w	;DMA is forbidden...
	movep	(a0),d0	;Fetch old screen address.
	st	(flock).w	;DMA allowed...
	cmp	min_screen(pc),d0	;If the actual screen address
	;is the lowest possible address, then do nothing.
	bls.s	no_movement

	move	number_of_shifter_lines(pc),d1	;Number of lines 
	;(for the shifter) to be scrolled.
	sub	d1,d0	;Sub (d1)lines from actual screen address.
	move	pixel_lines(pc),d1
	sub	d1,max_height
	sub	d1,min_height
	bra.s	move_screen

down:
	sf	(flock).w	;DMA is forbidden...
	movep	(a0),d0
	st	(flock).w	;DMA allowed...
	cmp	max_screen(pc),d0	;If max_screen>=actual screen address,
	;then no movement.
	bhs.s	no_movement
	move	number_of_shifter_lines(pc),d1
	add	d1,d0
	move	pixel_lines(pc),d1
	add	d1,max_height
	add	d1,min_height
move_screen:
	sf	(flock).w	;DMA is forbidden...
	movep	d0,(a0)
	st	(flock).w	;DMA allowed...
no_movement:
	movem.l	(sp)+,d0-d1/a0
	move.l	old_vbl(pc),-(sp)
	rts

	copyright
	even

start:
	move.l	4(sp),a6
	lea	my_stack(pc),sp
	move.l	$c(a6),a0
	add.l	$14(a6),a0
	add.l	$1c(a6),a0
	lea	$100(a0),a0

	move.l	a0,-(sp)	;Mshrink	a0,(a6)
	pea	(a6)
	move.l	#$4a0000,-(sp)
	trap	#1
	lea	$c(sp),sp

	move	#$b,-(sp)	;Cconis
	trap	#1
	addq.l	#2,sp
	tst	d0
	bmi	abort

	move	#4,-(sp)	;Getrez
	trap	#14
	addq.l	#2,sp
	cmp	#2,d0
	bhi	wrong_resolution
	ext	d0
	lsl	#2,d0	;Because we're addressing long words.

	lea	setup_table(pc),a0
	lea	number_of_shifter_lines(pc),a1
	move.l	(a0,d0.w),(a1)	;Setup for different resolutions.

	move.l	#32000*2+256,d4
	moveq	#-1,d0
	bsr	malloc
	cmp.l	d4,d0
	bls	wrong_resolution

	move.l	d4,d0
	bsr	malloc
	move.l	d0,d3
	add.l	#256,d3
	and.l	#$ffffff00,d3

	move.l	d3,d5
	move.l	d3,d6
	lsr.l	#8,d6
	move	d6,min_screen
	add.l	#32000,d5
	lsr.l	#8,d5
	move	d5,max_screen

	move	#-1,-(sp)	;Setscreen	#-1,d0,d0
	move.l	d3,-(sp)
	move.l	d3,-(sp)
	move	#5,-(sp)
	trap	#14
	lea	$c(sp),sp

	dc.w	$a000		;Line-$a000 (to get some variables)
	move.l	a0,line_a	;Save line-$a variable pointer.
	move	V_REZ_VT(a0),d0	;Line-$a variables, offset: -4.
	;It is the screen height in pixels.
	subq	#1,d0
	move	d0,max_height
	move	#15,min_height

	pea	install(pc)	;Supexec install(pc)
	move	#$26,-(sp)
	trap	#14
	addq.l	#6,sp

	pea	message(pc)	;Cconws	message(pc)
	move	#9,-(sp)
	trap	#1
	addq.l	#6,sp

	clr	-(sp)		;Ptermres	#0,#$100+start-main
	move.l	#$100+start-main,-(sp)
	move	#$31,-(sp)
	trap	#1

abort:
	move	#1,-(sp)	;Cconin
	trap	#1
	addq.l	#2,sp
wrong_resolution:
	clr	-(sp)		;Pterm0
	trap	#1

malloc:
	move.l	d0,-(sp)
	move	#$48,-(sp)
	trap	#1
	addq.l	#6,sp
	rts
	
install:
	move.l	d3,(_v_bas_ad).w	;screenbase
	move.l	(trap_2).w,old_trap2	;Patch trap #2
	move.l	#my_trap2,(trap_2).w
	move.l	(auto_int4).w,old_vbl	;Patch VBL
	move.l	#new_vbl,(auto_int4).w
	rts			;Installing finished.

setup_table:	dc.w	5,8,10,16,5,16
message:	dc.b	$d,$a,'MAG-DUBL, (c) Magnum -BlackBox-.',$d,$a
	dc.b	'Installing double desktop height.',$d,$a

stack:	ds.l	8		;32 bytes of stack are enough for this tiny
my_stack:			;program.
