;utility, um klax.tos auf die trax 80&81 einer disk abzuspeichern.
;(was man nicht alles aus bequemlichkeit entwickelt...)

start_track	=	80	;starttrack. ab hier wird klax.inc
	;abgespeichert

	include	tos_fnkt

	Cconws	insert(pc)
	Bconin	#2

	moveq	#start_track,d1
	bsr	format
	addq	#1,d1
	bsr	format

	Fopen	#0,klax_prg(pc)
	move.w	d0,d6
	bmi.s	error
	Fread	buffer(pc),#$2800,d6
	tst.l	d0
	bmi.s	error
	Fclose	d6

	lea	buffer(pc),a0
	moveq	#start_track,d1
	bsr.s	write
	lea	$1400(a0),a0
	addq	#1,d1
	bsr.s	write
	Cconws	fertig(pc)
error
	Bconin	#2
	Pterm0
write
	Flopwr	#10,#0,d1,#1,#0,(a0)
	bsr.s	test
	rts
test
	tst.w	d0
	bne.s	error
	rts
format
	Flopfmt	#$e5e5,#1,#0,d1,#10,#0,buffer(pc)
	bsr.s	test
	rts


	data
insert	dc.b	'Insert KLAXcopy in DRIVE A:!',$d,$a,0
fertig	dc.b	'Ready.',$d,$a,0

klax_prg	dc.b	'klax.prg',0

	bss
buffer	ds.l	$a00
ende