	opt	t+,a+,c+,o+,w+,m+
	include	include\tos_fnkt
	include	include\gem
	output	f:\mag_utis\mag_font.prg
;konvertiert *.fed-dateien mit FNT-HEADER in tos-gerechte zeichens�tze
;ohne header. ebenso werden zeichens�tze ohne header umkopiert in ein
;anderes format:
;vorher:	c1:line1,c2:line1,c3:line1
;nachher:	c1:line1,line2,line3...

start
	init	stack(pc)
	appl_init
again
	moveq	#1,d0
	lea	alert(pc),a0
	bsr	form_alert

	lea	titel_laden(pc),a2
	lea	pfad(pc),a1
	lea	filename(pc),a0
	bsr	fsel_input
	tst	d0
	beq	error

	Fsfirst	#0,filename(pc)
	tst	d0
	bmi	error
	Fgetdta
	move.l	d0,a0
	move.l	$1a(a0),length

	move.l	length(pc),d1
	bsr	malloc
	move.l	d0,font_old
	beq	error
	bsr	malloc
	move.l	d0,font_new
	beq	error1

	Fopen	#0,filename(pc)
	move	d0,handle
	beq	error

	move.l	font_old(pc),a0
	Fread	(a0),length(pc),handle(pc)

	bsr	fclose

	moveq	#0,d7
	lea	filename(pc),a0
.loop
	cmp.b	#'.',(a0)+
	bne.s	.loop
	moveq	#2,d0
.copy_it
	move.b	(a0)+,d7
	lsl.l	#8,d7
	dbf	d0,.copy_it
	lsr.l	#8,d7
	cmp.l	#'FED',d7
	beq	gem_font

	move	#255,anzahl
	move.l	font_new(pc),save_address_new

	moveq	#1,d0
	lea	hoehe_alert(pc),a0
	bsr	form_alert
	subq.b	#1,d0
	bne.s	.n1
	move	#15,hoehe
	bra.s	.weiter
.n1
	subq.b	#1,d0
	bne.s	.n2
	move	#7,hoehe
	bra.s	.weiter
.n2
	move	#5,hoehe
.weiter

	move.l	font_old(pc),a0
	move.l	font_new(pc),a1
	move	anzahl,d0
setup_font_loop
	move.l	a1,a2
	move	hoehe(pc),d6
.loop
	move.b	(a0)+,(a2)
	add.l	#$100,a2
	dbf	d6,.loop
	addq.l	#1,a1
	dbf	d0,setup_font_loop
save_new_font
	lea	titel_speichern(pc),a2
	lea	filename(pc),a0
	lea	pfad(pc),a1
	bsr	fsel_input
	tst	d0
	beq.s	error

	Fcreate	#0,filename(pc)
	move	d0,handle
	bmi.s	error

	moveq	#1,d5
	moveq	#1,d6
	add	hoehe(pc),d5
	add	anzahl(pc),d6
	mulu	d5,d6
	move.l	save_address_new(pc),a0
	Fwrite	(a0),d6,handle

	bsr.s	fclose

	move.l	font_new(pc),a1
	bsr	mfree
error1
	move.l	font_old(pc),a1
	bsr	mfree
error
	moveq	#2,d0
	lea	again_alert(pc),a0
	bsr.s	form_alert
	cmp.b	#2,d0
	bne	again
	appl_exit
	Pterm0

fclose
	Fclose	handle(pc)
	rts
form_alert
	form_alert	d0,a0
	move	int_out(pc),d0
	rts
	include	sub\fsel_inp.s
mfree
	Mfree	(a1)
	rts
malloc
	Malloc	d1
	rts
gem_font
	moveq	#0,d0
	move.l	font_old(pc),a0
	move	$26(a0),d0
	sub	$24(a0),d0
	move	d0,anzahl
	move	$28(a0),d0
	add	$2e(a0),d0
	add	$30(a0),d0
	move.l	d0,d1
	sub	#15,d1
	bmi.s	kleiner_als_16_pixel
	sub	d1,d0
zurueck_16_pixel
	move	d0,hoehe
	mulu	#$100,d1
	lea	$5a(a0,d1.l),a0
	move.l	a0,save_address_new
	bra	save_new_font
kleiner_als_16_pixel
	moveq	#0,d1
	bra.s	zurueck_16_pixel
	include	include\call_gem
	bss
pfad	ds.l	64
filename	ds.l	64
save_address_new	ds.l	1
font_new	ds.l	1
font_old	ds.l	1
buffer	ds.l	1
length	ds.l	1
anzahl	ds.w	1
handle	ds.w	1
hoehe	ds.w	1
	data
titel_laden	dc.b	'Font einladen:',0
titel_speichern	dc.b	'Font abspeichern:',0
hoehe_alert	dc.b	'[2][ Welche H�he hat | ein Zeichen ? |'
	dc.b	' (In Punkten) ][ 16 | 8 | 6 ]',0
again_alert	dc.b	'[2][ Soll noch ein Font | konvertiert werden ? ][ Ja | Abbruch ]',0
alert	dc.b	'[1][ MAG-FONT, | (c) Magnum -BlackBox-. |'
	dc.b	' Dieses Programm wandelt |'
	dc.b	' Fonts vom Tempus-Format |'
	dc.b	' ins GEM-Format (ohne Header!). ][ OK ]',0
	copyright