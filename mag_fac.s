	include	tos_fnkt	;GemDos-, Bios- & Xbios-Macros einbinden.
	include	gem	;Vdi- & Aes-Macros einbinden.

	init	stack(pc)
	lea	zahl_str(pc),a1
start:
	Cconws	text1(pc)
	bsr	clr
	Cconrs	#30,(a1)
	lea	(a1),a0
	bsr	str_zahl
	cmp.b	#12,d1
	bhi.s	start
	move.l	d1,d0
	bsr.s	fac
	move.l	d0,d1
	bsr.s	clr
	bsr	copy_ldez
	Cconws	text2(pc)
	Cconws	(a1)
	Cconws	text3(pc)
	Cconin
	cmp.b	#' ',d0
	beq	start
	Pterm0
clr:
	lea	(a1),a2
	moveq	#9,d7
.loop:	clr.b	(a2)+
	dbf	d7,.loop
	rts
fac:	incbin	fac.bin
	include	str_zahl.s
	include	copyldez.s
	include	call_gem
	data
text1:	dc.b	27,'EBitte eine Zahl eingeben: (0<x<13, x',238,'N) ',0
text2:	dc.b	$d,$a,'Die Fakult�t der eingegebenen Zahl ist ',0
text3:	dc.b	$d,$a,'Noch einmal? (SPACE=Ja) ',0
	bss
zahl_str:	ds.b	32
